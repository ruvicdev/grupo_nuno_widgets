<?php (defined('BASEPATH')) or exit('No direct script access allowed.');

/**
 * Base class will contain all the methods used as generic for the different
 * widgets created in this package. These methods will be used in all the
 * application calling them when they are required
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class MY_Controller extends MX_Controller {
    private   $template = '';
    private   $header   = '';
    private   $footer   = '';
    protected $setArray = array();

    /**
     * Contructor ....
     */
    public function __construct() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

        parent::__construct();

        // Load PDF controller to use in every module
        $this->load->module('exportFiles/ExportFiles');
        $this->load->module('emailSend/EmailSend');
        $this->load->module('templates/Templates');

        // Load helpers will be used in the system
        $this->load->helper(array('html', 'url', 'form'));
        $this->load->library('session');

        $this->header   = 'layouts/header';
        $this->footer   = 'layouts/footer';
        $this->template = 'layouts/layout';

        $this->setAuxiliarTemplate();
    }

    /**
     * Method will load the views we're going to use
     * with different information
     *
     * @param $view string
     * @param $data array
     * @param $flag boolean
     *
     * @return $view parameters for the view
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function loadSingleView($view, $data, $flag) {
        if (!empty($data)) {
            $views = $this->load->view($view, $data, $flag);
        } else {
            $views = $this->load->view($view, '', $flag);
        }

        return $views;
    }

    /**
     * Load the views in the main template
     *
     * @param $data array
     *
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function loadTemplate($data) {
        $this->load->view($this->template, $data);
    }

    /**
     * Set the header and footer template in the main template system
     *
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function setAuxiliarTemplate() {
        $footer = $this->loadSingleView($this->footer, '', TRUE);
        $header = $this->loadSingleView($this->header, '', TRUE);

        $this->__setArray('header', $header);
        $this->__setArray('footer', $footer);
    }

    /**
     * Method for making an array send by parameters the key and the value
     *
     * @param $key string
     * @param $value string
     *
     * @return $setArray associative array
     *
     * @author Ruben Alonso Cortes Mendoza
     * @version 1.0
     * @company ruvicdev
     */
    public function __setArray($key, $value) {
        $this->setArray[$key] = $value;

        return $this->setArray;
    }

    /**
     * Method will clean the $setArray object
     *
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __cleanArray() {
        $this->setArray = array();
    }

    /**
     * Method used to redirect to another page
     * once the method will be called to reload
     * the page and start up another request
     *
     * @return void
     */
    public function __redirect() {
        redirect('/');
    }
}
