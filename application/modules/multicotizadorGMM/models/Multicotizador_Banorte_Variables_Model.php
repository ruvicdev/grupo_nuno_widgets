<?php

/**
 * Class that contains all the methods will be used
 * for saving information for the user in the database
 * and will contains a historical of the users cotizations
 * done in this Club Salud widget
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class Multicotizador_Banorte_Variables_Model extends CI_Model {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // name of the table
        $this->table = "multicotizador_banorte_variables";
    }

    /**
     * Save data
     *
     * @param array $array
     * @return int id
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function insertData($array) {
        $this->db->insert($this->table, $array);
        return $this->db->insert_id();
    }

    /**
     * Get data
     *
     * @param array $array
     * @return int id
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getData() {
      $this->db->select($this->table.'.*');
      $this->db->from($this->table);
      $this->db->join('external_files_cotizaciones', 'external_files_cotizaciones.id = '.$this->table.'.external_files_cotizaciones_id');
      $this->db->where('estatus', 'ACTIVO');
      $query = $this->db->get();

      return $query->result_array();
    }

    /**
     * Update data
     *
     * @param array $array
     * @return int id
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function updateData($dataArray, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->table, $dataArray);
    }
}
