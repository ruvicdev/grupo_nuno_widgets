<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * INSURANCE OF MAYOR MEDICAL EXPENSES
 *
 * Class that contains all the functionality for
 * the Insurance of Major & Minor Medical Expenses Quota widget
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */

Class MultiCotizadorGMM extends MY_Controller {
    // "global" items
    //Array that going to contain all data from excel file
    var $array_excelFile_DB_Data = array();

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // load libraries
        $this->load->library('session');

        // load the models
        $this->load->model( array('Multicotizador_Banorte_Historial_Model',
                                  'Multicotizador_Banorte_Historial_Integrantes_Model',
                                  'External_Files_Cotizaciones_Model',
                                  'Multicotizador_Banorte_Variables_Model',
                                  'Multicotizador_Banorte_Variables_Tarifas_Basica_H_M_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Suma_Asegurada_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Niveles_Hospitalarios_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Deducibles_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Coaseguros_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Honorarios_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Zonas_Model',
                                  'Multicotizador_Banorte_Variables_Factores_Maternidad_Model',
                                  'Multicotizador_Banorte_Variables_Estados_Model',
                                  'Multicotizador_Banorte_Variables_Costos_Emergencias_Extranjeros_Model',
                                  'Multicotizador_Banorte_Variables_Costos_Enfermedades_Graves_Model'
                                 )
                          );

        // create the object to export the PDF
        $this->pdf = new ExportFiles();

        // creates the email and send the data
        $this->emailSend = new EmailSend();

        // creates the template object
        $this->template = new Templates();

        // Load utils
        $this->load->library('utils/NumberToLetterConverter');
        $this->load->library('utils/GetDataFromExcelFile');

        //Load all variables from excel file and save it into a global variable
        $this->array_excelFile_DB_Data = $this->retrieveDataFromExcelFile();
    }

    /**
     * Method that set data from excel file to DB, this only if the excel file was modified
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function retrieveDataFromExcelFile() {
        //Get values excel files ----------------------------------------------------
        $array_variables_return = [];
        $cotizadorName          = 'cotizadorBanorte';
        $fileName               = 'Segumedic_Comercial_Occidente_2017_comercial2.xlsx';

        //Instance class GetDataFromExcelFile
        $getDataFromExcelFileInstance = new GetDataFromExcelFile($fileName);

        //Get timestamp modified of excel file name in order to compare it with the database
        $fileTimestampModified = $getDataFromExcelFileInstance->getFileModified();

        //Get data from DB
        $arrayExternalFilesData = $this->External_Files_Cotizaciones_Model->getExternalFilesData($cotizadorName);

        //Get id and 'archivo_modificado'
        $id            = -1;
        $file_modified = 0;
        if (! empty($arrayExternalFilesData)) {
            $id            = $arrayExternalFilesData[0]['id'];
            $file_modified = $arrayExternalFilesData[0]['archivo_modificado'];
        }

        //date to timestamp
        $dbTimestampModified = strtotime($file_modified);

        //Compare timestamps between database record and the file
        //Get data from excel and save it into DB, if DateTimeModified from database is SMALLER than DateTimeModified from excel file
        if ($file_modified == 0 || $dbTimestampModified < $fileTimestampModified) {
            //update estatus to 'CANCELADO' of the old record if exist
            if ($id != 1) {
                $arrayToUpdateTable = array('estatus' => 'CANCELADO');
                $this->External_Files_Cotizaciones_Model->updateData($arrayToUpdateTable, $id);
            }

            //Create the new record with status 'ACTIVO'
            $arrayToInsertTable = array(
                                        'nombre_cotizador'   => $cotizadorName,
                                        'archivo_modificado' => date('Y-m-d H:i:s', $fileTimestampModified),
                                        'estatus'            => 'ACTIVO'
                                        );

            $externalFilesCotizacionesID = $this->External_Files_Cotizaciones_Model->insertData($arrayToInsertTable);

            //Array that contains variables and coordenates from excel fiels, and those name of variables will be used for insert them into DB
            $array_coordinates_variables =
                   array('FACTORES'  => 'nivelHospitalario_1:22-13,
                                         nivelHospitalario_2:22-14,
                                         catalogoHonorarios_1:12-14,
                                         catalogoHonorarios_2:12-15,
                                         maternidadMonto_1:12-30,
                                         maternidadMonto_2:12-31,
                                         maternidadMonto_3:12-32,
                                         enfermedadesGravesMonto_1:28-12,
                                         enfermedadesGravesMonto_2:28-13,
                                         enfermedadesGravesMonto_3:28-14,
                                         enfermedadesGravesMonto_4:28-15,
                                         enfermedadesGravesMonto_5:28-16,
                                         enfermedadesGravesMonto_6:28-17,
                                         enfermedadesGravesMonto_7:28-18,
                                         tipoCotizacion_1:22-17,
                                         tipoCotizacion_2:22-18,
                                         sumaAsegurada_1:25-3,
                                         sumaAsegurada_2:25-4,
                                         sumaAsegurada_3:25-5,
                                         sumaAsegurada_4:25-6,
                                         sumaAsegurada_5:25-7,
                                         sumaAsegurada_6:25-8,
                                         sumaAsegurada_7:25-9,
                                         sumaAsegurada_8:25-10,
                                         sumaAsegurada_9:25-11,
                                         sumaAsegurada_10:25-12,
                                         sumaAsegurada_11:25-13,
                                         sumaAsegurada_12:25-14,
                                         sumaAsegurada_13:25-15,
                                         sumaAsegurada_14:25-16,
                                         sumaAsegurada_15:25-17,
                                         sumaAsegurada_16:25-18,
                                         sumaAsegurada_17:25-19,
                                         sumaAsegurada_18:25-20,
                                         sumaAsegurada_19:25-21,
                                         sumaAsegurada_20:25-22,
                                         sumaAsegurada_21:25-23,
                                         sumaAsegurada_22:25-24,
                                         sumaAsegurada_23:25-25,
                                         sumaAsegurada_24:25-26,
                                         sumaAsegurada_25:25-27,
                                         sumaAsegurada_26:25-28,
                                         sumaAsegurada_27:25-29,
                                         deducible_1:9-3,
                                         deducible_2:9-4,
                                         deducible_3:9-5,
                                         deducible_4:9-6,
                                         deducible_5:9-7,
                                         deducible_6:9-8,
                                         deducible_7:9-9,
                                         deducible_8:9-10,
                                         deducible_9:9-11,
                                         deducible_10:9-12,
                                         deducible_11:9-13,
                                         deducible_12:9-14,
                                         deducible_13:9-15,
                                         deducible_14:9-16,
                                         deducible_15:9-17,
                                         deducible_16:9-18,
                                         deducible_17:9-19,
                                         deducible_18:9-20,
                                         deducible_19:9-21,
                                         deducible_20:9-22,
                                         coaseguro_1:12-3,
                                         coaseguro_2:12-4,
                                         coaseguro_3:12-5,
                                         coaseguro_4:12-6,
                                         coaseguro_5:12-7,
                                         coaseguro_6:12-8,
                                         coaseguro_7:12-9,
                                         coaseguro_8:12-10,
                                         zona_1:22-3,
                                         zona_2:22-4,
                                         zona_3:22-5,
                                         zona_4:22-6,
                                         zona_5:22-7,
                                         zona_6:22-8,
                                         reduccionDeducibePorAcc:34-23,
                                         asistencias:34-28,
                                         coberturaIntegralDentalVision:34-32,
                                         visionIncremental:34-36,
                                         derechoPoliza:23-30',
                         'COTIZACION'  => 'iva:3-22'
                        );

            //Get data from excel file depend on $array_coordinates_variables
            $array_coordinates_variables_results = $getDataFromExcelFileInstance->get_value_worksheet($array_coordinates_variables);

            //Add variables to insert
            $array_coordinates_variables_results["external_files_cotizaciones_id"] = $externalFilesCotizacionesID;
            $array_coordinates_variables_results["date_created"]                   = date("Y-m-d H:i:s");

            //Ids for the difference tables of catalogs
            $array_coordinates_variables_results["tarifa_basica_h_m_id"]           = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['tarifa_basica_h_m_id'])*1)+1;
            $array_coordinates_variables_results["factor_nivel_hospitalario_id"]   = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_nivel_hospitalario_id'])*1)+1;
            $array_coordinates_variables_results["factor_suma_asegurada_id"]       = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_suma_asegurada_id'])*1)+1;
            $array_coordinates_variables_results["factor_deducible_id"]            = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_deducible_id'])*1)+1;
            $array_coordinates_variables_results["factor_coaseguro_id"]            = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_coaseguro_id'])*1)+1;
            $array_coordinates_variables_results["factor_honorario_id"]            = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_honorario_id'])*1)+1;
            $array_coordinates_variables_results["factor_zona_id"]                 = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_zona_id'])*1)+1;
            $array_coordinates_variables_results["factor_maternidad_id"]           = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['factor_maternidad_id'])*1)+1;
            $array_coordinates_variables_results["estados_id"]                     = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['estados_id'])*1)+1;
            $array_coordinates_variables_results["costos_emerg_ext_id"]            = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['costos_emerg_ext_id'])*1)+1;
            $array_coordinates_variables_results["costos_enfer_graves_id"]         = $this->Multicotizador_Banorte_Variables_Model->getData()==null?'1':(($this->Multicotizador_Banorte_Variables_Model->getData()[0]['costos_enfer_graves_id'])*1)+1;

            //Insert data to from $array_coordinates_variables_results to Multicotizador_Banorte_Variables table
            $this->Multicotizador_Banorte_Variables_Model->insertData($array_coordinates_variables_results);

            // Get ages per sex array
            $array_tarifa_basica_h_m_results = $this->getArraysEdadesPorSexo($getDataFromExcelFileInstance, $array_coordinates_variables_results["tarifa_basica_h_m_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Tarifas_Basica_H_M_Model->insertData($array_tarifa_basica_h_m_results);

            // Get niveles hospitalarios
            $array_nivele_hospitalario_results = $this->getNivelesHospitalarios($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_nivel_hospitalario_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Niveles_Hospitalarios_Model->insertData($array_nivele_hospitalario_results);

            //Get factores suma asegurada from excel
            $array_factor_suma_asegurada_results = $this->getFactorSumaAsegurada($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_suma_asegurada_id"]);
          // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Suma_Asegurada_Model->insertData($array_factor_suma_asegurada_results);

            //Get deducibles from excel
            $array_factor_deducible_results = $this->getFactorDeducibles($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_deducible_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Deducibles_Model->insertData($array_factor_deducible_results);

            //Get coaseguros from excel
            $array_factor_coaseguro_results = $this->getFactorCoaseguros($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_coaseguro_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Coaseguros_Model->insertData($array_factor_coaseguro_results);

            //Get honorarios from excel
            $array_factor_honorario_results = $this->getFactorHonorarios($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_honorario_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Honorarios_Model->insertData($array_factor_honorario_results);

            //Get zonas from excel
            $array_factor_zona_results = $this->getFactorZonas($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_zona_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Zonas_Model->insertData($array_factor_zona_results);

            //Get maternidad from excel
            $array_factor_maternidad_results = $this->getFactorMaternidad($getDataFromExcelFileInstance, $array_coordinates_variables_results["factor_maternidad_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Factores_Maternidad_Model->insertData($array_factor_maternidad_results);

            //Get estados from excel
            $array_estados_results = $this->getEstados($getDataFromExcelFileInstance, $array_coordinates_variables_results["estados_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Estados_Model->insertData($array_estados_results);

            //Get Costos Emergencias Extranjeros from excel
            $array_costos_emerg_ext_results = $this->getCostosEmergExt($getDataFromExcelFileInstance, $array_coordinates_variables_results["costos_emerg_ext_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Costos_Emergencias_Extranjeros_Model->insertData($array_costos_emerg_ext_results);

            //Get Costos Enfermedades Graves from excel
            $array_costos_enfer_graves_results = $this->getCostosEnferGraves($getDataFromExcelFileInstance, $array_coordinates_variables_results["costos_enfer_graves_id"]);
            // Insert data into table
            $this->Multicotizador_Banorte_Variables_Costos_Enfermedades_Graves_Model->insertData($array_costos_enfer_graves_results);
        }

        //Get variables from DB after insert data, always get from DB
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables']                                = $this->Multicotizador_Banorte_Variables_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_tarifas_basica_h_m']             = $this->Multicotizador_Banorte_Variables_Tarifas_Basica_H_M_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_niveles_hospitalarios'] = $this->Multicotizador_Banorte_Variables_Factores_Niveles_Hospitalarios_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_suma_asegurada']        = $this->Multicotizador_Banorte_Variables_Factores_Suma_Asegurada_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_deducibles']            = $this->Multicotizador_Banorte_Variables_Factores_Deducibles_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_coaseguros']            = $this->Multicotizador_Banorte_Variables_Factores_Coaseguros_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_honorarios']            = $this->Multicotizador_Banorte_Variables_Factores_Honorarios_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_zonas']                 = $this->Multicotizador_Banorte_Variables_Factores_Zonas_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_factores_maternidad']            = $this->Multicotizador_Banorte_Variables_Factores_Maternidad_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_estados']                        = $this->Multicotizador_Banorte_Variables_Estados_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_costos_emergencias_extranjeros'] = $this->Multicotizador_Banorte_Variables_Costos_Emergencias_Extranjeros_Model->getData();
        $array_multicotizador_banorte_variables['multicotizador_banorte_variables_costos_enfermedades_graves']     = $this->Multicotizador_Banorte_Variables_Costos_Enfermedades_Graves_Model->getData();

        return $array_multicotizador_banorte_variables;
    }

/**
 * Method that get costos enfermedades graves
 *
 * @return array
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
public function getCostosEnferGraves($getDataFromExcelFileInstance, $id) {
  $array_costos_enfer_graves_results = $getDataFromExcelFileInstance->get_range_worksheet('AC11:AF18', 'FACTORES');
  $array_of_array_associative = [];

  for ($i=0; $i < sizeof($array_costos_enfer_graves_results); $i++) {
    $array_associative = [];
    $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
    $array_associative = $this->array_push_assoc($array_associative, 'suma_asegurada', $array_costos_enfer_graves_results[$i][0]);
    $array_associative = $this->array_push_assoc($array_associative, 'tarifa', $array_costos_enfer_graves_results[$i][1]);
    $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_costos_enfer_graves_results[$i][2]);
    $array_associative = $this->array_push_assoc($array_associative, 'costo', $array_costos_enfer_graves_results[$i][3]);

    $array_of_array_associative[$i] = $array_associative;
  }

  return $array_of_array_associative;
}

/**
 * Method that get costos emergencias en el extranjero
 *
 * @return array
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
public function getCostosEmergExt($getDataFromExcelFileInstance, $id) {
  $array_costos_emerg_ext_results = $getDataFromExcelFileInstance->get_range_worksheet('AK13:AL15', 'FACTORES');
  $array_of_array_associative = [];

  for ($i=0; $i < sizeof($array_costos_emerg_ext_results); $i++) {
    $array_associative = [];
    $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
    $array_associative = $this->array_push_assoc($array_associative, 'region', strval($array_costos_emerg_ext_results[$i][0]));
    $array_associative = $this->array_push_assoc($array_associative, 'costo', $array_costos_emerg_ext_results[$i][1]);

    $array_of_array_associative[$i] = $array_associative;
  }

  return $array_of_array_associative;
}

/**
 * Method that get estados
 *
 * @return array
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
public function getEstados($getDataFromExcelFileInstance, $id) {
  $array_estados_results = $getDataFromExcelFileInstance->get_range_worksheet('P3:U40', 'FACTORES');
  $array_of_array_associative = [];

  for ($i=0; $i < sizeof($array_estados_results); $i++) {
    $array_associative = [];
    $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
    $array_associative = $this->array_push_assoc($array_associative, 'estado', strval($array_estados_results[$i][0]));
    $array_associative = $this->array_push_assoc($array_associative, 'regioncn', $array_estados_results[$i][1]);
    $array_associative = $this->array_push_assoc($array_associative, 'regionee', $array_estados_results[$i][2]);
    $array_associative = $this->array_push_assoc($array_associative, 'regionte', $array_estados_results[$i][3]);
    $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_estados_results[$i][4]);
    $array_associative = $this->array_push_assoc($array_associative, 'factortt', $array_estados_results[$i][5]);

    $array_of_array_associative[$i] = $array_associative;
  }

  return $array_of_array_associative;
}

    /**
     * Method that get factors of maternidad
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getFactorMaternidad($getDataFromExcelFileInstance, $id) {
      $array_factor_maternidad_results = $getDataFromExcelFileInstance->get_range_worksheet('M29:N32', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_factor_maternidad_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'incrementoHQ', strval($array_factor_maternidad_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_factor_maternidad_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of honorarios
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getFactorZonas($getDataFromExcelFileInstance, $id) {
      $array_factor_zona_results = $getDataFromExcelFileInstance->get_range_worksheet('W3:X8', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_factor_zona_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'zona', strval($array_factor_zona_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_factor_zona_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of honorarios
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getFactorHonorarios($getDataFromExcelFileInstance, $id) {
      $array_factor_honorario_results = $getDataFromExcelFileInstance->get_range_worksheet('M14:N15', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_factor_honorario_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'catalogo', strval($array_factor_honorario_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_factor_honorario_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of coaseguro
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getFactorCoaseguros($getDataFromExcelFileInstance, $id) {
      $array_factor_coaseguro_results = $getDataFromExcelFileInstance->get_range_worksheet('M3:N10', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_factor_coaseguro_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'coaseguro', strval($array_factor_coaseguro_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_factor_coaseguro_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of deducible
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getFactorDeducibles($getDataFromExcelFileInstance, $id) {
      $array_factor_deducible_results = $getDataFromExcelFileInstance->get_range_worksheet('J3:K22', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_factor_deducible_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'deducible', strval($array_factor_deducible_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_factor_deducible_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of sum asegurada
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getFactorSumaAsegurada($getDataFromExcelFileInstance, $id) {
      $array_factor_suma_asegurada_results = $getDataFromExcelFileInstance->get_range_worksheet('Z3:AA29', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_factor_suma_asegurada_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'suma_asegurada', strval($array_factor_suma_asegurada_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_factor_suma_asegurada_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of nivel hospitalario
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getNivelesHospitalarios($getDataFromExcelFileInstance, $id) {
      $array_nivel_hospitalario_results = $getDataFromExcelFileInstance->get_range_worksheet('W13:X14', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_nivel_hospitalario_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'nivel_hospitalario', strval($array_nivel_hospitalario_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'factor', $array_nivel_hospitalario_results[$i][1]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }

    /**
     * Method that get factors of tarifa ages per sex
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getArraysEdadesPorSexo($getDataFromExcelFileInstance, $id) {
      $array_tarifa_basica_h_m_results = $getDataFromExcelFileInstance->get_range_worksheet('F3:H103', 'FACTORES');
      $array_of_array_associative = [];

      for ($i=0; $i < sizeof($array_tarifa_basica_h_m_results); $i++) {
        $array_associative = [];
        $array_associative = $this->array_push_assoc($array_associative, 'id', strval($id));
        $array_associative = $this->array_push_assoc($array_associative, 'edad', strval($array_tarifa_basica_h_m_results[$i][0]));
        $array_associative = $this->array_push_assoc($array_associative, 'hombres', $array_tarifa_basica_h_m_results[$i][1]);
        $array_associative = $this->array_push_assoc($array_associative, 'mujeres', $array_tarifa_basica_h_m_results[$i][2]);

        $array_of_array_associative[$i] = $array_associative;
      }

      return $array_of_array_associative;
    }


    function array_push_assoc($array, $key, $value) {
      $array[$key] = $value;
      return $array;
    }

    /**
     * Method will load all the information for the insurance quote
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function index() {
        $dataView = $this->__generateDropdowns($this->array_excelFile_DB_Data);

        $content = $this->loadSingleView('index', $dataView, TRUE);
        $data    = $this->__setArray('content', $content);
        $data    = $this->__setArray('title', 'Multicotizador de GMM');

        $this->loadTemplate($data);
    }

    /**
     * Method will calculate the quotation with user data sent
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function previewData() {
        $data = json_decode(file_get_contents('php://input'), true);

        $nombre                                = $data["fullname"];
        $email                                 = $data["email"];
        $telefono                              = $data["telefono"];
        $estado                                = $data["estado"];
        $ciudad                                = $data["ciudad"];
        $nivelHospitalario                     = $data["nivelHospitalario"];
        $sumaAsegurada                         = $data["sumaAsegurada"];
        $coaseguro                             = $data["coaseguro"];
        $deducible                             = $data["deducible"];
        $zona                                  = $data["zona"];
        $tipoCotizacion                        = $data["tipoCotizacion"];
        $tabuladorHonorariosMedicos            = $data["tabuladorHonorariosMedicos"];
        $reduccionDeducibleAccidente           = $data["reduccionDeducibleAccidente"];
        $incrementoPartoCesarea                = $data["incrementoPartoCesarea"];
        $cesareaMonto                          = $data["cesareaMonto"];
        $coberturaVisionIncremental            = $data["coberturaVisionIncremental"];
        $coberturaIndemnizacionEnfermedadGrave = $data["coberturaIndemnizacionEnfermedadGrave"];
        $graveMonto                            = $data["graveMonto"];
        $emergenciaExtranjero                  = $data["emergenciaExtranjero"];
        $integrantesArray                      = $data["integrantes"]; //Array of integrantes, integrante, nombre, sexo, edad, primaBrutaAnual

        //Get the amount per year/bianual/quarterly/monthly of each integrante
        $integrantesArrayPrimaBrutaAnual = $this->getPrimaTotalPerIntegrante($data);

        //Build the response
        $response = array('status'           => 'success',
                          'integrantesArray' => $integrantesArrayPrimaBrutaAnual);

        // create json response
        echo json_encode($response);
    }

    /**
     * Method will get the amount per year/bianual/quarterly/monthly of each integrante
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getPrimaTotalPerIntegrante($data) {
      //Get global data from excel
      $array_excelFile_DB_Data = $this->array_excelFile_DB_Data;

      //Get aplicaciones de factores total
      $aplicaciones_de_factores_total = $this->getAplicacionesDeFactores($data);

      $integrantesArrayPrimaBrutaAnual = [];
      $integrantesArray        = $data["integrantes"];

      $i = 0;
      foreach ($integrantesArray as $value) {
        $edadIntegrante = $value['edad'];
        $sexoIntegrante = strtolower($value['sexo']);
        $integrante     = strtolower($value['integrante']);

        // *** "Factores" *** Get 'Basica' ammount per $integrantesArray of FACTORES sheet
        $basicaValue = 0;
        for ($j=0; $j < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_tarifas_basica_h_m']); $j++) {
          if ($array_excelFile_DB_Data['multicotizador_banorte_variables_tarifas_basica_h_m'][$j]['edad'] == $edadIntegrante) {
            $hombresFactor = str_replace( ',', '', $array_excelFile_DB_Data['multicotizador_banorte_variables_tarifas_basica_h_m'][$j]['hombres'] );
            $mujeresFactor = str_replace( ',', '', $array_excelFile_DB_Data['multicotizador_banorte_variables_tarifas_basica_h_m'][$j]['mujeres'] );

            if($sexoIntegrante == 'hombre') {
                 $basicaValue = $hombresFactor * $aplicaciones_de_factores_total;
            }else if ($sexoIntegrante == 'mujer') {
                 $basicaValue = $mujeresFactor * $aplicaciones_de_factores_total;
            }
          }
        }

        // *** "Factores" *** Get 'Maternidad' ammount per $integrantesArray of FACTORES sheet
        $maternidadValue        = 0;
        $original               = array("$", ",");
        $replace                = array("", "");
        $incrementoHQ_from_form = trim($data["cesareaMonto"]);

        if( ($sexoIntegrante == 'mujer') && ($edadIntegrante > 17 && $edadIntegrante < 50) ) {
          for ($k=0; $k < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_factores_maternidad']); $k++) {
            $incrementoHQ_from_array = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_factores_maternidad'][$k]['incrementoHQ']);
            $incrementoHQ_from_array = str_replace($original, $replace, trim($incrementoHQ_from_array));

            if($incrementoHQ_from_form == $incrementoHQ_from_array) {
              $maternidadValue = floatval(str_replace(",", "", trim($array_excelFile_DB_Data['multicotizador_banorte_variables_factores_maternidad'][$k]['factor'])));
            }
          }
        }

        // *** "Factores" *** Get 'Adicionales' ammount per $integrantesArray of FACTORES sheet AQ-2
        $adicionalesValue = 0;
        $adicionalesValue = $this->getCoberturasAdicionales($data, $integrante);

        // *** "Factores" *** Get 'TOTAL EXT' ammount per $integrantesArray of FACTORES sheet AR-2
        $totalEXTValue = 0;

        if ($data['emergenciaTotalExtranjero'] == 'SI') {
          $estadoSeleccionado         = $data['estado'];
          $estadoSeleccionadoFactorTT = 0;

          //Iterate in estados array
          for ($l=0; $i < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_estados']); $l++) {
            $estadoNombreDB = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_estados'][$l]['estado']);
            $estadoFactorTT = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_estados'][$l]['factortt']);

            if ($estadoSeleccionado == $estadoNombreDB) {
              $estadoSeleccionadoFactorTT = $estadoFactorTT;
              break;
            }
          }

          $totalEXTValue = $estadoSeleccionadoFactorTT * $basicaValue;
        }

        //Add $totales to integrantes array for each integrante
        $integrantesArray[$i]["primaBrutaAnual"] = floatval($basicaValue) + floatval($maternidadValue) + floatval($adicionalesValue) + floatval($totalEXTValue);
        $integrantesArrayPrimaBrutaAnual[$i]     = $integrantesArray[$i];

        $i ++;
      }

      //return the new array ($integrantesArrayPrimaBrutaAnual) with the $primaBrutaAnual of each field
      return $integrantesArrayPrimaBrutaAnual;
    }

    /*
    * COBERTURAS ADICIONALES excel Factores sheet->  AK-1
    */
    public function getCoberturasAdicionales($data, $integrante) {
      // We will sum all factors (Emergencia en el extranjero + Reducción de deducible x acc + Asistencias + Cobertura integral D y V + Visión Incremental + Enfermedades graves)

      //Get global data from excel
      $array_excelFile_DB_Data = $this->array_excelFile_DB_Data;

      // Get Emergencia en el extranjero
      $emergenciaExtranjero = 0;

      if ($data["emergenciaExtranjero"] == "SI") {
        $estadoSeleccionadoCotizador = trim($data['estado']);
        //Iterate in estados array
        for ($i=0; $i < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_estados']); $i++) {
          $estadoNombreDB = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_estados'][$i]['estado']);
          $estadoRegionEE = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_estados'][$i]['regionee']);

          if ($estadoSeleccionadoCotizador == $estadoNombreDB) {
            //Iterate in costos emergencias extranjeros array in order to get the cost of region
            $emergenciaExtranjero = floatval($this->getCostoEmergenciaExtranjero($array_excelFile_DB_Data, $estadoRegionEE));
            break;
          }
        }
      }

      // Get Reducción de deducible x acc
      $reduccionDeducibleACC = 0;

      if ($data["reduccionDeducibleAccidente"] == "SI") {
        $reduccionDeducibleACC = floatval($array_excelFile_DB_Data['multicotizador_banorte_variables'][0]['reduccionDeducibePorAcc']);
      }

      // Get Asistencias
      $asistencias = floatval($array_excelFile_DB_Data['multicotizador_banorte_variables'][0]['asistencias']);

      // Get Cobertura integral D y V
      $coberturaIntegralDV = floatval($array_excelFile_DB_Data['multicotizador_banorte_variables'][0]['coberturaIntegralDentalVision']);;

      // Get Visión Incremental
      $visionIncremental = 0;

      if ($data["coberturaVisionIncremental"] == "SI") {
        $visionIncremental = floatval($array_excelFile_DB_Data['multicotizador_banorte_variables'][0]['visionIncremental']);;
      }

      // Get Enfermedades graves
      $enfermedadesGraves = 0;

      if ($data["coberturaIndemnizacionEnfermedadGrave"] == "SI") {
        $coberturaEnfermedadesGravesMontoSeleccionado = trim($data['graveMonto']);
        //Iterate in enfermedades graves array str_replace("world","Peter","Hello world!")
        for ($i=0; $i < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_costos_enfermedades_graves']); $i++) {
          $sumaAsegurada = str_replace(",", "", str_replace("$", "", trim($array_excelFile_DB_Data['multicotizador_banorte_variables_costos_enfermedades_graves'][$i]['suma_asegurada'])));
          $costo         = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_costos_enfermedades_graves'][$i]['costo']);

          if ($coberturaEnfermedadesGravesMontoSeleccionado == $sumaAsegurada) {
            //Iterate in enfermedades array in order to get the cost depend on suma asegurada selected
            $enfermedadesGraves = floatval($costo);
            break;
          }
        }
      }

      // Sum all variables in order to get "Coberturas Adicionales"

      if ($integrante == "titular") {
        //If integrante is not equal to titualar, will pay $enfermedadesGraves
        return $emergenciaExtranjero + $reduccionDeducibleACC + $asistencias + $coberturaIntegralDV + $visionIncremental + $enfermedadesGraves;
      }
      //If integrante is not equal to titualar, will NOT pay $enfermedadesGraves
      return $emergenciaExtranjero + $reduccionDeducibleACC + $asistencias + $coberturaIntegralDV + $visionIncremental;
    }

    //Get costo emergencia extranjeros
    public function getCostoEmergenciaExtranjero($array_excelFile_DB_Data, $estadoRegionEE) {
      $emergenciaExtranjero = 0;

      for ($i=0; $i < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_costos_emergencias_extranjeros']); $i++) {
        $emerExtRegion = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_costos_emergencias_extranjeros'][$i]['region']);
        $emerExtCosto  = trim($array_excelFile_DB_Data['multicotizador_banorte_variables_costos_emergencias_extranjeros'][$i]['costo']);

        if ($estadoRegionEE == $emerExtRegion) {
          $emergenciaExtranjero = $emerExtCosto;
          break;
        }
      }

      return $emergenciaExtranjero;
    }

    /*
    * APLICACIÓN DE FACTORES excel Factores sheet-> AH-1 Factores sheet
    */
    public function getAplicacionesDeFactores($data) {
      // We will multiply all factors (Nivel Hospitalario * Suma Asegurada * Deducible * Coaseguro * Tabulador de Honorarios Médicos * Zona)

      //Get global data from excel
      $array_excelFile_DB_Data = $this->array_excelFile_DB_Data;

      // **** Get Nivel Hospitalario ****
      //Get multicotizador_banorte_variables_factores_niveles_hospitalarios array
      $array_multicotizador_banorte_variables_factores_niveles_hospitalarios = $array_excelFile_DB_Data['multicotizador_banorte_variables_factores_niveles_hospitalarios'];

      $nivelHospitalarioFactor = 0;
      for ($i=0; $i < sizeof($array_multicotizador_banorte_variables_factores_niveles_hospitalarios) ; $i++) {
        $nivelHospitalario_from_array = trim(str_replace(',', "", $array_multicotizador_banorte_variables_factores_niveles_hospitalarios[$i]['nivel_hospitalario']));
        $nivelHospitalario_from_form  = trim(str_replace(',', "", $data["nivelHospitalario"]));

        if ($nivelHospitalario_from_array == $nivelHospitalario_from_form) {
          $nivelHospitalarioFactor = $array_multicotizador_banorte_variables_factores_niveles_hospitalarios[$i]['factor'];
          break;
        }
      }

      // **** Get Suma Asegurada ****
      //Get multicotizador_banorte_variables_factores_suma_asegurada array
      $array_multicotizador_banorte_variables_factores_suma_asegurada = $array_excelFile_DB_Data['multicotizador_banorte_variables_factores_suma_asegurada'];

      $sumaAseguradaFactor = 0;
      for ($i=0; $i < sizeof($array_multicotizador_banorte_variables_factores_suma_asegurada) ; $i++) {
        $sumaAsegurada_from_array = trim(str_replace(',', "", $array_multicotizador_banorte_variables_factores_suma_asegurada[$i]['suma_asegurada']));
        $sumaAsegurada_from_form  = trim(str_replace(',', "", $data["sumaAsegurada"]));

        if ($sumaAsegurada_from_array == $sumaAsegurada_from_form) {
          $sumaAseguradaFactor = $array_multicotizador_banorte_variables_factores_suma_asegurada[$i]['factor'];
          break;
        }
      }

      // **** Get Deducible ****
      $array_multicotizador_banorte_variables_factores_deducibles = $array_excelFile_DB_Data['multicotizador_banorte_variables_factores_deducibles'];

      $deducibleFactor = 0;
      for ($i=0; $i < sizeof($array_multicotizador_banorte_variables_factores_deducibles) ; $i++) {
        $deducible_from_array = trim(str_replace(',', "", $array_multicotizador_banorte_variables_factores_deducibles[$i]['deducible']));
        $deducible_from_form  = trim(str_replace(',', "", $data["deducible"]));

        if ($deducible_from_array == $deducible_from_form) {
          $deducibleFactor = $array_multicotizador_banorte_variables_factores_deducibles[$i]['factor'];
          break;
        }
      }

      // **** Get Coaseguro ****
      $array_multicotizador_banorte_variables_factores_coaseguros = $array_excelFile_DB_Data['multicotizador_banorte_variables_factores_coaseguros'];

      $coaseguroFactor = 0;
      for ($i=0; $i < sizeof($array_multicotizador_banorte_variables_factores_coaseguros) ; $i++) {
        $coaseguro_from_array = strlen($array_multicotizador_banorte_variables_factores_coaseguros[$i]['coaseguro']) <= 4 ? trim(str_replace('%', "", $array_multicotizador_banorte_variables_factores_coaseguros[$i]['coaseguro'])) : trim($array_multicotizador_banorte_variables_factores_coaseguros[$i]['coaseguro']);
        $coaseguro_from_form  = strlen($data["coaseguro"]) <= 4 ? trim(str_replace('%', "", $data["coaseguro"]))*100 : trim($data["coaseguro"]);

        if ($coaseguro_from_array == $coaseguro_from_form) {
          $coaseguroFactor = $array_multicotizador_banorte_variables_factores_coaseguros[$i]['factor'];
          break;
        }
      }

      // **** Get Tabulador de Honorarios Médicos ****
      $array_multicotizador_banorte_variables_factores_honorarios = $array_excelFile_DB_Data['multicotizador_banorte_variables_factores_honorarios'];

      $honorarioFactor = 0;
      for ($i=0; $i < sizeof($array_multicotizador_banorte_variables_factores_honorarios) ; $i++) {
        $honorario_from_array = trim($array_multicotizador_banorte_variables_factores_honorarios[$i]['catalogo']);
        $honorario_from_form  = trim($data["tabuladorHonorariosMedicos"]);

        if ($honorario_from_array == $honorario_from_form) {
          $honorarioFactor = $array_multicotizador_banorte_variables_factores_honorarios[$i]['factor'];
          break;
        }
      }

      // **** Get Zona ****
      $array_multicotizador_banorte_variables_factores_zonas = $array_excelFile_DB_Data['multicotizador_banorte_variables_factores_zonas'];

      $zonaFactor = 0;
      for ($i=0; $i < sizeof($array_multicotizador_banorte_variables_factores_zonas) ; $i++) {
        $zona_from_array = trim($array_multicotizador_banorte_variables_factores_zonas[$i]['zona']);
        $zona_from_form  = trim($data["zona"]);

        if ($zona_from_array == $zona_from_form) {
          $zonaFactor = $array_multicotizador_banorte_variables_factores_zonas[$i]['factor'];
          break;
        }
      }

      //Multiply all factors
      return $nivelHospitalarioFactor * $sumaAseguradaFactor * $deducibleFactor * $coaseguroFactor * $honorarioFactor * $zonaFactor;
    }

    /**
     * Method will calculate the quotation with user data sent
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData() {
        $json_array = json_decode(file_get_contents('php://input'), true);

        //Save data into history tables------------------------------------------------------------------------------
        $arrayToInsertHistorial = array(
            'session'                                  => session_id(),
            'vendedor'                                 => $json_array['asesor'],
            'fullname'                                 => $json_array['fullname'],
            'email'                                    => $json_array['email'],
            'telefono'                                 => $json_array['telefono'],
            'estado'                                   => $json_array['estado'],
            'ciudad'                                   => $json_array['ciudad'],
            'nivel_hospitalario'                       => $json_array["nivelHospitalario"],
            'suma_asegurada'                           => $json_array["sumaAsegurada"],
            'coaseguro'                                => $json_array["coaseguro"],
            'deducible'                                => $json_array["deducible"],
            'tipo_cotizacion'                          => $json_array["tipoCotizacion"],
            'tabulador_honorarios_medicos'             => $json_array["tabuladorHonorariosMedicos"],
            'reduccion_deducible_accidente'            => $json_array["reduccionDeducibleAccidente"],
            'incremento_parto_cesarea'                 => $json_array["incrementoPartoCesarea"],
            'cesarea_monto'                            => $json_array["cesareaMonto"],
            'cobertura_vision_incremental'             => $json_array["coberturaVisionIncremental"],
            'cobertura_indemnizacion_enfermedad_grave' => $json_array["coberturaIndemnizacionEnfermedadGrave"],
            'enfermedad_grave_monto'                   => $json_array["graveMonto"],
            'emergencia_extranjero'                    => $json_array["emergenciaExtranjero"],
            'fecha_creacion'                           => date("Y-m-d H:i:s") );

        //Insert data into Multicotizador_Banorte_Historial table ------------------------------------------------------------
        $multicotizador_banorte_historial_inserted_id =
            $this->Multicotizador_Banorte_Historial_Model->insertData($arrayToInsertHistorial);

        //Get MaxIntegrantesId data from Cubre_Deducible_Historial_Integrantes table
        $max_id_integrantes = $this->Multicotizador_Banorte_Historial_Integrantes_Model->getMaxIntegrantesId();

        $arrayToInsertIntegrantes        = array();
        $totalBrutoPrimaAnualIntegrantes = 0;
        for ($i=0; $i < count( $json_array['integrantes'] ); $i++) {
            $arrayTemp = array(
                'id_integrantes'     => $max_id_integrantes,
                'integrante'         => $json_array['integrantes'] [$i] ['integrante'],
                'nombre'             => $json_array['integrantes'] [$i] ['nombre'],
                'sexo'               => $json_array['integrantes'] [$i] ['sexo'],
                'edad'               => $json_array['integrantes'] [$i] ['edad'],
                'primaBrutaAnual'    => $json_array['integrantes'] [$i] ['primaBrutaAnual']
            );

            array_push($arrayToInsertIntegrantes, $arrayTemp);

            $totalBrutoPrimaAnualIntegrantes += floatval($json_array['integrantes'] [$i] ['primaBrutaAnual']);
        }

        //Get and save data for totales anuales.
        $derechoPoliza                 =  floatval($this->Multicotizador_Banorte_Variables_Model->getData()[0]['derechoPoliza']);
        $iva                           =  floatval($this->Multicotizador_Banorte_Variables_Model->getData()[0]['iva']);
        $json_array['primaTotalAnual'] = $totalBrutoPrimaAnualIntegrantes;
        $json_array['derechoPoliza']   = $derechoPoliza;
        $json_array['subTotal']        = floatval($totalBrutoPrimaAnualIntegrantes + $derechoPoliza);
        $json_array['ivaSubTotal']     = floatval($json_array['subTotal'] * $iva);
        $json_array['primaTotalAnual'] = floatval($json_array['subTotal'] + $json_array['ivaSubTotal']);

print_r($json_array); //debug

        $multicotizador_banorteHistorialIntegrantes_inserted_id =
            $this->Multicotizador_Banorte_Historial_Integrantes_Model->insertData($arrayToInsertIntegrantes);

        //Create relation between historial table and integrantes table
        $arrayToUpdateHistorial = array(
            'id_integrantes' => $max_id_integrantes);

        $this->Multicotizador_Banorte_Historial_Model->updateData($arrayToUpdateHistorial, $multicotizador_banorte_historial_inserted_id);
        //End data insert-------------------------------------------------------------------------------------------

        // Generate the Template and init the PDF file creation-----------------------------------------------------
        //add id of historial into json_array in order to show it in PDF
        $json_array["multicotizador_banorte_historial_inserted_id"] = $multicotizador_banorte_historial_inserted_id;

        //Add Current date
        $json_array["fechaCotizacion"] = date("d/m/Y");

        //$template = $this->template->multicotizadorBanorteTemplate($json_array);
        //$PDF      = $this->pdf->__initPDFMulticotizador($template, "Cotiza Multicotizador Banorte GMM", "Cotiza Multicotizador Banorte GMM");

        // Set PDF name and create PDF file
        $fileName = "cotizaMulticotizadorBanorte_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

        // Create the file
        //ob_clean();
        $PDF->Output(FCPATH . 'public/files/' . $fileName, 'F');
        $completePath = FCPATH . 'public/files/' . $fileName;
        $returnedPath = base_url() . 'public/files/' . $fileName;
        //End of PDF creater----------------------------------------------------------------------------------------

        // Send the information via e-mail
        //$this->emailSend->__initEmailCubreTuDeducible($json_array, $PDF, $completePath, "Cubre Tu Deducible GMM");

        // create json response
        $response = array('status'   => 'success',
                          'pathFile' => $returnedPath);

        echo json_encode($response);
    }

    /**
     * Method will generate all dropdown information
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __generateDropdowns($array_excelFile_DB_Data = null) {
        $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);

        // Get states from DB according to excel file
        $array_states = array();
        for ($i=0; $i < sizeof($array_excelFile_DB_Data['multicotizador_banorte_variables_estados']); $i++) {
          $estadoNombre = $array_excelFile_DB_Data['multicotizador_banorte_variables_estados'][$i]['estado'];

          $array_states = $this->array_push_assoc($array_states, $estadoNombre, $estadoNombre);
        }

        $array_si_no                      = array('SI' => 'Si',
                                                  'NO' => 'No');

        $array_integrantes                = array('0' => 'Titular',
                                                  '1' => 'Conyuge',
                                                  '2' => 'Hijo(a)');

        $array_sexo                       = array('1' => 'Hombre',
                                                  '0' => 'Mujer');

        $array_tiposCotizacion            = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['tipoCotizacion_1'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['tipoCotizacion_1'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['tipoCotizacion_2'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['tipoCotizacion_2']);

        $array_nivelHospitalario          = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['nivelHospitalario_1'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['nivelHospitalario_1'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['nivelHospitalario_2'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['nivelHospitalario_2']);

        $array_tabuladorHonorariosMedicos = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['catalogoHonorarios_1'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['catalogoHonorarios_1'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['catalogoHonorarios_2'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['catalogoHonorarios_2']);

        $array_maternidadMontos           = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['maternidadMonto_1'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['maternidadMonto_1']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['maternidadMonto_2'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['maternidadMonto_2']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['maternidadMonto_3'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['maternidadMonto_3']));

        $array_enfermedadesGravesMontos   = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_1'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_1']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_2'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_2']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_3'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_3']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_4'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_4']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_5'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_5']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_6'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_6']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_7'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['enfermedadesGravesMonto_7']));

        $array_sumaAsegurada              = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_1'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_1']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_2'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_2']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_3'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_3']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_4'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_4']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_5'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_5']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_6'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_6']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_7'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_7']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_8'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_8']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_9'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_9']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_10'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_10']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_11'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_11']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_12'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_12']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_13'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_13']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_14'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_14']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_15'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_15']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_16'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_16']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_17'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_17']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_18'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_18']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_19'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_19']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_20'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_20']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_21'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_21']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_22'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_22']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_23'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_23']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_24'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_24']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_25'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_25']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_26'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_26']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_27'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['sumaAsegurada_27']));

        $array_deducibles                 = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_1'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_1']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_2'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_2']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_3'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_3']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_4'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_4']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_5'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_5']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_6'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_6']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_7'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_7']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_8'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_8']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_9'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_9']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_10'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_10']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_11'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_11']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_12'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_12']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_13'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_13']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_14'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_14']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_15'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_15']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_16'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_16']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_17'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_17']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_18'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_18']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_19'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_19']),
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_20'] => $fmt->format($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['deducible_20']));

        $array_coaseguros                 = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_1'] => (($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_1'])*100)."%",
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_2'] => (($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_2'])*100)."%",
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_3'] => (($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_3'])*100)."%",
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_4'] => (($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_4'])*100)."%",
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_5'] => (($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_5'])*100)."%",
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_6'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_6'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_7'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_7'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_8'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['coaseguro_8']);

        $array_zonas                       = array($array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_1'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_1'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_2'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_2'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_3'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_3'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_4'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_4'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_5'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_5'],
                                                  $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_6'] => $array_excelFile_DB_Data["multicotizador_banorte_variables"][0]['zona_6']);

        $dataView                         = array('array_states'                     => $array_states,
                                                  'array_si_no'                      => $array_si_no,
                                                  'array_integrantes'                => $array_integrantes,
                                                  'array_sexo'                       => $array_sexo,
                                                  'array_tiposCotizacion'            => $array_tiposCotizacion,
                                                  'array_nivelHospitalario'          => $array_nivelHospitalario,
                                                  'array_tabuladorHonorariosMedicos' => $array_tabuladorHonorariosMedicos,
                                                  'array_maternidadMontos'           => $array_maternidadMontos,
                                                  'array_enfermedadesGravesMontos'   => $array_enfermedadesGravesMontos,
                                                  'array_sumaAsegurada'              => $array_sumaAsegurada,
                                                  'array_deducibles'                 => $array_deducibles,
                                                  'array_coaseguros'                 => $array_coaseguros,
                                                  'array_zonas'                       => $array_zonas);


        return $dataView;
    }
}
