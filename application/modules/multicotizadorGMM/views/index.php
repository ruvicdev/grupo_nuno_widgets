<script type="text/javascript"
      src="<?php echo base_url() . 'public/js/custom/multicotizadorGMM.js'; ?>"></script>
<script type="text/javascript"
      src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>

<div class="default-margin-top ui grid centered removePadding">
    <!-- SECTION FOR STEPS -->
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos De Cotización</div>
                <div class="description">Ingresa los Datos de la Cotización</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Envío</div>
                <div class="description">Envío Cotización</div>
            </div>
        </div>
    </div>
</div>

<div class="ui grid centered">
    <form class="ui form">
        <div class="row">&nbsp;</div>
        <div>
          <h1 class="ui header">Multicotizador De Gastos Médicos Mayores</h1>
        </div>
        <div>
            <h4 class="ui header">Llene todos los campos para ver su cotización</h3>
        </div>
        <div class="row">&nbsp;</div>
        <div class="ui styled accordion">
            <!--Errors for form page-->
            <div class="ui error message"></div>
            <!-- FIRST SECTION -->
            <div id="accordion-1"><!-- disabled or enabled accordion -->
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos De Cotización
                </div>
                <div class="content active" id="content-1">
                    <div class="two fields">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="asesorchk" id="asesorcheckbox" tabindex="0" data-value="asesorchk" class="checkdata">
                                <label>
                                    ¿Eres Vendedor de Grupo Nuño?
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui segment hidden-segment-value" id="asesor-div">
                                <input type="text" name="asesor" id="asesor" placeholder="Nombre del Vendedor">
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>
                            Nombre Completo
                        </label>
                        <input type="text" name="fullname" id="fullname" placeholder="Nombre Completo">
                    </div>
                  <div class="two fields">
                      <div class="required field">
                          <label>
                              Correo Electrónico
                          </label>
                          <input type="text" name="email" id="email" placeholder="Corre Electrónico">
                      </div>
                      <div class="required field">
                          <label>
                              Teléfono
                          </label>
                          <input type="text" name="telefono" id="telefono" placeholder="Teléfono" class="form-data-selected tel">
                      </div>
                    </div>
                    <div class="two fields">
                        <div class="required field">
                            <label>
                                Estado
                            </label>
                            <div id ="estadoDropdown" class="ui selection dropdown">
                                <input type="hidden" name="estado" id="estado">
                                <i class="dropdown icon"></i>
                                <div class="default text">Seleccione</div>
                                <div class="menu">
                                    <?php foreach ($array_states as $key => $value): ?>
                                        <div class="item" data-value="<?php echo $key; ?>">
                                            <?php echo $value; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label>
                                Ciudad
                            </label>
                            <input type="text" name="ciudad" id="ciudad" placeholder="Ciudad">
                        </div>
                    </div>
                  <h3 class="ui header">Datos de su Póliza de Gastos Médicos Mayores</h3>
                  <div class="inline fields">
                      <div class="required field">
                          <label>
                              Tipo de Cotización
                          </label>
                          <div id ="tipoCotizacionDropdown" class="ui selection dropdown">
                              <input type="hidden" name="tipoCotizacion" id="tipoCotizacion">
                              <i class="dropdown icon"></i>
                              <div class="default text">Seleccione</div>
                              <div class="menu">
                                  <?php foreach ($array_tiposCotizacion as $key => $value): ?>
                                      <div class="item" data-value="<?php echo $key; ?>">
                                          <?php echo $value; ?>
                                      </div>
                                  <?php endforeach; ?>
                              </div>
                          </div>
                      </div>
                      <div class="required field">
                          <label>
                              Tab. de Honorarios
                          </label>
                          <div id ="tabuladorHonorariosMedicosDropdown" class="ui selection dropdown">
                              <input type="hidden" name="tabuladorHonorariosMedicos" id="tabuladorHonorariosMedicos">
                              <i class="dropdown icon"></i>
                              <div class="default text">Seleccione</div>
                              <div class="menu">
                                  <?php foreach ($array_tabuladorHonorariosMedicos as $key => $value): ?>
                                      <div class="item" data-value="<?php echo $key; ?>">
                                          <?php echo $value; ?>
                                      </div>
                                  <?php endforeach; ?>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="inline fields">
                    <div class="required field">
                        <label>
                            Nivel Hospitalario
                        </label>
                        <div id ="nivelHospitalarioDropdown" class="ui selection dropdown">
                            <input type="hidden" name="nivelHospitalario" id="nivelHospitalario">
                            <i class="dropdown icon"></i>
                            <div class="default text">Seleccione</div>
                            <div class="menu">
                                <?php foreach ($array_nivelHospitalario as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>
                            Suma Asegurada
                        </label>
                        <div id ="sumaAseguradaDropdown" class="ui selection dropdown">
                            <input type="hidden" name="sumaAsegurada" id="sumaAsegurada">
                            <i class="dropdown icon"></i>
                            <div class="default text">Seleccione</div>
                            <div class="menu">
                                <?php foreach ($array_sumaAsegurada as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inline fields">
                    <div class="required field">
                        <label>
                            Coaseguro
                        </label>
                        <div id ="coaseguroDropdown" class="ui selection dropdown">
                            <input type="hidden" name="coaseguro" id="coaseguro">
                            <i class="dropdown icon"></i>
                            <div class="default text">Seleccione</div>
                            <div class="menu">
                                <?php foreach ($array_coaseguros as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>
                            Deducible
                        </label>
                        <div id ="deducibleDropdown" class="ui selection dropdown">
                            <input type="hidden" name="deducible" id="deducible">
                            <i class="dropdown icon"></i>
                            <div class="default text">Seleccione</div>
                            <div class="menu">
                                <?php foreach ($array_deducibles as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="field">
                    <div class="required field">
                        <label>
                            Zona
                        </label>
                        <div id ="zonaDropdown" class="ui selection dropdown">
                            <input type="hidden" name="zona" id="zona">
                            <i class="dropdown icon"></i>
                            <div class="default text">Seleccione</div>
                            <div class="menu">
                                <?php foreach ($array_zonas as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inline fields">
                      <div class="fields">
                          <div class="ui checkbox">
                              <input type="checkbox" name="reduccionDeducibleAccidente" id="reduccionDeducibleAccidente" tabindex="0" data-value="reduccionDeducibleAccidente" class="checkdata">
                              <label>
                                  Reduccion de Deducible por Accidente
                              </label>
                          </div>
                      </div>
                </div>
                <div class="two fields">
                        <div class="fields">
                            <div class="field">
                                <div class="ui checkbox">
                                    <input type="checkbox" name="incrementoPartoCesarea" id="incrementoPartoCesarea" tabindex="0" data-value="incrementoPartoCesareaChk" class="checkdata">
                                    <label>
                                        Incremento en Parto o Cesárea
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="ui segment hidden-segment-value" id="cesareaMonto-div">
                          <div id ="cesareaMontoDropdown" class="ui selection dropdown">
                              <input type="hidden" name="cesareaMonto" id="cesareaMonto">
                              <i class="dropdown icon"></i>
                              <div class="default text">Seleccione Monto</div>
                              <div class="menu">
                                  <?php foreach ($array_maternidadMontos as $key => $value): ?>
                                      <div class="item" data-value="<?php echo $key; ?>">
                                          <?php echo $value; ?>
                                      </div>
                                  <?php endforeach; ?>
                              </div>
                          </div>
                        </div>
                </div>
                <div class="inline fields">
                      <div class="fields">
                          <div class="ui checkbox">
                              <input type="checkbox" name="coberturaVisionIncremental" id="coberturaVisionIncremental" tabindex="0" data-value="coberturaVisionIncremental" class="checkdata">
                              <label>
                                  Cobertura de Visión Incremental
                              </label>
                          </div>
                      </div>
                </div>
                <div class="two fields">
                        <div class="fields">
                            <div class="field">
                                <div class="ui checkbox">
                                    <input type="checkbox" name="coberturaIndemnizacionEnfermedadGrave" id="coberturaIndemnizacionEnfermedadGrave" tabindex="0" data-value="enfermedadGraveChk" class="checkdata">
                                    <label>
                                        Cobertura de Indemnización por Enfermedad Grave
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="ui segment hidden-segment-value" id="graveMonto-div">
                          <div id ="graveMontoDropdown" class="ui selection dropdown">
                              <input type="hidden" name="graveMonto" id="graveMonto">
                              <i class="dropdown icon"></i>
                              <div class="default text">Seleccione Monto</div>
                              <div class="menu">
                                  <?php foreach ($array_enfermedadesGravesMontos as $key => $value): ?>
                                      <div class="item" data-value="<?php echo $key; ?>">
                                          <?php echo $value; ?>
                                      </div>
                                  <?php endforeach; ?>
                              </div>
                          </div>
                        </div>
                </div>
                <div class="inline fields">
                      <div class="fields">
                          <div class="ui checkbox">
                              <input type="checkbox" name="emergenciaExtranjero" id="emergenciaExtranjero" tabindex="0" data-value="emergenciaExtranjero" class="checkdata">
                              <label>
                                  Emergencia en el Extranjero
                              </label>
                          </div>
                      </div>
                </div>
                <div class="inline fields">
                      <div class="fields">
                          <div class="ui checkbox">
                              <input type="checkbox" name="emergenciaTotalExtranjero" id="emergenciaTotalExtranjero" tabindex="0" data-value="emergenciaTotalExtranjero" class="checkdata">
                              <label>
                                  Cobertura Total en el Extranjero
                              </label>
                          </div>
                      </div>
                </div>
                <!--Errors for integrantes table-->
                <div class="hidden-segment-value" id="errorsIntegrantesDiv"></div>
                <div class="fields">
                    <table class="ui striped celled table segment" id="integrantes_table">
                        <thead>
                            <tr>
                                <th class="center aligned">Integrante</th>
                                <th class="center aligned">Nombre</th>
                                <th class="center aligned">Sexo</th>
                                <th class="center aligned">Edad</th>
                                <th class="center aligned"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div id="newIntegranteDropdown" class="ui fluid selection dropdown">
                                        <input name="titulo_integrante" id="titulo_integrante" type="hidden">
                                        <i class="dropdown icon"></i>
                                        <div class="default text">Seleccione</div>
                                        <div class="menu">
                                            <?php foreach ($array_integrantes as $key => $value): ?>
                                                <div class="item" data-value="<?php echo $key; ?>">
                                                    <?php echo $value; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" id="newNombre" placeholder="Nombre">
                                </td>
                                <td>
                                    <div id="newSexoDropdown" class="ui fluid selection dropdown">
                                        <input name="sexo_integrante" id="sexo_integrante" type="hidden">
                                        <i class="dropdown icon"></i>
                                        <div class="default text">Sexo&nbsp;&nbsp;&nbsp;</div>
                                        <div class="menu">
                                            <?php foreach ($array_sexo as $key => $value): ?>
                                                <div class="item" data-value="<?php echo $key; ?>">
                                                    <?php echo $value; ?>
                                                </div>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" id="newEdad" placeholder="Edad" maxlength="2" class="form-data-selected edad">
                                </td>
                                <td>
                                    <font class="ui fluid primary button" id="addRowIntegrantesTable">
                                        +
                                    </font>
                                    <!--Save the total of rows addedd of integrantes-->
                                    <input id="integrantesRowsHidden" type="hidden">
                                    <input id="existeTitularHidden" type="hidden">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!--Errors for form page-->
                <div class="ui error message"></div>
                <div class="row">
                    <button class="ui right floated positive button submit-button">
                        Cotizar
                    </button>
                </div>
                <div class="row">&nbsp;</div>
                <div class="row">&nbsp;</div>
            </div>
        </div>
        <!-- FINAL FIRST SECTION -->
        <!-- SECOND SECTION -->
        <div id="accordion-2"  class="disabled-content"><!-- disabled or enabled accordion -->
            <div class="title" id="title-2">
                <i class="dropdown icon"></i>
                Preview / Envío
            </div>
            <div class="content" id="content-2">
                <div class="row margin-top-buttons">
                    <div class="ui buttons right floated">
                        <button class="ui right floated negative button backButton-2">
                            Regresar
                        </button>
                        <div class="or" data-text="o"></div>
                        <button class="ui right floated positive button submit-button-2">
                            Confirmar y Enviar
                        </button>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <!-- HEADER OF THE PREVIEW -->
                    <div class="column">
                        <div class="ui segment">
                            <div class="field bold_text_title">
                                DATOS GENERALES
                            </div>
                            <div class="content active" id="content-1">
                                <div class="field">
                                    <label>
                                        Nombre Completo
                                    </label>
                                    <label id="fullname_preview" class="light_text_desc"/>
                                </div>
                              <div class="two fields">
                                  <div class="field">
                                      <label>
                                          Correo Electrónico
                                      </label>
                                      <label id="email_preview" class="light_text_desc"/>
                                  </div>
                                  <div class="field">
                                      <label>
                                          Teléfono
                                      </label>
                                      <label id="telefono_preview" class="light_text_desc"/>
                                  </div>
                                </div>
                                <div class="two fields">
                                    <div class="field">
                                        <label>
                                            Estado
                                        </label>
                                        <label id="estado_preview" class="light_text_desc"/>
                                    </div>
                                    <div class="field">
                                        <label>
                                            Ciudad
                                        </label>
                                        <label id="ciudad_preview" class="light_text_desc"/>
                                    </div>
                                </div>
                                <div>
                                  <h3 class="ui header">Datos de su Póliza de Gastos Médicos Mayores</h3>
                                </div>
                            <div class="row">&nbsp;</div>
                            <div class="two fields">
                              <div class="field">
                                <label>
                                    Tipo Cotización
                                </label>
                                <label id="tipoCotizacionDropdown_preview" class="light_text_desc"/>
                              </div>
                                <div class="field">
                                  <label>
                                      Tabulador de Honorarios Médicos
                                  </label>
                                  <label id="tabuladorHonorariosMedicosDropdown_preview" class="light_text_desc"/>
                                </div>
                            </div>
                            <div class="two fields">
                              <div class="field">
                                  <label>
                                      Nivel Hospitalario
                                  </label>
                                  <label id="nivelHospitalarioDropdown_preview" class="light_text_desc"/>
                              </div>
                              <div class="field">
                                  <label>
                                      Suma Asegurada
                                  </label>
                                  <label id="sumaAsegurada_preview" class="light_text_desc"/>
                              </div>
                            </div>
                          <div class="two fields">
                              <div class="field">
                                  <label>
                                      Coaseguro
                                  </label>
                                  <label id="coaseguro_preview" class="light_text_desc"/>
                              </div>
                              <div class="field">
                                  <label>
                                      Deducible
                                  </label>
                                  <label id="deducible_preview" class="light_text_desc"/>
                              </div>
                          </div>
                          <div class="field">
                            <label>
                                Zona
                            </label>
                            <label id="zona_preview" class="light_text_desc"/>
                          </div>
                          <div class="inline fields">
                            <div class="fields">
                                <div class="field">
                                  <div class="ui checkbox">
                                    <input type="checkbox" id="reduccionDeducibleAccidente_preview" disabled="true">
                                    <label>
                                        Reduccion de Deducible por Accidente
                                    </label>
                                  </div>
                                </div>
                            </div>
                          </div>
                          <div class="two fields">
                              <div class="fields">
                                  <div class="field">
                                      <div class="ui checkbox">
                                          <input type="checkbox" id="incrementoPartoCesarea_preview" disabled="true">
                                          <label>
                                              Incremento en Parto o Cesárea
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="fields">
                                  <div class="field">
                                      <label>
                                          Monto
                                      </label>
                                  </div>
                                  <div class="field">
                                      <label id="cesareaMonto_preview" class="light_text_desc"/>
                                  </div>
                              </div>
                          </div>
                          <div class="inline fields">
                            <div class="fields">
                                <div class="field">
                                  <div class="ui checkbox">
                                    <input type="checkbox" id="coberturaVisionIncremental_preview" disabled="true">
                                    <label>
                                        Cobertura de Visión Incremental
                                    </label>
                                  </div>
                                </div>
                            </div>
                          </div>
                          <div class="two fields">
                              <div class="fields">
                                  <div class="field">
                                      <div class="ui checkbox">
                                          <input type="checkbox" id="coberturaIndemnizacionEnfermedadGrave_preview" disabled="true">
                                          <label>
                                              Cobertura de Indemnización por Enfermedad Grave
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="fields">
                                  <div class="field">
                                      <label>
                                          Monto
                                      </label>
                                  </div>
                                  <div class="field">
                                      <label id="graveMonto_preview" class="light_text_desc"/>
                                  </div>
                              </div>
                          </div>
                          <div class="inline fields">
                            <div class="fields">
                                <div class="field">
                                  <div class="ui checkbox">
                                    <input type="checkbox" id="emergenciaExtranjero_preview" disabled="true">
                                    <label>
                                        Emergencia en el Extranjero
                                    </label>
                                  </div>
                                </div>
                            </div>
                          </div>
                          <div class="inline fields">
                            <div class="fields">
                                <div class="field">
                                  <div class="ui checkbox">
                                    <input type="checkbox" id="emergenciaTotalExtranjero_preview" disabled="true">
                                    <label>
                                        Emergencia Total en el Extranjero
                                    </label>
                                  </div>
                                </div>
                            </div>
                          </div>
                            <div class="field bold_text_title">
                                INTEGRANTES CAPTURADOS
                            </div>
                            <div class="fields">
                                <table class="ui striped celled table segment" id="integrantes_table_preview">
                                    <thead>
                                        <tr>
                                            <th class="center aligned">Integrante</th>
                                            <th class="center aligned">Nombre</th>
                                            <th class="center aligned">Sexo</th>
                                            <th class="center aligned">Edad</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                      <!--This seccion is loaded automatically from js function fillIntegrantesTablePreview()-->
                                    </tbody>
                                </table>
                            </div>
                            <div class="ui buttons right floated">
                                <button class="ui right floated negative button backButton-2">
                                    Regresar
                                </button>
                                <div class="or" data-text="o"></div>
                                <button class="ui right floated positive button submit-button-2">
                                    Confirmar y Enviar
                                </button>
                            </div>
                            <div>&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- FINAL SECOND SECTION -->
    </div>
</form>
</div>
