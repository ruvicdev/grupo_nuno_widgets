<?php

/**
 * Model will be used to get all
 * the information is going to be
 * stored in the database to then could
 * get the data and displaying it to the user
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class HistorialSeguroResponsabilidad_model extends CI_Model {

	/**
	 * Constructor ......
	 */
	public function __construct() {
		parent::__construct();

		$this->table = 'historial_seguro_responsabilidad';
	}

	/**
	 * Method will be used to save
	 * the information sent by the system once the
	 * user has ended up the process of all the form
	 * and this data will be used to create the PDF file
	 *
	 * @param array $data
	 * @return int $id
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function saveData($data) {
		$this->db->insert($this->table, $data);

		return $this->db->insert_id();
	}
}
