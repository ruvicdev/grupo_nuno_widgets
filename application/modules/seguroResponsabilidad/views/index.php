<script type="text/x-handlebars-template" id="second_dropdown">
    {{#each data}}
        <div class="item" data-value="{{value}}-{{index}}-{{old}}">
            {{value}}
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="errors_messages">
    <div class="row">
        <div class="ui negative hidden message">
            <i class="close icon"></i>
            <div class="header">
                Error
            </div>
            <p>
                Por favor, llene los campos requeridos
            </p>
            <p id="messageErrorMoney">
                {{data}}
            </p>
        </div>
    </div>
</script>

<div class="default-margin-top ui grid centered removePadding">
    <!-- SECTION FOR STEPS -->
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos Personales</div>
                <div class="description">Informacion Personal</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Cotizacion</div>
                <div class="description">Cotizacion - Seguro de Responsabilidad</div>
            </div>
        </div>
    </div>
</div>

<div class="ui centered grid">
    <!-- SECTION FOR FORMS -->
    <div class="row">
        <h1 class="ui header">
            Seguro de Responsabilidad por Administracion para Ejecutivos y la Empresa
        </h1>
    </div>
    <div>&nbsp;</div>
    <form class="ui form">
        <div class="ui styled accordion">
            <!-- FIRST SECTION -->
            <div id="accordion-1" class="disabled-content1">
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos del Asegurado
                </div>
                <div class="content active" id="content-1">
                    <div class="row" id="error_first_section"></div>
                    <div>&nbsp;</div>
                    <div class="row">
                        <label class="left_align mini_messages_stars bold_text_title"
                               style="font-size: 11px; line-height: 11px">
                            NOTA: Este formulario y producto s&oacute;lo aplica para empresas privadas,
                                  con ingresos anuales inferiores a $ 50,000,000 USD,
                                  reportados a diciembre 31 del a&ntilde;o inmediatamente anterior.
                        </label>
                    </div>
                    <div>&nbsp;</div>
                    <div class="fields">
                        <div class="seven wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" id="vendedor"
                                       class="form-data-selected obj-send">
                                <label class="bold_text_title">
                                    ¿Eres vendedor de Grupo Nu&ntilde;o?
                                </label>
                            </div>
                        </div>
                        <div class="nine wide field">
                            <input type="text" name="nombre_vendedor" id="name_seller"
                                   class="" placeholder="Nombre del Vendedor">
                        </div>
                    </div>
                    <div class="fields">
                        <input type="hidden" id="tipo_persona">
                        <div class="five wide field">
                            <label class="left_align bold_text_title" for="tipo_persona">
                                Tipo de Persona:
                            </label>
                        </div>
                        <div class="five wide field required sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="fisica"
                                       data-class="hidden-segment-value"
                                       data-index="1"
                                       class="checkdata required tperson">
                                <label>
                                    Persona Fisica
                                </label>
                            </div>
                        </div>
                        <div class="five wide field required sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="moral"
                                       data-class="hidden-segment-value"
                                       data-index="2"
                                       class="checkdata required tperson">
                                <label>
                                    Persona Moral
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="ui segment hidden-segment-value" data-value="moral" id="moral">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Empresa:
                            </label>
                            <input type="text" name="empresa" id="empresa" class="required" placeholder="Empresa">
                        </div>
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre del Contacto:
                            </label>
                            <input type="text" name="nombre_contacto" id="nombre_contacto" class="" placeholder="Nombre del Contacto">
                        </div>
                    </div>
                    <div class="ui segment hidden-segment-value" data-value="fisica" id="fisica">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre(s):
                            </label>
                            <input type="text" name="nombres" id="nombres" class="required" placeholder="Nombre(s)">
                        </div>
                        <div class="fields">
                            <div class="eight wide field">
                                <label class="left_align bold_text_title">
                                    Apellido Paterno:
                                </label>
                                <input type="text" name="a_paterno" id="a_paterno" class="required" placeholder="Apellido Paterno">
                            </div>
                            <div class="eight wide field">
                                <label class="left_align bold_text_title">
                                    Apellido Materno:
                                </label>
                                <input type="text" name="a_materno" id="a_materno" class="required" placeholder="Apellido Materno">
                            </div>
                        </div>
                    </div>
                    <!--div class="field">
                        <label class="left_align bold_text_title">
                            Nombre completo de la empresa solicitante:
                        </label>
                        <input type="text" name="nombre" class="required" id="nombre"
                               placeholder="Nombre completo de la empresa solicitante">
                    </div -->
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Direccion principal de la empresa solicitante:
                        </label>
                        <input type="text" name="direccion" class="required" id="direccion"
                               placeholder="Direccion principal de la empresa solicitante">
                    </div>
                    <!-- div class="field">
                        <label class="left_align bold_text_title">
                            Email:
                        </label>
                        <input type="text" name="email" class="required" id="email"
                               placeholder="Email">
                    </div -->
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Telefono:
                        </label>
                        <input type="text" name="tel" id="tel" class="required"
                               placeholder="Telefono">
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Email:
                            </label>
                            <input type="email" name="email" class="required" id="email"
                                   placeholder="Email">
                            <!--label class="left_align bold_text_title">
                                Fecha de creaci&oacute;n:
                            </label>
                            <input type="text" name="fecha" id="fecha"
                                   placeholder="Fecha de creacion" class="required" -->
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                N&uacute;mero de Empleados:
                            </label>
                            <input type="text" name="no_empleados" id="no_empleados" class="required"
                                   placeholder="Numero de Empleados">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Facturaci&oacute;n: <span style="color: #FF0000">(Cifras en USD)</span>
                        </label>
                        <div class="ui selection dropdown" data-url="getLimitCompensation" id="first_data_dropdown">
                            <input type="hidden" name="facturacion" class="required" value="0" id="facturacion">
                            <i class="dropdown icon"></i>
                            <div class="default text">Facturaci&oacute;n</div>
                            <div class="menu">
                                <?php foreach($limites as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $value . '-' . $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="field deshabilitar_campo_main">
                        <label class="left_align bold_text_title">
                            Limite de Indemnizaci&oacute;n: <span style="color: #FF0000">(Cifras en USD)</span>
                        </label>
                        <div class="ui selection dropdown" data-url="getFinalRecords" id="second_data_dropdown">
                            <input type="hidden" name="indemnizacion" id="indemnizacion" class="required" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text">Limite de Indemnizaci&oacute;n (Cifras en USD)</div>
                            <div class="menu limite_menu">
                            </div>
                        </div>
                    </div>
                    <div class="fields deshabilitar_campo">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Sublimite de Responsabilidad Laboral:
                                <br><span style="color: #FF0000">(Cifras en USD)</span>
                            </label>
                            <label id="sublimite_label" class="light_text_title"></label>
                            <input type="hidden" name="sublimite" id="sublimite">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Deducible:
                                <br><span style="color: #FF0000">(Cifras en USD)</span>
                            </label>
                            <label class="light_text_title" id="deducible_label"></label>
                            <input type="hidden" name="deducible" id="deducible">
                        </div>
                    </div>
                    <div class="fields deshabilitar_campo">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Deducible Responsabilidad Laboral:
                                <br><span style="color: #FF0000">(Cifras en USD)</span>
                            </label>
                            <label class="light_text_title" id="laboral_label"></label>
                            <input type="hidden" name="laboral" id="laboral">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Prima Neta Anual:
                                <br><span style="color: #FF0000">(Cifras en USD)</span>
                            </label>
                            <label class="light_text_title" id="prima_label"></label>
                            <input type="hidden" name="prima" id="prima">
                        </div>
                    </div>
                    <div class="row">
                        <button class="ui right floated positive button submit-button">
                            Siguiente
                        </button>
                    </div>
                    <div>&nbsp;</div>
                </div>
            </div>
            <div id="accordion-2" class="disabled-content1">
                <div class="title" id="title-2">
                    <i class="dropdown icon"></i>
                    Preview / Cotizaci&oacute;n
                </div>
                <div class="content" id="content-2">
                    <div class="row margin-top-buttons">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton">
                                Confirmar y Enviar
                            </button>
                        </div>
                        <div class="clear-floats">&nbsp;</div>
                        <!-- HEADER OF THE PREVIEW -->
                        <div class="column"><!-- SECTION GENERAL INFORMATION -->
                            <div class="ui segment">
                                <div class="field bold_text_title" style="float: left;">
                                    Vendedor Grupo Nu&ntilde;o
                                </div>
                                <div style="clear: both;"></div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Eres Vendedor:</label>
                                    <label class="light_text_desc" id="eres_vendedor"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Nombre del Vendedor:</label>
                                    <label class="light_text_desc" id="nombre_vendedor"></label>
                                </div>
                            </div>
                        </div>
                        <div class="column margin-top-segment">
                            <div class="ui segment">
                                <div class="field bold_text_title" style="float: left;">
                                    DATOS GENERALES
                                </div>
                                <div style="clear: both"></div>
                                <!--div class="field left_align inline">
                                    <label class="bold_text_title">Nombre de la Empresa:</label>
                                    <label class="light_text_desc" id="nombre_empresa_txt"></label>
                                </div-->
                                <div class="field left_align inline fisica_temp">
                                    <label class="bold_text_title">Nombre:</label>
                                    <label class="light_text_desc" id="nombre_txt"></label>
                                </div>
                                <div class="field left_align inline fisica_temp">
                                    <label class="bold_text_title">Apellido Paterno:</label>
                                    <label class="light_text_desc" id="ap_txt"></label>
                                </div>
                                <div class="field left_align inline fisica_temp">
                                    <label class="bold_text_title">Apellido Materno:</label>
                                    <label class="light_text_desc" id="am_txt"></label>
                                </div>
                                <div class="field left_align inline moral_temp">
                                    <label class="bold_text_title">Nombre de la Empresa:</label>
                                    <label class="light_text_desc" id="empresa_txt"></label>
                                </div>
                                <div class="field left_align inline moral_temp">
                                    <label class="bold_text_title">Nombre del Contacto:</label>
                                    <label class="light_text_desc" id="nombre_contacto_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Direccion de la Empresa:</label>
                                    <label class="light_text_desc" id="direccion_empresa_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Email:</label>
                                    <label class="light_text_desc" id="email_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Telefono:</label>
                                    <label class="light_text_desc" id="tel_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <!-- div class="eight wide field">
                                        <label class="bold_text_title">Fecha de Creaci&oacute;n:</label>
                                        <label class="light_text_desc" id="fecha_txt"></label>
                                    </div>
                                    <div class="eight wide field" -->
                                    <label class="bold_text_title">Numero de Empleados</label>
                                    <label class="light_text_desc" id="numero_empleados_txt"></label>
                                    <!--/div-->
                                </div>
                            </div>
                        </div><!-- SECTION GENERAL INFORMATION -->
                        <div class="column margin-top-segment"><!-- SECTION PAYMENTS INFORMATION -->
                            <div class="ui segment">
                                <div class="field bold_text_title" style="float: left;">
                                    DATOS DEL SEGURO
                                </div>
                                <div style="clear: both;"></div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Facturaci&oacute;n:</label>
                                    <label class="light_text_desc" id="facturacion_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Limite de Indemnizaci&oacute;n:</label>
                                    <label class="light_text_desc" id="limite_txt"></label> USD
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Sublimite de Responsabilida Laboral:</label>
                                    <label class="light_text_desc" id="sublimite_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Deducible Responsabilidad Laboral:</label>
                                    <label class="light_text_desc" id="deducible_txt"></label>
                                </div>
                                <div class="two fields left_align inline">
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Deducibles:</label>
                                        <label class="light_text_desc" id="dedicible_cantidad_txt"></label>
                                    </div>
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Prima Neta Anual:</label>
                                        <label class="light_text_desc" id="prima_neta_txt"></label>
                                    </div>
                                </div>
                            </div>
                        </div><!-- SECTION PAYMENTS INFORMATION -->
                        <div class="column margin-top-segment">
                            <!-- here I'm going to put all the information of clauses -->
                        </div>
                        <div class="row margin-top-buttons">
                            <div class="ui buttons right floated">
                                <button class="ui right floated negative button backButton">
                                    Regresar
                                </button>
                                <div class="or" data-text="o"></div>
                                <button class="ui right floated positive button nextButton">
                                    Confirmar y Enviar
                                </button>
                            </div>
                        </div>
                        <div class="clear-floats">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/mathElements.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo base_url() . 'public/js/custom/seguroResponsabilidad.js'; ?>"></script>
