<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Class contains the methods will be used
 * to display and save all the information
 * for the responsability insurance of the
 * user that wants to purchase this
 * kind of insurance
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */

class SeguroResponsabilidad extends MY_Controller {

	/**
	 * Construct .....
	 */
	public function __construct() {
		parent::__construct();

		$this->load->model(array('HistorialSeguroResponsabilidad_model'));

        $this->pdf   	 = new ExportFiles();
        $this->emailSend = new EmailSend();
        $this->template  = new Templates();
	}

	/**
	 * Method will contains all the data
	 * is going to display the form the user
	 * should fill to know what will be the
	 * price for purchase the insurance
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function index() {
		$limites = array('limites' => $this->__profitCompany());

		$content = $this->loadSingleView('index', $limites, TRUE);
		$data	 = $this->__setArray('content', $content);
		$data	 = $this->__setArray('title', 'Seguro de Responsabilidad por Administracion para Ejecutivos y la Empresa');

		$this->loadTemplate($data);
	}

	/**
	 * Method used to get all the information from the request
	 * to then create the PDF file, send the mail and also
	 * save the data in the database that will be done
	 * once the user click the confirm button
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function save() {
		$json_array = json_decode(file_get_contents('php://input'), TRUE);
		$data 		= $this->__createArray($json_array);
		$id 		= $this->HistorialSeguroResponsabilidad_model->saveData($data);

		$template 	= $this->template->seguroResponsabilidadTemplate($data, $id);
		$PDF 		= $this->pdf->__initPDFInsuranceResponsability($template, "Seguro de Responsabilidad",
																			  "Seguro de Responsabilidad");
		$name 		="seguroResponsabilidad_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

		// Create a file
		$PDF->Output(FCPATH . 'public/files/' . $name, 'F');
		$completePath = FCPATH . 'public/files/' . $name;
		$returnedPath = base_url() . 'public/files/' . $name;

		$this->emailSend->__initEmailResponsability($data, $PDF, $completePath, "Seguro de Responsabilidad");

		echo json_encode(array(
				'flag' 	   => 1,
				'pathFile' => $returnedPath
			));
	}

	/**
	 * Method will be used for create the
	 * array is going to be used to insert
	 * data in the database and then use that
	 * data to create the PDF file
	 *
	 * @param array $data
	 * @return array $array
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __createArray($data) {
		$array 	  	= array();
		$vendedor 	= "";
		$nombreUser = "";
		$apUser		= "";
		$amUser		= "";
		$nombreE 	= "";
		$contactoE	= "";

		if ($data['vendedor'] == 'Si') {
			$vendedor = $data['name_seller'];
		}

		if ($data['tipo_persona'] == 0 || $data['tipo_persona'] == '0') {
			$nombreUser = $data['nombres'];
			$apUser 	= $data['a_paterno'];
			$amUser 	= $data['a_materno'];
		} else {
			$nombreE   = $data['empresa'];
			$contactoE = $data['nombre_contacto'];
		}

		$division  = explode("-", $data["facturacion"]);
		$division2 = explode("-", $data["indemnizacion"]);

		$array = array(
			'empresa' 					=> $data['empresa'],
			'tipo_persona'				=> $data['tipo_persona'],
			'nombre_user'				=> $nombreUser,
			'ap_user'					=> $apUser,
			'am_user'					=> $amUser,
			'empresa_user'				=> $nombreE,				
			'contacto_user'				=> $contactoE,
			'direccion' 				=> $data['direccion'],
			'email'						=> $data['email'],
			'telefono'					=> $data['tel'],
			'fecha_cotizacion' 			=> date('d-m-Y H:i:s'), //$data['fecha'],
			'numero_empleados' 			=> $data['no_empleados'],
			'facturacion' 				=> $division[0],
			'indemnizacion' 			=> $division2[0],
			'sublimite_responsabilidad' => $data['sublimite'],
			'deducible' 				=> $data['deducible'],
			'deducible_responsabilidad' => $data['laboral'],
			'prima_neta_anual' 			=> $data['prima'],
			'es_vendedor' 				=> $data['vendedor'],
			'nombre_vendedor' 			=> $vendedor,
			'id_facturacion'			=> $division2[2],
			'id_seccion_facturacion'	=> $division2[1],
			'fecha_creacion' 			=> date('d-m-Y H:i:s')
		);

		return $array;
	}

	/**
	 * Method used to set all the information about the
	 * total company has gained during the past year
	 * and depending on that, the user is going to
	 * select any record
	 *
	 * @return array $array
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __profitCompany() {
		$array = array(
			'1' => 'Hasta $5M USD',
			'2' => 'Desde $5M USD hasta $15M USD',
			'3' => 'Desde $15M USD hasta $25M USD',
			'4' => 'Desde $25M USD hasta $50M USD'
		);

		return $array;
	}

	/**
	 * Method will be used to get the records will be loaded
	 * depending on the option selected by the user talking
	 * about the limits of all the profits
	 *
	 * @param int $id
	 * @return response $jsonResponse
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function getLimitCompensation($id) {
		$arrayData = array();

		if ($id == 1) {
			$array = array(
				array('index' => '1', 'value' => '$ 100,000', 'old' => $id),
				array('index' => '2', 'value' => '$ 250,000', 'old' => $id),
				array('index' => '3', 'value' => '$ 500,000', 'old' => $id),
				array('index' => '4', 'value' => '$ 1,000,000', 'old' => $id),
				array('index' => '5', 'value' => '$ 2,000,000', 'old' => $id)
			);
		} else if($id == 2) {
			$array = array(
				array('index' => '1', 'value' => '$ 250,000', 'old' => $id),
				array('index' => '2', 'value' => '$ 500,000', 'old' => $id),
				array('index' => '3', 'value' => '$ 1,000,000', 'old' => $id),
				array('index' => '4', 'value' => '$ 2,000,000', 'old' => $id)
			);
		} else if ($id == 3) {
			$array = array(
				array('index' => '1', 'value' => '$ 250,000', 'old' => $id),
				array('index' => '2', 'value' => '$ 500,000', 'old' => $id),
				array('index' => '3', 'value' => '$ 1,000,000', 'old' => $id),
				array('index' => '4', 'value' => '$ 2,000,000', 'old' => $id),
				array('index' => '5', 'value' => '$ 3,000,000', 'old' => $id),
				array('index' => '6', 'value' => '$ 5,000,000', 'old' => $id)
			);
		} else if ($id == 4) {
			$array = array(
				array('index' => '1', 'value' => '$ 250,000', 'old' => $id),
				array('index' => '2', 'value' => '$ 500,000', 'old' => $id),
				array('index' => '3', 'value' => '$ 1,000,000', 'old' => $id),
				array('index' => '4', 'value' => '$ 2,000,000', 'old' => $id),
				array('index' => '5', 'value' => '$ 3,000,000', 'old' => $id),
				array('index' => '6', 'value' => '$ 5,000,000', 'old' => $id)
			);
		}

		echo json_encode(array('flag' 	 => '1',
							   'options' => $array));
	}

	/**
	 * Method that contains the information missing to
	 * finish the cotization will be done by the
	 * user once he selects the options displayed on the
	 * screen
	 *
	 * @param int $id - old id
	 * @param int $id2 - new section id
	 *
	 * @return json
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function getFinalRecords($id, $id2) {
		$data = array();

		if ($id == 1) {
			$data = $this->__firstSection($id2);
		} else if ($id == 2) {
			$data = $this->__secondSection($id2);
		} else if ($id == 3) {
			$data = $this->__thirdSection($id2);
		} else if ($id == 4) {
			$data = $this->__fourSection($id2);
		}

		echo json_encode(array('flag' => 1,
							   'id'   => $id,
							   'id2'  => $id2,
							   'info' => $data));
	}

	/**
	 * Method will return the information needed to display it
	 * on the view of the user and can see the information
	 * regarding to the data selected
	 *
	 * @param int $id
	 * @return void
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __firstSection($id) {
		$array = array();

		if ($id == 1) {
			$array = array(
				'sublimite' => '$ 100,000',
				'deducible' => '$ 2,000',
				'laboral'	=> '$ 5,000',
				'prima'		=> '$ 600'
			);
		} else if ($id == 2) {
			$array = array(
				'sublimite' => '$ 250,000',
				'deducible' => '$ 2,000',
				'laboral'	=> '$ 5,000',
				'prima'		=> '$ 850'
			);
		} else if ($id == 3) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 2,000',
				'laboral'	=> '$ 5,000',
				'prima'		=> '$ 1,125'
			);
		} else if ($id == 4) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 2,000',
				'laboral'	=> '$ 5,000',
				'prima'		=> '$ 1,950'
			);
		} else {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 2,000',
				'laboral'	=> '$ 5,000',
				'prima'		=> '$ 2,625'
			);
		}

		return $array;
	}

	/**
	 * Method will return the information needed to display it
	 * on the view of the user and can see the information
	 * regarding to the data selected
	 *
	 * @param int $id
	 * @return void
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __secondSection($id) {
		$array = array();

		if ($id == 1) {
			$array = array(
				'sublimite' => '$ 250,000',
				'deducible' => '$ 5,000',
				'laboral'	=> '$ 10,000',
				'prima'		=> '$ 1,000'
			);
		} else if ($id == 2) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 5,000',
				'laboral'	=> '$ 10,000',
				'prima'		=> '$ 1,400'
			);
		} else if ($id == 3) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 5,000',
				'laboral'	=> '$ 10,000',
				'prima'		=> '$ 1,875'
			);
		} else {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 5,000',
				'laboral'	=> '$ 10,000',
				'prima'		=> '$ 2,895'
			);
		}

		return $array;
	}

	/**
	 * Method will return the information needed to display it
	 * on the view of the user and can see the information
	 * regarding to the data selected
	 *
	 * @param int $id
	 * @return void
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __thirdSection($id) {
		$array = array();

		if ($id == 1) {
			$array = array(
				'sublimite' => '$ 250,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 1,200'
			);
		} else if ($id == 2) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 1,650'
			);
		} else if ($id == 3) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 2,250'
			);
		} else if ($id == 4) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 2,925'
			);
		} else if($id == 5) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 3,650'
			);
		} else {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 5,650'
			);
		}

		return $array;
	}

	/**
	 * Method will return the information needed to display it
	 * on the view of the user and can see the information
	 * regarding to the data selected
	 *
	 * @param int $id
	 * @return void
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __fourSection($id) {
		$array = array();

		if ($id == 1) {
			$array = array(
				'sublimite' => '$ 250,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 1,450'
			);
		} else if ($id == 2) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 1,850'
			);
		} else if ($id == 3) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 2,550'
			);
		} else if ($id == 4) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 3,325'
			);
		} else if($id == 5) {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 4,150'
			);
		} else {
			$array = array(
				'sublimite' => '$ 500,000',
				'deducible' => '$ 10,000',
				'laboral'	=> '$ 15,000',
				'prima'		=> '$ 6,950'
			);
		}

		return $array;
	}

	public function demo() {
		$temp = $this->template->seguroResponsabilidadTemplate();

		$p = $this->pdf->__initPDFInsuranceResponsability($temp, "Seguro de Responsabilidad1", "Seguro de Responsabilidad");
		//var_dump($p);
		$p->Output("demo.pdf", 'I');
	}
}
