<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Class that contains all the templates and everything
 * related with the emails and related to send data
 * once the process of the forms has finished
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */

Class EmailSend extends MY_Controller {

    /**
     * Contructor ....
     */
    public function __construct() {
        parent::__construct();

        $this->load->library('email');

        // initialize the email settings
        $config['charset']  = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['mailtype'] = 'html';

        $this->email->initialize($config);
    }

    public function emails() {
        $this->email->from('noReply@ruvicdev.com');
        $this->email->to('ruben.alonso21@gmail.com');
        //$this->email->bcc('email');

        $this->email->subject("holas");
        $this->email->message("<div>holas</div><br><div>que pedo putos</div>");

        $this->email->send();
    }

    /**
     * Method will be used to create all the configuration
     * to send the email to the different emails (client
     * and the company)
     *
     * @param array $array
     * @param object $pdf
     * @param string $pdfFile
     * @param string $subject
     *
     * @return bool true
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmail($array, $pdf, $pdfFile, $subject) {

        $templateBody  = $this->__getTextTemplate($array, $subject);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('noReply@gruponuno.com');
        //$this->email->from('noReply@ruvicdev.com');
        $this->email->to($array['email']);
        $this->email->bcc('infocarga@gruponuno.com');

        $this->email->subject($subject);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    /**
     * Method will be used to create all the configuration
     * to send the email to the different emails (client
     * and the company)
     *
     * @param array $array
     * @param object $pdf
     * @param string $pdfFile
     * @param string $subject
     *
     * @return bool true
     *
     * @author Victor Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmailMinor($array, $pdf, $pdfFile, $subject) {

        $templateBody  = $this->__getTextTemplateMinor($array, $subject);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('noReply@gruponuno.com');
        //$this->email->from('noReply@ruvicdev.com');
        $this->email->to($array['email']);
        $this->email->bcc('infogmm@gruponuno.com');

        $this->email->subject($subject);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    /**
     * Method will be used to create all the configuration
     * to send the email to the different emails (client
     * and the company)
     *
     * @param array $array
     * @param object $pdf
     * @param string $pdfFile
     * @param string $subject
     *
     * @return bool true
     *
     * @author Victor Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmailCubreTuDeducible($array, $pdf, $pdfFile, $widget) {

        $templateBody  = $this->__getTextTemplateCubreTuDeducible($array, $widget);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('infomydeducible@gruponuno.com');
        $this->email->to($array['email']);
        $this->email->bcc('infomydeducible@gruponuno.com');

        $this->email->subject($widget);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    /**
     * Method will be used to create all the configuration
     * to send the email to the differents users involved in this
     * purchase (client and company)
     *
     * @param array $array
     * @param object $pdf
     * @param string $pdfFile
     * @param string $subject
     *
     * @return bool true
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmailPets($array, $pdf, $pdfFile, $subject) {
        $templateBody  = $this->__getTextTemplatePets($array, $subject);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('infomascota@gruponuno.com');
        $this->email->to($array['email']);
        $this->email->bcc('infomascota@gruponuno.com');

        $this->email->subject($subject);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    /**
     * Method will be used to send the email to the client
     * with all the information filled in before in the form.
     * This method contains all the configuration to send the
     * information to the client
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmailResponsability($array, $pdf, $pdfFile, $subject) {
        $templateBody  = $this->__getTextTemplateResponsabilityInsurance($array, $subject);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('infodo@gruponuno.com');
        $this->email->to($array['email']);
        $this->email->bcc('infodo@gruponuno.com');

        $this->email->subject($subject);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    /**
     * Method will be used to send the email to the client
     * with all the information filled in before in the form.
     * This method contains all the configuration to send
     * the information to the client
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmailDataProtection($array, $pdf, $pdfFile, $subject) {
        $templateBody  = $this->__getTextTemplateDataProtection($array, $subject);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('ruben.alonso21@gmail.com');
        $this->email->to($array['email']);
        //$this->email->bcc('infodo@gruponuno.com');

        $this->email->subject($subject);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    /**
     * Method will be used to send all the information
     * filled in by the user via email but with a PDF
     * file generated before by the system. The Email contains the
     * file and also the main information of the user
     *
     * @param array $array
     * @param object $pdf
     * @param sring $pdfFile
     * @param string $subject
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __initEmailFraudulentActs($array, $pdf, $pdfFile, $subject) {
        $templateBody  = $this->__getTextTemplateFraudulentActs($array, $subject);
        $finalTemplate = $this->__setBodyEmail($templateBody);

        $this->email->from('ruben.cortes@ruvicdev.com');
        $this->email->to($array['email']);

        $this->email->subject($subject);
        $this->email->message($finalTemplate);

        $this->email->attach($pdfFile);

        $this->email->send();
    }

    public static function demos($pdf) {
        echo "holas";
        //$this->__initEmail(array(), '');
        //$pdf->Output('example_006.pdf', 'I');
    }

    /**
     * Create the template used for set the values in the headers
     * of the template will send the data to the user and the
     * company
     *
     * @return string $html
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __templateHeader() {
        $html = "<html>" .
                    "<header>" .
                    "</header>" .
                    "<body>";

        return $html;
    }

    /**
     * Create the template to create the footer
     * will be used during the creation of the
     * email will be sent to the user and the company
     *
     * @return string $html
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __templateFooter() {
        $html = "</body>" .
                "</html>";

        return $html;
    }

    /**
     * Method used for create the template as body
     * will contain all the information filled in
     * by the user and will be displayed to the
     * PDF and attaching the file
     *
     * @return string $template
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __setBodyEmail($templates) {
        $header = $this->__templateHeader();
        $footer = $this->__templateFooter();

        $body = "<div>" .
                    $templates .
                "</div>";

        $finalTemplate = $header . $body . $footer;

        return $finalTemplate;
    }

    /**
     * Method that contains all the text will be
     * displayed at the moment to send the information
     * via e-mail, where the text of the mail will be
     * created in this method
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getTextTemplate($array, $widget) {
        $name     = "";
        $vendedor = "";

        if ($array['tipo_persona'] == 2 || $array['tipo_persona'] == '2') {
            $name = $array['persona_moral'];
        } else {
            $name = $array['nombre'] . " " . $array['apellido_paterno'] . " " . $array['apellido_materno'];
        }

        if ($array['vendedor'] == 'Si' || $array['vendedor'] == 'si') {
            $vendedor = "Vendedor: " . $array['nombre_vendedor'];
        }

        $templateText = "<div>Buen dia " . $name . ", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizacion que realizaste en " . $widget .
                            ".<br>" .
                            "El archivo adjunto contiene la informacion que has ingresado durante el
                             proceso de cotizacion de tu carga." .
                            "Tus datos de contacto son: <br />" .
                            "Nombre: " . $name . "<br />" .
                            "Telefono: " . $array['telefono'] . "<br />" .
                            "E-mail: " . $array['email'] . "<br />" .
                            $vendedor . "<br /><br />" .
                            "<br>" .
                            "Cualquier duda, por favor contactanos!" .
                            "<br><br>" .
                            "Gracias por realizar tu cotizacion en Grupo Nu&ntilde;o." .
                        "</div>";

        return $templateText;
    }

    /**
     * Method that contains all the text will be
     * displayed at the moment to send the information
     * via e-mail, where the text of the mail will be
     * created in this method
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Victor Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getTextTemplateMinor($array, $widget) {

        $templateText = "<div>Buen dia ".$array['fullname'].", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizaci&oacute;n que realizaste en " . $widget .
                            ".<br>".
                            "Cotizaci&oacute;n realizada por el asesor: ".$array['asesor']."<br />".
                            "El archivo adjunto contiene la informaci&oacute;n que has ingresado durante el
                            proceso de cotizaci&oacute;n de tu seguro Club Salud. <br />".
                            "Tus datos de contacto son: <br />".
                            "Nombre:   ".$array['fullname']."<br />".
                            "Telefono: ".$array['telefono']."<br />".
                            "E-mail:   ".$array['email']."<br /><br />".
                            "<br>".
                            "Cualquier duda, por favor contactanos!".
                            "<br><br>".
                            "Gracias por realizar tu cotizaci&oacute;n en Grupo Nu&ntilde;o.".
                        "</div>";

        return $templateText;
    }

    /**
     * Method that contains all the text will be
     * displayed at the moment to send the information
     * via e-mail, where the text of the mail will be
     * created in this method
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Victor Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getTextTemplateCubreTuDeducible($array, $widget) {

        $templateText = "<div>Buen dia ".$array['fullname'].", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizaci&oacute;n que realizaste en Cotiza " . $widget .
                            ".<br>".
                            "Cotizaci&oacute;n realizada por el asesor: ".$array['asesor']."<br />".
                            "El archivo adjunto contiene la informaci&oacute;n que has ingresado durante el
                            proceso de cotizaci&oacute;n de tu seguro ".$widget.". <br />".
                            "Tus datos de contacto son: <br />".
                            "Nombre:   ".$array['fullname']."<br />".
                            "Telefono: ".$array['telefono']."<br />".
                            "E-mail:   ".$array['email']."<br /><br />".
                            "<br>".
                            "Cualquier duda, por favor contactanos!".
                            "<br><br>".
                            "Gracias por realizar tu cotizaci&oacute;n en Grupo Nu&ntilde;o.".
                        "</div>";

        return $templateText;
    }

    /**
     * Method will contain all the body of the
     * email is going to send to the user and
     * grupo nuno to notify them that one possible
     * purchase has been done.
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getTextTemplatePets($array, $widget) {
        $vendedor = "";
        $name     = "";
        $contact  = "";
        $nameMain = "";

        if ($array['vendedor'] == 'Si' || $array['vendedor'] == 'si') {
            $vendedor = $array['name_seller'];
        }

        if ($array['tipo_persona'] == 0 || $array['tipo_persona'] == '0') {
            $name     = "Nombre: " . $array['nombre'] . " " . $array['paterno'] . " " . $array['materno'];
            $nameMain = $array['nombre'] . " " . $array['paterno'] . " " . $array['materno'];
        } else{
            $name     = "Empresa: " . $array['empresa'];
            $nameMain = $array['empresa'];
            $contact  = "Nombre del Contacto: " . $array['nombre_contacto'] . " <br />";
        }

        $templateText = "<div>Buen dia " . $nameMain . ", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizacion que realizaste en " . $widget .
                            ".<br>" .
                            "El archivo adjunto contiene la informacion que has ingresado durante el
                             proceso de cotizacion del seguro de tu mascota." .
                            "<br>" .
                            "Tus datos de contacto son: <br />" .
                            $name . "<br />" .
                            $contact .
                            "Telefono: " . $array['tel'] . "<br />" .
                            "E-mail: " . $array['email'] . "<br />" .
                            $vendedor . "<br /><br />" .
                            "Cualquier duda, por favor contactanos!" .
                            "<br><br>" .
                            "Gracias por realizar tu cotizacion en Grupo Nu&ntilde;o." .
                        "</div>";

        return $templateText;
    }

    /**
     * Method that contains all the body of the email
     * that will be sent at the moment the user has confirm
     * that the data set in the form is correct
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getTextTemplateResponsabilityInsurance($array, $widget) {
        $vendedor = "";
        $nombre   = "";
        $contacto = "";

        if ($array['es_vendedor'] == 'Si' || $array['es_vendedor'] == 'si') {
            $vendedor = "Vendedor: " . $array['nombre_vendedor'];
        }

        if ($array['tipo_persona'] == 0) {
            $nombre = $array['nombre_user'] . " " . $array['ap_user'] . " " . $array['am_user'];
        } else {
            $nombre   = $array['empresa_user'];
            $contacto = "Nombre del Contacto: " . $array['contacto_user'] . "<br />";
        }

        $templateText = "<div>Buen dia " . $nombre . ", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizacion que realizaste en " . $widget .
                            ".<br>" .
                            "El archivo adjunto contiene la informacion que has ingresado durante el
                             proceso de cotizacion del 'Seguro de Responsabilidad'." .
                            "Tus datos de contacto son: <br />" .
                            "Nombre: " . $nombre . "<br />" .
                            $contacto . 
                            "Telefono: " . $array['telefono'] . "<br />" .
                            "E-mail: " . $array['email'] . "<br />" .
                            $vendedor . "<br /><br />" .
                            "<br>" .
                            "Cualquier duda, por favor contactanos!" .
                            "<br><br>" .
                            "Gracias por realizar tu cotizacion en Grupo Nu&ntilde;o." .
                        "</div>";

        return $templateText;
    }

    /**
     * Method will contain all the information will
     * be used to send via email once the user
     * finished to fill in the form
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __getTextTemplateDataProtection($array, $widget) {
        $vendedor = "";
        $body     = "";
        $message  = "";
        $contact  = "";

        if ($array['es_vendedor'] == 'Si' || $array['es_vendedor'] == 'si') {
            $vendedor = "Vendedor: " . $array['name_seller'];
        }

        if($array['tipo_persona'] == '0' || $array['tipo_persona'] == 0) {
            $body = $array['nombres'] . ' ' . $array['a_paterno'] . ' ' . $array['a_materno'];
            $message = "Nombre: " . $body;
        } else {
            $body    = $array['empresa'];
            $contact = "Nombre del Contacto: " . $array['nombre_contacto'] . "<br />";
            $message = "Nombre de la Empresa: " . $body;
        }

        $templateText = "<div>Buen dia " . $body . ", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizacion que realizaste en " . $widget .
                            ".<br>" .
                            "El archivo adjunto contiene la informacion que has ingresado durante el
                             proceso de cotizacion del 'Seguro de Proteccion de Datos'." .
                            "Tus datos de contacto son: <br />" .
                            $message . "<br />" .
                            $contact . 
                            "Telefono: " . $array['telefono'] . "<br />" .
                            "E-mail: " . $array['email'] . "<br />" .
                            $vendedor . "<br /><br />" .
                            "<br>" .
                            "Cualquier duda, por favor contactanos!" .
                            "<br><br>" .
                            "Gracias por realizar tu cotizacion en Grupo Nu&ntilde;o." .
                        "</div>";

        return $templateText;
    }

    /**
     * Method will be used to create all
     * the body will be used to show
     * specific information in the email body
     * to the user and the owner to check all
     * this kind of cotization
     *
     * @param array $array
     * @param string $widget
     * @return string $templateText
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __getTextTemplateFraudulentActs($array, $widget) {
        $vendedor = "";
        $body     = "";
        $message  = "";

        if ($array['es_vendedor'] == 'Si' || $array['es_vendedor'] == 'si') {
            $vendedor = "Vendedor: " . $array['name_seller'];
        }

        if($array['tipo_persona'] == '0' || $array['tipo_persona'] == 0) {
            $body = $array['nombres'] . ' ' . $array['a_paterno'] . ' ' . $array['a_materno'];
            $message = "Nombre: " . $body;
        } else {
            $body    = $array['empresa'];
            $message = "Nombre de la Empresa: " . $body;
        }

        $templateText = "<div>Buen dia " . $body . ", </div><br><br>" .
                        "<div>" .
                            " Has recibido la cotizacion que realizaste en " . $widget .
                            ".<br>" .
                            "El archivo adjunto contiene la informacion que has ingresado durante el
                             proceso de cotizacion del 'Seguro de Proteccion de Datos'." .
                            "Tus datos de contacto son: <br />" .
                            $message . "<br />" .
                            "Telefono: " . $array['telefono'] . "<br />" .
                            "E-mail: " . $array['email'] . "<br />" .
                            $vendedor . "<br /><br />" .
                            "<br>" .
                            "Cualquier duda, por favor contactanos!" .
                            "<br><br>" .
                            "Gracias por realizar tu cotizacion en Grupo Nu&ntilde;o." .
                        "</div>";

        return $templateText;
    }
}
