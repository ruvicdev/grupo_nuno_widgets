<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Class that contain pdf export funcionallity
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
Class ExportFiles extends MY_Controller {

    // Instances will be used
    private $pdf;
    private $PDFInsuranceMinorME;

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // load PDF library
        $this->load->library('tcpdf/tcpdf');
        $this->load->library('tcpdf/PDFExtendsFunc');

        // create PDF object
        $this->pdf =  new tcpdf(PDF_PAGE_ORIENTATION,
                                PDF_UNIT,
                                PDF_PAGE_FORMAT,
                                true,
                                'UTF-8',
                                false);

        $this->PDFExtendsFunc = new PDFExtendsFunc(PDF_PAGE_ORIENTATION,
                                                   PDF_UNIT,
                                                   PDF_PAGE_FORMAT,
                                                   true,
                                                   'UTF-8',
                                                   false);
    }

    /**
     * Method will create a pdf file
     *
     * @return pdf file
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    function index() {
         $this->load->library('/tcpdf/tcpdf');
         $pdf = new tcpdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
         $pdf->SetCreator(PDF_CREATOR);
         $pdf->SetAuthor('Nicola Asuni');
         $pdf->SetTitle('TCPDF Example 006');
         $pdf->SetSubject('TCPDF Tutorial');
         $html = "<div>
             <strong>Hello TCPDF!</strong>
         </div>";

         // add a page
        // $pdf->AddPage();
         // output some RTL HTML content
         $pdf->writeHTML($html, true, false, true, false, '');
         $pdf->Output('example_006.pdf', 'I');
    }

     /**
      * Method used to create a PDF file
      * just passing the template will be used
      * to create the PDF file will be sent
      * via email
      *
      * @param string $template
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function __initPDF($template, $subject = null, $title = null) {
         // Set the name of the images
         $this->PDFExtendsFunc->__set("logoGrupoNuno.png", "FooterClubSaludPdf.png");

         $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
         $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

         if ($title != null) {
             $this->PDFExtendsFunc->SetTitle($title);
         }

         if ($subject != null) {
             $this->PDFExtendsFunc->SetSubject($subject);
         }

         // Set margins
         $this->PDFExtendsFunc->SetMargins(4, 43, 4); // Margin for page (left, top, right)
         $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
         $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
         $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

         $this->PDFExtendsFunc->AddPage();
         $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

         //ob_end_clean();
         return $this->PDFExtendsFunc;
     }

     /**
      * Method used to create a PDF file
      * passing a template that will be use
      * in writeHTML method
      *
      * @param string $template
      *
      * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function __initPDFMinor($template, $subject = null, $title = null) {
          // Set the name of the images
          $this->PDFExtendsFunc->__set("HeaderClubSaludPdf.png", "FooterClubSaludPdf.png");

          $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
          $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

          if ($title != null) {
              $this->PDFExtendsFunc->SetTitle($title);
          }

          if ($subject != null) {
              $this->PDFExtendsFunc->SetSubject($subject);
          }

          // Set margins
          $this->PDFExtendsFunc->SetMargins(4, 45, 4); // Margin for page (left, top, right)
          $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
          $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
          $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

          $this->PDFExtendsFunc->AddPage();
          $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

          //ob_end_clean();
          return $this->PDFExtendsFunc;
     }

     /**
      * Method used to create a PDF file
      * passing a template that will be use
      * in writeHTML method
      *
      * @param string $template
      *
      * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function __initPDFCubreTuDeducible($template, $subject = null, $title = null) {
          // Set the name of the images
          $this->PDFExtendsFunc->__set("HeaderCubreTuDeducible.png", "FooterClubSaludPdf.png");

          $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
          $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

          if ($title != null) {
              $this->PDFExtendsFunc->SetTitle($title);
          }

          if ($subject != null) {
              $this->PDFExtendsFunc->SetSubject($subject);
          }

          // Set margins
          $this->PDFExtendsFunc->SetMargins(4, 45, 4); // Margin for page (left, top, right)
          $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
          $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
          $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

          $this->PDFExtendsFunc->AddPage();
          $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

          //ob_end_clean();
          return $this->PDFExtendsFunc;
     }

     /**
      * Method used to create a PDF file
      * passing a template that will be use
      * in write HTML method
      *
      * @param string $template
      *
      * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function __initPDFMulticotizador($template, $subject = null, $title = null) {
          // Set the name of the images
          $this->PDFExtendsFunc->__set("HeaderCubreTuDeducible.png", "FooterClubSaludPdf.png");

          $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
          $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

          if ($title != null) {
              $this->PDFExtendsFunc->SetTitle($title);
          }

          if ($subject != null) {
              $this->PDFExtendsFunc->SetSubject($subject);
          }

          // Set margins
          $this->PDFExtendsFunc->SetMargins(4, 45, 4); // Margin for page (left, top, right)
          $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
          $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
          $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

          $this->PDFExtendsFunc->AddPage();
          $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

          //ob_end_clean();
          return $this->PDFExtendsFunc;
     }

     /**
      * Method will be used to create the PDF
      * file and will be exported during the
      * request of the user or inclusively
      * could be sent via email
      *
      * @param string $template
      * @param string $subject
      * @param string $title
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
      public function __initPDFInsurancePet($template, $subject = null, $title = null) {
          // Set the name of the images
          $this->PDFExtendsFunc->__set("logoGrupoNuno.png", "FooterClubSaludPdf.png");

          $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
          $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

          if ($title != null) {
              $this->PDFExtendsFunc->SetTitle($title);
          }

          if ($subject != null) {
              $this->PDFExtendsFunc->SetSubject($subject);
          }

          // Set margins
          $this->PDFExtendsFunc->SetMargins(4, 43, 4); // Margin for page (left, top, right)
          $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
          $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
          $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

          $this->PDFExtendsFunc->AddPage();
          $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

          //ob_end_clean();
          return $this->PDFExtendsFunc;
      }

      /**
       * Method will be used to generate the
       * PDF file. This method returns all the information
       * with all the data setup for create the PDF file
       * once we have the template
       *
       * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
       * @version 1.0
       * @company ruvicdev
       */
      public function __initPDFInsuranceResponsability($template, $subject = null, $title = null) {
          $this->PDFExtendsFunc->__set("logoGrupoNuno.png", "FooterClubSaludPdf.png");
          $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
          $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

          if ($title != null) {
              $this->PDFExtendsFunc->SetTitle($title);
          }

          if ($subject != null) {
              $this->PDFExtendsFunc->SetSubject($subject);
          }

          // Set margins
          $this->PDFExtendsFunc->SetMargins(4, 43, 4); // Margin for page (left, top, right)
          $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
          $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
          $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

          $this->PDFExtendsFunc->AddPage();
          $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

          //ob_end_clean();
          return $this->PDFExtendsFunc;
      }

      /**
       * Method will be used to generate the
       * PDF file. This method returns all the information
       * with all the data setup for create the PDF file
       * once we have the template
       *
       * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
       * @version 1.0
       * @company ruvicdev
       */
      public function __initPDFFraudulentActs($template, $subject = null, $title = null) {
          $this->PDFExtendsFunc->__set("logoGrupoNuno.png", "FooterClubSaludPdf.png");
          $this->PDFExtendsFunc->SetCreator(PDF_CREATOR);
          $this->PDFExtendsFunc->SetAuthor('Ruvicdev');

          if ($title != null) {
              $this->PDFExtendsFunc->SetTitle($title);
          }

          if ($subject != null) {
              $this->PDFExtendsFunc->SetSubject($subject);
          }

          // Set margins
          $this->PDFExtendsFunc->SetMargins(4, 43, 4); // Margin for page (left, top, right)
          $this->PDFExtendsFunc->SetHeaderMargin(0); // Minimun distance between header and bottom page margin
          $this->PDFExtendsFunc->SetFooterMargin(45); // Minimun distance between footer and bottom page margin
          $this->PDFExtendsFunc->SetAutoPageBreak(TRUE, 45); // Set auto page breaks

          $this->PDFExtendsFunc->AddPage();
          $this->PDFExtendsFunc->writeHTML($template, true, false, true, false, '');

          //ob_end_clean();
          return $this->PDFExtendsFunc;
      }
}
