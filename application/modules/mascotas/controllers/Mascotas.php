<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Class contains all the information and
 * methods for pet insurance widget will be
 * used to check the prices of the different
 * packages enabled to
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class Mascotas extends MY_Controller {

    /**
     * Constructor .......
     */
    public function __construct() {
        parent::__construct();

        // load the model
        $this->load->model(array('MascotasHistorial_model'));

        // create the object to export the PDF
        $this->pdf   = new ExportFiles();
        // creates the email and send the data
        $this->emailSend = new EmailSend();
        // creates the template object
        $this->template = new Templates();
    }

    /**
     * Method will be used to load the main
     * form is going to fill the user in for
     * create a pets insurance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function index() {
        // Get data from dropdown about packages enabled
        $dataDropdown = $this->__getPackagesPetInsurance();

        $content = $this->loadSingleView('index', $dataDropdown, TRUE);
        $data    = $this->__setArray('content', $content);
        $data    = $this->__setArray('title', 'Seguro de Mascotas');

        $this->loadTemplate($data);
    }

    /**
     * Method will get all the information typed by the user
     * about the personal data and pets data to save in the
     * database to then create the PDF report will be sent to the
     * email of the user wants to know the price og the insurace for the pet
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData() {
        $json_array = json_decode(file_get_contents('php://input'), true);

        // Create the array will be used to save info in database
        $data = $this->__createArrayPdfData($json_array);

        // Save the user information
        $id = $this->MascotasHistorial_model->saveData($data);

        // Generate the template and init the PDF file creation
        $template = $this->template->mascotasTemplate($json_array, $id);
        $PDF      = $this->pdf->__initPDFInsurancePet($template, 'Seguro de Mascotas', 'Seguro de Mascotas');

        // Set PDF name and create PDF file
        $name = "seguroMascota_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

        // Create the file
        $PDF->Output(FCPATH . 'public/files/' . $name, 'F');
        $completePath = FCPATH . 'public/files/' . $name;
        $returnedPath = base_url() . 'public/files/' . $name;

        // Send the informacion via email
        $this->emailSend->__initEmailPets($json_array, $PDF, $completePath, "Seguro Mascotas");

        // Create json will be returned
        $response =  array('flag'     => '1',
                           'pathFile' => $returnedPath);

        echo json_encode($response);
    }

    /**
     * Method will be used to create the array will be sent
     * at the moment to try to save the information in the
     * database and the user could check the data value in the
     * table via query or generating a report
     *
     * @param $array array
     * @return $data array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __createArrayPdfData($array) {
        $finalArray = array();
        $nombre     = "";
        $empresa    = "";
        $contacto   = "";

        // Define variables will be used
        $no_microchip = '';
        $registro     = '';
        $num_registro = '';

        if ($array['tipo_persona'] == 0 || $array['tipo_persona'] == '0'){
            $nombre = $array['nombre'] . " " . $array['paterno'] . " " . $array['materno'];
        } else {
            $empresa  = $array['empresa'];
            $contacto = $array['nombre_contacto'];
        }

        // Validate if some values are empty
        if ($array['registro_option'] == 'Si') {
            $registro     = $array['registro'];
            $num_registro = $array['num_registro'];
        } else {
            $registro     = '';
            $num_registro = '';
        }

        if ($array['microchip'] == 'Si') {
            $no_microchip = $array['no_microchip'];
        } else {
            $no_microchip = '';
        }

        $finalArray = array(
            'tipo_persona'       => $array['tipo_persona'],
            'empresa'            => $empresa,
            'contacto'           => $contacto,
            'nombre'             => $nombre,
            'rfc'                => $array['rfc'],
            'direccion'          => $array['direccion'],
            'cp'                 => $array['cp'],
            'colonia'            => $array['colonia'],
            'municipio'          => $array['municipio'],
            'estado'             => $array['estado'],
            'telefono'           => $array['tel'],
            'email'              => $array['email'],
            'nombre_mascota'     => $array['mascota_n'],
            'edad'               => $array['edad'],
            'tipo'               => $array['tipo'],
            'sexo'               => $array['sexo'],
            'raza'               => $array['raza'],
            'otra_raza'          => $array['otra_raza'],
            'color'              => $array['color'],
            'esterilizado'       => $array['esteril'],
            'pedigree'           => $array['pedigree'],
            'registro_option'    => $array['registro_option'],
            'registro'           => $registro,
            'no_registro'        => $num_registro,
            'senas_particulares' => $array['s_part'],
            'microchip'          => $array['microchip'],
            'no_microchip'       => $no_microchip,
            'accidente'          => $array['accidente'],
            'criptorquidismo'    => $array['criptorquidismo'],
            'fractura'           => $array['fractura'],
            'hernias'            => $array['hernias'],
            'sida_felino'        => $array['sida_felino'],
            'luxacion'           => $array['luxacion'],
            'displacia'          => $array['displacia'],
            'cardiopatias'       => $array['cardiopatias'],
            'ehrlichia'          => $array['ehrlichia'],
            'leucemia'           => $array['leucemia'],
            'borrelia'           => $array['borrelia'],
            'oculares'           => $array['oculares'],
            'gusano_c'           => $array['gusano_c'],
            'dermatologicas'     => $array['dermatologicas'],
            'parasitarias'       => $array['parasitarias'],
            'convulsiones'       => $array['convulsiones'],
            'especificaciones'   => $array['desc_ant'],
            //'precio'             => $array['precio'],
            //'paquete_id'         => $array['paquete'],
            //'paquete_desc'       => $array['paquete_desc'],
            'fecha_creacion'     => date('d-m-Y H:i:s'),
            'vendedor'           => $array['vendedor'],
            'nombre_vendedor'    => $array['nombre_vendedor']
        );

        return $finalArray;
    }

    /**
     * Method will containd the information of
     * the different packages the user could cover
     * for the pet insurance
     *
     * @return array $array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getPackagesPetInsurance() {
        $array = array();

        $arrayData = array(
            array('id' => '1', 'paquete' => 'Medipet Basico - $ 1,088.08', 'precio' => '1,088.08'),
            array('id' => '2', 'paquete' => 'Medipet Platino - $ 2,109.58', 'precio' => '2,109.58'),
            array('id' => '3', 'paquete' => 'Medipet Premier - $ 3,088.62', 'precio' => '3,088.62'),
            array('id' => '4', 'paquete' => 'Medipet Elite I - $ 3,657.48', 'precio' => '3,657.48'),
            array('id' => '5', 'paquete' => 'Medipet Elite II - $ 4,587.80', 'precio' => '4,587.80'),
            array('id' => '6', 'paquete' => 'Medipet Elite III - $ 5,347.60', 'precio' => '5,347.60')
        );

        return $array = array('paquetes' => $arrayData);
    }

    public function demo() { //$id) {
        //echo "holas: " . $id;
        //$data = $this->Cotizaciones_model->getDataSpecificRecord($id);
        //$demo = $data->row_array();
        /*var_dump($data->result_array());
        $datas = array(
            'id' => $demo['id']
        );
        var_dump($datas);
        die();*/
        //$data = $this->Pais_model->get_all_continents();
        //var_dump($data->result_array());
        //$dir __DIR__ . '../public';
        //$dir = dirname(dirname(__FILE__));
        //echo FCPATH . '\n';
        //echo $dir;
        //echo $dir . "\n";
        //$this->load->library('email');
        //$json_array = json_decode(file_get_contents('php://input'), true);
        $temp = $this->template->mascotasTemplate();
        //var_dump($temp);
        //ob_clean();
        $p = $this->pdf->__initPDFInsurancePet($temp, "Seguro de Mascota", "Seguro de Mascota"); //'<p>hola</p>');
        //var_dump($p);
        $p->Output("demo.pdf", 'I');
        //var_dump($p);
        //ob_clean();

        //$pdf->Output(FCPATH . 'public/files/example_006.pdf', 'F');

        //var_dump($pdfF);
        //$this->emailSend->__initEmail(array(), $pdf);
        //$this->email->demos(array(), $pdf);
        //$this->email->demos($pdf);
        //$this->email->from('r@ruvicdev.com', 'nombre');
        //$this->email->to('ruben.alonso21@gmail.com');
        //$this->email->bcc('email');

        //$this->email->subject('subject demo');
        //$this->email->message('template');

        //$data    = chunk_split($pdfFile);
        //$this->email->attach(FCPATH . 'public/files/example_006.pdf');

        //$this->email->send();
    }
}
