<script type="text/x-handlebars-template" id="errors_messages">
    <div class="row">
        <div class="ui negative hidden message">
            <div class="header">
                Error
            </div>
            <p>
                Por favor, llene los campos requeridos
            </p>
            <p class="messageErrorMoney">
                {{data}}
            </p>
        </div>
    </div>
</script>

<!-- SECTION TO OPEN MODAL ** START ** -->
<div class="ui modal product-information">
    <div class="header">
        Cobertura / Suma Asegurada
    </div>
    <div class="content">
        <table class="ui striped table">
            <thead>
                <tr>
                    <th>PRODUCTO</th>
                    <th>GTOS. MEDICOS MASCOTA</th>
                    <th>RC POR TENENCIA DE MASCOTA</th>
                    <th>SERVICIO FUNERARIO</th>
                    <th>COSTO ANUAL</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>MEDIPET BASICO</td>
                    <td>$ 5,000.00 </td>
                    <td> ----- </td>
                    <td> ----- </td>
                    <td>$ 1,088.08 </td>
                </tr>
                <tr>
                    <td>MEDIPET PLATINO</td>
                    <td>$ 10,000.00 </td>
                    <td>$ 25,000.00 </td>
                    <td> ----- </td>
                    <td>$ 2,109.58 </td>
                </tr>
                <tr>
                    <td>MEDIPET PREMIER</td>
                    <td>$ 25,000.00 </td>
                    <td>$ 25,000.00 </td>
                    <td>AMPARAD0 HASTA $ 1,500.00 </td>
                    <td>$ 3,088.62 </td>
                </tr>
                <tr>
                    <td>MEDIPET ELITE I</td>
                    <td>$ 25,000.00 </td>
                    <td>$ 50,000.00 </td>
                    <td>AMPARADO HASTA $ 1,500.00 </td>
                    <td>$ 3,657.48 </td>
                </tr>
                <tr>
                    <td>MEDIPET ELITE II</td>
                    <td>$ 25,000.00 </td>
                    <td>$ 100,000.00 </td>
                    <td>AMPARADO HASTA $ 1,500.00 </td>
                    <td>$ 4,587.80 </td>
                </tr>
                <tr>
                    <td>MEDIPET ELITE III</td>
                    <td>$ 25,000.00 </td>
                    <td>$ 150,000.00 </td>
                    <td>AMPARADO HASTA $ 1,500.00 </td>
                    <td>$ 5,347.60 </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="actions">
        <div class="ui positive approve button ok-modal">Ok</div>
    </div>
</div>
<!-- SECTION TO OPEN MODAL ** END ** -->

<!-- SECTION FOR STEPS **START** -->
<div class="default-margin-top ui grid centered removePadding">
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos Personales</div>
                <div class="description">Informacion Personal</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="paw circle icon"></i>
            <div class="content">
                <div class="title">Datos de la Mascota</div>
                <div class="description">Informacion de la Mascota</div>
            </div>
        </div>
        <div class="disabled step" id="step-3">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Cotizacion</div>
                <div class="description">Cotizacion - Seguro de Mascotas</div>
            </div>
        </div>
    </div>
</div>
<!-- SECTION FOR STEPS **END** -->

<div class="ui grid centered">
    <div class="row">
        <h1 class="ui header">
            Seguro de Mascota
        </h1>
    </div>
    <div>&nbsp;</div>
    <form class="ui form">
        <div class="ui styled accordion">
            <div class="accordion-1" id="disabled-content1"><!-- 1ST SECTION ** START ** -->
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos del Propietario
                </div>
                <div class="content active" id="content-1"><!-- content form ** START ** -->
                    <div class="row" id="error_first_section"></div>
                    <div>&nbsp;</div>
                    <div class="fields">
                        <div class="seven wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" id="vendedor"
                                       class="">
                                <label class="bold_text_title">
                                    ¿Eres vendedor de Grupo Nu&ntilde;o?
                                </label>
                            </div>
                        </div>
                        <div class="nine wide field">
                            <input type="text" name="nombre_vendedor" id="name_seller"
                                   class="" placeholder="Nombre del Vendedor">
                        </div>
                    </div>
                    <div class="fields">
                        <input type="hidden" id="tipo_persona">
                        <div class="five wide field">
                            <label class="left_align bold_text_title" for="tipo_persona">
                                Tipo de Persona:
                            </label>
                        </div>
                        <div class="five wide field required sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="fisica"
                                       data-class="hidden-segment-value"
                                       data-index="1"
                                       class="checkdata required tperson">
                                <label>
                                    Persona Fisica
                                </label>
                            </div>
                        </div>
                        <div class="five wide field required sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="moral"
                                       data-class="hidden-segment-value"
                                       data-index="2"
                                       class="checkdata required tperson">
                                <label>
                                    Persona Moral
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="ui segment hidden-segment-value" data-value="moral" id="moral">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Empresa:
                            </label>
                            <input type="text" name="empresa" id="empresa" class="required first-section" placeholder="Empresa">
                        </div>
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre del Contacto:
                            </label>
                            <input type="text" name="nombre_contacto" id="nombre_contacto" class="" placeholder="Nombre del Contacto">
                        </div>
                    </div>
                    <div class="ui segment hidden-segment-value" data-value="fisica" id="fisica">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre(s):
                            </label>
                            <input type="text" name="nombre" id="nombre" class="required first-section" placeholder="Nombre(s)">
                        </div>
                        <div class="fields">
                            <div class="eight wide field">
                                <label class="left_align bold_text_title">
                                    Apellido Paterno:
                                </label>
                                <input type="text" name="paterno" id="paterno" class="required first-section" placeholder="Apellido Paterno">
                            </div>
                            <div class="eight wide field">
                                <label class="left_align bold_text_title">
                                    Apellido Materno:
                                </label>
                                <input type="text" name="materno" id="materno" class="required first-section" placeholder="Apellido Materno">
                            </div>
                        </div>
                    </div>
                    <!--div class="field">
                        <label class="left_align">
                            Nombre(s) del Solicitante:
                        </label>
                        <input type="text" name="nombre" id="nombre" class="first-section"
                               placeholder="Nombre(s) del Solicitante">
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align">
                                Apellido Paterno:
                            </label>
                            <input type="text" name="paterno" id="paterno" class="first-section"
                                   placeholder="Apellido Paterno">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                Apellido Materno:
                            </label>
                            <input type="text" name="materno" id="materno" class="first-section"
                                   placeholder="Apellido Materno">
                        </div>
                    </div-->
                    <div class="field">
                        <label class="left_align">
                            R.F.C:
                        </label>
                        <input type="text" name="rfc" id="rfc" class="first-section"
                               placeholder="R.F.C">
                    </div>
                    <div class="fields">
                        <div class="ten wide field">
                            <label class="left_align">
                                Calle y N&uacute;mero:
                            </label>
                            <input type="text" name="direccion" id="direccion" class="first-section"
                                   placeholder="Calle y Numero">
                        </div>
                        <div class="six wide field">
                            <label class="left_align">
                                Codigo Postal:
                            </label>
                            <input type="text" name="cp" id="cp" class="first-section"
                                   placeholder="Codigo Postal">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="five wide field">
                            <label class="left_align">
                                Colonia:
                            </label>
                            <input type="text" name="colonia" id="colonia" class="first-section"
                                   placeholder="Colonia">
                        </div>
                        <div class="five wide field">
                            <label class="left_align">
                                Municipio:
                            </label>
                            <input type="text" name="municipio" id="municipio" class="first-section"
                                   placeholder="Municipio">
                        </div>
                        <div class="six wide field">
                            <label class="left_align">
                                Estado:
                            </label>
                            <input type="text" name="estado" id="estado" class="first-section"
                                   placeholder="Estado">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align">
                                Tel&eacute;fono de oficina, casa o celular:
                            </label>
                            <input type="text" name="tel" id="tel" class="first-section"
                                   placeholder="Telefono de oficina, casa o celular">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                E-mail
                            </label>
                            <input type="email" name="email" id="email" class="first-section"
                                   placeholder="E-mail">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="sixteen wide field">
                            <button class="ui right floated positive button pet-section">
                                Siguiente
                            </button>
                            <button class="ui right floated blue button clear-fields" data-type="first">
                                Limpiar
                            </button>
                        </div>
                    </div>
                </div><!-- content form ** END ** -->
            </div><!-- 1ST SECTION ** END ** -->

            <div class="accordion-2" id="disabled-content"><!-- 2ND SECTION ** START ** -->
                <div class="title" id="title-2">
                    <i class="dropdown icon"></i>
                    Datos de la Mascota
                </div>
                <div class="content" id="content-2">
                    <div class="row" id="error_second_section"></div>
                    <div>&nbsp;</div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align">
                                Nombre de la Mascota:
                            </label>
                            <input type="text" name="mascota_n" id="mascota_n" class="second-section"
                                   placeholder="Nombre de la Mascota">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                Edad:
                            </label>
                            <input type="text" name="edad" id="edad" class="second-section"
                                   placeholder="Edad">
                        </div>
                    </div>
                    <!--div class="fields">
                        <div class="twelve wide field">
                            <label class="left_align">
                                Paquete /  Producto:
                            </label>
                            <div class="ui selection dropdown paquete-main">
                                <input type="hidden" name="paquete" class="second-section"
                                    value="0" id="paquete">
                                <i class="dropdown icon"></i>
                                <div class="default text">Paquete / Producto</div>
                                <div class="menu">
                                    < ?php foreach($paquetes as $key => $value): ?>
                                        <div class="item" data-value="< ?php echo $value['id']; ?>"
                                                          data-desc="< ?php echo $value['paquete']; ?>">
                                            < ?php echo $value['paquete']; ?>
                                            <input type="hidden"
                                                   data-name="< ?php echo $value['paquete']; ?>"
                                                   id="< ?php echo $value['id']; ?>"
                                                   value="< ?php echo $value['precio']; ?>">
                                        </div>
                                    < ?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="four wide field">
                            <label>&nbsp;</label>
                            <button class="ui red button help-button">
                                <i class="help circle icon"></i> Ayuda
                            </button>
                        </div>
                    </div -->
                    <div class="fields">
                        <div class="two wide field tipo">
                            <label class="left_align">
                                Tipo:
                            </label>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Perro" data-text="tipo"
                                       class="checkdata2 requiredClass tipo tipo-21 second-section"
                                       name="tipo"
                                       data-class="tipo-21">
                                <label>
                                    Perro
                                </label>
                            </div>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Gato" data-text="tipo"
                                       class="checkdata2 requiredClass tipo tipo-21 second-section"
                                       name="tipo"
                                       data-class="tipo-21">
                                <label>
                                    Gato
                                </label>
                            </div>
                        </div>
                        <div class="two wide field sexo">
                            <label class="left_align">
                                Sexo:
                            </label>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Macho" data-text="sexo"
                                       class="checkdata2 requiredClass sexo sexo-21 second-section"
                                       name="sexo"
                                       data-class="sexo-21">
                                <label>
                                    Macho
                                </label>
                            </div>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Hembra" data-text="sexo"
                                       class="checkdata2 requiredClass sexo sexo-21 second-section"
                                       name="sexo"
                                       data-class="sexo-21">
                                <label>
                                    Hembra
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align">
                                Raza:
                            </label>
                            <input type="text" name="raza" id="raza" class="second-section"
                                   placeholder="Raza">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                Otra Raza:
                            </label>
                            <input type="text" name="otra_raza" id="otra_raza" class=""
                                   placeholder="Otra Raza">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align">
                                Color(es):
                            </label>
                            <input type="text" name="color" id="color" class="second-section"
                                   placeholder="Color(es)">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                Esterilizado
                            </label>
                            <div class="ui selection dropdown esteril">
    							<input name="esteril" type="hidden" class="requiredClass second-section"
                                       value="0" id="esteril">
    							<i class="dropdown icon"></i>
    							<div class="default text">Esterilizado</div>
    							<div class="menu">
    								<div class="item" data-value="Si">
    									Si
    								</div>
    								<div class="item" data-value="No">
    									No
    								</div>
    							</div>
    						</div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="two wide field pedigree">
                            <label class="left_align">
                                Pedigree:
                            </label>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" data-text="pedigree"
                                       class="checkdata2 requiredClass pedigree pedigree-21 second-section"
                                       name="pedigree"
                                       data-class="pedigree-21">
                                <label>
                                    Si
                                </label>
                            </div>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="No" data-text="pedigree"
                                       class="checkdata2 requiredClass pedigree pedigree-21 second-section"
                                       name="pedigree"
                                       data-class="pedigree-21">
                                <label>
                                    No
                                </label>
                            </div>
                        </div>
                        <div class="two wide field registro_option">
                            <label class="left_align">
                                Registro:
                            </label>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" data-text="registro_option"
                                       class="checkdata2 requiredClass registro_option registro-21 second-section"
                                       name="registro_option"
                                       data-class="registro-21">
                                <label>
                                    Si
                                </label>
                            </div>
                        </div>
                        <div class="three wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="No" data-text="registro_option"
                                       class="checkdata2 requiredClass registro_option registro-21 second-section"
                                       name="registro_option"
                                       data-class="registro-21">
                                <label>
                                    No
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field registro">
                            <label class="left_align">
                                Registro:
                            </label>
                            <div class="ui selection dropdown registro">
                                <input name="registro" type="hidden" class="requiredClass second-section"
                                       value="0" id="registro">
                                <i class="dropdown icon"></i>
                                <div class="default text registroTexto">Registro</div>
                                <div class="menu">
                                    <div class="item" data-value="Nacional">
                                        Nacional
                                    </div>
                                    <div class="item" data-value="Extranjero">
                                        Extranjero
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                Numero de Registro: (en caso afirmativo)
                            </label>
                            <input type="text" name="num_registro" id="num_registro" class="second-section"
                                   placeholder="Numero de Registro">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align">
                            Se&ntilde;as Particulares:
                        </label>
                        <textarea rows="3" id="s_part" class="second-section"></textarea>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align">
                                Cuenta con microchip AVID?
                            </label>
                            <div class="ui selection dropdown microchip">
    							<input name="microchip" type="hidden" class="requiredClass second-section"
                                       value="0" id="microchip">
    							<i class="dropdown icon"></i>
    							<div class="default text">Cuenta Microchip</div>
    							<div class="menu">
    								<div class="item" data-value="Si">
    									Si
    								</div>
    								<div class="item" data-value="No">
    									No
    								</div>
    							</div>
    						</div>
                        </div>
                        <div class="eight wide field">
                            <label class="left_align">
                                No. Microchip:
                            </label>
                            <input type="text" name="no_microchip" id="no_microchip" class="second-section"
                                   placeholder="No. Microchip">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align">
                            Tu mascota, ¿ha sufrido?
                            <small style="margin-left: 5px; color: #FF0000">
                                * En caso que de que su mascota tenga alguna enfermedad, no ser&aacute; posible asegurarla
                            </small>
                        </label>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui selection dropdown">
                                <input name="accidente" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="accidente">
                                <i class="dropdown icon"></i>
                                <div class="default text">Accidente</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui selection dropdown">
                                <input name="criptorquidismo" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="criptorquidismo">
                                <i class="dropdown icon"></i>
                                <div class="default text">Criptorquidismo</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="fractura" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="fractura">
                                <i class="dropdown icon"></i>
                                <div class="default text">Fractura</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="hernias" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="hernias">
                                <i class="dropdown icon"></i>
                                <div class="default text">Hernias</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="sida_felino" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="sida_felino">
                                <i class="dropdown icon"></i>
                                <div class="default text">Sida Felino</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="luxacion" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="luxacion">
                                <i class="dropdown icon"></i>
                                <div class="default text">Luxacion Patelar</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="displacia" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="displacia">
                                <i class="dropdown icon"></i>
                                <div class="default text">Displacia de Cadera</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="cardiopatias" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="cardiopatias">
                                <i class="dropdown icon"></i>
                                <div class="default text">Cardiopatias</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="ehrlichia" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="ehrlichia">
                                <i class="dropdown icon"></i>
                                <div class="default text">Ehrlichia</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="leucemia" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="leucemia">
                                <i class="dropdown icon"></i>
                                <div class="default text">Leucemia Viral Felina</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="borrelia" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="borrelia">
                                <i class="dropdown icon"></i>
                                <div class="default text">Borrelia</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="oculares" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="oculares">
                                <i class="dropdown icon"></i>
                                <div class="default text">Enfermedades Oculares</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="gusano_c" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="gusano_c">
                                <i class="dropdown icon"></i>
                                <div class="default text">Enfermedades de Gusano de Corazon</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="dermatologicas" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="dermatologicas">
                                <i class="dropdown icon"></i>
                                <div class="default text">Enfermedades Dermatologicas</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="parasitarias" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="parasitarias">
                                <i class="dropdown icon"></i>
                                <div class="default text">Enfermedades Parasitarias</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <div class="ui compact selection dropdown">
                                <input name="convulsiones" type="hidden" class="requiredClass second-section sufrido"
                                       value="0" id="convulsiones">
                                <i class="dropdown icon"></i>
                                <div class="default text">Convulsiones o Desmayos</div>
                                <div class="menu">
                                    <div class="item" data-value="Si">Si</div>
                                    <div class="item" data-value="No">No</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align">
                            En caso Afirmativo, especificar fecha y antecedentes:
                        </label>
                        <textarea rows="4" id="desc_ant" class="second-section"></textarea>
                    </div>
                    <div class="fields">
                        <div class="sixteen wide field">
                            <button class="ui right floated positive button report-section">
                                Siguiente
                            </button>
                            <button class="ui right floated negative button back-form">
                                Regresar
                            </button>
                            <button class="ui right floated blue button clear-fields" data-type="second">
                                Limpiar
                            </button>
                        </div>
                    </div>
                </div>
            </div><!-- 2ND SECTION ** END ** -->

            <div class="accordion-3" id="disabled-content"><!-- 3RD SECTION ** START ** -->
                <div class="title" id="title-3">
                    <i class="dropdown icon"></i>
                    Preview
                </div>
                <div class="content" id="content-3">
                    <div class="row">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-4">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-4">
                                Confirmar y Enviar
                            </button>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left">
                                Vendedor Grupo Nu&ntilde;o
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Eres Vendedor?:</label>
                                <label id="vendedor_txt" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Nombre del Vendedor:</label>
                                <label id="nombre_vendedor_txt" class="light_text_desc"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left">
                                DATOS DEL PROPIETARIO
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline fisica_temp">
                                <label class="bold_text_title">Nombre:</label>
                                <label id="nombre_txt" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline moral_temp">
                                <label class="bold_text_title">Empresa:</label>
                                <label id="empresa_txt" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline moral_temp">
                                <label class="bold_text_title">Nombre del Contacto:</label>
                                <label id="nombre_contacto_txt" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">RFC:</label>
                                <label id="rfc_txt" class="light_text_desc"></label>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eleven wide field">
                                    <label class="bold_text_title">Calle y Numero:</label>
                                    <label id="direccion_txt" class="light_text_desc"></label>
                                </div>
                                <div class="five wide field">
                                    <label class="bold_text_title">Codigo Postal:</label>
                                    <label id="codigo_postal_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Colonia:</label>
                                    <label id="colonia_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Municipio:</label>
                                    <label id="municipio_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Estado:</label>
                                <label id="estado_txt" class="light_text_desc"></label>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="nine wide field">
                                    <label class="bold_text_title">Email:</label>
                                    <label id="email_txt" class="light_text_desc"></label>
                                </div>
                                <div class="seven wide field">
                                    <label class="bold_text_title">Telefono:</label>
                                    <label id="tel_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left">
                                DATOS DE LA MASCOTA
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="two fields left_align inline" style="margin-top: 15px">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Nombre de la Mascota:</label>
                                    <label id="nombre_masc_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Edad:</label>
                                    <label id="edad_masc_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <!--div class="two fields left_align inline" style="margin-top: 15px">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Paquete:</label>
                                    <label id="paquete_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Precio:</label>
                                    <label id="precio_txt" class="light_text_desc"></label>
                                </div>
                            </div-->
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Tipo:</label>
                                    <label id="tipo_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Sexo:</label>
                                    <label id="sexo_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Raza:</label>
                                    <label id="raza_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Otra Raza:</label>
                                    <label id="otra_raza_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Color(es):</label>
                                    <label id="color_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Esterilizado:</label>
                                    <label id="esterilizado_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="five wide field">
                                    <label class="bold_text_title">Pedigree:</label>
                                    <label id="pedigree_txt" class="light_text_desc"></label>
                                </div>
                                <div class="six wide field">
                                    <label class="bold_text_title">Cuenta con Registro:</label>
                                    <label id="cuenta_registro" class="light_text_desc"></label>
                                </div>
                                <div class="five wide field">
                                    <label class="bold_text_title">Registro:</label>
                                    <label id="registro_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="field left_align">
                                <label class="bold_text_title">Numero de Registro (en caso afirmativo):</label>
                            </div>
                            <div class="field left_align">
                                <label id="no_registro_txt" class="light_text_desc"></label>
                            </div>
                            <div class="two fields">
                                <div class="field left_align">
                                    <label class="bold_text_title">Se&ntilde;as Particulares:</label>
                                </div>
                                <div class="field left_align">
                                    <label id="sen_part_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Cuenta con Microchp AVID:</label>
                                    <label id="micro_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">No. Microchip:</label>
                                    <label id="no_micro_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Tu mascota, ¿ha sufrido?</label>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Accidente:</label>
                                    <label id="accidente_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Criptorquidismo:</label>
                                    <label id="cript_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Fractura:</label>
                                    <label id="fractura_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Hernias:</label>
                                    <label id="hernias_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Sida Felino:</label>
                                    <label id="sida_f" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Luxacion Patelar:</label>
                                    <label id="luxacion_p" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Cardiopatias:</label>
                                    <label id="cardio" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Ehrlichia:</label>
                                    <label id="ehrlichia_e" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Displacia de Cadera:</label>
                                    <label id="displacia_c" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Borrelia:</label>
                                    <label id="borrelia_e" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Leucemia Viral Felina:</label>
                                    <label id="leucemia_vf" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Enfermedades Oculares:</label>
                                    <label id="enfermedad_o" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Enfermedades de Gusano de Corazon:</label>
                                    <label id="gusano_de_c" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Enfermedades Dermatologicas:</label>
                                    <label id="enfermedad_d" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Enfermedades Parasitarias:</label>
                                    <label id="enfermedad_p" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Convulsiones o Desmayos:</label>
                                    <label id="convulcion_desmayo" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">
                                    En caso Afirmativo, especificar fecha y antecedentes:
                                </label>
                            </div>
                            <div class="field left_align inline">
                                <label id="afirm_txt" class="light_text_desc"></label>
                            </div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <div class="row">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-4">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-4">
                                Confirmar y Enviar
                            </button>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div><!-- 3RD SECTION ** END ** -->
        </div>
    </form>
</div>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/custom/mascotas.js'; ?>"></script>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>
