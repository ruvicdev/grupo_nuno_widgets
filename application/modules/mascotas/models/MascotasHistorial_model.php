<?php

/**
 * Model will contain all the Methods
 * we are going to use to do different
 * processes at the moment to try to
 * save information or get information
 * we are going to display for the user
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class MascotasHistorial_model extends CI_Model {

    /**
     * Constructor...
     */
    public function __construct() {
        parent::__construct();

        $this->table = "mascotas_historial";
    }

    /**
     * Method is going to be used to save
     * the data typed by the user and then use
     * the data to show them in the PDF report
     *
     * @param $array $data
     * @return $id integer
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData($data) {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }
}
