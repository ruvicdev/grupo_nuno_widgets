<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * INSURANCE OF MAYOR MEDICAL EXPENSES
 *
 * Class that contains all the functionality for
 * the Insurance of Major & Minor Medical Expenses Quota widget
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
Class CubreDeducibleGMM extends MY_Controller {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // load libraries
        $this->load->library('session');

        // load the models
        $this->load->model( array('Cubre_Deducible_Historial_Model',
                                  'Cubre_Deducible_Historial_Integrantes_Model',
                                  'External_Files_Cotizaciones_Model',
                                  'Cubre_Deducible_Variables_Model') );

        // create the object to export the PDF
        $this->pdf = new ExportFiles();

        // creates the email and send the data
        $this->emailSend = new EmailSend();

        // creates the template object
        $this->template = new Templates();

        // Load utils
        $this->load->library('utils/NumberToLetterConverter');
        $this->load->library('utils/GetDataFromExcelFile');
    }

    /**
     * Method will load all the information for the insurance quote
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function index() {
        $dataView = $this->__generateDropdowns();

        $content = $this->loadSingleView('index', $dataView, TRUE);
        $data    = $this->__setArray('content', $content);
        $data    = $this->__setArray('title', 'Cubre tu Deducible GMM');

        $this->loadTemplate($data);
    }

    /**
     * Method will calculate the quotation with user data sent
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function previewData() {
        $data = json_decode(file_get_contents('php://input'), true);

        $nombre                 = $data["fullname"];
        $email                  = $data["email"];
        $telefono               = $data["telefono"];
        $estado                 = $data["estado"];
        $ciudad                 = $data["ciudad"];
        $finVigencia            = $data["finVigencia"];
        $deduciblePoliza        = $data["deduciblePoliza"];
        $coberturaExtranjero    = $data["coberturaExtranjero"];
        $coberturaDeducible     = $data["coberturaDeducible"];
        $emergenciaExtranjero   = $data["emergenciaExtranjero"];
        $emergenciaDeducible    = $data["emergenciaDeducible"];
        $integrantesArray       = $data["integrantes"]; //Array of integrantes, integrante, nombre, sexo, edad, prima_neta_anual

        //Save deducibles
        $arrayDeduciblesCapturados = array(
          "deduciblePoliza"     => $deduciblePoliza,
          "coberturaDeducible"  => $coberturaDeducible,
          "emergenciaDeducible" => $emergenciaDeducible
        );

        //Get the amount per year of each integrante
        $integrantesArrayPrimaNetaAnual = $this->getPrimaNetaAnualPerIntegrante($arrayDeduciblesCapturados, $integrantesArray);

        //Build the response
        $response = array('status'           => 'success',
                          'integrantesArray' => $integrantesArrayPrimaNetaAnual);

        // create json response
        echo json_encode($response);
    }

    /**
     * Method that get the amount per year for each integrante
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getPrimaNetaAnualPerIntegrante($arrayDeduciblesCapturados, $integrantesArray) {
        //Get data from DB or excel file depend on the date of the excel file
        $array_excelFile_DB_Data = $this->retrieveDataFromExcelFile();

        // *** "Cotizador Basica" *** Tabla de Primas de Riesgo por Edad
        $array_CotizadorBasica_Primas_de_Riesgo_x_Edad = $this->getPrimaRiesgoXEdad($array_excelFile_DB_Data, $arrayDeduciblesCapturados, "cotizador_basica");
        // *** "Cotizador Cobertura Ext" *** Tabla de Primas de Riesgo por Edad
        $array_CotizadorCoberturaExt_Primas_de_Riesgo_x_Edad = $this->getPrimaRiesgoXEdad($array_excelFile_DB_Data, $arrayDeduciblesCapturados, "cotizador_cobertura_ext");
        // *** "Cotizador Emergencia Ext" *** Tabla de Primas de Riesgo por Edad
        $array_CotizadorEmergenciaExt_Primas_de_Riesgo_x_Edad = $this->getPrimaRiesgoXEdad($array_excelFile_DB_Data, $arrayDeduciblesCapturados, "cotizador_emergencia_ext");
        // *** "Factor_extranjero" *** Rango de Edades
        $array_FactorRecargo_x_Edad = $this->getFactorRecargoXEdad($array_excelFile_DB_Data);

        // *** "Cotizacion" *** Get Prima de Riesgo per $integrantesArray of cotizacion sheet
        $integrantesArrayPrimaNetaAnual = [];
        $i = 0;
        foreach ($integrantesArray as $value) {
            $edad = $value['edad'];

            //Get Prima de Riesgo table data
            $coberturaDeducible  = $arrayDeduciblesCapturados['coberturaDeducible'] == null? 0 : $arrayDeduciblesCapturados['coberturaDeducible'];
            $emergenciaDeducible = $arrayDeduciblesCapturados['emergenciaDeducible'] == null? 0 : $arrayDeduciblesCapturados['emergenciaDeducible'];

            $prima_de_riesgo_basica_prima                        = $this->getPrimaRiesgo($array_CotizadorBasica_Primas_de_Riesgo_x_Edad, $edad);
            //If $coberturaDeducible is not equal to Zero, get $prima_de_riesgo_cobertura_extranjero_prima_basica if not return 0
            $prima_de_riesgo_cobertura_extranjero_prima_basica   = $coberturaDeducible == 0 ? 0 : $this->getPrimaRiesgo($array_CotizadorCoberturaExt_Primas_de_Riesgo_x_Edad, $edad);
            $prima_de_riesgo_cobertura_extranjero_factor         = $this->getPrimaRiesgoFactor($array_FactorRecargo_x_Edad, $edad, 'factor_recargo_cobertura_extranjero_round');
            $prima_de_riesgo_cobertura_extranjero_prima_cob_ext  = $prima_de_riesgo_cobertura_extranjero_prima_basica * ($prima_de_riesgo_cobertura_extranjero_factor - 1);
            //If $emergenciaDeducible is not equal to Zero, get $prima_de_riesgo_emergencia_extranjero_prima_basica if not return 0
            $prima_de_riesgo_emergencia_extranjero_prima_basica  = $emergenciaDeducible == 0 ? 0 :$this->getPrimaRiesgo($array_CotizadorEmergenciaExt_Primas_de_Riesgo_x_Edad, $edad);
            $prima_de_riesgo_emergencia_extranjero_factor        = $this->getPrimaRiesgoFactor($array_FactorRecargo_x_Edad, $edad, 'Factor_recargo_emergencia_extranjero_round');
            $prima_de_riesgo_emergencia_extranjero_prima_eme_ext = $prima_de_riesgo_emergencia_extranjero_prima_basica * ($prima_de_riesgo_emergencia_extranjero_factor - 1);

            //Get Prima de Tarifa table data
            $gastoAdministracion = $array_excelFile_DB_Data[0]['cotizacion_gasto_administracion'];
            $gastoAdquisicion    = $array_excelFile_DB_Data[0]['cotizacion_gasto_adquisicion'];
            $utilidad            = $array_excelFile_DB_Data[0]['cotizacion_utilidad'];

            $prima_de_tarifa_basica_prima                        = $prima_de_riesgo_basica_prima / (1 - $gastoAdministracion - $gastoAdquisicion - $utilidad);
            $prima_de_tarifa_cobertura_extranjero_prima_cob_ext  = $prima_de_riesgo_cobertura_extranjero_prima_cob_ext / (1 - $gastoAdministracion - $gastoAdquisicion - $utilidad);
            $prima_de_tarifa_emergencia_extranjero_prima_eme_ext = $prima_de_riesgo_emergencia_extranjero_prima_eme_ext / (1 - $gastoAdministracion - $gastoAdquisicion - $utilidad);

            $integrantesArray[$i]['prima_neta_anual'] = $prima_de_tarifa_basica_prima + $prima_de_tarifa_cobertura_extranjero_prima_cob_ext + $prima_de_tarifa_emergencia_extranjero_prima_eme_ext;
            $integrantesArrayPrimaNetaAnual[$i] = $integrantesArray[$i];

            $i ++;
        }

        return $integrantesArrayPrimaNetaAnual;
    }

    /**
     * Method that get "Cotizacion" -> "PRIMA DE RIESGO" table data
     *
     * @return integer
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getPrimaRiesgo($array_Primas_de_Riesgo_x_Edad, $edad) {
        return $array_Primas_de_Riesgo_x_Edad["'".$edad."'"];

    }

    /**
     * Method that get "Factor_extranjero" -> "FACTOR" table data
     *
     * @return integer
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getPrimaRiesgoFactor($array_FactorRecargo_x_Edad, $edad, $tipoFactor) {
        $array_FactorRecargoEdadRango = $array_FactorRecargo_x_Edad[$this->rangeEdad($edad)];
        return $array_FactorRecargoEdadRango[$tipoFactor];
    }

    private function rangeEdad($edad) {
      if ($edad >= 0 && $edad <= 25) {
        return 0;
      }

      if ($edad >= 26 && $edad <= 50) {
        return 26;
      }

      if ($edad >= 51 && $edad <= 75) {
        return 51;
      }

      if ($edad >= 76 ) {
        return 76;
      }
    }

    /**
     * Method that set data from excel file to DB, this only if the excel file was modified
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function retrieveDataFromExcelFile() {
        //Get values excel files ----------------------------------------------------
        $array_variables_return = [];
        $cotizadorName          = 'cubretudeducible';
        $fileName               = '20170309_COTIZADOR_deducible_v1_2_prorrata_3_sisnova.xlsx';

        //Instance class GetDataFromExcelFile
        $getDataFromExcelFile = new GetDataFromExcelFile($fileName);

        //Get timestamp modified of excel file name in order to compare it with the database
        $fileTimestampModified = $getDataFromExcelFile->getFileModified();

        //Get data from DB
        $arrayExternalFilesData = $this->External_Files_Cotizaciones_Model->getExternalFilesData($cotizadorName);

        //Get id and 'archivo_modificado'
        $id            = -1;
        $file_modified = 0;
        if (! empty($arrayExternalFilesData)) {
            $id            = $arrayExternalFilesData[0]['id'];
            $file_modified = $arrayExternalFilesData[0]['archivo_modificado'];
        }

        //date to timestamp
        $dbTimestampModified = strtotime($file_modified);

        //Compare timestamps between database record and the file
        //Get data from excel and save it into DB, if DateTimeModified from database is SMALLER than DateTimeModified from excel file
        if ($file_modified == 0 || $dbTimestampModified < $fileTimestampModified) {
            //update estatus to 'CANCELADO' of the old record if exist
            if ($id != 1) {
                $arrayToUpdateTable = array('estatus' => 'CANCELADO');
                $this->External_Files_Cotizaciones_Model->updateData($arrayToUpdateTable, $id);
            }

            //Create the new record with status 'ACTIVO'
            $arrayToInsertTable = array(
                                        'nombre_cotizador'   => $cotizadorName,
                                        'archivo_modificado' => date('Y-m-d H:i:s', $fileTimestampModified),
                                        'estatus'            => 'ACTIVO'
                                        );

            $externalFilesCotizacionesID = $this->External_Files_Cotizaciones_Model->insertData($arrayToInsertTable);

            //Array that contains variables and coordenates from excel fiels, and those name of variables will be used for insert them into DB
            $array_coordinates =
                   array('Cotizador Basica'         => 'cotizador_basica_no_reclamos_promedio_0_25:9-13,
                                                        cotizador_basica_no_asegurados_promedio_0_25:9-19,
                                                        cotizador_basica_ex_0_25:12-11,
                                                        cotizador_basica_coaseguro_0_25:2-2,
                                                        cotizador_basica_no_reclamos_promedio_26_50:9-28,
                                                        cotizador_basica_no_asegurados_promedio_26_50:9-34,
                                                        cotizador_basica_ex_26_50:12-26,
                                                        cotizador_basica_coaseguro_26_50:2-3,
                                                        cotizador_basica_no_reclamos_promedio_51_75:9-43,
                                                        cotizador_basica_no_asegurados_promedio_51_75:9-49,
                                                        cotizador_basica_ex_51_75:12-41,
                                                        cotizador_basica_coaseguro_51_75:2-4,
                                                        cotizador_basica_no_reclamos_promedio_76_MAS:9-58,
                                                        cotizador_basica_no_asegurados_promedio_76_MAS:9-64,
                                                        cotizador_basica_ex_76_MAS:12-56,
                                                        cotizador_basica_coaseguro_76_MAS:2-5,
                                                        cotizador_basica_x_0_25:17-3,
                                                        cotizador_basica_x_26_50:17-4,
                                                        cotizador_basica_x_51_75:17-5,
                                                        cotizador_basica_x_76_MAS:17-6,
                                                        cotizador_basica_n:18-9',
                         'Cotizador Cobertura Ext'  => 'cotizador_cobertura_ext_no_reclamos_promedio_0_25:9-13,
                                                        cotizador_cobertura_ext_no_asegurados_promedio_0_25:9-19,
                                                        cotizador_cobertura_ext_ex_0_25:12-11,
                                                        cotizador_cobertura_ext_coaseguro_0_25:2-2,
                                                        cotizador_cobertura_ext_no_reclamos_promedio_26_50:9-28,
                                                        cotizador_cobertura_ext_no_asegurados_promedio_26_50:9-34,
                                                        cotizador_cobertura_ext_ex_26_50:12-26,
                                                        cotizador_cobertura_ext_coaseguro_26_50:2-3,
                                                        cotizador_cobertura_ext_no_reclamos_promedio_51_75:9-43,
                                                        cotizador_cobertura_ext_no_asegurados_promedio_51_75:9-49,
                                                        cotizador_cobertura_ext_ex_51_75:12-41,
                                                        cotizador_cobertura_ext_coaseguro_51_75:2-4,
                                                        cotizador_cobertura_ext_no_reclamos_promedio_76_MAS:9-58,
                                                        cotizador_cobertura_ext_no_asegurados_promedio_76_MAS:9-64,
                                                        cotizador_cobertura_ext_ex_76_MAS:12-56,
                                                        cotizador_cobertura_ext_coaseguro_76_MAS:2-5,
                                                        cotizador_cobertura_ext_x_0_25:17-3,
                                                        cotizador_cobertura_ext_x_26_50:17-4,
                                                        cotizador_cobertura_ext_x_51_75:17-5,
                                                        cotizador_cobertura_ext_x_76_MAS:17-6,
                                                        cotizador_cobertura_ext_n:18-9',
                         'Cotizador Emergencia Ext' => 'cotizador_emergencia_ext_no_reclamos_promedio_0_25:9-13,
                                                        cotizador_emergencia_ext_no_asegurados_promedio_0_25:9-19,
                                                        cotizador_emergencia_ext_ex_0_25:12-11,
                                                        cotizador_emergencia_ext_coaseguro_0_25:2-2,
                                                        cotizador_emergencia_ext_no_reclamos_promedio_26_50:9-28,
                                                        cotizador_emergencia_ext_no_asegurados_promedio_26_50:9-34,
                                                        cotizador_emergencia_ext_ex_26_50:12-26,
                                                        cotizador_emergencia_ext_coaseguro_26_50:2-3,
                                                        cotizador_emergencia_ext_no_reclamos_promedio_51_75:9-43,
                                                        cotizador_emergencia_ext_no_asegurados_promedio_51_75:9-49,
                                                        cotizador_emergencia_ext_ex_51_75:12-41,
                                                        cotizador_emergencia_ext_coaseguro_51_75:2-4,
                                                        cotizador_emergencia_ext_no_reclamos_promedio_76_MAS:9-58,
                                                        cotizador_emergencia_ext_no_asegurados_promedio_76_MAS:9-64,
                                                        cotizador_emergencia_ext_ex_76_MAS:12-56,
                                                        cotizador_emergencia_ext_coaseguro_76_MAS:2-5,
                                                        cotizador_emergencia_ext_x_0_25:17-3,
                                                        cotizador_emergencia_ext_x_26_50:17-4,
                                                        cotizador_emergencia_ext_x_51_75:17-5,
                                                        cotizador_emergencia_ext_x_76_MAS:17-6,
                                                        cotizador_emergencia_ext_n:18-9',
                         'Factor_extranjero'        => 'factor_extranjero_total_estadistica_promedio_grupo_0_25:1-4,
                                                        factor_extranjero_unicamente_extranjero_promedio_grupo_0_25:3-4,
                                                        factor_extranjero_total_estadistica_promedio_grupo_26_50:1-12,
                                                        factor_extranjero_unicamente_extranjero_promedio_grupo_26_50:3-12,
                                                        factor_extranjero_total_estadistica_promedio_grupo_51_75:1-20,
                                                        factor_extranjero_unicamente_extranjero_promedio_grupo_51_75:3-20,
                                                        factor_extranjero_total_estadistica_promedio_grupo_76_MAS:1-28,
                                                        factor_extranjero_unicamente_extranjero_promedio_grupo_76_MAS:3-28',
                         'Cotizacion'               => 'cotizacion_gasto_administracion:14-40,
                                                        cotizacion_gasto_adquisicion:14-41,
                                                        cotizacion_utilidad:14-42'
                        );

            //Get data from excel file depend on $array_coordinates
            $array_coordinates_results = $getDataFromExcelFile->get_value_worksheet($array_coordinates);

            //Add variables to insert
            $array_coordinates_results["external_files_cotizaciones_id"] = $externalFilesCotizacionesID;
            $array_coordinates_results["date_created"] = date("Y-m-d H:i:s");

            //Insert data to from $array_coordinates_results to DB table
            $this->Cubre_Deducible_Variables_Model->insertData($array_coordinates_results);
        }

        //Get variables from DB
        return $this->Cubre_Deducible_Variables_Model->getData();
    }

    /**
     * Method that retireve factor recargo per age range
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getFactorRecargoXEdad($array_excelFile_DB_Data) {
      $factor_extranjero_total_estadistica_promedio_grupo_0_25       = $array_excelFile_DB_Data[0]["factor_extranjero_total_estadistica_promedio_grupo_0_25"];
      $factor_extranjero_unicamente_extranjero_promedio_grupo_0_25   = $array_excelFile_DB_Data[0]["factor_extranjero_unicamente_extranjero_promedio_grupo_0_25"];
      $factor_extranjero_total_estadistica_promedio_grupo_26_50      = $array_excelFile_DB_Data[0]["factor_extranjero_total_estadistica_promedio_grupo_26_50"];
      $factor_extranjero_unicamente_extranjero_promedio_grupo_26_50  = $array_excelFile_DB_Data[0]["factor_extranjero_unicamente_extranjero_promedio_grupo_26_50"];
      $factor_extranjero_total_estadistica_promedio_grupo_51_75      = $array_excelFile_DB_Data[0]["factor_extranjero_total_estadistica_promedio_grupo_51_75"];
      $factor_extranjero_unicamente_extranjero_promedio_grupo_51_75  = $array_excelFile_DB_Data[0]["factor_extranjero_unicamente_extranjero_promedio_grupo_51_75"];
      $factor_extranjero_unicamente_extranjero_promedio_grupo_76_MAS = $array_excelFile_DB_Data[0]["factor_extranjero_unicamente_extranjero_promedio_grupo_76_MAS"];
      $factor_extranjero_total_estadistica_promedio_grupo_76_MAS     = $array_excelFile_DB_Data[0]["factor_extranjero_total_estadistica_promedio_grupo_76_MAS"];

      //0-25 UNISEX
      $sin_nombre_0_25                    = $factor_extranjero_unicamente_extranjero_promedio_grupo_0_25 / $factor_extranjero_total_estadistica_promedio_grupo_0_25;
      $emergencia_extranjero_20_perc_0_25 = 0.2 * $sin_nombre_0_25;
      $cobertura_extranjero_80_perc_0_25  = 0.8 * $sin_nombre_0_25;

      //26-50 UNISEX
      $sin_nombre_26_50                    = $factor_extranjero_unicamente_extranjero_promedio_grupo_26_50 / $factor_extranjero_total_estadistica_promedio_grupo_26_50;
      $emergencia_extranjero_20_perc_26_50 = 0.2 * $sin_nombre_26_50;
      $cobertura_extranjero_80_perc_26_50  = 0.8 * $sin_nombre_26_50;

      //51-75 UNISEX
      $sin_nombre_51_75                    = $factor_extranjero_unicamente_extranjero_promedio_grupo_51_75 / $factor_extranjero_total_estadistica_promedio_grupo_51_75;
      $emergencia_extranjero_20_perc_51_75 = 0.2 * $sin_nombre_51_75;
      $cobertura_extranjero_80_perc_51_75  = 0.8 * $sin_nombre_51_75;

      //76 - + UNISEX
      $sin_nombre_76_MAS                    = $factor_extranjero_unicamente_extranjero_promedio_grupo_76_MAS / $factor_extranjero_total_estadistica_promedio_grupo_76_MAS;
      $emergencia_extranjero_20_perc_76_MAS = 0.2 * $sin_nombre_76_MAS;
      $cobertura_extranjero_80_perc_76_MAS  = 0.8 * $sin_nombre_76_MAS;

      //Get factores per age
      $arrayFactoresXEdad_0_25 ['factor_recargo_emergencia_extranjero']       = $emergencia_extranjero_20_perc_0_25 + 1;
      $arrayFactoresXEdad_0_25 ['factor_recargo_cobertura_extranjero']        = $cobertura_extranjero_80_perc_0_25 + 1;
      $arrayFactoresXEdad_0_25 ['Factor_recargo_emergencia_extranjero_round'] = round(($emergencia_extranjero_20_perc_0_25 + 1), 4);
      $arrayFactoresXEdad_0_25 ['factor_recargo_cobertura_extranjero_round']  = round(($cobertura_extranjero_80_perc_0_25 + 1), 4);

      $arrayFactoresXEdad_26_50 ['factor_recargo_emergencia_extranjero']       = $emergencia_extranjero_20_perc_26_50 + 1;
      $arrayFactoresXEdad_26_50 ['factor_recargo_cobertura_extranjero']        = $cobertura_extranjero_80_perc_26_50 + 1;
      $arrayFactoresXEdad_26_50 ['Factor_recargo_emergencia_extranjero_round'] = round(($emergencia_extranjero_20_perc_26_50 + 1), 4);
      $arrayFactoresXEdad_26_50 ['factor_recargo_cobertura_extranjero_round']  = round(($cobertura_extranjero_80_perc_26_50 + 1), 4);

      $arrayFactoresXEdad_51_75 ['factor_recargo_emergencia_extranjero']       = $emergencia_extranjero_20_perc_51_75 + 1;
      $arrayFactoresXEdad_51_75 ['factor_recargo_cobertura_extranjero']        = $cobertura_extranjero_80_perc_51_75 + 1;
      $arrayFactoresXEdad_51_75 ['Factor_recargo_emergencia_extranjero_round'] = round(($emergencia_extranjero_20_perc_51_75 + 1), 4);
      $arrayFactoresXEdad_51_75 ['factor_recargo_cobertura_extranjero_round']  = round(($cobertura_extranjero_80_perc_51_75 + 1), 4);

      $arrayFactoresXEdad_76_MAS ['factor_recargo_emergencia_extranjero']       = $emergencia_extranjero_20_perc_76_MAS + 1;
      $arrayFactoresXEdad_76_MAS ['factor_recargo_cobertura_extranjero']        = $cobertura_extranjero_80_perc_76_MAS + 1;
      $arrayFactoresXEdad_76_MAS ['Factor_recargo_emergencia_extranjero_round'] = round(($emergencia_extranjero_20_perc_76_MAS + 1), 4);
      $arrayFactoresXEdad_76_MAS ['factor_recargo_cobertura_extranjero_round']  = round(($cobertura_extranjero_80_perc_76_MAS + 1), 4);

      //Save array that contain al factors depends on age range
      $arrayFactoresRecargoData ['0']  = $arrayFactoresXEdad_0_25;
      $arrayFactoresRecargoData ['26'] = $arrayFactoresXEdad_26_50;
      $arrayFactoresRecargoData ['51'] = $arrayFactoresXEdad_51_75;
      $arrayFactoresRecargoData ['76'] = $arrayFactoresXEdad_76_MAS;

      return $arrayFactoresRecargoData;
    }

    /**
     * Method that get Cotizador Basica Data
     *
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getPrimaRiesgoXEdad($array_excelFile_DB_Data, $arrayDeduciblesCapturados, $nombreHojaExcelPrimaRiesgo) {
      $deducibleCapturado = 0.0;

      switch ($nombreHojaExcelPrimaRiesgo) {
        case 'cotizador_basica':
          $deducibleCapturado = $arrayDeduciblesCapturados["deduciblePoliza"];
          break;
        case 'cotizador_cobertura_ext':
          $deducibleCapturado = $arrayDeduciblesCapturados["coberturaDeducible"];
          break;
        case 'cotizador_emergencia_ext':
          $deducibleCapturado = $arrayDeduciblesCapturados["emergenciaDeducible"];
          break;
      }

      //Calculo de la Prima de Riesgo Anual por Persona Asegurada----------------
      //*****0-25, 26-50, 51-75, 76-+ UNISEX AÑOS*****
      $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_0_25   = $this->getRiesgoAnualPersonaAseg_0_25($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo);
      $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_26_50  = $this->getRiesgoAnualPersonaAseg_26_50($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo);
      $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_51_75  = $this->getRiesgoAnualPersonaAseg_51_75($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo);
      $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_76_MAS = $this->getRiesgoAnualPersonaAseg_76_MAS($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo);

      //Obtener - Primas  de riesgo por edad (años cumpidos)
      $y_0_25   = $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_0_25;
      $y_26_50  = $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_26_50;
      $y_51_75  = $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_51_75;
      $y_76_MAS = $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_76_MAS;

      //X=x
      $X_0_25   = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_x_0_25"];
      $X_26_50  = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_x_26_50"];
      $X_51_75  = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_x_51_75"];
      $X_76_MAS = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_x_76_MAS"];
      $X_SUM    = $X_0_25 + $X_26_50 + $X_51_75 + $X_76_MAS;

      //Y = log(y) logarithm of y
      $Y_0_25   = log($y_0_25);
      $Y_26_50  = log($y_26_50);
      $Y_51_75  = log($y_51_75);
      $Y_76_MAS = log($y_76_MAS);
      $Y_SUM    = $Y_0_25 + $Y_26_50 + $Y_51_75 + $Y_76_MAS;

      //x^2
      $x2_0_25   = pow($X_0_25, 2);
      $x2_26_50  = pow($X_26_50, 2);
      $x2_51_75  = pow($X_51_75, 2);
      $x2_76_MAS = pow($X_76_MAS, 2);
      $x2_SUM    = $x2_0_25 + $x2_26_50 + $x2_51_75 + $x2_76_MAS;

      //XY
      $XY_0_25   = $X_0_25   * $Y_0_25;
      $XY_26_50  = $X_26_50  * $Y_26_50;
      $XY_51_75  = $X_51_75  * $Y_51_75;
      $XY_76_MAS = $X_76_MAS * $Y_76_MAS;
      $XY_SUM    = $XY_0_25 + $XY_26_50 + $XY_51_75 + $XY_76_MAS;

      //n
      $n = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_n"];

      //B
      $B = ($X_SUM * $Y_SUM - $n * $XY_SUM) / (pow($X_SUM, 2) - $n * $x2_SUM);

      //A
      $A = ($XY_SUM - $B * $x2_SUM) / $X_SUM;

      //a
      $a = exp($A);

      //b
      $b = $B;

      //Get Primas de riesgo por edad (años cumpidos) tabla de edades y su prima
      $array_Primas_de_Riesgo_x_Edad = [];
      for ($i=0; $i <= 100; $i++) {
        $array_Primas_de_Riesgo_x_Edad["'".$i."'"] =  $a * EXP($b * $i);
      }

      return $array_Primas_de_Riesgo_x_Edad;
    }

    /**
     * Method that get $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_0_25
     *
     * @return decimal
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getRiesgoAnualPersonaAseg_0_25($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo) {
      $coaseguro_0_25              = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_coaseguro_0_25"];
      $no_reclamos_promedio_0_25   = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_reclamos_promedio_0_25"];
      $no_asegurados_promedio_0_25 = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_asegurados_promedio_0_25"];
      $ex_0_25                     = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_ex_0_25"];
      $lambda_0_25                 = 1/$ex_0_25;
      $en_0_25                     = $no_reclamos_promedio_0_25;

      //Calculo de la función de distribución acumulada
      $d_P_X_d_1_coa_0_25 = (1-(1-exp(-$lambda_0_25 * $deducibleCapturado))) * $deducibleCapturado * (1-$coaseguro_0_25);

      //Calculo de la Prima de Riesto Anual por Persona Asegurada de 0-25 years
      $es_0_25 = $en_0_25 * $d_P_X_d_1_coa_0_25;

      //Get Prima de Riesto Anual por Persona Asegurada 0-25 años
      return $es_0_25 / $no_asegurados_promedio_0_25;
    }

    /**
     * Method that get $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_26_50
     *
     * @return decimal
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getRiesgoAnualPersonaAseg_26_50($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo) {
      $coaseguro_26_50              = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_coaseguro_26_50"];
      $no_reclamos_promedio_26_50   = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_reclamos_promedio_26_50"];
      $no_asegurados_promedio_26_50 = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_asegurados_promedio_26_50"];
      $ex_26_50                     = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_ex_26_50"];
      $lambda_26_50                 = 1/$ex_26_50;
      $en_26_50                     = $no_reclamos_promedio_26_50;

      //Calculo de la función de distribución acumulada
      $d_P_X_d_1_coa_26_50 = (1-(1-exp(-$lambda_26_50 * $deducibleCapturado))) * $deducibleCapturado * (1-$coaseguro_26_50);

      //Calculo de la Prima de Riesto Anual por Persona Asegurada de 26 - 50 years
      $es_26_50 = $en_26_50 * $d_P_X_d_1_coa_26_50;

      //Get Prima de Riesto Anual por Persona Asegurada 26-50 años
      return $es_26_50 / $no_asegurados_promedio_26_50;
    }

    /**
     * Method that get $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_51_75
     *
     * @return decimal
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getRiesgoAnualPersonaAseg_51_75($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo) {
      $coaseguro_51_75              = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_coaseguro_51_75"];
      $no_reclamos_promedio_51_75   = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_reclamos_promedio_51_75"];
      $no_asegurados_promedio_51_75 = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_asegurados_promedio_51_75"];
      $ex_51_75                     = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_ex_51_75"];
      $lambda_51_75                 = 1/$ex_51_75;
      $en_51_75                     = $no_reclamos_promedio_51_75;

      //Calculo de la función de distribución acumulada
      $d_P_X_d_1_coa_51_75 = (1-(1-exp(-$lambda_51_75 * $deducibleCapturado))) * $deducibleCapturado * (1-$coaseguro_51_75);

      //Calculo de la Prima de Riesto Anual por Persona Asegurada de 51 - 75 years
      $es_51_75 = $en_51_75 * $d_P_X_d_1_coa_51_75;

      //Get Prima de Riesto Anual por Persona Asegurada 26-50 años
      return $es_51_75 / $no_asegurados_promedio_51_75;
    }

    /**
     * Method that get $PRIMA_RIESGO_ANUAL_POR_PERSONA_ASEGURADA_76_MAS
     *
     * @return decimal
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function getRiesgoAnualPersonaAseg_76_MAS($array_excelFile_DB_Data, $deducibleCapturado, $nombreHojaExcelPrimaRiesgo) {
      $coaseguro_76_MAS              = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_coaseguro_76_MAS"];
      $no_reclamos_promedio_76_MAS   = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_reclamos_promedio_76_MAS"];
      $no_asegurados_promedio_76_MAS = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_no_asegurados_promedio_76_MAS"];
      $ex_76_MAS                     = $array_excelFile_DB_Data[0][$nombreHojaExcelPrimaRiesgo."_ex_76_MAS"];
      $lambda_76_MAS                 = 1/$ex_76_MAS;
      $en_76_MAS                     = $no_reclamos_promedio_76_MAS;

      //Calculo de la función de distribución acumulada
      $d_P_X_d_1_coa_76_MAS = (1-(1-exp(-$lambda_76_MAS * $deducibleCapturado))) * $deducibleCapturado * (1-$coaseguro_76_MAS);

      //Calculo de la Prima de Riesto Anual por Persona Asegurada de 76 - MAS years
      $es_76_MAS = $en_76_MAS * $d_P_X_d_1_coa_76_MAS;

      //Get Prima de Riesto Anual por Persona Asegurada 26-50 años
      return $es_76_MAS / $no_asegurados_promedio_76_MAS;
    }

    /**
     * Method will calculate the quotation with user data sent
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData() {
        $json_array = json_decode(file_get_contents('php://input'), true);

        //Save data into history tables------------------------------------------------------------------------------
        $arrayToInsertHistorial = array(
            'session'                    => session_id(),
            'vendedor'                   => $json_array['asesor'],
            'fullname'                   => $json_array['fullname'],
            'email'                      => $json_array['email'],
            'telefono'                   => $json_array['telefono'],
            'estado'                     => $json_array['estado'],
            'ciudad'                     => $json_array['ciudad'],
            'fin_vigencia'               => $json_array['finVigencia'],
            'deducible_poliza'           => $json_array['deduciblePoliza'],
            'cobertura_extranjero'       => $json_array['coberturaExtranjero']=='Amparada'?'SI':'NO',
            'cobertura_deducible'        => $json_array['coberturaDeducible'],
            'emergencia_extranjero'      => $json_array['emergenciaExtranjero']=='Amparada'?'SI':'NO',
            'emergencia_deducible'       => $json_array['emergenciaDeducible'],
            'fecha_creacion'             => date("Y-m-d H:i:s") );

        //Insert data into Cubre_Deducible_Historial table ------------------------------------------------------------
        $cubre_DeducibleHistorial_inserted_id =
            $this->Cubre_Deducible_Historial_Model->insertData($arrayToInsertHistorial);

        //Get MaxIntegrantesId data from Cubre_Deducible_Historial_Integrantes table
        $max_id_integrantes = $this->Cubre_Deducible_Historial_Integrantes_Model->getMaxIntegrantesId();

        $arrayToInsertIntegrantes = array();
        for ($i=0; $i < count( $json_array['integrantes'] ); $i++) {
            $arrayTemp = array(
                'id_integrantes'     => $max_id_integrantes,
                'integrante'         => $json_array['integrantes'] [$i] ['integrante'],
                'nombre'             => $json_array['integrantes'] [$i] ['nombre'],
                'sexo'               => $json_array['integrantes'] [$i] ['sexo'],
                'edad'               => $json_array['integrantes'] [$i] ['edad'],
                'prima_neta_anual'   => $json_array['integrantes'] [$i] ['prima_neta_anual'],
            );

            array_push($arrayToInsertIntegrantes, $arrayTemp);
        }

        $cubre_DeducibleHistorialIntegrantes_inserted_id =
            $this->Cubre_Deducible_Historial_Integrantes_Model->insertData($arrayToInsertIntegrantes);

        //Create relation between historial table and integrantes table
        $arrayToUpdateHistorial = array(
            'id_integrantes' => $max_id_integrantes);

        $this->Cubre_Deducible_Historial_Model->updateData($arrayToUpdateHistorial, $cubre_DeducibleHistorial_inserted_id);
        //End data insert-------------------------------------------------------------------------------------------

        // Generate the Template and init the PDF file creation-----------------------------------------------------
        //add id of historial into json_array in order to show it in PDF
        $json_array["cubre_DeducibleHistorial_inserted_id"] = $cubre_DeducibleHistorial_inserted_id;

        //Add Current date
        $json_array["fechaCotizacion"] = date("d/m/Y");

        $template = $this->template->cubreTuDeducibleTemplate($json_array);
        $PDF      = $this->pdf->__initPDFCubreTuDeducible($template, "Cotiza Cubre Deducible GMM", "Cotiza Cubre Deducible GMM");

        // Set PDF name and create PDF file
        $fileName = "cotizaCubre_Deducible_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

        // Create the file
        //ob_clean();
        $PDF->Output(FCPATH . 'public/files/' . $fileName, 'F');
        $completePath = FCPATH . 'public/files/' . $fileName;
        $returnedPath = base_url() . 'public/files/' . $fileName;
        //End of PDF creater----------------------------------------------------------------------------------------

        // Send the information via e-mail
        $this->emailSend->__initEmailCubreTuDeducible($json_array, $PDF, $completePath, "Cubre Tu Deducible GMM");

        // create json response
        $response = array('status'   => 'success',
                          'pathFile' => $returnedPath);

        echo json_encode($response);
    }

    /**
     * Method will generate all dropdown information
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __generateDropdowns() {
        $array_states      = array( 'Aguascalientes'                  => 'Aguascalientes',
                                    'Baja California'	              => 'Baja California',
                                    'Baja California Sur'             => 'Baja California Sur',
                                    'Chiapas'                         => 'Chiapas',
                                    'Chihuahua'	                      => 'Chihuahua',
                                    'Ciudad de Mexico'	              => 'Ciudad de Mexico',
                                    'Coahuila'	                      => 'Coahuila',
                                    'Colima'	                      => 'Colima',
                                    'Durango'                         => 'Durango',
                                    'Estado de Mexico'	              => 'Estado de Mexico',
                                    'Guanajuato'                      => 'Guanajuato',
                                    'Guerrero'                        => 'Guerrero',
                                    'Hidalgo'                         => 'Hidalgo',
                                    'Jalisco'                         => 'Jalisco',
                                    'Michoacan'                       => 'Michoacan',
                                    'Morelos'                         => 'Morelos',
                                    'Nayarit'                         => 'Nayarit',
                                    'Nuevo Leon'                      => 'Nuevo Leon',
                                    'Oaxaca'                          => 'Oaxaca',
                                    'Puebla'                          => 'Puebla',
                                    'Queretaro'                       => 'Queretaro',
                                    'Quintana Roo'                    => 'Quintana Roo',
                                    'San Luis Potosi'                 => 'San Luis Potosi',
                                    'Sinaloa'                         => 'Sinaloa',
                                    'Sonora'                          => 'Sonora',
                                    'Tabasco'                         => 'Tabasco',
                                    'Tamaulipas'                      => 'Tamaulipas',
                                    'Tlaxcala'                        => 'Tlaxcala',
                                    'Veracruz'                        => 'Veracruz',
                                    'Yucatan'                         => 'Yucatan',
                                    'Zacatecas'                       => 'Zacatecas' );

        $array_si_no       = array('SI' => 'Si',
                                   'NO' => 'No');

        $array_integrantes = array('0' => 'Titular',
                                   '1' => 'Conyuge',
                                   '2' => 'Hijo(a)');

        $array_sexo        = array('1' => 'Hombre',
                                   '0' => 'Mujer');

        $dataView          = array('array_states'      => $array_states,
                                   'array_si_no'       => $array_si_no,
                                   'array_integrantes' => $array_integrantes,
                                   'array_sexo'        => $array_sexo);

        return $dataView;
    }
}
