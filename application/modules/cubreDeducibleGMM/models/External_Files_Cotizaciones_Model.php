<?php

/**
 * Class that contains all the methods will be used
 * for saving information for the user in the database
 * and will contains a historical of the users cotizations
 * done in this Club Salud widget
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class External_Files_Cotizaciones_Model extends CI_Model {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // name of the table
        $this->table = "external_files_cotizaciones";
    }

    /**
     * Save history data
     *
     * @param array $array
     * @return int id
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function insertData($array) {
        $this->db->insert($this->table, $array);
        return $this->db->insert_id();
    }

    /**
     * Update history data
     *
     * @param array $array
     * @return int id
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function updateData($dataArray, $id) {
        $this->db->where('id', $id);
        $this->db->update($this->table, $dataArray);
    }

    /**
     * Get file_modified and id from table
     *
     * @param nombreCotizador $string
     * @return array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getExternalFilesData($nombreCotizador) {
        $query_file_modified = "SELECT id, COALESCE(archivo_modificado, 0) AS archivo_modificado
                                FROM `external_files_cotizaciones`
                                WHERE estatus = 'ACTIVO'
                                AND nombre_cotizador = '".$nombreCotizador."'";

        $results = $this->db->query($query_file_modified);

        return $results->result_array();
    }
}
