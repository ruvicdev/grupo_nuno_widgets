<script type="text/x-handlebars-template" id="errors_messages">
    <div class="row">
        <div class="ui negative hidden message">
            <div class="header">
                Error
            </div>
            <p>
                Por favor, llene los campos requeridos
            </p>
            <p class="messageErrorMoney">
                {{data}}
            </p>
        </div>
    </div>
</script>

<div class="default-margin-top ui grid centered removePadding">
    <!-- SECTION FOR STEPS -->
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos Personales</div>
                <div class="description">Informacion Personal</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="file pdf outline icon"></i>
            <div class="content">
                <div class="title">Datos Poliza</div>
                <div class="description">Datos de la Poliza</div>
            </div>
        </div>
        <div class="disabled step" id="step-3">
            <i class="building icon"></i>
            <div class="content">
                <div class="title">Datos Empresa</div>
                <div class="description">Informacion de la Empresa</div>
            </div>
        </div>
        <div class="disabled step" id="step-4">
            <i class="dollar icon"></i>
            <div class="content">
                <div class="title">Cotizacion</div>
                <div class="description">Cotizacion Final</div>
            </div>
        </div>
        <div class="disabled step" id="step-5">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Cotizacion</div>
                <div class="description">Cotizacion - Seguro de Mascotas</div>
            </div>
        </div>
    </div>
</div>

<div class="ui grid centered">
    <!-- SECTION FOR FORMS -->
    <div class="row">
        <h1 class="ui header">
            Seguro de Proteccion Contra Actos Fraudulentos PyME
        </h1>
    </div>
    <div>&nbsp;</div>
    <form class="ui form">
        <div class="ui styled accordion">
            <!-- FIRST SECTION -->
            <div id="accordion-1" class="disabled-content1">
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos Asegurado
                </div>
                <div class="content active" id="content-1">
                    <div class="row" id="error_first_section"></div>
                    <div>&nbsp;</div>
                    <div class="fields">
                        <div class="seven wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" id="vendedor"
                                       class="">
                                <label class="bold_text_title">
                                    ¿Eres vendedor de Grupo Nu&ntilde;o?
                                </label>
                            </div>
                        </div>
                        <div class="nine wide field">
                            <input type="text" name="nombre_vendedor" placeholder="Nombre del Vendedor"
                               class="" id="nombre_vendedor">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Fecha:
                        </label>
                        <input type="text" name="fecha_cotizacion" placeholder="Fecha" class="required first_section"
                               disabled="disabled" value="<?php echo $currentDate; ?>" id="fecha_cotizacion">
                    </div>

                    <div class="fields">
                        <input type="hidden" id="tipo_persona">
                        <div class="five wide field">
                            <label class="left_align bold_text_title" for="tipo_persona">
                                Tipo de Persona:
                            </label>
                        </div>
                        <div class="five wide field sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="fisica"
                                       data-class="hidden-segment-value"
                                       data-index="1"
                                       class="checkdata tperson required first_section">
                                <label>
                                    Persona Fisica
                                </label>
                            </div>
                        </div>
                        <div class="five wide field sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="moral"
                                       data-class="hidden-segment-value"
                                       data-index="2"
                                       class="checkdata tperson required first_section">
                                <label>
                                    Persona Moral
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="ui segment hidden-segment-value" data-value="moral" id="moral">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Empresa:
                            </label>
                            <input type="text" name="empresa" id="empresa" class="" placeholder="Empresa">
                        </div>
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre del Contacto:
                            </label>
                            <input type="text" name="nombre_contacto" id="nombre_contacto" class="" placeholder="Nombre del Contacto">
                        </div>
                    </div>
                    <div class="field ui segment hidden-segment-value" data-value="fisica" id="fisica">
                        <label class="left_align bold_text_title">
                            Nombre del Asegurado
                        </label>
                        <input type="text" name="nombre_asegurado" placeholder="Nombre del Asegurado"
                               class="" id="nombre_asegurado">
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Email
                        </label>
                        <input type="text" name="email" placeholder="Email"
                               class="required first_section" id="email">
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Telefono
                        </label>
                        <input type="text" name="telefono" placeholder="Telefono"
                               class="required first_section" id="telefono">
                    </div>
                    <div class="field bold_text_title">
                        DIRECCION FISCAL
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Calle y N&uacute;mero:
                            </label>
                            <input type="text" name="calle_numero" placeholder="Calle y Numero" class="required first_section" id="calle_numero">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                RFC:
                            </label>
                            <input type="text" name="rfc" placeholder="RFC" class="required first_section" id="rfc">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Colonia:
                        </label>
                        <input type="text" name="colonia" placeholder="Colonia" class="required first_section" id="colonia">
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Delegacion / Municipio:
                            </label>
                            <input type="text" name="del_mun" class="required first_section" placeholder="Delegacion / Municipio" id="del_mun">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                C&oacute;digo Postal:
                            </label>
                            <input type="text" name="cp" class="required first_section" placeholder="Codigo Postal" id="cp">
                        </div>
                    </div>
                    <div class="row">
                        <button class="ui right floated positive button section-2-button">
                            Siguiente
                        </button>
                    </div>
                    <div>&nbsp;</div>
                </div>
            </div>
            <!-- FIRST SECTION **END** -->
            <!-- SECOND SECTION -->
            <div class="accordion-2" class="disabled-content">
                <div class="title" id="title-2">
                    <i class="dropdown icon" id="title-2"></i>
                    Datos Poliza
                </div>
                <div class="content" id="content-2">
                    <div class="row" id="error_second_section"></div>
                    <div>&nbsp;</div>
                    <div id="test_section_two">&nbsp;</div>
                    <div class="fields" id="second_section_focus">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Vigencia Desde:
                            </label>
                            <input type="text" name="vigencia_desde" id="vigencia_desde" class="required second_section"
                                   placeholder="Vigencia Desde">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Vigencia Hasta:
                            </label>
                            <input type="text" name="vigencia_hasta" id="vigencia_hasta" class="required second_section"
                                   placeholder="Vigencia Hasta" disabled="disabled">
                        </div>
                    </div>
                    <input type="hidden" name="renovacion" id="renovacion" class="required second_section" value="No">
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Forma de Pago:
                        </label>
                        <div class="ui selection dropdown forma_pago_main">
                            <input type="hidden" name="forma_pago" id="forma_pago" class="required second_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text currencyTextaPago">Formas de Pago</div>
                            <div class="menu">
                                <div class="item" data-value="contado" data-index="0">
                                    Contado
                                </div>
                                <div class="item" data-value="trimestral" data-index="1">
                                    Trimestral
                                </div>
                                <div class="item" data-value="semestral" data-index="2">
                                    Semestral
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Ingresos Anuales:
                            </label>
                            <label id="error_ingreso_maximo" class="left_align bold_text_title hidden-segment-value"
                                   style="color: #FF0000">
                                Ingreso Maximo Anual es de MXN $ 2,500,000
                            </label>
                            <input type="text" name="ingresos_anuales" id="ingresos_anuales" class="required second_section"
                                   placeholder="Ingresos Anuales">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                M&aacute;ximo
                            </label>
                            MXN <span id="maximo_dinero_sec_2"> $ 2,500,000.00</span>
                            <input type="hidden" name="maximo" id="maximo_dinero" value="1" class="required second_section">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                N&uacute;mero de Empleados:
                            </label>
                            <label id="error_count_employees" class="left_align bold_text_title hidden-segment-value"
                                   style="color: #FF0000">
                                Excedes el maximo de empleados
                            </label>
                            <input type="text" name="no_empleados" id="no_empleados" class="required second_section"
                                   placeholder="Numero de Empleados">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                M&aacute;ximo
                            </label>
                            <span>300</span> Empleados
                            <input type="hidden" name="maximo_empleados" id="maximo_empleados" value="300" class="required second_section">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                L&iacute;mite Requerido:
                            </label>
                            <div class="ui selection dropdown limite-requerido-main">
                                <input type="hidden" name="limite_requerido" id="limite_requerido" class="required second_section" value="0">
                                <i class="dropdown icon"></i>
                                <div class="default text currencyText">Limite Requerido</div>
                                <div class="menu">
                                    <?php foreach ($limits as $key => $values): ?>
                                        <div class="item" data-key="<?php echo $key; ?>" data-name="<?php echo $values; ?>">
                                            <?php echo $values; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="four wide field">
                            <label class="left_align bold_text_title">
                                Desde
                            </label>
                            MXN <span>$ 250,000.00</span>
                            <input type="hidden" id="desde" name="desde" value="250000.00" class="required second_section">
                        </div>
                        <div class="four wide field">
                            <label class="left_align bold_text_title">
                                Hasta
                            </label>
                            MXN <span>5,000,000.00</span>
                            <input type="hidden" name="hasta" id="hasta" value="5000000.00" class="required second_section">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Giro de la Empresa:
                        </label>
                        <div class="ui selection dropdown giro-empresa-main">
                            <input type="hidden" name="giro_empresa" id="giro_empresa" class="required second_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text">Giro de la Empresa</div>
                            <div class="menu">
                                <?php foreach($companies as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>" data-name="<?php echo $value; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Numero de Sucursales (MAX. 20 Sucursales):
                            <span class="hidden-segment-value" style="color: #FF0000" id="error_no_sucursales">Excede el numero total de sucursales</span>
                        </label>
                        <input type="text" name="no_sucursales" id="no_sucursales" class="required second_section" placeholder="Numero de Sucursales">
                    </div>
                    <div class="row">
                        <button class="ui right floated positive button section-3-button">
                            Siguiente
                        </button>
                        <button class="ui right floated negative button section-2-button-back">
                            Regresar
                        </button>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- SECOND SECTION **END** -->
            <!-- THIRD SECTION -->
            <div class="accordion-3" class="disabled-content">
                <div class="title" id="title-3">
                    <i class="dropdown icon"></i>
                    Informacion de la Empresa
                </div>
                <div class="content" id="content-3">
                    <div class="row" id="error_third_section"></div>
                    <div>&nbsp;</div>
                    <div id="test_section_three">&nbsp;</div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿Efect&uacute;a una auditoria de su oficina principal, sucursales y agencias, por lo menos una (1) vez en cada periodo de doce (12) meses?
                        </label>
                        <div class="ui selection dropdown auditoria-main">
                            <input type="hidden" name="auditoria" id="auditoria" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿Determina las funciones y deberes de cada empleado de tal manera que ninguno
                             le sea posible por si solo, controlar cualquier transaccion desde su principio hasta si fin?
                        </label>
                        <div class="ui selection dropdown transaccion-main">
                            <input type="hidden" name="transaccion" id="transaccion" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿El departamento que concilia las cuentas con los clientes es diferente al departamento que
                             recibe pagos de los clientes?
                        </label>
                        <div class="ui selection dropdown pagos-main">
                            <input type="hidden" name="pagos" id="pagos" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿Se realiza conteo de inventarios minimo una vez al a&ntilde;o?
                        </label>
                        <div class="ui selection dropdown conteo-main">
                            <input type="hidden" name="conteo" id="conteo" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿Se hacen conciliaciones bancarias como minimo una vez al mes?
                        </label>
                        <div class="ui selection dropdown conciliaciones-main">
                            <input type="hidden" name="conciliaciones" id="conciliaciones" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿Son las funciones segregadas de tal forma que ningun individuo pueda controlar
                             de principio a fin la firma de cheques o autorizacion de pagos sin la intervencion
                             de un tercero?
                        </label>
                        <div class="ui selection dropdown segregadas-main">
                            <input type="hidden" name="segregadas" id="segregadas" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿Son las funciones segregadas de tal forma que ningun individuo pueda controlar de principio a fin
                             las instrucciones de transferencia de fondos sin la intervencion de un tercero?
                        </label>
                        <div class="ui selection dropdown intervencion-main">
                            <input type="hidden" name="intervencion" id="intervencion" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            ¿El historial de siniestralidad o el conocimiento de algun siniestro que afecte a esta
                             poliza es nulo?
                        </label>
                        <div class="ui selection dropdown siniestro-main">
                            <input type="hidden" name="siniestro" id="siniestro" class="required third_section optionsAdd" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> -- </div>
                            <div class="menu">
                                <div class="item" data-value="Si" data-index="1">
                                    Si
                                </div>
                                <div class="item" data-value="No" data-index="0">
                                    No
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>&nbsp;</div>
                    <div class="row hidden-segment-value" style="color: #FF0000" id="label_historial">
                        Favor de referir esta cuenta al area correspondiente de Grupo Nu&ntilde;o
                    </div>
                    <div>&nbsp;</div>
                    <input type="hidden" name="controles" class="" id="controles" value="0">
                    <div class="field bold_text_title">
                        SINIESTRALIDAD
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            &Uacute;ltimo a&ntilde;o:
                        </label>
                        <div class="ui labeled input">
                            <label class="ui label">MXP $</label>
                            <input type="text" name="ultimo_ano" id="ultimo_ano" placeholder="Ultimo año"
                                   class="required third_section">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Pen&uacute;ltimo a&ntilde;o:
                        </label>
                        <div class="ui labeled input">
                            <label class="ui label">MXP $</label>
                            <input type="text" name="penultimo_ano" id="penultimo_ano" placeholder="Penultimo año"
                                   class="required third_section">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Antepen&uacute;ltimo a&ntilde;o:
                        </label>
                        <div class="ui labeled input">
                            <label class="ui label">MXP $</label>
                            <input type="text" name="antepenultimo_ano" id="antepenultimo_ano"
                                   placeholder="Antepenultimo año" class="required third_section">
                        </div>
                    </div>
                    <input type="hidden" name="porcentaje_comision" id="porcentaje_comision" class="" value="15">
                    <input type="hidden" name="apreciacion_riesgo" id="apreciacion_riesgo" class="" value="0">
                    <!-- GIRO TASA ADICIONAL -->
                    <input type="hidden" name="tasa_adicional" id="tasa_adicional" class="" value="0">
                    <input type="hidden" name="tienda_abarrotes" id="tienda_abarrotes" class="" value="0">
                    <input type="hidden" name="transportadores" id="transportadores" class="" value="0">
                    <input type="hidden" name="restaurantes" id="restaurantes" class="" value="0">
                    <input type="hidden" name="gasolineras" id="gasolineras" class="" value="0">
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Giros Excluidos
                        </label>
                        <div class="left_align">
                            <ul>
                                <li>Empresas relacionadas con procesamientos de datos o procesadores de datos</li>
                                <li>Negocios de IT ( Tecnología, asesoría, outsourcing, etc )</li>
                                <li>Empresas de empleos temporales</li>
                                <li>Casas de cambio</li>
                                <li>Casas de empe&ntilde;o</li>
                                <li>Transportadores de valores</li>
                                <li>Juegos de azar y loter&iacute;as</li>
                                <li>Joyer&iacute;as</li>
                                <li>Ventas de tarjetas de tel&eacute;fonos celulares</li>
                                <li>Instituciones Financieras, cualquier negocio financiero (Captaci&oacute;n, colocaci&oacute;n o transportaci&oacute;n
                                    de dinero o valores)</li>
                            </ul>
                        </div>
                    </div>
                    <!--div class="field bold_text_title">
                        GIRO CON TASA ADICIONAL
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            A las empresas que tengan los siguientes giros se les aplica una tasa adicional de:
                        </label>
                        <div class="ui selection dropdown tasa-adicional-main">
                            <input type="hidden" name="tasa_adicional" id="tasa_adicional" class="required third_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> - - </div>
                            <div class="menu">
                                <div class="item" data-value="25%" data-index="1">
                                    25%
                                </div>
                                <div class="item" data-value="50%" data-index="2">
                                    50%
                                </div>
                                <div class="item" data-value="75%" data-index="3">
                                    75%
                                </div>
                                <div class="item" data-value="100%" data-index="4">
                                    100%
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            I. Tiendas, abarrotes y supermecados:
                        </label>
                        <div class="ui selection dropdown tienda-abarrotes-main">
                            <input type="hidden" name="tienda_abarrotes" id="tienda_abarrotes" class="required third_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> - - </div>
                            <div class="menu">
                                <div class="item" data-value="1-0" data-index="1">
                                    N/A
                                </div>
                                <div class="item" data-value="2-1" data-index="2">
                                    Aplica
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            II. Transportadores de bienes:
                        </label>
                        <div class="ui selection dropdown transportadores-main">
                            <input type="hidden" name="transportadores" id="transportadores" class="required third_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> - - </div>
                            <div class="menu">
                                <div class="item" data-value="1-0" data-index="1">
                                    N/A
                                </div>
                                <div class="item" data-value="2-1" data-index="2">
                                    Aplica
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            III. Restaurantes, bares:
                        </label>
                        <div class="ui selection dropdown restaurantes-main">
                            <input type="hidden" name="restaurantes" id="restaurantes" class="required third_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> - - </div>
                            <div class="menu">
                                <div class="item" data-value="1-0" data-index="1">
                                    N/A
                                </div>
                                <div class="item" data-value="2-1" data-index="2">
                                    Aplica
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            IV. Gasolineras:
                        </label>
                        <div class="ui selection dropdown gasolineras-main">
                            <input type="hidden" name="gasolineras" id="gasolineras" class="required third_section" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text"> - - </div>
                            <div class="menu">
                                <div class="item" data-value="1-0" data-index="1">
                                    N/A
                                </div>
                                <div class="item" data-value="2-1" data-index="2">
                                    Aplica
                                </div>
                            </div>
                        </div>
                    </div -->
                    <div class="row">
                        <button class="ui right floated positive button section-4-button">
                            Siguiente
                        </button>
                        <button class="ui right floated negative button section-3-button-back">
                            Regresar
                        </button>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- THIRD SECTION **END** -->
            <!-- FOUR SECTION **START** -->
            <div class="accordion-4" class="disabled-content">
                <div class="title" id="title-4">
                    <i class="dropdown icon"></i>
                    Cotizaci&oacute;n
                </div>
                <div class="content" id="content-4">
                    <div class="row" id="error_four_section"></div>
                    <div>&nbsp;</div>
                    <div id="test_section_four">&nbsp;</div>
                    <div class="field hidden-segment-value">
                        <label class="left_align bold_text_title" id="detalle_siniestro"></label>
                        <textarea rows="5" id="detalles_siniestros" name="detalles_siniestros" class="four_section"></textarea>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Prima Neta:
                        </label>
                        <div class="ui labeled input">
                            <label class="ui label">MXP $</label>
                            <input type="text" name="prima_neta" id="prima_neta" disabled="disabled"
                                   placeholder="Prima Neta" class="required four_section">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Deducible (10% de la p&eacute;rdida con minimo de):
                        </label>
                        <div class="ui labeled input">
                            <label class="ui label">MXP $</label>
                            <input type="text" name="deducible" id="deducible" disabled="disabled"
                                   placeholder="Deducible" class="required four_section">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            Total:
                        </label>
                        <div class="ui labeled input">
                            <label class="ui label">MXP $</label>
                            <input type="text" name="total" id="total" disabled="disabled"
                                   placeholder="Total" class="required four_section">
                        </div>
                    </div>
                    <div class="row">
                        <button class="ui right floated positive button section-5-button">
                            Siguiente
                        </button>
                        <button class="ui right floated negative button section-4-button-back">
                            Regresar
                        </button>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- FOUR SECTION **END** -->
            <!-- FINAL SECTION **START** -->
            <div class="accordion-5" id="disabled-content">
                <div class="title" id="title-5">
                    <i class="dropdown icon"></i>
                    Preview
                </div>
                <div id="test_section_five">&nbsp;</div>
                <div class="content" id="content-5">
                    <div class="row">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-5">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-5">
                                Confirmar y Enviar
                            </button>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left;">
                                DATOS DEL ASEGURADO
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline">
                                <label class="bold_text_title" id="tipo_asegurado"></label>
                                <label id="nombre_asegurado_txt" class="light_text_desc"></label>
                            </div>
                            <div class="left_align field inline hidden-segment-value" id="contacto_name">
                                <label class="bold_text_title">Nombre del Contacto:</label>
                                <label id="nombre_contacto_txt" class="light_text_desc"></label>
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="row field bold_text_title" style="float: left;">
                                DIRECCION FISCAL
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Calle y N&uacute;mero:</label>
                                <label class="light_text_desc" id="calle_numero_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">RFC:</label>
                                <label class="light_text_desc" id="rfc_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Colonia:</label>
                                <label class="light_text_desc" id="colonia_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Delegacion / Municipio:</label>
                                <label class="light_text_desc" id="del_mun_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">C&oacute;digo Postal:</label>
                                <label class="light_text_desc" id="cp_txt"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left;">
                                DATOS DE POLIZA
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Vigencia desde:</label>
                                <label class="light_text_desc" id="vigencia_desde_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Vigencia hasta:</label>
                                <label class="light_text_desc" id="vigencia_hasta_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Renovaci&oacute;n:</label>
                                <label class="light_text_desc" id="renovacion_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Forma de Pago:</label>
                                <label class="light_text_desc" id="forma_pago_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Ingresos Anuales:</label>
                                <label class="light_text_desc" id="ingresos_anuales_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Maximo:</label>
                                <label class="light_text_desc" id="maximo_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">N&uacute;mero de Empleados:</label>
                                <label class="light_text_desc" id="no_empleados_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">M&aacute;ximo:</label>
                                <label class="light_text_desc" id="maximo_emp_txt"></label>
                            </div>
                            <div class="field inline left_align">
                                <label class="bold_text_title">Limite Requerido:</label>
                                <label class="light_text_desc" id="limite_req_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Desde:</label>
                                <label class="light_text_desc" id="desde_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Hasta:</label>
                                <label class="light_text_desc" id="hasta_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Giro de la Empresa:</label>
                                <label class="light_text_desc" id="giro_empresa_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">N&uacute;mero de Sucursales:</label>
                                <label class="light_text_desc" id="no_sucursales_txt"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left;">
                                INFORMACION DE LA EMPRESA
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿Efectúa una auditoria de su oficina principal, sucursales y agencias, por lo menos una
                                (1) vez en cada periodo de doce (12) meses?</label>
                                <label class="light_text_desc" id="auditoria_txt"></label>
                            </div>
                            <div class="left_align field inline">
                                <label class="bold_text_title">¿Determina las funciones y deberes de cada empleado de tal manera que ninguno le sea posible
                                por si solo, controlar cualquier transaccion desde su principio hasta si fin? </label>
                                <label class="light_text_desc" id="func_deberes_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿El departamento que concilia las cuentas con los clientes es diferente al departamento que
                                recibe pagos de los clientes? </label>
                                <label class="light_text_desc" id="cuentas_clientes_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿Se realiza conteo de inventarios minimo una vez al año?</label>
                                <label class="light_text_desc" id="conteo_inventarios_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿Se hacen conciliaciones bancarias como minimo una vez al mes?</label>
                                <label class="light_text_desc" id="conciliaciones_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿Son las funciones segregadas de tal forma que ningun individuo pueda controlar de principio a
                                fin la firma de cheques o autorizacion de pagos sin la intervencion de un tercero?</label>
                                <label class="light_text_desc" id="segregadas_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿Son las funciones segregadas de tal forma que ningun individuo pueda controlar de principio
                                a fin las instrucciones de transferencia de fondos sin la intervencion de un tercero? </label>
                                <label class="light_text_desc" id="segregadas_ind_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">¿El historial de siniestralidad o el conocimiento de algun siniestro que afecte a esta poliza
                                es nulo?</label>
                                <label class="light_text_desc" id="historial_sin_txt"></label>
                            </div>
                            <!--div class="field left_align inline">
                                <label class="bold_text_title">Controles:</label>
                                <label class="light_text_desc" id="controles_txt"></label>
                            </div-->
                            <div class="clear: both">&nbsp;</div>
                            <div class="row field bold_text_title" style="float: left;">
                                SINIESTRALIDAD
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">&Uacute;ltimo A&ntilde;o:</label>
                                <label class="light_text_desc" id="ultimo_ano_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Pen&uacute;ltimo A&ntilde;o:</label>
                                <label class="light_text_desc" id="penultimo_ano_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Antepen&uacute;ltimo A&ntilde;o:</label>
                                <label class="light_text_desc" id="antepenultimo_ano_txt"></label>
                            </div>
                            <!--div class="field left_align inline">
                                <label class="bold_text_title">Apreciaci&oacute;n del riesgo:</label>
                                <label class="light_text_desc" id="apreciacion_riesgo_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Porcentaje de Comisi&oacute;n:</label>
                                <label class="light_text_desc" id="porcentaje_comision_txt"></label>
                            </div-->
                            <!--div class="row field bold_text_title" style="float: left;">
                                GIRO CON TASA ADICIONAL
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">A las empresas que tengan los siguientes giros se les aplica una tasa adicional de:</label>
                                <label class="light_text_desc" id="tasa_adicional_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">I. Tiendas, abarrotes y supermercados:</label>
                                <label class="light_text_desc" id="tienda_abarrote_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">II. Transportadores de bienes:</label>
                                <label class="light_text_desc" id="trans_bienes_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">III. Restaurantes, bares:</label>
                                <label class="light_text_desc" id="restaurant_bar_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">IV. Gasolineras</label>
                                <label class="light_text_desc" id="gasolineras_txt"></label>
                            </div-->
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="column">
                        <div class="ui segment">
                            <div class="row field bold_text_title" style="float: left;">
                                COTIZACI&Oacute;N
                            </div>
                            <div class="clear: both">&nbsp;</div>
                            <div class="field left_align inline hidden-segment-value">
                                <label class="bold_text_title">Detalles del Siniestro:</label>
                                <label class="light_text_desc" id="detalles_siniestros_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Prima Neta:</label>
                                <label class="light_text_desc" id="prima_neta_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Deducible (10% de la p&eacute;rdida con minimo de):</label>
                                <label class="light_text_desc" id="deducible_10_txt"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Total:</label>
                                <label class="light_text_desc" id="total_txt"></label>
                            </div>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <div class="row">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-5">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-5">
                                Confirmar y Enviar
                            </button>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- FINAL SECTION ** -->
        </div>
    </form>
</div>

<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/custom/actosFraudulentos.js'; ?>"></script>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>
