<?php

/**
 * Class will be used to store the records that will
 * be got from the excel file. These percentages will
 * be ised when trying to do some calculation
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class ApreciacionActosFraudulentos_model extends CI_Model {

    /**
     * Constructor ....
     */
    public function __construct() {
        parent::__construct();

        $this->table = 'apreciacion_actos_fraudulentos';
    }

    /**
     * Method will be used to get the specific
     * percentage defined by the user in the
     * excel file once the file has been exported
     *
     * @param int $id
     * @return string $percentage
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getPercentagePerId($id) {
        $this->db->select('percentage')
                 ->where('id', $id)
                 ->where('status', 1);
        $query = $this->db->get($this->table);

        return $query->row();
    }
}
