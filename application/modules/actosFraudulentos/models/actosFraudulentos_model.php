<?php

/**
 * Class will contains all the information
 * that is related with the fraudulen acts
 * that are done in the company
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class ActosFraudulentos_model extends CI_Model {

    /**
     * Constructor ....
     */
    public function __construct() {
        parent::__construct();

        $this->table = "actos_fraudulentos";
    }

    /** 
     * Method used to store the information will be
     * added the data typed by the user and then use
     * the data to show them in the PDF file
     *
     * @param array $data
     * @return int $id
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
   	public function save($data) {
   		$this->db->insert($this->table, $data);

   		return $this->db->insert_id();
   	}
}
