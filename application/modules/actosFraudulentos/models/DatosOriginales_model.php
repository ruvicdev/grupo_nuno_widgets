<?php

/**
 * Class will contain the information of the
 * table to get data from table created in the
 * excel to create different calcs
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class DatosOriginales_model extends CI_Model {

    /**
     * Constructor....
     */
    public function __construct() {
        parent::__construct();
        $this->table = 'datos_originales';
    }
}
