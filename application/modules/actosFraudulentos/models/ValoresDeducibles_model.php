<?php

/**
 * Class will contain all the information
 * of the table will be used to made some
 * calculations that we are going to use
 * to get the deductible in the cotization
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class ValoresDeducibles_model extends CI_Model {

    /**
     * Constructor ....
     */
    public function __construct() {
        parent::__construct();

        $this->table = "valores_deducibles";
    }

    /**
     * Method will be used to get the data
     * depending the Id passed by parameter in the functions
     * that will return the information
     * requested by the system
     *
     * @param int $id
     * @return float $row
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getValuePerID($id) {
        $this->db->select('final_price')
                 ->where('id', $id)
                 ->where('status', 1);
        $query = $this->db->get($this->table);

        return $query->row();
    }

    /**
     * Method will return all the information
     * of the table to check what value should be applied in
     * the calulation is doing the system
     *
     * @param int $tipo_moneda
     * @return array $array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getAllData($tipo_moneda) {
        $this->db->where('status', 1)
                 -> where('tipo_moneda', $tipo_moneda);
        $query = $this->db->get($this->table);

        return $query;
    }
}
