<?php

/**
 * Class will contain the information of the
 * table to get data from table created in the
 * excel to create different calcs
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
 class DeducibleVariable_model extends CI_Model {

     /**
      * Constructor.....
      */
     public function __construct() {
         parent::__construct();

         $this->table = 'deducible_variable';
     }

     /**
      * Method will be used to get all
      * the information stores in the table
      * and that is enabled as well
      *
      * @return array $array
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function getAllData() {
         $this->db->where('status', 1);
         $query = $this->db->get($this->table);

         return $query;
     }
 }
