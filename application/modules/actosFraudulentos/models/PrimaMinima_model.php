<?php

/**
 * Class will contain the information of the
 * table to get data from table created in the
 * excel to create different calcs
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
 class PrimaMinima_model extends CI_Model {

     /**
      * Constructor.....
      */
     public function __construct() {
         parent::__construct();

         $this->table = 'prima_minima';
     }

     /**
      * Get the data depending the City is
      * passing as parameter to get the information
      * from the datatbase to do some calculations
      *
      * @param string $city
      * @return array $data
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function getDataByCity($city) {
         $this->db->where('region', $city)
                  ->where('status', 1);
         $query = $this->db->get($this->table);

         return $query->row();
     }
 }
