<?php

/**
 * Class that will contain all the Methods
 * will be used to get the information of the
 * table enabled to do some calcs.
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class DeducibleIngreso_model extends CI_Model {

    /**
     * Constructor ....
     */
    public function __construct() {
        parent::__construct();

        $this->table = 'deducible_ingresos';
    }

    /**
     * Methdo that will return all the
     * information related to the values
     * that will be checked to keep doing
     * the calcs to get the final result
     *
     * @return object $query
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getAllDeductibleProfit() {
        $this->db->where('status', 1);
        $query = $this->db->get($this->table);

        return $query;
    }
}
