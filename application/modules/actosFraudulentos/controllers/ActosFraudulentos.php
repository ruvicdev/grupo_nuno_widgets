<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Controller will be used to set all the values for
 * the widget used to create a cotization for fraudulent
 * acts. Once finished this, the form will display
 * all the information
 *
 * @author Ruben Alonso Cortes Mendoza
 * @version 1.0
 * @company ruvicdev
 */

class ActosFraudulentos extends MY_Controller {

	/**
	 * Constructor .....
	 */
	public function __construct() {
		parent::__construct();

		// load the model
        $this->load->model(array('ActosFraudulentos_model',
								 'ApreciacionActosFraudulentos_model',
							 	 'ValoresDeducibles_model',
							 	 'DeducibleIngreso_model',
							 	 'DeducibleVariable_model',
							 	 'PrimaMinima_model'));

        // create the object to export the PDF
        $this->pdf   = new ExportFiles();
        // creates the email and send the data
        $this->emailSend = new EmailSend();
        // creates the template object
        $this->template = new Templates();
	}

	/**
	 * Method will load the information for the
	 * cotization will do the user since the widget
	 * set in the main site
	 *
	 * @return void
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function index() {
		$arrayCompanies['companies']   = $this->giroEmpresa();
		$arrayCompanies['limits'] 	   = $this->limiteCantidad();
		$arrayCompanies['currentDate'] = date('d-m-Y H:i:s A');

		$content = $this->loadSingleView('index', $arrayCompanies, TRUE);
		$data    = $this->__setArray('content', $content);
		$data	 = $this->__setArray('title', 'Seguro de Proteccion Contra Actos Fraudulentos PyME');

		$this->loadTemplate($data);
	}

	/**
	 * Method will be used to save all the information
	 * in the database that will be used to display this
	 * information in the PDF report
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function saveData() {
		$json_array = json_decode(file_get_contents('php://input'), true);

		// Create the array will be used to save info in database
		$data = $this->__createArray($json_array);

		//var_dump($data);
		// Save the user information
		$id = $this->ActosFraudulentos_model->save($data);

		// Generate the template and init the PDF file creation
        $template = $this->template->actosFraudulentosTemplate($data, $id);
        $PDF      = $this->pdf->__initPDFFraudulentActs($template,
        											  	'Seguro de Proteccion Contra Actos Fraudulentos PyME',
        											  	'Seguro de Proteccion Contra Actos Fraudulentos PyME');

        // Set PDF name and create PDF file
        $name = "actosFraudulentos_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

        // Create the file
        $PDF->Output(FCPATH . 'public/files/' . $name, 'F');
        $completePath = FCPATH . 'public/files/' . $name;
        $returnedPath = base_url() . 'public/files/' . $name;

        // Send the informacion via email
        //$this->emailSend->__initEmailPets($json_array, $PDF, $completePath, "Seguro de Proteccion Contra Actos Fraudulentos PyME");

        // Create json will be returned
        $response =  array('flag'     => '1',
                           'pathFile' => $returnedPath);

        echo json_encode($response);
	}

	/**
	 * Method will contains all the information about
	 * what are the activities of the company and what
	 * are the products are manage
	 *
	 * @return array $array
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function giroEmpresa() {
		$array = array(
			'1'  => 'Agricultura',
			'2'  => 'Ganaderia',
			'7'  => 'Servicios para Agricultura',
			'8'  => 'Silvicultura',
			'9'  => 'Caza y Pesca',
			'10' => 'Minerales Metalicos',
			'11' => 'Antracita',
			'12' => 'Lignito y Hulla',
			'13' => 'Petroleo y Gas Natural',
			'14' => 'Mineria y Explotacion de Minerales no Metalicos',
			'15' => 'Construcciones de Obras',
			'16' => 'Construcciones Pesadas',
			'17' => 'Contratistas Especializados',
			'20' => 'Indumentaria Alimentaria',
			'21' => 'Fabricantes de Tabaco',
			'22' => 'Industria Textil',
			'23' => 'Prendas Confeccionadas',
			'24' => 'Industria de la Madera',
			'25' => 'Mueble y Mobiliario',
			'26' => 'Papel y Derivados',
			'27' => 'Editorial - Artes Graficas',
			'28' => 'Productos Quimicos',
			'29' => 'Petroleo y sus Derivados',
			'30' => 'Productos de Goma y Plasticos',
			'31' => 'Cuero y Derivados',
			'32' => 'Productos de Piedra, Arcilla, Vidrio y Hormigon',
			'33' => 'Siderurgia',
			'34' => 'Fabricacion de Metal excepto Maquinaria y Equipo de Transporte',
			'35' => 'Maquinaria',
			'36' => 'Maquinaria Electrica y Electronica',
			'37' => 'Equipos de Transporte',
			'38' => 'Instrumentos de Medida, Analisis, Control, Fotografia, Optica y Relojes',
			'39' => 'Fabricantes Diversos',
			'40' => 'Transportes Ferroviarios',
			'41' => 'Transportes de Pasajeros',
			'42' => 'Transportes de Mercancias por Carretera y Almacenamiento',
			'43' => 'Servicio Postal',
			'44' => 'Transportes Maritimos',
			'45' => 'Transportes Aereos',
			'46' => 'Oleoductos',
			'47' => 'Servicios para el Transporte',
			'48' => 'Comunicaciones',
			'49' => 'Servicios de Agua, Electricidad, Gas y Sanitarios',
			'50' => 'Mayoristas de Mercancias no Perecederas',
			'51' => 'Mayoristas de Mercancias Perecederas',
			'52' => 'Detallistas de Materiales para la Construccion y Ferreteria',
			'53' => 'Almacenes y Bazares',
			'54' => 'Detallistas de Alimentacion',
			'55' => 'Concesionarios de Automoviles, Gasolineras y Estaciones de Servicio',
			'56' => 'Detallistas de Prendas de Vestir',
			'57' => 'Detallistas de Muebles, Utensilios y Articulos para el Hogar',
			'58' => 'Bares y Restaurantes',
			'59' => 'Detallistas de Articulos Diversos',
			'60' => 'Bancos, Cajas de Ahorro',
			'61' => 'Financieras',
			'62' => 'Agencias de Cambios y Bolsa',
			'63' => 'Seguros y Reaseguros',
			'64' => 'Agencia de Seguros',
			'65' => 'Bienes Raices',
			'66' => 'Empresas Mixtas',
			'67' => 'Sociedades de Cartera (Holdings)',
			'70' => 'Hosteleria',
			'72' => 'Servicios Personales',
			'73' => 'Servicios Comerciales',
			'75' => 'Reparaciones y Servicios Relacionados con el Automovil',
			'76' => 'Servicios de Reparaciones e Instalaciones Diversas',
			'78' => 'Cinematografia y Servicio',
			'79' => 'Servicios para el Entretenimiento y Ocio',
			'80' => 'Servicios Sanitarios Individuales',
			'81' => 'Servicios Legales',
			'82' => 'Servicios Educativos',
			'83' => 'Servicios Sociales',
			'84' => 'Museos, Galerias de Arte, Jardines Botanicos y Zoologicos',
			'86' => 'Organizaciones Mutualistas',
			'88' => 'Servicios Domesticos para el Hogar',
			'89' => 'Servicios Diversos',
			'91' => 'Organismos Oficiales Estatales',
			'92' => 'Organismos Oficiales Autonomicos',
			'93' => 'Organismos Oficiales Locales',
			'97' => 'Varios'
		);

		return $array;
	}

	/**
	 * Method will return all the information about the
	 * limit amount required by the companies to check
	 * what is the cotizations
	 *
	 * @return array $array
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function limiteCantidad() {
		$array = array(
			'1'  => '$ 250,000.00',
			'2'  => '$ 500,000.00',
			'3'  => '$ 750,000.00',
			'4'  => '$ 1,000,000.00',
			'5'  => '$ 1,250,000.00',
			'6'  => '$ 1,500,000.00',
			'7'  => '$ 1,750,000.00',
			'8'  => '$ 2,000,000.00',
			'9'  => '$ 2,250,000.00',
			'10' => '$ 2,500,000.00',
			'11' => '$ 2,750,000.00',
			'12' => '$ 3,000,000.00',
			'13' => '$ 3,250,000.00',
			'14' => '$ 3,500,000.00',
			'15' => '$ 3,750,000.00',
			'16' => '$ 4,000,000.00',
			'17' => '$ 4,250,000.00',
			'18' => '$ 4,500,000.00',
			'19' => '$ 4,750,000.00',
			'20' => '$ 5,000,000.00'
		);

		return $array;
	}

	/**
	 * Method will be used to generate the array will be used
	 * to insert the data to the database but with the correct
	 * name defined in the database
	 *
	 * @param array $data
	 * @return array $result
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __createArray($data) {
		$nombre_vendedor = "";

		if ($data['vendedor'] == 'Si' || $data['vendedor'] == 'si') {
			$nombre_vendedor = $data['nombre_vendedor'];
		}

		// Check if deducible, prima neta and total exists
		$detalleSiniestro = (isset($data['detalles_siniestros'])) ? $data['detalles_siniestros'] : "";
		$prima_neta 	  = (isset($data['prima_neta'])) ? $data['prima_neta'] : 0;
		$deducible  	  = (isset($data['deducible'])) ? $data['deducible'] : 0;
		$total			  = (isset($data['total'])) ? $data['total'] : 0;
		$nameUser		  = ($data['tipo_persona'] == 'fisica') ? $data['nombre_asegurado'] : $data['empresa'];

		// Check if sould be set N/A or SI
		$tienda 		 = ($data['tienda_abarrotes'] == "1-0") ? "N/A" : "SI";
		$transportadores = ($data['transportadores'] == "1-0") ? "N/A" : "SI";
		$restaurantes	 = ($data['restaurantes'] == "1-0") ? "N/A" : "SI";
		$gasolineras	 = ($data['gasolineras'] == "1-0") ? "N/A" : "SI";


		$result = array(
			'fecha_creacion' 	  => $data['fecha_cotizacion'],
			'tipo_persona'		  => $data['tipo_persona'],
			'nombre_asegurado' 	  => $nameUser,
			'email'				  => $data['email'],
			'contacto'			  => $data['nombre_contacto'],
			'telefono'			  => $data['telefono'],
			'direccion' 		  => $data['calle_numero'],
			'rfc' 				  => $data['rfc'],
			'colonia' 			  => $data['colonia'],
			'municipio' 		  => $data['del_mun'],
			'codigo_postal' 	  => $data['cp'],
			'vigencia_desde' 	  => $data['vigencia_desde'],
			'vigencia_hasta' 	  => $data['vigencia_hasta'],
			'renovacion' 		  => $data['renovacion'],
			'forma_pago' 		  => ucfirst($data['forma_pago']),
			'ingresos_anuales' 	  => $data['ingresos_anuales'],
			'maximo_dinero' 	  => $data['maximo_dinero'],
			'no_empleados' 		  => $data['no_empleados'],
			'maximo_empleados' 	  => $data['maximo_empleados'],
			'limite_requerido' 	  => $data['limite_requerido'],
			'giro_empresa' 		  => $data['giro_empresa'],
			'no_sucursales' 	  => $data['no_sucursales'],
			'auditoria' 		  => $data['auditoria'],
			'transaccion' 		  => $data['transaccion'],
			'pagos' 			  => $data['pagos'],
			'conteo' 			  => $data['conteo'],
			'conciliaciones' 	  => $data['conciliaciones'],
			'segregadas' 		  => $data['segregadas'],
			'intervencion' 		  => $data['intervencion'],
			'siniestro' 		  => $data['siniestro'],
			'controles' 		  => $data['controles'],
			'ultimo_ano' 		  => $data['ultimo_ano'],
			'penultimo_ano' 	  => $data['penultimo_ano'],
			'antepenultimo_ano'   => $data['antepenultimo_ano'],
			'apreciacion_riesgo'  => $data['apreciacion_riesgo'],
			'porcentaje_comision' => $data['porcentaje_comision'],
			'tasa_adicional' 	  => $data['tasa_adicional'],
			'tienda_abarrotes'	  => $tienda,
			'transportadores' 	  => $transportadores,
			'restaurantes' 		  => $restaurantes,
			'gasolineras' 		  => $gasolineras,
			'detalle_siniestro'	  => $detalleSiniestro,
			'prima_neta' 		  => $prima_neta,
			'deducible' 		  => $deducible,
			'total' 			  => $total,
			'desde'				  => $data['desde'],
			'hasta'				  => $data['hasta'],
			'vendedor'			  => $data['vendedor'],
			'nombre_vendedor'	  => $nombre_vendedor,
			'fecha_actualizacion' => date('d-m-Y H:i:s')
		);

		return $result;
	}

	/**
	 * Class will contains the information executes every call request to
	 * calculate the payments will be displayed on the
	 * platforn once the user filled in all the request
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @ccompany ruvicdev
	 */
	public function makesCalculationData() {
		$json_array = json_decode(file_get_contents('php://input'), true);

		// Create the array will be used to save info in database
		$data = $this->__createArray($json_array);

		$prima_neta = $this->__getNetPremium($data);
		$deducibles = $this->__getDeductible($data);
		$finalTotal = $this->__getFinalTotal($data, $prima_neta);

		$finalTotal['prima_neta'] = $prima_neta;
		$finalTotal['deducibles'] = $deducibles;

		echo json_encode($finalTotal);
	}

	/**
	 * Method will contain all the calculations that
	 * is going to be used for displaying in the final
	 * price to the user. To create the cotization, is going to
	 * use data typed by the user and also the data will get from
	 * the excel file
	 *
	 * @param array $data
	 * @return float $prima_neta
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __getNetPremium($data) {
		//var_dump($data);

		// Waiting for the flag to know if its neccesary set Pesos or Dollars
		$flagTrue   = TRUE;
		$flagFalse  = TRUE;
		$primaFinal = "";

		// I66 + 1
		$i66 = $data['porcentaje_comision'];

		// W88
		$z100 = ((float)$data['ultimo_ano'] + (float)$data['penultimo_ano'] + (float)$data['antepenultimo_ano']) / 3;
		$v101 = $z100 / 0.65;
		$w88  = $v101;

		//w89 MAIN
		$q76 = 1 + (intval($data['tasa_adicional']) / 100);

		//v120
		$q77  = ($data['tienda_abarrotes'] == 'Si') ? 1 : 0;
		$q78  = ($data['transportadores'] == 'Si') ? 1 : 0;
		$q79  = ($data['restaurantes'] == 'Si') ? 1 : 0;
		$v120 = ($q77 == 1 && $q78 == 1 && $q79 == 1) ? 1 : 0;

		// w87*(1+q76)
		$i23 = $data['limite_requerido'];
		$i23 = str_replace('$', '', $i23);
		$i23 = str_replace(',', '', $i23);
		$x78 = 2.5; // 2.5%
		//echo "celda X78: " . $x78;
		// AA77
		$f21 = $data['no_empleados'];
		$a77 = 0;

		if ($f21 < 51 && $f21 > 0 ) {
			$a77 = 0;
		} else if ($f21 < 101 && $f21 >= 51 ) {
			$a77 = $x78 * .1;
		} else if ($f21 < 151 && $f21 >= 101) {
			$a77 = $x78 * .2;
		} else if ($f21 < 201 && $f21 >= 151) {
			$a77 = $x78 * .3;
		} else if ($f21 < 251 && $f21 >= 201) {
			$a77 = $x78 * .4;
		} else if ($f21 < 301 && $f21 >= 251) {
			$a77 = $x78 * .45;
		} else {
			$a77 = 0;
		}
		//echo "<br> celda AA77: " . $a77;
		// AB77
		$f28  = $data['no_sucursales'];
		$ab77 = 0;

		if ($f28 < 6) {
			$ab77 = 0;
		} else if ($f28 < 11 && $f28 >= 6) {
			$ab77 = $x78 * .2;
		} else if ($f28 < 21 && $f28 >= 11) {
			$ab77 = $x78 * .3;
		} else {
			$ab77 = 0;
		}
		//echo "<br />AB77: " . $ab77;

		// DP77
		$i57  = $data['controles'];
		$dp77 = 0;

		if ($i57 == 0) {
			$dp77 = ($x78/100) * -.2;
		} else {
			$dp77 = ($x78/100) * .2;
		}
		//echo "<br /> DP77: " . $dp77;

		// AD77
		$i64  = $data['apreciacion_riesgo'];
		$ad77 = $this->ApreciacionActosFraudulentos_model->getPercentagePerId($i64);
		//echo "<br /> AD77: " . $ad77->percentage;

		// SUM(X78 + AA77 + AB77 + DP77 + AD77)
		$w85 = ($x78) + ($a77) + ($ab77) + ($dp77 * 100) + ($ad77->percentage);
		//echo "<br /> W85: " . $w85;
		$w87  = 0;
		$w85F = ($w85 / 100) * $i23;
		//echo "<br /> w85F: " . $w85F;

		if ($v120 == 1) {
			// w87*(1+q76)
			$w87 = $w85F * (1 + $q76);
		} else {
			// w87
			$w87 = $w85F;
		}
		//echo "<br /> W87: " . $w87;
		$w89 = $w87;

		// Get Final Net Premium
		$w90 = 0;
		//echo "<br /> w89: " . $w89;
		//echo "<br /> w88: " . $w88;

		if ($w89 > $w88) {
			$w90 = $w89;
		} else {
			$w90 = $w88;
		}
		//echo "<br /> w90: " . $w90;

		// W90
		$x91 = 0;
		if ($flagTrue == TRUE) {
			$x91 = $w90;
		} else {
			$x91 = $w90 * 0.9;
		}
		//echo "<br /> x91: " . $x91;

		// Final calc
		$firstOperations = 1 + ($i66 / 100);
		//echo "<br /> First Operation: " . $firstOperations;
		$x93 = $x91 * $firstOperations;

		//echo "valor total: " . $x93;

		return $x93;
	}

	/**
	 * Method will contains all the calculation will be done
	 * to get the final price will be displayed to the
	 * final user with the process of all the calculations
	 * getting from the file
	 *
	 * @param array $data
	 * @return float $deducibles
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __getDeductible($data) {
		// define variables
		$currency = 'Pesos';

		// get all data from valores_deducibles to check what value will be applied
		if ($currency == 'Pesos') {
			$values = $this->ValoresDeducibles_model->getAllData(1);
		} else {
			$values = $this->ValoresDeducibles_model->getAllData(0);
		}

		// Z145
		$l60 = (float)$data['ultimo_ano'];
		$l61 = (float)$data['penultimo_ano'];
		$l62 = (float)$data['antepenultimo_ano'];

		$u103 = $l60 + $l61 + $l62;

		$z134  = 0;
		$ad134 = 0;

		if ($currency == 'Pesos') {
			if ($u103 > 0) {
				foreach($values->result() as $key => $value) {
					$moneda = $value->final_price;
					if ($u103 >= (float)$value->rango_uno && $u103 <= (float)$value->rango_dos) {
						$z134 = (float)$value->final_price;
					}
				}
			} else {
				$z134 = 0;
			}
		} else {
			if ($u103 > 0) {
				foreach ($values->result() as $key => $value) {
					if ($u103 >= $value->rango_uno && $u103 <= $value->rango_dos) {
						$ad134 = $value->final_price;
					}
				}
			} else {
				$ad134 = 0;
			}
		}
		$z145 = ($currency == 'Pesos') ? $z134 : $ad134;

		// V146
		$v134 = 0;
		$v145 = 0;
		$ii19 = $data['ingresos_anuales'];
		$i19 = intval($ii19);

		if ($currency == 'Pesos') {
			$value2 = $this->DeducibleIngreso_model->getAllDeductibleProfit();

			foreach ($value2->result() as $key => $value) {
				$rango_uno = intval($value->rango_uno);
				$rango_dos = intval($value->rango_dos);

				if ($i19 > $rango_uno && $i19 <= $rango_dos) {
					$v145 = $value->precio_final;
					break;
				} else {
					$v145 = 0;
				}
			}
		} else {
			$v134 = 60000001;
		}

		$v146 = ($currency == 'Pesos') ? $v145 : $v134;

		// Z146
		$z146 = 0;
		if ($u103) {
			$z146 = (float)$v146 + (float)$z145;
		} else {
			$z146 = (float)$v146;
		}

		return $z146;
	}

	/**
	 * Method will be used to get the final price given to the user
	 * once the platform is checking all the calculations done with
	 * the final process and data required to get done
	 *
	 * @param array $data
	 * @param float $i96
	 * @return float $total
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function __getFinalTotal($data, $i96) {
		// Variables
		$currency = 'Pesos';
		$mensaje1 = '';
		$mensaje2 = '';
		$ae98	  = 0;
		$l60   	  = $data['ultimo_ano'];
		$l61   	  = $data['penultimo_ano'];
		$l62   	  = $data['antepenultimo_ano'];
		$i23   	  = $data['limite_requerido'];
		$i23	  = str_replace('$', '', $i23);
		$i23 	  = str_replace(',', '', $i23);

		// AK11
		// Y63
		$v56 = $i23;
		$w63 = 10000000;
		$w64 = 1000000;
		$x63 = 30000;
		$x64 = 3000;

		if ($currency == 'Pesos') {
			$y63 = ($v56 > $w63) ? TRUE : FALSE;
			if ($y63 == TRUE) {
				$mensaje1 = 'EXCEDE LA SUMA ASEGURADA ACEPTABLE FAVOR DE TURNAR LA CUENTA AL PROFIT';
			}

			// Z63
			$z63 = ($v56 < $x63) ? TRUE : FALSE;
			if ($z63 == TRUE) {
				$mensaje2 = 'SUMA ASEGURADA POR DEBAJO DEL MINIMO';
			}
		} else {
			// AK12
			// Y64
			$y64 = ($v56 > $w64) ? TRUE : FALSE;
			if ($y64 == TRUE) {
				$mensaje1 = 'EXCEDE LA SUMA ASEGURADA ACEPTABLE FAVOR DE TURNAR LA CUENTA AL PROFIT';
			}

			// Z64
			$z64 = ($v56 < $x64) ? TRUE : FALSE;
			if ($z64 == TRUE) {
				$mensaje2 = 'SUMA ASEGURADA POR DEBAJO DEL MINIMO';
			}
		}

		// AB98
		$ab98 = 0;
		$aa98 = 0;
		$ad98 = 0;
		$ad99 = 0;
		$i66  = $data['porcentaje_comision'];

		if ($mensaje1 != '' && $mensaje2 != '') {
			return array('mensaje1' => $mensaje1,
						 'mensaje2' => $mensaje2,
						 'mensaje3' => $mensaje3,
					 	 'total' 	=> '');
		} else {
			$aa98 = $i96 * (1 + ($i66 / 100));
		}

		if ($currency == 'Pesos') {
			if ($aa98 < 2500) {
				$ab98 = 2500;
			} else {
				$ab98 = $aa98;
			}

			$ad98 = $ab98;
		} else {
			if ($aa98 < 250) {
				$ab98 = 250;
			} else {
				$ab98 = $aa98;
			}
			$ad99 = $ab98;
		}

		$ae98 = ($currency == 'Pesos') ? $ad98 : $ad99;

		// AD105
		$ad105 = 0;
		$mensaje3 = '';
		$ad105Sum = $l60 + $l61 + $l62;
		$operation = $i23 * .25;

		if ($currency == "Pesos") {
			if ($ad105Sum == 0) {
				$ad105 = $ae98;
			} elseif($ad105Sum > $operation || $ad105Sum >= 250000) {
				$mensaje3 = 'Turnar la Cuenta al Profit';
			} else {
				$ad105 = $ae98 * 1.4;
			}
		} else {
			if ($ad105Sum == 0) {
				$ad106 = $ae98;
			} elseif($ad105Sum > $operation || $ad105Sum >= 25000) {
				$mensaje3 = 'Turnar la Cuenta al Profit';
			} else {
				$ad106 = $ae98 * 1.4;
			}
		}

		// Mensaje regresado y truena proceso
		if ($mensaje3 != '') {
			// regresar mensaje 3
			if ($mensaje1 != '' || $mensaje2 != '') {
				return array('mensaje1' => $mensaje1,
							 'mensaje2' => $mensaje2,
							 'mensaje3' => $mensaje3,
						 	 'total' 	=> '');
			}
		}

		$mensaje4 = "";
		if ($ad105Sum > 0) {
			$mensaje4 = "FAVOR DE PROPORCIONAR LOS DETALLES DEL SINIESTRO Y MEDIDAS PREVENTIVAS TOMADAS PARA EVITAR OTRO SINIESTRO";
		}

		// R146
		$r146 	  = 0;
		$perData  = 0;
		$tipoPago = $data['forma_pago'];

		if ($tipoPago == 'Contado') {
			$perData = 0;
		} else if ($tipoPago == 'Semestral') {
			$perData = 2.8;
		} else {
			$perData = 4.3;
		}

		if ($currency != ' Pesos') {
			$r146 = $perData;
		} else {
			$r146 = '';
		}

		// AD108
		$ad108 = 0;
		if ($currency == 'Pesos') {
			$ad108 = $ad105 * (1 + 0);
		} else {
			$ad108 = $ad108 * (1 + $r146);
		}

		// AD109
		$ad109 = $ad108;

		// Loss Ratios
		$c126 = 18;
		$c127 = 15;
		$c125 = 0;

		// B122
		$b122 = 0;
		$sum1 = $this->DeducibleVariable_model->getAllData();
		foreach ($sum1->result() as $key => $value) {
			if ($value->sum_incurred == '-') {
				$b122 += 0;
			}

			$valInt = intval($value->sum_incurred);
			$b122 += $valInt;
		}

		// G122
		$sumOfPrima    	 = 0;
		$comisionGRow    = 0;
		$gastosGeneral   = 0;
		$primaNeta       = 0;
		$primaNetaGastos = 0;
		$g122 			 = 0;

		foreach ($sum1->result() as $key => $value) {
			$dataCity 	   = $this->PrimaMinima_model->getDataByCity($value->region);
			$sumOfPrima	   = $dataCity->sum_prima_minima;
			$comisionGRow  = $dataCity->comisiones;
			$gastosGeneral = $dataCity->gastos_generales;

			// E Column
			$primaNeta = (1 - ($comisionGRow / 100)) * $sumOfPrima;
			$primaNetaGastos = (1 - ($gastosGeneral / 100)) * $primaNeta;
			$g122 += $primaNetaGastos;
		}

		// C125
		$c125 = (($b122 - $g122) / $g122) * 100;
		$c128 = $c125 + $c126 + $c127;

		// AB109
		$ab109     = 0;
		$ab109Temp = round($ad109 * (1 + ($c128 / 100)), 2);

		if ($ab109Temp < 10000) {
			$ab109 = 10000;
		} else {
			$ab109 = $ab109Temp;
		}

		// PRIMA NETA = TOTAL
		$tiendaAbarrotes = $data['tienda_abarrotes'];
		$transportadores = $data['transportadores'];
		$restaurantes	 = $data['restaurantes'];
		$gasolineras	 = $data['gasolineras'];

		$w110 = ($tiendaAbarrotes == 'N/A' && $transportadores == 'N/A' && $restaurantes == 'N/A' && $gasolineras == 'N/A') ? 0 : 1;

		$totalFinal    = 0;
		$tasaAdicional = $data['tasa_adicional'];

		if ($w110 > 0) {
			$totalFinal = $ab109 * (1 + ($tasaAdicional / 100));
		} else {
			$totalFinal = $ab109;
		}

		//echo $totalFinal;
		return array('total' 	=> $totalFinal,
					 'mensaje1' => $mensaje1,
				 	 'mensaje2' => $mensaje2,
				 	 'mensaje3' => $mensaje3,
				 	 'mensaje4' => $mensaje4);
	}

	/**
	 * Method will be used to update the data in the database
	 * regarding the version of the excel uploaded by the user
	 * and that is going to be loaded to the server. If the file is
	 * updated, the next cotizations will be done with the new
	 * data
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function loadFileToDatabase() {
	}

	/**
	 */
	public function demo() {
		$temp = $this->template->actosFraudulentosTemplate();
		$p = $this->pdf->__initPDFInsurancePet($temp, "Actos Fraudulentos", "Actos Fraudulentos");
        $p->Output("demo.pdf", 'I');

	/*	$info = array(
           'id' => 1,
	         'fecha_creacion'   => 'fecha cotizacion',
	         'nombre_asegurado' => 'nombre_asegurado',
	         'direccion'        => 'calle_numero',
	         'rfc'           	=> 'rfc',
	         'colonia'         	=> 'colonia',
	         'municipio'       	=> 'del_mun',
	         'codigo_postal'    => 'cp',
	         'vigencia_desde'   => 'vigencia_desde',
	         'vigencia_hasta'   => 'vigencia_hasta',
	         'renovacion'       => 'renovacion',
	         'forma_pago'       => 'forma_pago',
	         'ingresos_anuales' => '100000000',
	         'maximo_dinero'    => '250000000',
	         'no_empleados'     => '50',
	         'maximo_empleados' => '300',
	         'limite_requerido' => '2000000',
	         'giro_empresa'     => 'giro_empresa',
	         'no_sucursales'    => '10',
	         'auditoria'        => 'Si',
	         'transaccion'      => 'Si',
	         'pagos'         	=> 'Si',
	         'conteo'       	=> 'Si',
	         'conciliaciones'   => 'Si',
	         'segregadas'       => 'Si',
	         'intervencion'     => 'Si',
	         'siniestro'        => 'Si',
	         'controles'        => '0',
	         'ultimo_ano'       => '0',
	         'penultimo_ano'    => '0',
	         'antepenultimo_ano' => '0',
	         'apreciacion_riesgo' => '1',
	         'porcentaje_comision' => '15',
	         'tasa_adicional'  => '25',
	         'tienda_abarrotes' => 'N/A',
	         'transportadores' => 'N/A',
	         'restaurantes'    => 'N/A',
	         'gasolineras'     => 'N/A',
	         'prima_neta'      => 'prima_neta',
	         'deducible'       => 'deducible',
	         'total' => 'total',
	         'desde' => 'desde',
	         'hasta' => 'hasta',
	         'vendedor' => 'Si',
	         'email' => 'email',
	         'telefono' => 'telefono',
	         'nombre_vendedor' => 'nombre vendedor',
	         'fecha_actualizacion' => date('d-m-Y H:i:s')
	     );

		 $data = $this->__getNetPremium($info);
		 //echo $data;
		 //die();
		 $data2 = $this->__getDeductible($info);
		 $data3 = $this->__getFinalTotal($info, $data);

		 $data3['prima_neta'] = $data;
		 $data3['deducible']  = $data2;

		 var_dump($data3);*/
	}
}
