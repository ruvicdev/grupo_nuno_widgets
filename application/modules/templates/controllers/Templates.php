<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Class will be used to create the
 * templates will use during the creation
 * of all the PDF will send via E-Mail
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */

 class Templates extends MY_Controller {

     /**
      * Constructor .....
      */
     public function __construct() {
         parent::__construct();
     }

     /**
      * Template will be used to set
      * all the information is going to
      * display all the data of the user
      *
      * @param array $infoData
      * @return template $template
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function merchandiseTemplate($infoData) {
         /*$infoData = array(
             'orden_id' => 1,
             'class' => 'A',
             'security' => array('hola', 'adios', 'que pedo'),
             'persona_moral' => '123123',
             'giro'          => '2',
             'vol_viajes'    => '11_a_15',
             'email'         => '123123@123213.com',
             'telefono'      => '123123',
             'rfc'           => '123123',
             'nombre_giro'   => 'Freight Forwarder',
             'tipo_persona'  => 2,
             'moneda'        => 'mxn',
             'descripcion_merc' => '123123213',
             'valor_merc' => '123123',
             'tipo_merc' => 2,
             'empaque' => '123123213',
             'tipo' => 'nueva',
             'cobertura' => 'nacional',
             'medio_transp' => 3,
             'tipo_moneda' => 'Pesos',
             'nombre_merc' => 'ABONOS Y FERTILIZANTES, HERBICIDAS, FITOESTABILIZADORES',
             'nombre_medio_trans' => 'Buque y conexiones',
             'current_date' => '02-05-2017',
             'validAt' => '02-01-2017',
             'prima_neta' => '393.9936',
             'total_pagar' => 0,
             'continente_origen' => 'continente',
             'pais_origen' => 'pais',
             'estado_origen' => 'estado',
             'ciudad_origen' => 'ciudad',
             'continente_destino' => 'continente',
             'pais_destino' => 'pais',
             'estado_destino' => 'estado',
             'ciudad_destino' => 'ciudad',
             'vendedor' => 'Si',
             'nombre_vendedor' => 'Ruben Alonso Cortes Mendoza'
         );*/

         $content = $this->loadSingleView('mt', $infoData, TRUE);
         $data    = $this->__setArray('content', $content);
         $data    = $this->__setArray('title', 'Cotiza Carga');

         return $content;
     }

     /**
      * Template will be used to set
      * all the information is going to
      * display all the data of the user
      *
      * @param array $infoData
      * @return template $template
      *
      * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function insuranceMinorMETemplate($infoData) {
         $content = $this->loadSingleView('minor', $infoData, TRUE);
         $data    = $this->__setArray('content', $content);
         $data    = $this->__setArray('title', 'Cotiza Club Salud');

         return $content;
     }

     /**
      * Template for cubre tu deducible widget
      *
      *
      * @param array $infoData
      * @return template $template
      *
      * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function cubreTuDeducibleTemplate($infoData) {
         $content = $this->loadSingleView('cubreTuDeducible', $infoData, TRUE);
         $data    = $this->__setArray('content', $content);
         $data    = $this->__setArray('title', 'Cubre Tu Deducible');

         return $content;
     }

     /**
      * View will be used as template to create
      * the PDF is going to be sent to the user
      * wants to know what is the price to purchase
      * a pet insurance
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function mascotasTemplate($petInsurance, $id) {
         /*$petInsurance = array(
             'orden_id' => '1',
             'current_date' => '19-03-86',
             'nombre' => 'Ruben Alonso Cortes Mendoza',
             'rfc' => 'comr8768jtd76',
             'direccion' => 'loma pihuamo ote 133 b',
             'cp' => '45418',
             'colonia' => 'loma dorada',
             'municipio' => 'tonala',
             'estado' => 'jalisco',
             'tel' => '333333',
             'email' => 'eee@fff.com',
             'mascota_n' => 'max',
             'edad' => '3 años',
             'tipo' => 'Perro',
             'sexo' => 'Macho',
             'raza' => 'Beagle',
             'otra_raza' => '',
             'color' => 'tricolor',
             'esteril' => 'Si',
             'pedigree' => 'Si',
             'registro_option' => 'No',
             'registro' => 'Internacional',
             'num_registro' => 'jjjeh2433jj',
             's_part' => 'asdasd asdasdasd asdasdsad asdsadas asdasdasd',
             'microchip' => 'No',
             'paquete_desc' => '123123 - $ 1234.99',
             'no_microchip' => 'sdfdsf98308',
             'accidente' => 'No',
             'criptorquidismo' => 'No',
             'fractura' => 'No',
             'hernias' => 'No',
             'sida_felino' => 'No',
             'luxacion' => 'No',
             'displacia' => 'No',
             'cardiopatias' => 'No',
             'ehrlichia' => 'No',
             'leucemia' => 'No',
             'borrelia' => 'No',
             'oculares' => 'No',
             'gusano_c' => 'No',
             'dermatologicas' => 'No',
             'parasitarias' => 'No',
             'convulsiones' => 'No',
             'desc_ant' => 'asdasjjj jkjkhjhj kjjhjhj',
             'current_date' => date('d-m-Y'),
             'vendedor' => 'No',
             'name_seller' => 'werererj ererew'
         );*/

         $petInsurance['orden_id']     = $id;
         $petInsurance['current_date'] = date('d-m-Y');

         $content = $this->loadSingleView('mascotas', $petInsurance, TRUE);
         $data    = $this->__setArray('content', $content);
         $data    = $this->__setArray('title', 'Seguro de Mascotas');

         return $content;
     }

     /**
      * View will be used as template to create the
      * PDF is going to be sent to the user once he finished
      * all the process for create a cotization of the
      * insurance and could check what is the price of
      * purchase the insurance for a company
      *
      * @param array $data
      * @param int $id
      * @return object $content
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function seguroResponsabilidadTemplate($data, $id) {
        /*$data = array(
          'id'        => 1,
          'empresa'           => 'nombre Empresa anterior',
          'tipo_persona'        => 1,
          'nombre_user'       => 'nombre usuario',
          'ap_user'         => 'ap usuario',
          'am_user'         => 'am usuario',
          'empresa_user'        => 'nombre empresa',        
          'direccion'         => 'direccion',
          'email'           => 'email',
          'telefono'          => 'telefono',
          'fecha_cotizacion'      => date('d-m-Y H:i:s'), //$data['fecha'],
          'numero_empleados'      => 'no empleado',
          'facturacion'         => 'fact',
          'indemnizacion'       => 'indemnizacion',
          'sublimite_responsabilidad' => 'sublimite',
          'deducible'         => 'deducible',
          'deducible_responsabilidad' => 'laboral',
          'prima_neta_anual'      => 'prima',
          'es_vendedor'         => 'No',
          'nombre_vendedor'       => 'nombre vendedor',
          'id_facturacion'      => 1,
          'id_seccion_facturacion'  => 2,
          'fecha_creacion'      => date('d-m-Y H:i:s')
        );*/

        $data['id'] = $id;
        $content    = $this->loadSingleView('seguroResponsabilidad', $data, TRUE);
        $data       = $this->__setArray('content', $content);
        $data       = $this->__setArray('title', 'Seguro de Resonsabilidad');

        return $content;
     }

     /**
      * Template will be used to set all the
      * information is going to be sent to
      * the user with all the data saved in the system
      *
      * @param array $info
      * @param int $id
      * @return template $template
      *
      * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
      * @version 1.0
      * @company ruvicdev
      */
     public function proteccionDatosTemplate($info, $id) {

        /*$info = array(
          'id' => '1',
          'nombre'        => 'nombre',
          'paterno'         => 'paterno',
          'materno'         => 'materno',
          'telefono'        => 'tel',
          'email'         => 'email',
          'limite_resp'     => 'limite',
          'limite_id' => '1',
          'factu_total'     => 'facturacion',
          'factu_id' => '2',
          'deducible'       => 'deducible',
          'tbd'         => 'tbd',
          'es_vendedor'   => 'Si',
          'nombre_vendedor' => 'nombreVendedor',
          'creacion'        => 'd-m-Y H:i:s',
          'actualizacion'   => 'd-m-Y H:i:s'
        );*/

         $info['id'] = $id;

         $content = $this->loadSingleView('proteccionDatos', $info, TRUE);
         $data    = $this->__setArray('content', $content);
         $data    = $this->__setArray('title', 'Segiro de Proteccion de Datos');

         return $content;
     }

    /**
     * Method will load the view with all the information
     * for the fraudulent acts and also I'm going to be using
     * to create the PDF will be sent via email once the user
     * confirms that the information is correct and they want
     * to reach out the seller
     *
     * @param array $info
     * @param int $id
     * @return object $content
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function actosFraudulentosTemplate($info, $id) {
      $info['id'] = $id;
      /*$info = array(
        'id' => 1,
        'fecha_creacion'    => 'fecha cotizacion',
        'nombre_asegurado'    => 'nombre_asegurado',
        'direccion'       => 'calle_numero',
        'rfc'           => 'rfc',
        'colonia'         => 'colonia',
        'municipio'       => 'del_mun',
        'codigo_postal'     => 'cp',
        'vigencia_desde'    => 'vigencia_desde',
        'vigencia_hasta'    => 'vigencia_hasta',
        'renovacion'      => 'renovacion',
        'forma_pago'      => 'forma_pago',
        'ingresos_anuales'    => 'ingresos_anuales',
        'maximo_dinero'     => 'maximo_dinero',
        'no_empleados'      => 'no_empleados',
        'maximo_empleados'    => 'maximo_empleados',
        'limite_requerido'    => 'limite_requerido',
        'giro_empresa'      => 'giro_empresa',
        'no_sucursales'     => 'no_sucursales',
        'auditoria'       => 'auditoria',
        'transaccion'       => 'transaccion',
        'pagos'         => 'pagos',
        'conteo'        => 'conteo',
        'conciliaciones'    => 'conciliaciones',
        'segregadas'      => 'segregadas',
        'intervencion'      => 'intervencion',
        'siniestro'       => 'siniestro',
        'controles'       => 'controles',
        'ultimo_ano'      => 'ultimo_ano',
        'penultimo_ano'     => 'penultimo_ano',
        'antepenultimo_ano'   => 'antepenultimo_ano',
        'apreciacion_riesgo'  => 'apreciacion_riesgo',
        'porcentaje_comision' => 'porcentaje_comision',
        'tasa_adicional'    => 'tasa_adicional',
        'tienda_abarrotes'    => 'tienda_abarrotes',
        'transportadores'     => 'transportadores',
        'restaurantes'      => 'restaurantes',
        'gasolineras'       => 'gasolineras',
        'prima_neta'      => 'prima_neta',
        'deducible'       => 'deducible',
        'total'         => 'total',
        'desde' => 'desde',
        'hasta' => 'hasta',
        'vendedor' => 'No',
        'email' => 'email',
        'telefono' => 'telefono',
        'nombre_vendedor' => 'nombre vendedor',
        'fecha_actualizacion' => date('d-m-Y H:i:s')
      );*/

      $content = $this->loadSingleView('actosFraudulentos', $info, TRUE);
      $data    = $this->__setArray('content', $content);
      $data    = $this->__setArray('title', 'Seguro de Proteccion Contra Actos Fraudulentos PyME');

      return $content;
    }
 }
