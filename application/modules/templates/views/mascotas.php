<style>
    /** FONT SIZES **/
    .font-size-text {
        font-size: 11px;
    }

    .font-weight-header {
        font-size: 11pt;
        font-weight: bold;
    }

    .font-size-main-header {
        font-size: 8pt;
        font-weight: bold;
    }

    /** FONT COLORS **/
    .font-color-label {
        color: red;
    }

    .font-color-dynamic-text {
        color: #C2E3ED;
    }

    .complete_width {
        width: 100%;
    }

    table.outside, th.inside, td.inside_two {
        border: 1px solid #999999;
        padding: 2px;
    }
</style>
<div>&nbsp;</div>
<table class="complete_width">
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            # de Cotización:
        </td>
        <td width="15%" align="left" style="color: #555555">
            <?php echo $orden_id; ?>
        </td>
    </tr>
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            Fecha de Creación:
        </td>
        <td width="15%" align="left" style="color: #555555">
            <?php echo $current_date; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<?php if($vendedor == 'Si' || $vendedor == 'si'): ?>
    <table class="outside">
        <tr align="center" class="font-weight-header">
            <td width="100%" style="font-size: 14px; color: #006C81">
                DATOS DEL VENDEDOR
            </td>
        </tr>
        <tr>
            <td align="right" width="25%" style="color: #006C81">
                Nombre del Vendedor:
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $name_seller; ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS DEL PROPIETARIO
        </td>
    </tr>
    <tr>
        <?php if($tipo_persona == 0 || $tipo_persona == '0'): ?>
            <td align="right" width="13%" style="color: #006C81">
                Nombre(s):
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $nombre; ?>
            </td>
        <?php else: ?>
            <td align="right" width="13%" style="color: #006C81">
                Empresa:
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $empresa; ?>
            </td>
            <td align="right" width="13%" style="color: #006C81">
                Contacto:
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $nombre_contacto; ?>
            </td>
        <?php endif; ?>
        <td width="16%" align="right" style="color: #006C81">
            RFC:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo strtoupper($rfc); ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Direccion:
        </td>
        <td width="50%" align="left" style="color: #555555">
            <?php echo $direccion; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Codigo Postal:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $cp; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Colonia:
        </td>
        <td align="left" width="50%" style="color: #555555">
            <?php echo $colonia; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Municipio:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $municipio; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Estado:
        </td>
        <td align="left" width="87%" style="color: #555555">
            <?php echo $estado; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Email:
        </td>
        <td align="left" width="50%" style="color: #555555">
            <?php echo $email; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Telefono:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $tel; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS DE LA MASCOTA
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Nombre:
        </td>
        <td align="left" width="50%" style="color: #555555">
            <?php echo $mascota_n; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Edad:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $edad; ?>
        </td>
    </tr>
    <!-- tr>
        < ?php $cut = explode("-", $paquete_desc); ?>
        <td align="right" width="13%" style="color: #006C81">
            Paquete:
        </td>
        <td align="left" width="50%" style="color: #EFB242">
            < ?php echo $cut[0]; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Precio:
        </td>
        <td align="left" width="21%" style="color: #EFB242">
            < ?php echo $cut[1]; ?>
        </td>
    </tr -->
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Tipo:
        </td>
        <td align="left" width="50%" style="color: #555555">
            <?php echo $tipo; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Sexo:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $sexo; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Raza:
        </td>
        <td align="left" width="50%" style="color: #555555">
            <?php echo $raza; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Otra Raza:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $otra_raza; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Color(es):
        </td>
        <td align="left" width="50%" style="color: #555555">
            <?php echo $color; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            Esterilizado:
        </td>
        <td align="left" width="21%" style="color: #555555">
            <?php echo $esteril; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="13%" style="color: #006C81">
            Pedigree:
        </td>
        <td align="left" width="15%" style="color: #555555">
            <?php echo $pedigree; ?>
        </td>
        <td align="right" width="23%" style="color: #006C81">
            Cuenta con Registro:
        </td>
        <td align="left" width="15%" style="color: #555555">
            <?php echo $registro_option; ?>
        </td>
        <td align="right" width="13%" style="color: #006C81">
            Registro:
        </td>
        <td align="left" width="20%" style="color: #555555">
            <?php if($registro_option == 'Si'): ?>
                <?php echo $registro; ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #006C81">
            Numero de Registro (en caso afirmativo):
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #555555">
            <?php if($registro_option == 'Si'): ?>
                <?php echo $num_registro; ?>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #006C81">
            Se&ntilde;as Particulares:
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #555555">
            <?php echo $s_part; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="20%" style="color: #006C81">
            Microchip AVID:
        </td>
        <td align="left" width="40%" style="color: #555555">
            <?php echo $microchip; ?>
        </td>
        <td align="right" width="16%" style="color: #006C81">
            No. Microchip:
        </td>
        <td align="left" width="23%" style="color: #555555">
            <?php if($microchip == 'Si'): ?>
                <?php echo $no_microchip; ?>
            <?php endif; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            TU MASCOTA, ¿HA SUFRIDO?
        </td>
    </tr>
    <tr>
        <td align="right" width="15%" style="color: #006C81">
            Accidente:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $accidente; ?>
        </td>
        <td align="right" width="25%" style="color: #006C81">
            Criptorquidismo:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $criptorquidismo; ?>
        </td>
        <td align="right" width="30%" style="color: #006C81">
            Fractura:
        </td>
        <td align="left" width="20%" style="color: #555555">
            <?php echo $fractura; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="15%" style="color: #006C81">
            Hernias:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $hernias; ?>
        </td>
        <td align="right" width="25%" style="color: #006C81">
            Sida Felino:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $sida_felino; ?>
        </td>
        <td align="right" width="30%" style="color: #006C81">
            Luxacion Patelar:
        </td>
        <td align="left" width="20%" style="color: #555555">
            <?php echo $hernias; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="15%" style="color: #006C81">
            Cardiopatias:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $cardiopatias; ?>
        </td>
        <td align="right" width="25%" style="color: #006C81">
            Ehrlichia:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $displacia; ?>
        </td>
        <td align="right" width="30%" style="color: #006C81">
            Displacia de Cadera:
        </td>
        <td align="left" width="20%" style="color: #555555">
            <?php echo $ehrlichia; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="15%" style="color: #006C81">
            Borrelia:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $borrelia; ?>
        </td>
        <td align="right" width="25%" style="color: #006C81">
            Leucemia Viral Felina:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $leucemia; ?>
        </td>
        <td align="right" width="30%" style="color: #006C81">
            Enfermedades Oculares:
        </td>
        <td align="left" width="20%" style="color: #555555">
            <?php echo $hernias; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="40%" style="color: #006C81">
            Enfermedades de Gusano de Corazon:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $gusano_c; ?>
        </td>
        <td align="right" width="40%" style="color: #006C81">
            Enfermedades Dermatologicas:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $dermatologicas; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="40%" style="color: #006C81">
            Enfermedades Parasitarias:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $parasitarias; ?>
        </td>
        <td align="right" width="40%" style="color: #006C81">
            Convulsiones o Desmayos:
        </td>
        <td align="left" width="10%" style="color: #555555">
            <?php echo $convulsiones; ?>
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #006C81">
            En caso Afirmativo, especificar fecha y antecedentes:
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #555555">
            <?php echo $desc_ant; ?>
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #FF0000; font-size: 7px">
            * Declaro que al momento de la contrataci&oacute;n de este servicio mi mascota se encuentra en
            perfecto estado de salud.
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table>
    <thead>
        <tr class="font-weight-header">
            <th class="inside" align="center" width="100%" style="font-size: 14px; color: #006C81">
                Cobertura / Suma Asegurada
            </th>
        </tr>
        <tr>
            <th align="center" class="inside" width="20%" style="color: #006C81; font-size: 10px">
                PRODUCTO
            </th>
            <th align="center" class="inside" width="20%" style="color: #006C81; font-size: 10px">
                GTOS. MEDICOS MASCOTA
            </th>
            <th align="center" class="inside" width="20%" style="color: #006C81; font-size: 10px">
                RC POR TENENCIA DE MASCOTA
            </th>
            <th align="center" class="inside" width="20%" style="color: #006C81; font-size: 10px">
                SERVICIO FUNERARIO
            </th>
            <th align="center" class="inside" width="20%" style="color: #006C81; font-size: 10px">
                COSTO ANUAL
            </th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                MEDIPET BASICO
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 5,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                 -----
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                 -----
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 1,088.08
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                MEDIPET PLATINO
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 10,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 25,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                 -----
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 2,109.58
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                MEDIPET PREMIER
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 25,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 25,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                AMPARAD0 HASTA $1,500.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 3,088.62
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                MEDIPET ELITE I
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 25,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 50,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                AMPARADO HASTA $1,500.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 3,657.48
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                MEDIPET ELITE II
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 25,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 100,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                AMPARADO HASTA $1,500.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 4,587.80
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                MEDIPET ELITE III
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 25,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 150,000.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                AMPARADO HASTA $1,500.00
            </td>
            <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 10px">
                $ 5,347.60
            </td>
        </tr>
    </tbody>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            CONTRATO
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" style="font-size: 7px; color: #555555">
            <ol>
                <li>De acuerdo a la disposici&oacute;n del artículo 47 de la Ley sobre el
                    Contrato de Seguro, misma que se transcriben a continuaci&oacute;n:</li>
                <li>Art&iacute;culo 47.- Cualquier omisi&oacute;n o inexacta declaraci&oacute;n
                    de los hechos a que se refieren los art&iacute;culos 8, 9 y 10 de la presente
                    ley, facultar&aacute; a la empresa aseguradora para considerar rescindido de
                    pleno derecho el contrato, aunque no hayan influido en la realizaci&oacute;n
                    del siniestro.</li>
                <li>Art&iacute;culo 8°.- El proponente estara obligado a declarar por escrito a la
                    empresa asegurdora, de acuerdo con el cuestionario relativo, todos los hechos
                    importantes para la apreciaci&oacute;n del riesgo que pueda influir en las
                    condiciones convenidas, tales como los conosca o deba conocer en el momento
                    de la celebraci&oacute;n del contrato.</li>
                <li>Art&iacute;culo 9°.- Si el contrato se celebra por un representante del
                    asegurado, deberan declararse todos los hechos importates que sean o deban
                    ser conocidos del representante y del representado.</li>
                <li>Art&iacute;culo 10°.- Cuando se proponga un seguro por cuenta de otro, el
                    proponente deber&aacute; declarar todos los hechos importantes que sean o
                    deban ser conocidos del tercero asegurado o de su intermediario.</li>
                <li>Este documento s&oacute;lo constituye una solicitud de seguro y, por tanto,
                    no representa garant&iacute;a alguna de que la misma ser&aacute; aceptada por
                    la empresa de seguros, ni de que, en caso de aceptarse, la aceptaci&oacute;n
                    concuerde totalmente con los t&eacute;rminos de la solicitud.</li>
            </ol>
        </td>
    </tr>
</table>
