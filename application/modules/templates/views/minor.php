<html>
    <head>
        <title>PDF Cotizacion</title>
        <style>
        .td-header-bold {
            font-weight: bold;
            text-align: center;
            background-color: #e0ebeb;
        }
        .font-size-consecutivo {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            text-align: right;
            font-size: 8pt;
        }
        .font-size-text {
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            font-size: 9pt;
        }
        .font-size-text-bold {
            font-family: Arial, Helvetica, sans-serif;
            text-align: justify;
            font-size: 9pt;
            font-weight: bold;
        }
        .with-main-table {
            width: 100%;
        }
        .with-image-td {
            border: 1px solid #00B0F0;
            background-color:#00B0F0;
            padding: 3px;
            width: 30%;
        }
        .with-info-td {
            width: 100%;
        }
        .title-info {
            font-family: Arial, Helvetica, sans-serif;
            text-align: justify;
            font-size: 9pt;
            font-weight: bold;
        }
        .subtitle-info {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9pt;
            text-align: justify;
        }

        .integrantes-table td {
             border: 1px solid black;
        }

        .totales-align {
            text-align: right;
        }
        </style>
    </head>
    <table class="with-main-table">
            <!--tr>
                <td class="font-size-consecutivo">
                    <?php echo $clubSaludHistorial_inserted_id; ?>
                </td>
            </tr-->
            <tr>
                <!--Image in left column-->
                <!--td class="with-image-td">
                    <img src="public/img/ClubSaludImagePDF.png"/>
                </td-->
                <td class="with-info-td">
                    <table>
                        <tr>
                            <td>
                                <font class="font-size-text">
                                    Sr:&nbsp;<?php echo ucwords(strtolower($fullname)); ?>
                                </font>
                            </td>
                            <td class="font-size-consecutivo">
                                <?php echo 'C-'.$clubSaludHistorial_inserted_id; ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    <p class="font-size-text">
                        Agradeciendo su preferencia presentamos la cotización del producto de su interés denominado CLUB SALUD que brindará a usted y su familia, la tranquilidad con los siguientes beneficios:
                    </p>
                    <p class="title-info">
                        COBERTURA DE GASTOS MEDICOS MAYORES
                    </p>
                    <ul class="font-size-text">
          				<li>
                        	Suma asegurada de $5,000,000.00 M.N. (cinco millones de pesos mexicanos) para hospitalización y accidente con copago de $7,000.00 M.N. (siete mil pesos mexicanos)
        				</li>
                        <br/>
          				<li>
                        	Suma asegurada de $ 32,000.00 M.N. (treinta y dos mil pesos mexicanos) para ayuda de gastos de maternidad con copago de $7,000.00 M.N. (siete mil pesos mexicanos)
        				</li>
        			</ul>
                    <p class="title-info">
                    	COBERTURA DE GASTOS MEDICOS MENORES
                    </p>
                    <ul class="font-size-text">
          				<li>
                        	Todos los medicamentos prescritos por su médico de red están incluidos, es decir no aplica deducible ni coaseguro
        				</li>
                        <br/>
          				<li>
                        	Consultas Médicas Generales y de Especialistas copago $400.00 (cuatrociento pesos mexicanos)
        				</li>
                        <br/>
          				<li>
                        	Estudios de Gabinete y Laboratorio 55% de descuento ordenado por médico tratante de red.
        				</li>
        			</ul>
                    <p class="title-info">
                    	COSTOS Y FORMAS DE PAGO
                    </p>
                    <p class="font-size-text">
                    	El pago deberá ser domiciliado a tarjeta de Crédito o Débito
                    </p>
                    <table class="integrantes-table">
                        <tr>
                            <td rowspan="2" class="td-header-bold font-size-text-bold">INTEGRANTES</td>
                            <td colspan="2" class="td-header-bold font-size-text-bold">MENSUAL</td>
                        </tr>
                        <tr>
                            <td class="td-header-bold font-size-text-bold">1er Recibo</td>
                            <td class="td-header-bold font-size-text-bold">11 PAGOS SUBSECUENTES</td>
                        </tr>
                        <?php
                            $hijos = 0;
                            foreach($integrantes as $obj):
                        ?>
                        <tr>
                            <td class="font-size-text">
                                <?php echo ucwords(strtolower($obj['integrante'])).' '.ucwords(strtolower($obj['nombre'])); ?>
                            </td>
                            <td class="font-size-text">
                                <?php
                                    switch ($obj['integrante']) {
                                        case "TITULAR":
                                            echo '$'.number_format($primerReciboTitularMes, 2).' M.N.';
                                        break;
                                        case "CONYUGE":
                                            echo '$'.number_format($primerReciboConyugeMes, 2).' M.N.';
                                        break;
                                        case "HIJO(A)":
                                            $hijos++;

                                            if($hijos == 1) {
                                                echo '$'.number_format($primerReciboPrimerHijoMes, 2).' M.N.';
                                            }else{
                                                echo '$'.number_format($segundoHijoAdelanteMes, 2).' M.N.';
                                            }
                                        break;
                                    }
                                ?>
                            </td>
                            <td class="font-size-text">
                                <?php
                                    switch ($obj['integrante']) {
                                        case "TITULAR":
                                            echo '$'.number_format($subsecuentesReciboTitularMes, 2).' M.N.';
                                        break;
                                        case "CONYUGE":
                                            echo '$'.number_format($subsecuentesReciboConyugeMes, 2).' M.N.';
                                        break;
                                        case "HIJO(A)":
                                            if($hijos == 1) {
                                                echo '$'.number_format($subsecuentesReciboPrimerHijoMes, 2).' M.N.';
                                            }else {
                                                echo '$'.number_format('0', 2).' M.N.';
                                            }
                                        break;
                                    }
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td class="font-size-text-bold totales-align">
                                TOTALES
                            </td>
                            <td class="font-size-text-bold">
                                <?php echo '$'.number_format($pagoTotalInicialMes, 2).' M.N.'; ?>
                            </td>
                            <td class="font-size-text-bold">
                                <?php echo '$'.number_format($pagoTotalSubsecuenteMes, 2).' M.N.'; ?>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <p class="font-size-text">
                    	Con una
                        <font class="font-size-text-bold">
                            inversión diaria de $<?php echo $inversionDiariaMasLetras;?>
                        </font>
                        podrá atender los gastos médicos menores y mayores en la restauración de su salud, con atención adecuada y oportuna, en red de hospitales de alto nivel al servicio de usted y su familia.
                    </p>
                    <p class="title-info">
                    	ADEMÁS CON LOS SIGUIENTES BENEFICIOS
                    </p>
                    <p class="subtitle-info">
                    	RENTAS MENSUALES:
                    </p>
                    <p class="font-size-text">
                    	Al fallecer el titular los beneficiarios recibirán 14 Rentas mensuales por año (junio y diciembre se duplica) El primer año $1,750.00 M.N. (mil setecientos cincuenta pesos mexicanos); $2,000.00 M.N. (dos mil pesos mexicanos) el segundo y $2,250.00 M.N. (dos mil docientos cincuenta pesos mexicanos) el tercero.
        			</p>
                    <p class="subtitle-info">
                    	GASTOS FINALES:
                    </p>
                    <ul class="font-size-text">
          				<li>
                        	Traslado del cuerpo para su preparación dentro del área metropolitana
        				</li>
          				<li>
                        	Embalsamamiento del cuerpo y preparación estética
        				</li>
          				<li>
                        	Apoyo en gestoría de trámites
        				</li>
          				<li>
                        	Servicio y uso de capilla hasta por 24 hrs en Capillas de la red o domicilio
        				</li>
          				<li>
                        	Carroza funeraria para traslado al parque funeral dentro del área metropolitana.
        				</li>
        			</ul>
                    <p class="subtitle-info">
                    	BONO DE RENOVACION:
                    </p>
                    <p class="font-size-text">
                    	Por ser cliente puntual, se otorga por única vez, un bono para servicios de gastos finales de los padres de titular o cónyuge. (No sujeto a reembolso).
        			</p>
                </td>
            </tr>
            <!--tr>
                <td class="">
                    <img src="public/img/FooterClubSaludPdf.png"/>
                </td>
            </tr-->
    </table>
</html>
