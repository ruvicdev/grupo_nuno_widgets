<style>
    /** FONT SIZES **/
    .font-size-text {
        font-size: 11px;
    }

    .font-weight-header {
        font-size: 11pt;
        font-weight: bold;
    }

    .font-size-main-header {
        font-size: 8pt;
        font-weight: bold;
    }

    /** FONT COLORS **/
    .font-color-label {
        color: red;
    }

    .font-color-dynamic-text {
        color: #C2E3ED;
    }

    .complete_width {
        width: 100%;
    }

    table.outside {
        border: 1px solid #999999;
        padding: 2px;
    }
</style>
<?php if($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
    <div>&nbsp;</div>
<?php endif; ?>
<table class="complete_width">
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            # de Cotización:
        </td>
        <td width="15%" align="left" style="color: #EFB242">
            <?php echo $orden_id; ?>
        </td>
    </tr>
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            Fecha de Creación:
        </td>
        <td width="15%" align="left" style="color: #EFB242">
            <?php echo $current_date; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS GENERALES
        </td>
    </tr>
    <tr>
        <?php if ($vendedor == 'Si'): ?>
            <td align="right" width="20%" style="color: #006C81">
                Eres Vendedor:
            </td>
            <td align="left" width="5%" style="color: #EFB242">
                <?php echo $vendedor; ?>
            </td>
            <td align="right" width="25%" style="color: #006C81">
                Nombre del Vendedor:
            </td>
            <td align="left" width="50%" style="color: #EFB242">
                <?php echo $nombre_vendedor; ?>
            </td>
        <?php else: ?>
            <td align="right" width="20%" style="color: #006C81">
                ¿Eres Vendedor?
            </td>
            <td align="left" width="80%" style="color: #EFB242">
                No
            </td>
        <?php endif; ?>
    </tr>
    <tr>
        <td align="right" width="10%" style="color: #006C81">
            Nombre:
        </td>
        <td width="55%" align="left" style="color: #EFB242">
            <?php if(isset($nombres)): ?>
                <?php $nombre = $nombres; ?>
            <?php else: ?>
                <?php $nombre = (isset($persona_moral)) ? $persona_moral : $nombre . " " . $apellido_paterno . " " . $apellido_materno; ?>
            <?php endif; ?>
            <?php echo $nombre; ?>
        </td>
        <td width="13%" align="right" style="color: #006C81">
            Moneda:
        </td>
        <td align="left" width="22%" style="color: #EFB242">
            <?php echo $tipo_moneda; ?>
        </td>
    </tr>
    <tr>
        <td width="10%" align="right" style="color: #006C81">
            Email:
        </td>
        <td width="55%" align="left" style="color: #EFB242">
            <?php echo $email; ?>
        </td>
        <td width="13%" align="right" style="color: #006C81">
            Telefono:
        </td>
        <td align="left" width="22%" style="color: #EFB242">
            <?php echo $telefono; ?>
        </td>
    </tr>
    <tr>
        <td width="10%" align="right" style="color: #006C81">
            Giro:
        </td>
        <td width="55%" align="left" style="color: #EFB242">
            <?php echo $nombre_giro; ?>
        </td>
        <td width="13%" align="right" style="color: #006C81">
            Vigencia al:
        </td>
        <td align="left" width="22%" style="color: #EFB242">
            <?php echo $validAt; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS DE EMBARQUE
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Valor de la Mercancia:
        </td>
        <td width="27%" align="left" style="color: #EFB242">
            <?php echo "$ " . $valor_merc; ?>
        </td>
        <td width="23%" align="right" style="color: #006C81">
            Tipo de la Mercancia:
        </td>
        <td width="27%" align="left" style="color: #EFB242">
            <?php echo $tipo; ?>
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Mercancia:
        </td>
        <td width="77%" align="left" style="color: #EFB242">
            <?php echo $nombre_merc; ?>
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Desc. de Mercancia:
        </td>
        <td width="77%" align="left" style="color: #EFB242">
            <?php echo $descripcion_merc; ?>
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Empaque:
        </td>
        <td width="77%" align="left" style="color: #EFB242">
            <?php echo $empaque; ?>
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Medio de Transporte:
        </td>
        <td width="77%" aling="left" style="color: #EFB242">
            <?php echo $nombre_medio_trans; ?>
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Cobertura:
        </td>
        <td width="77%" align="left" style="color: #EFB242">
            <?php echo $cobertura; ?>
        </td>
    </tr>
    <tr>
        <td width="23%" align="right" style="color: #006C81">
            Prima Neta
        </td>
        <td width="27%" align="left" style="color: #EFB242">
            <?php echo "$ " . $prima_neta; ?>
        </td>
        <td width="50%" style="font-size: 10px; color: #FF0000; font-weight: bold">
            <small>* No incluye IVA ni derecho de poliza.</small>
            <small>* Este costó no aplica en gastos de expedición.</small>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            ORIGEN
        </td>
    </tr>
    <tr>
        <td width="22%" align="right" style="color: #006C81">
            Continente Origen:
        </td>
        <td width="30%" align="left" style="color: #EFB242">
            <?php echo $continente_origen; ?>
        </td>
        <td width="17%" align="right" style="color: #006C81">
            Pais Origen:
        </td>
        <td width="31%" align="left" style="color: #EFB242">
             <?php echo $pais_origen; ?>
        </td>
    </tr>
    <tr>
        <td width="22%" align="right" style="color: #006C81">
            Estado Origen:
        </td>
        <td width="30%" align="left" style="color: #EFB242">
            <?php echo $estado_origen; ?>
        </td>
        <td width="17%" align="right" style="color: #006C81">
            Ciudad Origen:
        </td>
        <td width="31%" align="left" style="color: #EFB242">
            <?php echo $ciudad_origen; ?>
        </td>
    </tr>
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DESTINO
        </td>
    </tr>
    <tr>
        <td width="22%" align="right" style="color: #006C81">
            Continente Destino:
        </td>
        <td width="30%" align="left" style="color: #EFB242">
            <?php echo $continente_destino; ?>
        </td>
        <td width="17%" align="right" style="color: #006C81">
            Pais Destino:
        </td>
        <td width="31%" align="left" style="color: #EFB242">
             <?php echo $pais_destino; ?>
        </td>
    </tr>
    <tr>
        <td width="22%" align="right" style="color: #006C81">
            Estado Destino:
        </td>
        <td width="30%" align="left" style="color: #EFB242">
            <?php echo $estado_destino; ?>
        </td>
        <td width="17%" align="right" style="color: #006C81">
            Ciudad Destino:
        </td>
        <td width="31%" align="left" style="color: #EFB242">
            <?php echo $ciudad_destino; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            COBERTURAS
        </td>
    </tr>
    <?php if($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
        <tr>
            <td width="25%" style="color: #006C81">
                Cobertura Nacional
            </td>
            <td width="75%" style="color: #EFB242">
                <ul class="list-defined">
                    <li>Riesgos ordinarios de transito</li>
                    <li>Robo total c/ violencia</li>
                    <li>Robo de bulto por entero</li>
                </ul>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td width="25%" style="color: #006C81">
                Cobertura Internacional
            </td>
            <td width="75%" style="color: #EFB242">
                <ul class="list-defined">
                    <li>Riesgos ordinarios de transito</li>
                    <li>Robo total c/ violencia</li>
                    <li>Contribución a la avería gruesa</li>
                    <li>Robo de bulto por entero</li>
                    <li>Echazón y barredura</li>
                    <li>Baratería del capital</li>
                </ul>
            </td>
        </tr>
    <?php endif; ?>
</table>
<div>&nbsp;</div>
<?php if($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<?php endif; ?>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DEDUCIBLES
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81">
            Deducible 10%
        </td>
        <td width="75%" align="left" style="color: #EFB242">
            <ul class="list-defined">
                <?php if ($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
                    <li>Riesgos Ordinarios de Tr&aacute;nsito</li>
                <?php else: ?>
                    <li>Riesgos Ordinarios de Tr&aacute;nsito</li>
                    <li>Contribuci&oacute;n a la aver&iacute;a gruesa</li>
                    <li>Echaz&oacute;n y barredura</li>
                    <li>Barater&iacute;a del capital</li>
                <?php endif; ?>
            </ul>
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81">
            Deducible 25%
        </td>
        <td width="75%" align="left" style="color: #EFB242">
            <ul class="list-defined">
                <li>Robo total de la carga</li>
                <li>Robo de bulto por entero</li>
            </ul>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            MEDIDAS DE SEGURIDAD
        </td>
    </tr>
    <tr>
        <td width="40%" align="right" style="color: #006C81">
            Medidas de Seguridad Aplicadas
        </td>
        <td width="60%" style="color: #EFB242">
            <ul class="list-defined">
                <?php foreach($security as $sec): ?>
                    <?php if(!empty(trim($sec))): ?>
                        <li><?php echo trim($sec); ?></li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            TERMINOS Y CONDICIONES
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" style="font-size: 7px; color: #EFB242">
            <ol>
                <li>El dispositivo de GPS deberá; estar activo y funcionando
                    desde el origen del viaje y hasta su destino. No se
                    tomará como cumplida esta medida en caso de que el
                    dispositivo colocado en el tracto o la caja no se
                    encuentra activo, se encuentre apagado, exista un mal
                    funcionamiento y/o las cuotas a las que está obligado el
                    contratante no hayan sido cubiertas y se haya suspendido
                    el servicio por falta de pago.</li>
                <li>Para poder tomar como cumplida la medida del rastreo
                    satelital se requiere de una empresa legalmente constituida
                    que brinde el servicio. No se aceptarán GPS de
                    celulares, computadoras, tablets y/o cualquier
                    instrumento electránico.</li>
                <li>En ningún momento deberá la mercancía quedar sin
                    vigilancia (chofer como mínimo) y/o seguridad durante
                    los períodos de espera y descarga. En caso que el embarque
                    lleve custodia, esta última deberá; vigilar el embarque
                    hasta que la mercancía sea totalmente recibida por la
                    persona responsable.</li>
                <li>Hacer los viajes de tal manera que al llegar al punto de
                    destino no se tenga que esperar fuera de estas instalaciones
                    para ser recibido, sin vigilancia (chofer como mínimo)
                    y/o seguridad durante los periodos de espera o descarga.</li>
                <li>Uso de caja metálica cerrada o en su defecto, si
                    la naturaleza de la mercancía lo permite, contar con
                    lonas en excelente estado (es decir no estén rotas,
                    rasgadas o perforadas) protegiendo la mercancía que en
                    su totalidad.</li>
                <li>Rutas establecidas con control de tiempos.</li>
                <li>Dicha ruta deberá considerar hora de salida y hora de
                    llegada al destino.</li>
                <li>Para el transporte terrestre así como para las
                    conexiones marítimas el asegurado garantiza el
                    uso de caminos de cuota cuando estén disponibles,
                    no sobrepasar el límite de capacidad de carga
                    especificado para el camión y el contenedor.</li>
                <li>Las escalas para recarga de combustible, descanso, etc.,
                    deberán estar programadas, autorizadas y supervisadas.</li>
                <li>No se permiten desviaciones para fines personales.</li>
                <li>Uso de sellos o candados de acero o naval locks para
                    contenedores y trailers.</li>
                <li>Uso de bitácora de inspección de las unidades por viaje.</li>
                <li>Utilizar carreteras de cuota siempre que existan.</li>
                <li>Deberán indicarse rutas de traslado, cualquier
                    variación deberá revisarse y aprobarse con
                    anterioridad a la salida de la plataforma con el
                    vehículo.</li>
            </ol>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            PAISES EXCLUIDOS
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" style="font-size: 7px; color: #EFB242">
        Afganistán, Algeria, Angola, Arabia Saudita, Armenia, Azerbaijan, Bangladesh, Burkina Faso, Burundi, Camerún (Marítimo: Bakassi Peninsula; Terrestre: Todo), Chad, Corea del Norte,
        Cote d'Ivoire (Costa de Marfil), Cuba, Egipto, Eritrea, Etiopía, Filipinas, Georgia, Guinea, Guinea Ecuatorial, Golfo de Aden zona marítima y 70° E frente a las costas de Somalia,
        limitando al norte entre latitud 20° N, y al sur 12° S, Golfo de Guinea, en la zona marítima entre 3°6'13.966"W 5°8'44.274"N (Cote d’Ivoire – límite de Ghana) y 3°6'13.966"W 2°20'12.8"N
        al oeste, y 9°46'18.636"E 2°20'12.8"N (límite Camerún - Guinea Ecuatorial) al este; India (Marítimo: Gujarat; Terrestre: Kashmir, Assam, Nagaland, Bihar, Jharkand, Chhattisgarh),
        Indonesia, Irán, Irak, Jordánia, Kazakhstan, Kenia, Kyrgyzstan, Líbano, Liberia, Libia (Marítimo: las aguas territoriales a 12 nm de Libia Terrestre: Todo), Madagascar, Mali,
        Mauritania, Moldova (Transnistria), Myanmar (Burma), Nagorno-Karabakh, Nepal, Niger, Nigeria, Palestina (Cisjordania y Gaza), Israel dentro de los 45km de los límites Gaza-Israel,
        incluye Beersheba y Ashdod, Pakistán, República de África Central, República Democrática del Congo, República Federal de Yugoslavia y Serbia, Ruanda, Rusia (Norte del Cáucaso), Sierra Leona,
        Siria, Somalia, Sudán del Norte, Sudán del Sur, Sudán, Tajikistan, Timor Oriental, Turkmenistan, Ucrania, Uganda (Marítimo: Lago Albert; Terrestre: Uganda del Norte), Uzbekistan, Yemen, Zimbabwe
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<?php if ($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
<?php endif; ?>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            MERCANCIAS EXCLUIDAS
        </td>
    </tr>
    <tr>
        <td width="100%" align="left" style="font-size: 7px; color: #EFB242">
            <ol>
                <li>Productos denominados como valores: Documentos o
                    registros de negocios, dinero en efectivo, títulos
                    valores, Instrumentos negociables, Tarjetas Crédito
                    o débito, Estampillas, Cheques o tarjetas de consumo
                    o similares, Billetes de lotería o similares, Oro,
                    metales preciosos y/o artículos hechos de o que
                    contengan metales preciosos y/o piedras, pinturas,
                    estatuas, obras de arte o antigüedades y los bienes
                    de naturaleza similar.</li>
                <li>Pseudoefedrina, efedrina</li>
                <li>Bebidas Alcohólicas, Licores</li>
                <li>Relojes y / o artículos de joyería que
                    no sean de fantasía</li>
                <li>Equipo electrónico portátil, laptops,
                    celulares, PDA’s, Ipod, Ipad, videojuegos y sus accesorios</li>
                <li>Armas y / o municiones, fuegos artificiales, explosivos</li>
                <li>Yates, lanchas y / o embarcaciones de vela por sus
                    propios medios</li>
                <li>Automóviles y Motocicletas</li>
                <li>Mercancías a granel (excepto semillas y cereales)</li>
                <li>Cartas geográficas, mapas o planos</li>
                <li>Cobre</li>
                <li>Polipropileno</li>
                <li>Contenedores</li>
                <li>Materiales de desecho</li>
                <li>Derivados del petróleo y
                    químicos inflamables</li>
            </ol>
        </td>
    </tr>
</table>
