<html>
    <head>
        <title>PDF Cotizacion</title>
        <style>
        .td-header-left {
            text-align: left;
        }
        .td-header-bold {
            font-weight: bold;
            text-align: center;
            background-color: #4F81BD;
        }
        .td-header-bold-2 {
            font-weight: bold;
            text-align: right;
            font-size: 9pt;
            color: white;
            background-color: #4F81BD;
        }
        .font-size-consecutivo {
            font-family: Arial, Helvetica, sans-serif;
            font-weight: bold;
            text-align: right;
            font-size: 8pt;
        }
        .font-size-text {
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            font-size: 9pt;
        }
        .font-size-text-black-bold {
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            font-size: 9pt;
            color: black;
            font-weight: bold;
            background-color: #e0ebeb;
        }
        .font-size-text-blue {
            font-family: Arial, Helvetica, sans-serif;
            text-align: left;
            font-size: 9pt;
            color: #4F81BD;
            font-weight: bold;
        }
        .font-size-text-bold {
            font-family: Arial, Helvetica, sans-serif;
            color: white;
            text-align: justify;
            font-size: 9pt;
            font-weight: bold;
        }
        .font-size-text-bold-right {
            font-family: Arial, Helvetica, sans-serif;
            color: white;
            text-align: right;
            font-size: 9pt;
            font-weight: bold;
        }
        .font-size-text-right {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            font-size: 9pt;
        }
        .font-size-text-right-blue {
            font-family: Arial, Helvetica, sans-serif;
            text-align: right;
            font-size: 9pt;
            font-weight: bold;
            color: #4F81BD;
        }
        .with-main-table {
            width: 100%;
        }
        .with-image-td {
            border: 1px solid #00B0F0;
            background-color:#00B0F0;
            padding: 3px;
            width: 30%;
        }
        .with-info-td {
            width: 100%;
        }
        .title-info {
            font-family: Arial, Helvetica, sans-serif;
            text-align: justify;
            font-size: 9pt;
            font-weight: bold;
            color: white;
            background-color:#4F81BD;
        }
        .subtitle-info {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 9pt;
            text-align: justify;
        }

        .integrantes-table td {
             border: 1px solid black;
        }

        .contado-table {
             border: 1px solid black;
             width: 60%;
        }
        .totales-align {
            text-align: right;
        }
        </style>
    </head>
    <table class="with-main-table">
            <!--tr>
                <td class="font-size-consecutivo">
                    <?php echo $cubre_DeducibleHistorial_inserted_id; ?>
                </td>
            </tr-->
            <tr>
                <td class="with-info-td">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="font-size-consecutivo">
                                <?php echo 'C-'.$cubre_DeducibleHistorial_inserted_id; ?>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    <p class="font-size-text">
                      Agradecemos la oportunidad que nos brinda para presentar esta cotización de Seguro de Gastos Médicos Mayores,
                      la presente propuesta fue elaborada de acuerdo a sus requerimientos, esperando cumpla con sus expectativas:
                    </p>
                    <p class="font-size-text-right-blue">
                      Fecha de Cotización:&nbsp;&nbsp;<?php echo $fechaCotizacion; ?>
                    </p>
                    <p class="font-size-text-blue">
                      CONTRATANTE:&nbsp;&nbsp;<?php echo ucwords(strtolower($fullname)); ?>
                    </p>
                    <p class="font-size-text-blue">
                      Suma Asegurada/Deducible:&nbsp;$<?php echo number_format($deduciblePoliza); ?>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Coaseguro:&nbsp;0%
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Núm. de Asegurados:&nbsp;<?php echo sizeof($integrantes); ?>
                    </p>
                    <p class="title-info">
                        Coberturas Básica
                    </p>
                    <p class="font-size-text">
                      Este seguro cubrirá el deducible pactado en la póliza primaria, por los gastos médicos en que incurra el Asegurado a consecuencia de un accidente y/o  enfermedad cubiertos,
                      Lo anterior, siempre y cuando los gastos médicos en que incurra el asegurado sean superiores o iguales al deducible contratado en la póliza primaria.
                    </p>
                    <p class="title-info">
                    	Coberturas Adicionales
                    </p>
                    <p class="font-size-text-blue">
                      Cobertura en el Extranjero:&nbsp;<?php echo $coberturaExtranjero; ?>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Suma Asegurada/Deducible:&nbsp;$<?php echo number_format($coberturaDeducible == null ? 0 : $coberturaDeducible, 2); ?>
                    </p>
                    <p class="font-size-text-blue">
                      Emergencia en el Extranjero:&nbsp;<?php echo $emergenciaExtranjero; ?>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      Suma Asegurada/Deducible:&nbsp;$<?php echo number_format($emergenciaDeducible == null ? 0 : $emergenciaDeducible, 2); ?>
                    </p>
                    <table class="integrantes-table">
                        <tr>
                            <td class="td-header-bold font-size-text-bold">NOMBRE</td>
                            <td class="td-header-bold font-size-text-bold">EDAD</td>
                            <td class="td-header-bold font-size-text-bold">PARENTESCO</td>
                            <td class="td-header-bold font-size-text-bold">SEXO</td>
                            <td class="td-header-bold font-size-text-bold">PRIMA NETA ANUAL</td>
                        </tr>
                        <?php
                            $primaNetaTotal = 0;
                            foreach($integrantes as $obj):
                        ?>
                        <tr>
                            <td class="font-size-text">
                                <?php echo ucwords(strtolower($obj['nombre'])); ?>
                            </td>
                            <td class="font-size-text">
                                <?php echo ucwords(strtolower($obj['edad'])); ?>
                            </td>
                            <td class="font-size-text">
                                <?php echo ucwords(strtolower($obj['integrante'])); ?>
                            </td>
                            <td class="font-size-text">
                                <?php echo ucwords(strtolower($obj['sexo'])); ?>
                            </td>
                            <td class="font-size-text">
                                <?php echo '$'.number_format($obj['prima_neta_anual'], 2);
                                $primaNetaTotal += $obj['prima_neta_anual'];
                                ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="4" class="font-size-text-black-bold totales-align">
                                PRIMA NETA&nbsp;&nbsp;
                            </td>
                            <td class="font-size-text-black-bold">
                                <?php echo '$'.number_format($primaNetaTotal, 2).' M.N.'; ?>
                            </td>
                        </tr>
                    </table>
                    <p>&nbsp;</p>
                    <table class="contado-table">
                      <?php
                        $recargoPorPagoFraccionado = 0;
                        $derechoDePoilza           = 100;
                        $subtotal                  = $primaNetaTotal+$recargoPorPagoFraccionado+$derechoDePoilza;
                        $iva                       = $subtotal * 0.16;
                        $primaTotal                = $subtotal + $iva;
                      ?>
                      <tr>
                          <td colspan="2" class="td-header-bold-2">Contado</td>
                      </tr>
                      <tr>
                          <td class="font-size-text">
                              PRIMA NETA
                          </td>
                          <td class="font-size-text">
                              <?php echo '$'.number_format($primaNetaTotal, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text">
                              Recargo por Pago Fraccionado
                          </td>
                          <td class="font-size-text">
                              <?php echo '$'.number_format($recargoPorPagoFraccionado, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text">
                              Derecho de Póilza
                          </td>
                          <td class="font-size-text">
                              <?php echo '$'.number_format($derechoDePoilza, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text">
                              Subtotal
                          </td>
                          <td class="font-size-text">
                              <?php echo '$'.number_format($subtotal, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text">
                              IVA
                          </td>
                          <td class="font-size-text">
                              <?php echo '$'.number_format($iva, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text">
                              PRIMA TOTAL
                          </td>
                          <td class="font-size-text">
                              <?php echo '$'.number_format($primaTotal, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text-black-bold">
                              Primer Pago&nbsp;&nbsp;
                          </td>
                          <td class="font-size-text-black-bold">
                              <?php echo '$'.number_format($primaTotal, 2).' M.N.'; ?>
                          </td>
                      </tr>
                      <tr>
                          <td class="font-size-text-black-bold">
                              Subsecuente(s)&nbsp;&nbsp;
                          </td>
                          <td class="font-size-text-black-bold">
                              &nbsp;
                          </td>
                      </tr>
                    </table>
                    <p>&nbsp;</p>
                    <p class="font-size-text-blue">
                      Nombre del Agente:&nbsp;&nbsp;<?php echo ucwords(strtolower($asesor)); ?>
                    </p>
                    <p class="font-size-text-blue">
                      NOTAS<br/>
                      - No se cubren preexistencias ni pago de complementos.<br/>
                      - Suscripción sujeta a los criterios de selección médica establecidos por la compañía.<br/>
                      - Esta cotización es vigente por treinta (30) días naturales a partir de la fecha de cotización.
                    </p>
                </td>
            </tr>
    </table>
</html>
