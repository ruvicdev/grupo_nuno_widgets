<style>
    /** FONT SIZES **/
    .font-size-text {
        font-size: 11px;
    }

    .font-weight-header {
        font-size: 11pt;
        font-weight: bold;
    }

    .font-size-main-header {
        font-size: 8pt;
        font-weight: bold;
    }

    /** FONT COLORS **/
    .font-color-label {
        color: red;
    }

    .font-color-dynamic-text {
        color: #C2E3ED;
    }

    .complete_width {
        width: 100%;
    }

    table.outside, th.inside, td.inside_two {
        border: 1px solid #999999;
        padding: 2px;
    }
</style>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="complete_width">
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            # de Cotización:
        </td>
        <td width="15%" align="left" style="color: #555555">
            <?php echo $id; ?>
        </td>
    </tr>
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            Fecha de Creación:
        </td>
        <td width="15%" align="left" style="color: #555555">
            <?php echo $fecha_creacion; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table border="0" style="border: none;">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            SEGURO DE RESPONSABILIDAD POR ADMINSITRACION PARA EJECUTIVOS Y LA EMPRESA
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<?php if ($es_vendedor == 'Si' || $es_vendedor == 'si'): ?>
    <table class="outside">
        <tr align="center" class="font-weight-header">
            <td width="100%" style="font-size: 14px; color: #006C81">
                DATOS DEL VENDEDOR
            </td>
        </tr>
        <tr>
            <td align="right" width="25%" style="color: #006C81">
                Eres Vendedor?
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $es_vendedor; ?>
            </td>
        </tr>
        <tr>
            <td align="right" width="25%" style="color: #006C81">
                Nombre del Vendedor:
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $nombre_vendedor; ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS GENERALES
        </td>
    </tr>
    <?php if ($tipo_persona == 0): ?>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Nombre:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $nombre_user; ?>
            </td>
        </tr>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Apellido Paterno:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $ap_user; ?>
            </td>
        </tr>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Apellido Materno:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $am_user; ?>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Nombre de la Empresa:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $empresa_user; ?>
            </td>
        </tr>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Nombre del Contacto:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $contacto_user; ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Direcci&oacute;n de la Empresa:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $direccion; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            E-mail:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $email; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Telefono:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $telefono; ?>
        </td>
    </tr>
    <!--tr>
        <td width="39%" align="right" style="color: #006C81">
            Fecha de creaci&oacute;n:
        </td>
        <td width="61%" align="left" style="color: #555555">
            < ?php echo $fecha_cotizacion; ?>
        </td>
    </tr-->
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Numero de Empleados:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $numero_empleados; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Facturacion: 
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $facturacion; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Limite de Indemnizaci&oacute;n:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $indemnizacion; ?> USD
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Sublimite de Responsabilidad Laboral:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $sublimite_responsabilidad; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Deducible:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $deducible; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Deducible Responsabilidad Laboral:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $deducible_responsabilidad; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Prima Neta Anual:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $prima_neta_anual; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table border="0" style="border: none;">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            SEGURO DE RESPONSABILIDAD POR ADMINSITRACION PARA EJECUTIVOS Y LA EMPRESA
        </td>
    </tr>
</table>
<table class="outside">
    <thead>
        <tr class="font-weight-header">
            <th class="inside" align="center" width="100%" style="font-size: 14px; color: #006C81">
                Coberturas
            </th>
        </tr>
        <tr>
            <th align="center" class="inside" width="15%" style="color: #006C81; font-size: 10px">
                Facturaci&oacute;n
            </th>
            <th align="center" class="inside" width="15%" style="color: #006C81; font-size: 10px">
                Limite de Indemnizaci&oacute;n
            </th>
            <th align="center" class="inside" width="15%" style="color: #006C81; font-size: 10px">
                Sublimite Responsabilidad Laboral
            </th>
            <th align="center" class="inside" width="15%" style="color: #006C81; font-size: 10px">
                Deducible
            </th>
            <th align="center" class="inside" width="15%" style="color: #006C81; font-size: 10px">
                Deducible Responsabilidad Laboral
            </th>
            <th align="center" class="inside" width="15%" style="color: #006C81; font-size: 10px">
                Prima Neta Anual
            </th>
            <th align="center" class="inside" width="10%" style="color: #006C81; font-size: 10px">
                Opcion
            </th>
        </tr>
    </thead>
        <tr>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 1): ?>
                    <span style="color: #FF0000">Hasta $5M USD</span>
                <?php else: ?>
                    Hasta $5M USD
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 1 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 100,000 USD</span><br>
                <?php else: ?>
                    $ 100,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 1 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 1 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 1,000,000 USD</span><br>
                <?php else: ?>
                    $ 1,000,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 1 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 2,000,000 USD</span><br>
                <?php else: ?>
                    $ 2,000,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 1 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 100,000 USD</span><br>
                <?php else: ?>
                    $ 100,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 1 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 2,000 USD</span><br>
                <?php else: ?>
                    $ 2,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 2,000 USD</span><br>
                <?php else: ?>
                    $ 2,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 2,000 USD</span><br>
                <?php else: ?>
                    $ 2,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 2,000 USD</span><br>
                <?php else: ?>
                    $ 2,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 2,000 USD</span><br>
                <?php else: ?>
                    $ 2,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 600 USD</span><br>
                <?php else: ?>
                    $ 600 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 850 USD</span><br>
                <?php else: ?>
                    $ 850 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 1,225 USD</span><br>
                <?php else: ?>
                    $ 1,225 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 1,950 USD</span><br>
                <?php else: ?>
                    $ 1,950 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion ==1 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 2,625 USD</span><br>
                <?php else: ?>
                    $ 2,625 USD <br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="10%" style="color: #555555; font-size: 9px">                
                <?php for($i = 1; $i <= 5; $i++): ?>
                    <?php if($i == $id_seccion_facturacion && $id_facturacion == 1): ?>
                        <span style="color: #FF0000">X</span><br />
                    <?php else: ?>
                        <span>--</span><br />
                    <?php endif; ?>
                <?php endfor; ?>
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 2): ?>
                    <span style="color: #FF0000">Desde $5M USD hasta $15M USD</span>
                <?php else: ?>
                    Desde $5M USD hasta $15M USD
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_seccion_facturacion == 1 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 2 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if ($id_seccion_facturacion == 3 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 1,000,000 USD</span><br>
                <?php else: ?>
                    $ 1,000,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 4 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 2,000,000 USD</span><br>
                <?php else: ?>
                    $ 2,000,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_seccion_facturacion == 1 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 2 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if ($id_seccion_facturacion == 3 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 4 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_seccion_facturacion == 1 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 2 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 3 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 4 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 5,000 USD</span><br>
                <?php else: ?>
                    $ 5,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                 <?php if($id_seccion_facturacion == 1 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 2 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 10,000 USD/span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 3 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 4 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
             <?php if($id_seccion_facturacion == 1 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 1,000 USD</span><br>
                <?php else: ?>
                    $ 1,000 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 2 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 1,400 USD</span><br>
                <?php else: ?>
                    $ 1,400 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 3 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 1,875 USD</span><br>
                <?php else: ?>
                    $ 1,875 USD<br>
                <?php endif; ?>

                <?php if($id_seccion_facturacion == 4 && $id_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 2,895 USD</span><br>
                <?php else: ?>
                    $ 2,895 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="10%" style="color: #555555; font-size: 9px">
                <?php for($i = 1; $i <= 4; $i++): ?>
                    <?php if($i == $id_seccion_facturacion && $id_facturacion == 2): ?>
                        <span style="color: #FF0000">X</span><br>
                    <?php else: ?>
                        <span>--</span><br>
                    <?php endif; ?>
                <?php endfor; ?>
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 3): ?>
                    <span style="color: #FF0000">Desde $15M USD hasta $25M USD</span>
                <?php else: ?>
                    Desde $15M USD hasta $25M USD
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 1,000,000 USD</span><br>
                <?php else: ?>
                    $ 1,000,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 2,000,000 USD</span><br>
                <?php else: ?>
                    $ 2,000,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 3,000,000 USD</span><br>
                <?php else: ?>
                    $ 3,000,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 5,000,000 USD</span><br>
                <?php else: ?>
                    $ 5,000,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 1,200 USD</span><br>
                <?php else: ?>
                    $ 1,200 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 1,650 USD</span><br>
                <?php else: ?>
                    $ 1,650 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 2,250 USD</span><br>
                <?php else: ?>
                    $ 2,250 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 2,925 USD</span><br>
                <?php else: ?>
                    $ 2,925 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 3,650 USD</span><br>
                <?php else: ?>
                    $ 3,650 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 3 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 5,650 USD</span><br>
                <?php else: ?>
                    $ 5,650 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="10%" style="color: #555555; font-size: 9px">
                <?php for($i = 1; $i <= 6; $i++): ?>
                    <?php if($i == $id_seccion_facturacion && $id_facturacion == 3): ?>
                        <span style="color: #FF0000">X</span><br>
                    <?php else: ?>
                        <span>--</span><br>
                    <?php endif; ?>
                <?php endfor; ?>
            </td>
        </tr>
        <tr>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 4): ?>
                    <span style="color: #FF0000">Desde $25M USD hasta $50M USD</span>
                <?php else: ?>
                    Desde $25M USD hasta $50M USD
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 1,000,000 USD</span><br>
                <?php else: ?>
                    $ 1,000,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 2,000,000 USD</span><br>
                <?php else: ?>
                    $ 2,000,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 3,000,000 USD</span><br>
                <?php else: ?>
                    $ 3,000,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 5,000,000 USD</span><br>
                <?php else: ?>
                    $ 5,000,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 250,000 USD</span><br>
                <?php else: ?>
                    $ 250,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 500,000 USD</span><br>
                <?php else: ?>
                    $ 500,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>

                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 10,000 USD</span><br>
                <?php else: ?>
                    $ 10,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 15,000 USD</span><br>
                <?php else: ?>
                    $ 15,000 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="15%" style="color: #555555; font-size: 9px">
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 1): ?>
                    <span style="color: #FF0000">$ 1,450 USD</span><br>
                <?php else: ?>
                    $ 1,450 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 2): ?>
                    <span style="color: #FF0000">$ 1,850 USD</span><br>
                <?php else: ?>
                    $ 1,850 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 3): ?>
                    <span style="color: #FF0000">$ 2,250 USD</span><br>
                <?php else: ?>
                    $ 2,250 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 4): ?>
                    <span style="color: #FF0000">$ 3,325 USD</span><br>
                <?php else: ?>
                    $ 3,325 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 5): ?>
                    <span style="color: #FF0000">$ 4,150 USD</span><br>
                <?php else: ?>
                    $ 4,150 USD<br>
                <?php endif; ?>
                
                <?php if($id_facturacion == 4 && $id_seccion_facturacion == 6): ?>
                    <span style="color: #FF0000">$ 6,950 USD</span><br>
                <?php else: ?>
                    $ 6,950 USD<br>
                <?php endif; ?>
            </td>
            <td align="center" class="inside_two" width="10%" style="color: #555555; font-size: 9px">
                <?php for($i = 1; $i <= 6; $i++): ?>
                    <?php if($i == $id_seccion_facturacion && $id_facturacion == 4): ?>
                        <span style="color: #FF0000">X</span><br>
                    <?php else: ?>
                        <span>--</span><br>
                    <?php endif; ?>
                <?php endfor; ?>
            </td>
        </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            SEGURO DE RESPONSABILIDAD POR ADMINSITRACION PARA EJECUTIVOS Y LA EMPRESA
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Periodo de descubrimiento
        </td>
        <td width="50%" align="left" style="color: #555555">
            Periodo autom&aacute;tico de 60 d&iacute;as
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Accionistas controladores
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubiertos
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            P&eacute;rdida por crisis
        </td>
        <td width="50%" align="left" style="color: #555555">
            Sublimitado a $ 50,000 USD
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Anticipo de costos de defensa
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Periodo de descubrimiento para personas aseguradas retiradas
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Costos de investigaci&oacute;n
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Herederos, herencias y representantes legales
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Nuevas empresas
        </td>
        <td width="50%" align="left" style="color: #555555">
            
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Costos por concepto de defensa en materias de seguridad social y laboral
        </td>
        <td width="50%" align="left" style="color: #555555">
            Sublimitado $ 50,000 USD
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Sanci&oacute;n pecuniaria contra ejecutivos
        </td>
        <td width="50%" align="left" style="color: #555555">
            Sublimitada a $ 30,000 USD
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Contaminaci&oacute;n
        </td>
        <td width="50%" align="left" style="color: #555555">
            Sublimitado a $ 100,000 USD
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Da&ntilde;as a la reputaci&oacute;n
        </td>
        <td width="50%" align="left" style="color: #555555">
            Sublimitado a $ 50,000 USD
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            C&oacute;nyuge
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Administraci&oacute;n de fondos de pensiones
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
    <tr>
        <td width="50%" align="right" style="color: #006C81">
            Reclamaciones por personas aseguradas
        </td>
        <td width="50%" align="left" style="color: #555555">
            Cubierto
        </td>
    </tr>
</table>