<style>
    /** FONT SIZES **/
    .font-size-text {
        font-size: 11px;
    }

    .font-weight-header {
        font-size: 11pt;
        font-weight: bold;
    }

    .font-size-main-header {
        font-size: 8pt;
        font-weight: bold;
    }

    /** FONT COLORS **/
    .font-color-label {
        color: red;
    }

    .font-color-dynamic-text {
        color: #C2E3ED;
    }

    .complete_width {
        width: 100%;
    }

    table.outside, th.inside, td.inside_two {
        border: 1px solid #999999;
        padding: 2px;
    }
</style>
<table class="complete_width">
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="38%" align="right" style="color: #006C81">
            # de Cotización:
        </td>
        <td width="17%" align="left" style="color: #555555">
            <?php echo $id; ?>
        </td>
    </tr>
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="38%" align="right" style="color: #006C81">
            Fecha de Cotizaci&oacute;n:
        </td>
        <td width="17%" align="left" style="color: #555555">
            <?php echo $fecha_creacion; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table border="0" style="border: none;">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            Seguro de Proteccion Contra Actos Fraudulentos PyME
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php if($vendedor == 'Si' || $vendedor == 'si'): ?>
    <table class="outside">
        <tr align="center" class="font-weight-header">
            <td width="100%" style="font-size: 14px; color: #006C81">
                DATOS DEL VENDEDOR
            </td>
        </tr>
        <tr>
            <td align="right" width="25%" style="color: #006C81">
                Nombre del Vendedor:
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $nombre_vendedor; ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS DEL ASEGURADO
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Nombre del Asegurado:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $nombre_asegurado; ?>
        </td>
    </tr>
    <!-- ?php if($contacto != ''): ?>
        <tr>
            <td align="right" width="30%" style="color: #006C81">
                Nombre del Contacto:
            </td>
            <td width="70%" align="left" style="color: #555555">
                < ?php echo $contacto; ?>
            </td>
        </tr>
    < ?php endif ; ? -->
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Email:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $email; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Telefono:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $telefono; ?>
        </td>
    </tr>
    <tr class="font-weight-header" align="center">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DIRECCION FISCAL
        </td>
    </tr>
    <tr>
        <td width="30%" align="right" style="color: #006C81">
            Calle y Numero:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $direccion; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            RFC
        </td>
        <td align="left" width="70%" style="color: #555555">
            <?php echo strtoupper($rfc); ?>
        </td>
    </tr>
    <tr>
        <td width="30%" align="right" style="color: #006C81">
            Colonia
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $colonia; ?>
        </td>
    </tr>
    <tr>
        <td width="30%" align="right" style="color: #006C81">
            Delegacion / Municipio:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $municipio; ?>
        </td>
    </tr>
    <tr>
        <td width="30%" align="right" style="color: #006C81">
            C&oacute;digo Postal:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $codigo_postal; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php if($vendedor == 'No' || $vendedor == 'no'): ?>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php else: ?>
<div>&nbsp;</div>
<?php endif; ?>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS DE LA POLIZA
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Vigencia desde:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $vigencia_desde; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Vigencia hasta:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $vigencia_hasta; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Renovaci&oacute;n:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $renovacion; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Forma de Pago:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $forma_pago; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Ingresos anuales:
        </td>
        <td width="70%" align="left" style="color: #555555">
            MXN $<?php echo $ingresos_anuales; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Maximo:
        </td>
        <td width="70%" align="left" style="color: #555555">
            MXN $<?php echo $maximo_dinero; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            N&uacute;mero de empleados:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $no_empleados; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Maximo:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $maximo_empleados; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            L&iacute;mite requerido:
        </td>
        <td width="70%" align="left" style="color: #555555">
            MXN $<?php echo $limite_requerido; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="15%" style="color: #006C81">
            Desde:
        </td>
        <td width="35%" align="left" style="color: #555555">
            MXN $<?php echo $desde; ?>
        </td>
        <td align="right" width="15%" style="color: #006C81">
            Hasta:
        </td>
        <td width="35%" align="left" style="color: #555555">
            MXN $<?php echo $hasta; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Giro de la Empresa:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $giro_empresa; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Numero de Sucursales:
        </td>
        <td width="70%" align="left" style="color: #555555">
            <?php echo $no_sucursales; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            SINIESTRALIDAD
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Ultimo a&ntilde;o:
        </td>
        <td width="70%" align="left" style="color: #555555">
            MXN $<?php echo $ultimo_ano; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Penuiltimo a&ntilde;o:
        </td>
        <td width="70%" align="left" style="color: #555555">
            MXN $<?php echo $penultimo_ano; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="30%" style="color: #006C81">
            Antepenultimo a&ntilde;o:
        </td>
        <td width="70%" align="left" style="color: #555555">
            MXN $<?php echo $antepenultimo_ano; ?>
        </td>
    </tr>
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            GIROS EXCLUIDOS
        </td>
    </tr>
    <tr>
        <td align="left" width="100%" style="color: #555555">
            <ul>
                <li>Empresas relacionadas con procesamientos de datos o procesadores de datos</li>
                <li>Negocios de IT ( Tecnología, asesoría, outsourcing, etc)</li>
                <li>Empresas de empleos temporales</li>
                <li>Empresas de empleos temporales</li>
                <li>Casas de cambio</li>
                <li>Casas de empe&ntilde;o</li>
                <li>Transportadores de valores</li>
                <li>Juegos de azar y loter&iacute;as</li>
                <li>Joyer&iacute;as</li>
                <li>Ventas de tarjetas de tel&eacute;fonos celulares</li>
                <li>Instituciones Financieras, cualquier negocio financiero (Captación, colocación o transportación
                    de dinero o valores)</li>
            </ul>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            COTIZACION
        </td>
    </tr>
    <?php if(isset($detalle_siniestro)): ?>
        <?php if (trim($detalle_siniestro) != ""): ?>
            <tr>
                <td align="right" width="50%" style="color: #006C81">
                    Detalles del Siniestro:
                </td>
                <td width="50%" align="left" style="color: #555555">
                    <?php echo $detalle_siniestro; ?>
                </td>
            </tr>
        <?php endif; ?>
    <?php endif; ?>
    <tr>
        <td align="right" width="60%" style="color: #006C81">
            Prima neta:
        </td>
        <td width="40%" align="left" style="color: #555555">
            MXN $ <?php echo $prima_neta; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="60%" style="color: #006C81">
            Deducible (10% de la pérdida con minimo de):
        </td>
        <td width="40%" align="left" style="color: #555555">
            MXN $ <?php echo $deducible; ?>
        </td>
    </tr>
    <tr>
        <td align="right" width="60%" style="color: #006C81">
            Total:
        </td>
        <td width="40%" align="left" style="color: #555555">
            MXN $ <?php echo $total; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            TERMINOS Y CONDICIONES
        </td>
    </tr>
    <tr>
        <td width="100%" style="font-size: 7px; color: #555555">
            EN CASO DE EXISTIR SINIESTRALIDAD O RESPUESTAS NEGATIVAS, FAVOR DE SOLICITAR COTIZACION AL PROFIT
        </td>
    </tr>
    <tr>
        <td width="100%" style="font-size: 7px; color: #555555">
            El solicitante está obligado a declarar al Asegurador, de acuerdo a este cuestionario, todos los hechos importantes
            para la apreciación del riesgo que pueda influir en las condiciones convenidas, tales como los conozca o deba conocer
            en el momento de la celebración del contrato. Cualquier omisión o inexacta declaración de los hechos a que se refieren
            los artículos 8, 9 y 10 de la Ley Sobre el Contrato de Seguro, facultará a la Aseguradora para considerar rescindido
            de pleno derecho el contrato, aunque no hayan influido en la realización del contrato.
        </td>
    </tr>
</table>
