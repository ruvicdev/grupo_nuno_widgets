td.header {
    width: 50%;
}

td.header_two {
    width: 60%;
}

td.size_one {
    width: 25%;
}

td.size_two {
    width: 75%;
}

td.size_three {
    width: 30%;
}

td.size_four {
    width: 70%;
}

td.size_five {
    width: 30%;
}

td.size_six {
    width: 70%;
}

td.size_seven {
    width: 50%;
}

td.size_eight {
    width: 100%;
}

table.outside {
    border: 1px solid black;
    padding: 2px;
}

<table>
    <tr>
        <td class="img"></td>
        <td class="img"><img src="public/img/logo.jpg" width="200"></td>
        <td class="img"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td class="header_two font-weight-header">
            COTIZACION GRUPO NUÑO
        </td>
        <td class="font-size-text">
            NUMERO DE COTIZACION: <?php echo $orden_id; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="header_two">
            SEGUROS DE TRANSPORTE DE MERCANCIAS
        </td>
        <td>
            FECHA DE CREACION: <?php echo $current_date; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr class="font-weight-header">
        <td>
            DATOS GENERALES
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="header">
            <?php $nombre = (isset($persona_moral)) ? $persona_moral : $nombre . " " . $apellido_paterno . " " . $apellido_materno; ?>
            NOMBRE: <?php echo $nombre; ?>
        </td>
        <td>
            MONEDA: <?php echo $tipo_moneda; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="header">
            GIRO: <?php echo $nombre_giro; ?>
        </td>
        <td class="header">
            VIGENCIA AL: <?php echo $validAt; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr class="font-size-text">
        <td class="size_three">
            VALOR DE LA MERCANCIA:
        </td>
        <td class="size_four">
            <?php echo "$ " . $valor_merc; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_three">
            MERCANCIA:
        </td>
        <td class="size_four">
            <?php echo $nombre_merc; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_three">
            DESCRIPCION DE LA MERCANCIA:
        </td>
        <td class="size_four">
            <?php echo $descripcion_merc; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_three">
            EMPAQUE
        </td>
        <td class="size_four">
            <?php echo $empaque; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_three">
            TIPO:
        </td>
        <td class="size_four">
            <?php echo $tipo; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_three">
            COBERTURA:
        </td>
        <td class="size_four">
            <?php echo $cobertura; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td>
            ORIGEN
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_seven">
            CONTINENTE ORIGEN: <?php echo $continente_origen; ?>
        </td>
        <td class="size_seven">
            PAIS ORIGEN: <?php echo $pais_origen; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_seven">
            ESTADO ORIGEN: <?php echo $estado_origen; ?>
        </td>
        <td class="size_seven">
            CIUDAD ORIGEN: <?php echo $ciudad_origen; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td>
            DESTINO:
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_seven">
            CONTINENTE DESTINO: <?php echo $continente_destino; ?>
        </td>
        <td class="size_seven">
            PAIS DESTINO: <?php echo $pais_destino; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_seven">
            ESTADO DESTINO: <?php echo $estado_destino; ?>
        </td>
        <td class="size_seven">
            CIUDAD DESTINO: <?php echo $ciudad_destino; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_three">
            MEDIO DE TRANSPORTE:
        </td>
        <td class="size_four">
            <?php echo $nombre_medio_trans; ?>
        </td>
    </tr>
    <tr class="font-size-text">
        <!--td class="header">
            TARIFA: < ?php echo "$ " . $total_pagar; ?>
        </td-->
        <td><!-- class="header" -->
            PRIMA TOTAL: <?php echo "$ " . $prima_neta; ?>
        </td>
    </tr>
    <tr class="font-size-text" style="line-height: -3px">
        <td class="size_eight">
            <small>* No incluye IVA ni derecho de poliza.</small>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_eigh">
            <small>* Este costó no aplica en gastos de expedición.</small>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr>
        <td class="font-weight-header size_five">COBERTURAS:</td>
        <td class="font-size-text size_six">
            <?php if($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
                <label class="bold_text_title">
                    Cobertura Nacional
                </label>
                <ul class="list-defined">
                    <li>Riesgos ordinarios de transito</li>
                    <li>Robo total c/ violencia</li>
                    <li>Robo de bulto por entero</li>
                </ul>
            <?php else: ?>
                <label class="bold_text_title">
                    Cobertura Internacional
                </label>
                <ul class="list-defined">
                    <li>Riesgos ordinarios de transito</li>
                    <li>Robo total c/ violencia</li>
                    <li>Contribución a la avería gruesa</li>
                    <li>Robo de bulto por entero</li>
                    <li>Echazón y barredura</li>
                    <li>Baratería del capital</li>
                </ul>
            <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td class="font-weight-header size_five">DEDUCIBLES:</td>
        <td class="font-size-text size_six">
            <label class="bold_text_title">
                Deducible 10%
            </label>
            <ul class="list-defined">
                <?php if ($cobertura == 'nacional' || $cobertura == 'Nacional'): ?>
                    <li>Riesgos Ordinarios de Tr&aacute;nsito</li>
                <?php else: ?>
                    <li>Riesgos Ordinarios de Tr&aacute;nsito</li>
                    <li>Contribuci&oacute;n a la aver&iacute;a gruesa</li>
                    <li>Echaz&oacute;n y barredura</li>
                    <li>Barater&iacute;a del capital</li>
                <?php endif; ?>
            </ul>
            <label class="bold_text_title">
                Deducible 25%
            </label>
            <ul class="list-defined">
                <li>Robo total de la carga</li>
                <li>Robo de bulto por entero</li>
            </ul>
        </td>
    </tr>
    <tr>
        <td class="font-weight-header size_five">MEDIDAS DE SEGURIDAD:</td>
        <td class="font-size-text size_six">
            <label class="bold_text_title">
                Aplica las siguientes medidas de seguridad:
            </label>
            <ul class="list-defined">
                <?php foreach($security as $sec): ?>
                    <li><?php echo $sec; ?></li>
                <?php endforeach; ?>
            </ul>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr class="font-weight-header">
        <td>
            CONDICIONES
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_one">TERMINOS Y CONDICIONES:</td>
        <td class="size_two">
            <ol>
                <li>El dispositivo de GPS deberá; estar activo y funcionando
                    desde el origen del viaje y hasta su destino. No se
                    tomará como cumplida esta medida en caso de que el
                    dispositivo colocado en el tracto o la caja no se
                    encuentra activo, se encuentre apagado, exista un mal
                    funcionamiento y/o las cuotas a las que está obligado el
                    contratante no hayan sido cubiertas y se haya suspendido
                    el servicio por falta de pago.</li>
                <li>Para poder tomar como cumplida la medida del rastreo
                    satelital se requiere de una empresa legalmente constituida
                    que brinde el servicio. No se aceptarán GPS de
                    celulares, computadoras, tablets y/o cualquier
                    instrumento electránico.</li>
                <li>En ningún momento deberá la mercancía quedar sin
                    vigilancia (chofer como mínimo) y/o seguridad durante
                    los períodos de espera y descarga. En caso que el embarque
                    lleve custodia, esta última deberá; vigilar el embarque
                    hasta que la mercancía sea totalmente recibida por la
                    persona responsable.</li>
                <li>Hacer los viajes de tal manera que al llegar al punto de
                    destino no se tenga que esperar fuera de estas instalaciones
                    para ser recibido, sin vigilancia (chofer como mínimo)
                    y/o seguridad durante los periodos de espera o descarga.</li>
                <li>Uso de caja metálica cerrada o en su defecto, si
                    la naturaleza de la mercancía lo permite, contar con
                    lonas en excelente estado (es decir no estén rotas,
                    rasgadas o perforadas) protegiendo la mercancía que en
                    su totalidad.</li>
                <li>Rutas establecidas con control de tiempos.</li>
                <li>Dicha ruta deberá considerar hora de salida y hora de
                    llegada al destino.</li>
                <li>Para el transporte terrestre así como para las
                    conexiones marítimas el asegurado garantiza el
                    uso de caminos de cuota cuando estén disponibles,
                    no sobrepasar el límite de capacidad de carga
                    especificado para el camión y el contenedor.</li>
                <li>Las escalas para recarga de combustible, descanso, etc.,
                    deberán estar programadas, autorizadas y supervisadas.</li>
                <li>No se permiten desviaciones para fines personales.</li>
                <li>Uso de sellos o candados de acero o naval locks para
                    contenedores y trailers.</li>
                <li>Uso de bitácora de inspección de las unidades por viaje.</li>
                <li>Utilizar carreteras de cuota siempre que existan.</li>
                <li>Deberán indicarse rutas de traslado, cualquier
                    variación deberá revisarse y aprobarse con
                    anterioridad a la salida de la plataforma con el
                    vehículo.</li>
            </ol>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_one">PAISES EXCLUIDOS:</td>
        <td class="size_two">
            <ol>
                <li>Afganistán</li>
                <li>Algeria</li>
                <li>Angola</li>
                <li>Arabia Saudita</li>
                <li>Armenia</li>
                <li>Azerbaijan</li>
                <li>Bangladesh</li>
                <li>Burkina Faso</li>
                <li>Burundi</li>
                <li>Camerún (Marítimo: Bakassi Peninsula; Terrestre: Todo)</li>
                <li>Chad</li>
                <li>Corea del Norte</li>
                <li>Cote d'Ivoire (Costa de Marfil)</li>
                <li>Cuba</li>
                <li>Egipto</li>
                <li>Eritrea</li>
                <li>Etiopía</li>
                <li>Filipinas</li>
                <li>Georgia</li>
                <li>Guinea</li>
                <li>Guinea Ecuatorial</li>
                <li>Golfo de Aden zona marítima y 70° E frente a
                    las costas de Somalia, limitando al norte entre
                    latitud 20° N, y al sur 12° S</li>
                <li>Golfo de Guinea, en la zona marítima entre
                    3°6'13.966"W 5°8'44.274"N (Cote d’Ivoire – límite de Ghana)
                    y 3°6'13.966"W 2°20'12.8"N al oeste, y
                    9°46'18.636"E 2°20'12.8"N (límite Camerún - Guinea Ecuatorial)
                    al este; India (Marítimo: Gujarat; Terrestre: Kashmir,
                    Assam, Nagaland, Bihar, Jharkand, Chhattisgarh)</li>
                <li>Indonesia</li>
                <li>Irán</li>
                <li>Irak</li>
                <li>Jordánia</li>
                <li>Kazakhstan</li>
                <li>Kenia</li>
                <li>Kyrgyzstan</li>
                <li>Líbano</li>
                <li>Liberia</li>
                <li>Libia (Marítimo: las aguas territoriales a
                    12 nm de Libia Terrestre: Todo)</li>
                <li>Madagascar</li>
                <li>Mali</li>
                <li>Mauritania</li>
                <li>Moldova (Transnistria)</li>
                <li>Myanmar (Burma)</li>
                <li>Nagorno-Karabakh</li>
                <li>Nepal</li>
                <li>Niger</li>
                <li>Nigeria</li>
                <li>Palestina (Cisjordania y Gaza)</li>
                <li>Israel dentro de los 45km de los límites
                    Gaza-Israel, incluye Beersheba y Ashdod</li>
                <li>Pakistán</li>
                <li>República de África Central</li>
                <li>República Democrática del Congo</li>
                <li>República Federal de Yugoslavia y Serbia</li>
                <li>Ruanda</li>
                <li>Rusia (Norte del Cáucaso)</li>
                <li>Sierra Leona</li>
                <li>Siria</li>
                <li>Somalia</li>
                <li>Sudán del Norte</li>
                <li>Sudán del Sur</li>
                <li>Sudán</li>
                <li>Tajikistan</li>
                <li>Timor Oriental</li>
                <li>Turkmenistan</li>
                <li>Ucrania</li>
                <li>Uganda (Marítimo: Lago Albert; Terrestre:
                    Uganda del Norte)</li>
                <li>Uzbekistan</li>
                <li>Yemen</li>
                <li>Zimbabwe</li>
            </ol>
        </td>
    </tr>
    <tr class="font-size-text">
        <td class="size_one">MERCANCIAS EXCLUIDAS:</td>
        <td class="size_two">
            <ol>
                <li>Productos denominados como valores: Documentos o
                    registros de negocios, dinero en efectivo, títulos
                    valores, Instrumentos negociables, Tarjetas Crédito
                    o débito, Estampillas, Cheques o tarjetas de consumo
                    o similares, Billetes de lotería o similares, Oro,
                    metales preciosos y/o artículos hechos de o que
                    contengan metales preciosos y/o piedras, pinturas,
                    estatuas, obras de arte o antigüedades y los bienes
                    de naturaleza similar.</li>
                <li>Pseudoefedrina, efedrina</li>
                <li>Bebidas Alcohólicas, Licores</li>
                <li>Relojes y / o artículos de joyería que
                    no sean de fantasía</li>
                <li>Equipo electrónico portátil, laptops,
                    celulares, PDA’s, Ipod, Ipad, videojuegos y sus accesorios</li>
                <li>Armas y / o municiones, fuegos artificiales, explosivos</li>
                <li>Yates, lanchas y / o embarcaciones de vela por sus
                    propios medios</li>
                <li>Automóviles y Motocicletas</li>
                <li>Mercancías a granel (excepto semillas y cereales)</li>
                <li>Cartas geográficas, mapas o planos</li>
                <li>Cobre</li>
                <li>Polipropileno</li>
                <li>Contenedores</li>
                <li>Materiales de desecho</li>
                <li>Derivados del petróleo y
                    químicos inflamables</li>
            </ol>
        </td>
    </tr>
</table>
