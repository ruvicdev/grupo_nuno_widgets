<style>
    /** FONT SIZES **/
    .font-size-text {
        font-size: 11px;
    }

    .font-weight-header {
        font-size: 11pt;
        font-weight: bold;
    }

    .font-size-main-header {
        font-size: 8pt;
        font-weight: bold;
    }

    /** FONT COLORS **/
    .font-color-label {
        color: red;
    }

    .font-color-dynamic-text {
        color: #C2E3ED;
    }

    .complete_width {
        width: 100%;
    }

    table.outside, th.inside, td.inside_two {
        border: 1px solid #999999;
        padding: 2px;
    }
</style>
<div>&nbsp;</div>
<div>&nbsp;</div>
<table class="complete_width">
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            # de Cotización:
        </td>
        <td width="15%" align="left" style="color: #555555">
            <?php echo $id; ?>
        </td>
    </tr>
    <tr class="font-size-main-header">
        <td width="45%">&nbsp;</td>
        <td style="" width="40%" align="right" style="color: #006C81">
            Fecha de Creación:
        </td>
        <td width="15%" align="left" style="color: #555555">
            <?php echo $creacion; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table border="0" style="border: none;">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            SEGURO DE PROTECCION DE DATOS
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<?php if ($es_vendedor == 'Si' || $es_vendedor == 'si'): ?>
    <table class="outside">
        <tr align="center" class="font-weight-header">
            <td width="100%" style="font-size: 14px; color: #006C81">
                DATOS DEL VENDEDOR
            </td>
        </tr>
        <tr>
            <td align="right" width="25%" style="color: #006C81">
                Eres Vendedor?
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $es_vendedor; ?>
            </td>
        </tr>
        <tr>
            <td align="right" width="25%" style="color: #006C81">
                Nombre del Vendedor:
            </td>
            <td width="50%" align="left" style="color: #555555">
                <?php echo $nombre_vendedor; ?>
            </td>
        </tr>
    </table>
<?php endif; ?>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            DATOS GENERALES
        </td>
    </tr>
    <?php if($tipo_persona == 0): ?>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Nombre:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $nombre; ?>
            </td>
        </tr>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Apellido Paterno:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $paterno; ?>
            </td>
        </tr>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Apellido Materno:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $materno; ?>
            </td>
        </tr>
    <?php else: ?>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Empresa:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $nombre_empresa; ?>
            </td>
        </tr>
        <tr>
            <td width="39%" align="right" style="color: #006C81">
                Nombre del Contacto:
            </td>
            <td width="61%" align="left" style="color: #555555">
                <?php echo $nombre_contacto; ?>
            </td>
        </tr>
    <?php endif; ?>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            RFC:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $rfc; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Domicilio:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $domicilio; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Codigo Postal:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $cp; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Colonia:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $colonia; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Estado:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $estado; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Telefono:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $telefono; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Email:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $email; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            L&iacute;mite de Responsabilidad (MXN):
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $limite_resp; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Facturacion total / Ingresos brutos totales anuales:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $factu_total; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Deducible MXN:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $deducible; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            TBD:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $tbd; ?>
        </td>
    </tr>
    <tr>
        <td width="39%" align="right" style="color: #006C81">
            Fecha de creaci&oacute;n:
        </td>
        <td width="61%" align="left" style="color: #555555">
            <?php echo $creacion; ?>
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<div>&nbsp;</div>
<?php if($es_vendedor == 'No'): ?>
<div>&nbsp;</div>
<?php endif; ?>
<table border="0" style="border: none;">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            Facturacion Total / Ingresos Brutos Totales Anuales
        </td>
    </tr>
</table>
<table class="outside">
    <thead>
        <tr>
            <th align="center" class="inside" width="20%" style="color: #006C81; font-size: 10px">
                Limite de Resposabilidad MXN (Por siniestro y en el agregado anual)
            </th>
            <th align="center" class="inside" width="16%" style="color: #006C81; font-size: 10px">
                Hasta MXN $0 a MXN $1'300,000
            </th>
            <th align="center" class="inside" width="16%" style="color: #006C81; font-size: 10px">
                Entre MXN $1'300,001 a $2'600,000
            </th>
            <th align="center" class="inside" width="16%" style="color: #006C81; font-size: 10px">
                Entre MXN $2'600,001 a $3'900,000
            </th>
            <th align="center" class="inside" width="16%" style="color: #006C81; font-size: 10px">
                Entre MXN $3'900,000 a $5'200'000
            </th>
            <th align="center" class="inside" width="16%" style="color: #006C81; font-size: 10px">
                Deducible MXN (Para cualquier reclamo)
            </th>
        </tr>
    </thead>
    <tr>
        <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 9px">
            $ 130,000 MXN
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '1' || $limite_id == 1) && ($factu_id == '1' || $factu_id == 1)): ?>
                <span style="color: #FF0000">$ 1,950 MXN - X </span>
            <?php else: ?>
                $ 1,950 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '1' || $limite_id == 1) && ($factu_id == '2' || $factu_id == 2)): ?>
                <span style="color: #FF0000">$ 2,600 MXN - X </span>
            <?php else: ?>
                $ 2,600 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '1' || $limite_id == 1) && ($factu_id == '3' || $factu_id == 3)): ?>
                <span style="color: #FF0000">$ 3,140 MXN - X </span>
            <?php else: ?>
                $ 3,140 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '1' || $limite_id == 1) && ($factu_id == '4' || $factu_id == 4)): ?>
                <span style="color: #FF0000">$ 3,617 MXN - X </span>
            <?php else: ?>
                $ 3,617 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            $ 6,500 MXN
        </td>
    </tr>
    <tr>
        <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 9px">
            $ 260,000 MXN
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '2' || $limite_id == 2) && ($factu_id == '1' || $factu_id == 1)): ?>
                <span style="color: #FF0000">$ 3,120 MXN - X </span>
            <?php else: ?>
                $ 3,120 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '2' || $limite_id == 2) && ($factu_id == '2' || $factu_id == 2)): ?>
                <span style="color: #FF0000">$ 4,144 MXN - X </span>
            <?php else: ?>
                $ 4,144 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '2' || $limite_id == 2) && ($factu_id == '3' || $factu_id == 3)): ?>
                <span style="color: #FF0000">$ 4,648 MXN - X </span>
            <?php else: ?>
                $ 4,648 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '2' || $limite_id == 2) && ($factu_id == '4' || $factu_id == 4)): ?>
                <span style="color: #FF0000">$ 5,392 MXN - X </span>
            <?php else: ?>
                $ 5,392 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            $ 13,000 MXN
        </td>
    </tr>
    <tr>
        <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 9px">
            $ 390,000 MXN
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '3' || $limite_id == 3) && ($factu_id == '1' || $factu_id == 1)): ?>
                <span style="color: #FF0000">$ 4,095 MXN - X </span>
            <?php else: ?>
                $ 4,095 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '3' || $limite_id == 3) && ($factu_id == '2' || $factu_id == 2)): ?>
                <span style="color: #FF0000">$ 5,363 MXN - X </span>
            <?php else: ?>
                $ 5,363 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '3' || $limite_id == 3) && ($factu_id == '3' || $factu_id == 3)): ?>
                <span style="color: #FF0000">$ 6,006 MXN - X </span>
            <?php else: ?>
                $ 6,006 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '3' || $limite_id == 3) && ($factu_id == '4' || $factu_id == 4)): ?>
                <span style="color: #FF0000">$ 6,689 MXN - X </span>
            <?php else: ?>
                $ 6,689 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            $ 19,500 MXN
        </td>
    </tr>
    <tr>
        <td align="center" class="inside_two" width="20%" style="color: #555555; font-size: 9px">
            $ 530,000 MXN
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '4' || $limite_id == 4) && ($factu_id == '1' || $factu_id == 1)): ?>
                <span style="color: #FF0000">$ 4,875 MXN - X </span>
            <?php else: ?>
                $ 4,875 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '4' || $limite_id == 4) && ($factu_id == '2' || $factu_id == 2)): ?>
                <span style="color: #FF0000">$ 6.419 MXN - X </span>
            <?php else: ?>
                $ 6.419 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '4' || $limite_id == 4) && ($factu_id == '3' || $factu_id == 3)): ?>
                <span style="color: #FF0000">$ 7,150 MXN - X </span>
            <?php else: ?>
                $ 7,150 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            <?php if(($limite_id == '4' || $limite_id == 4) && ($factu_id == '4' || $factu_id == 4)): ?>
                <span style="color: #FF0000">$ 7,508 MXN - X </span>
            <?php else: ?>
                $ 7,508 MXN
            <?php endif; ?>
        </td>
        <td align="center" class="inside_two" width="16%" style="color: #555555; font-size: 9px">
            $ 26,000 MXN
        </td>
    </tr>
</table>
<div>&nbsp;</div>
<table class="outside">
    <tr align="center" class="font-weight-header">
        <td width="100%" style="font-size: 14px; color: #006C81">
            COBERTURA DE LA POLIZA
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81; font-size: 10px">
            Cobertura y beneficios adicionales
        </td>
        <td width="15%" align="left" style="color: #555555; font-size: 10px">
            Amparado o NO Amparado
        </td>
        <td width="20%" align="left" style="color: #555555; font-size: 10px">
            Limite de Responsabilidad / sublimite
        </td>
        <td width="40%" align="left" style="color: #555555; font-size: 10px">
            Deducible
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Responsabilidad por datos personales
        </td>
        <td width="15%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Limite de la poliza completo
        </td>
        <td width="40%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Deducible general de la poliza de acuerdo con el lfmite de responsabilidad seleccionado.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81; font-size: 10px">
            Responsabilidad por empresas subcontratista
        </td>
        <td width="15%" align="left" style="color: #555555; font-size: 10px">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #555555; font-size: 10px">
            Limite de la poliza completo
        </td>
        <td width="40%" align="left" style="color: #555555; font-size: 10px">
            Deducible general de la poliza de acuerdo con el lfmite de responsabilidad seleccionado.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Responsabilidad por seguridad de datos
        </td>
        <td width="15%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Limite de la poliza completo
        </td>
        <td width="40%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Deducible general de la poliza de acuerdo con el lfmite de responsabilidad seleccionado.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81; font-size: 10px">
            Costos de defensa
        </td>
        <td width="15%" align="left" style="color: #555555; font-size: 10px">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #555555; font-size: 10px">
            Limite de la poliza completo
        </td>
        <td width="40%" align="left" style="color: #555555; font-size: 10px">
            Deducible general de la poliza de acuerdo con el lfmite de responsabilidad seleccionado.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Investigacion administrativa
        </td>
        <td width="15%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Sublimite MXN $65,000
        </td>
        <td width="40%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Sin deducible.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81; font-size: 10px">
            Sancion administrativa
        </td>
        <td width="15%" align="left" style="color: #555555; font-size: 10px">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #555555; font-size: 10px">
            Sublimite MXN $65,000
        </td>
        <td width="40%" align="left" style="color: #555555; font-size: 10px">
            10% por cualquier Sancion Administrativa sujeta a un mfnimo del Deducible General para cualquier Reclamo que
            corresponda segun la opcion seleccionado en la Tabla 1. Cualquier Deducible pagado con respedo a alguna Sancion
            Administrativa sera en adicion, y no erosionard, el Deducible General de la Poliza
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Restitucion de la imagen de la sociedad
        </td>
        <td width="15%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Sublimite MXN $65,000
        </td>
        <td width="40%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Sin deducible.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81; font-size: 10px">
            Restitution de la imagen personal
        </td>
        <td width="15%" align="left" style="color: #555555; font-size: 10px">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #555555; font-size: 10px">
            Sublimite MXN $65,000
        </td>
        <td width="40%" align="left" style="color: #555555; font-size: 10px">
            Sin deducible.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Gastos de notification y monitoreo
        </td>
        <td width="15%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Sublimite MXN $65,000
        </td>
        <td width="40%" align="left" style="color: #FFFFFF; font-size: 10px; background-color: #006C81">
            Sin deducible.
        </td>
    </tr>
    <tr>
        <td width="25%" align="right" style="color: #006C81; font-size: 10px">
            Datos electronicos
        </td>
        <td width="15%" align="left" style="color: #555555; font-size: 10px">
            Amparado
        </td>
        <td width="20%" align="left" style="color: #555555; font-size: 10px">
            Sublimite MXN $65,000
        </td>
        <td width="40%" align="left" style="color: #555555; font-size: 10px">
            Sin deducible.
        </td>
    </tr>
</table>
