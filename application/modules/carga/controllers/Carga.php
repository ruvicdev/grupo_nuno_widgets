<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * Class that contains all the functionality for
 * the merchandise widget, where the user could
 * do a cotization about all the kind of products
 * wants to send to another countries or states
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
Class Carga extends MY_Controller {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // load libraries
        $this->load->library('session');

        // load the models
        $this->load->model(array('Pais_model', 'Ciudad_model', 'Cotizaciones_model'));

        // create the object to export the PDF
        $this->pdf   = new ExportFiles();
        // creates the email and send the data
        $this->emailSend = new EmailSend();
        // creates the template object
        $this->template = new Templates();
    }

    /**
     * Method will load all the information for the cotization
     * will do the external user. This user must follow the steps
     * filled in the fields
     *
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function index() {
        // Regenerate the session id
        session_regenerate_id();

        $dataView = $this->__generateDropdowns();
        $dataDate = date('d/m/Y');
        $dataView['dateFormat'] = $dataDate;

        $content = $this->loadSingleView('index', $dataView, TRUE);
        $data    = $this->__setArray('content', $content);
        $data    = $this->__setArray('title', 'Cotiza Carga');

        $this->loadTemplate($data);
    }

    /**
     * Method used for save all the infomation and
     * send the data to the company and client
     * about the data wants to be provided for
     * the merchandises will be sent
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData() {
        $json_array = json_decode(file_get_contents('php://input'), true);

        // Generate the Template and init the PDF file creation
        $template = $this->template->merchandiseTemplate($json_array);
        $PDF      = $this->pdf->__initPDF($template, "Cotiza Carga", "Cotiza Carga");

        // Set PDF name and create PDF file
        $name = "cotizaCarga_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

        // Create the file
        //ob_clean();
        $PDF->Output(FCPATH . 'public/files/' . $name, 'F');
        $completePath = FCPATH . 'public/files/' . $name;
        $returnedPath = base_url() . 'public/files/' . $name;

        // Update the user's information once finish the process
        $this->Cotizaciones_model->updateBaseData($json_array, $json_array['orden_id']);

        // Send the information via mail
        $this->emailSend->__initEmail($json_array, $PDF, $completePath, "Cotiza Carga");

        // create json response
        $response = array('flag'     => '1',
                          'pathFile' => $returnedPath);

        // Regenerate the session id
        session_regenerate_id();

        echo json_encode($response);
    }

    /**
     * Private method for create all the dropdowns
     * will be displayed as options in the form for
     * merchandises and could be selected by the user
     * will use this platform
     *
     * @return array
     *
     * @author Ruben Alonso Cortes Mendoza
     * @version 1.0
     * @company ruvicdev
     */
    private function __generateDropdowns() {
        $dropdowns = array();

        $array_giro = array('1' => 'Agente Aduanal',
                            '2' => 'Freight Forwarder',
                            '3' => 'Operador Logistico',
                            '4' => 'Transportista',
                            '5' => 'Comercializadora',
                            '6' => 'Empresa o Industria');

        $array_trans = array('1'  => 'Avión',
                             '2'  => 'Avión y conexiones',
                             '3'  => 'Buque y conexiones',
                             '4'  => 'Camión abierto de servicio público federal',
                             '5'  => 'Camión abierto propio',
                             '6'  => 'Camión cerrado de servicio público federal',
                             '7'  => 'Camión cerrado propio',
                             '8'  => 'Camión plataforma o cama baja propio',
                             '9'  => 'Camíon plataforma o cama baja servicio público federal',
                             '10' => 'Ferrocarril furgón abierto',
                             '11' => 'Ferrocarriles y conexiones',
                             '12' => 'Solo buque');

        $merchandise = $this->kindMerchandise();

        // loading dropdown continents
        $continentes = $this->Pais_model->getAllContinents();

        return $dropdowns = array('giro'  => $array_giro,
                                  'trans' => $array_trans,
                                  'merch' => $merchandise,
                                  'cont'  => $continentes);
    }

    /**
     * Private method used for create the array
     * will be used for create the data will be
     * displayed in the merchandise dropdown
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function kindMerchandise() {
        $arrayMerchandise = array(
            array('id' => 1,  'class' => 'C', 'desc' => 'ABARROTES (EXCEPTO VINOS/LICORES/AZUCAR/CAFE)'),
            array('id' => 2,  'class' => 'A', 'desc' => 'ABONOS Y FERTILIZANTES, HERBICIDAS, FITOESTABILIZADORES'),
            array('id' => 3,  'class' => 'C', 'desc' => 'ACEITES Y GRASAS VEGETALES, MINERALES O LEGUMINOSAS'),
            array('id' => 4,  'class' => 'E', 'desc' => 'ACEITES, GRASAS Y LUBRICANTES'),
            array('id' => 5,  'class' => 'A', 'desc' => 'AGUA NATURAL ENVASADA'),
            array('id' => 6,  'class' => 'E', 'desc' => 'AGUAS MINERALES O GASEOSAS'),
          //  array('id' => 7,  'class' => 'E', 'desc' => 'AGUARDIENTE, VINOS, SIDRAS, TEQUILA O LICORES EMBOTELLADOS'),
            array('id' => 8,  'class' => 'E', 'desc' => 'ALGODON Y BORRA EN PACAS'),
            array('id' => 9,  'class' => 'E', 'desc' => 'ALFOMBRAS, TAPETES Y TAPICES'),
            array('id' => 10, 'class' => 'A', 'desc' => 'ALIMENTOS PARA ANIMALES, INCLUYENDO FORRAJES, PASTOS SECOS Y PAJA'),
            array('id' => 11, 'class' => 'C', 'desc' => 'APARATOS DE USO DOMESTICO (LAVADORAS,LAVAVAJILLAS ESTUFAS, REFRIGERADORES,T.V.,RA'),
            array('id' => 12, 'class' => 'E', 'desc' => 'APARATOS ELECTRONICOS (EQUIPO DE COMPUTO, FAXES, IMPRESORAS, APARATOS TELEFONICOS'),
            array('id' => 13, 'class' => 'A', 'desc' => 'ARTESANIAS Y CURIOSIDADES (EXC. OBRAS DE ARTE, PLATERIA Y JOYERIA)'),
            array('id' => 14, 'class' => 'A', 'desc' => 'ATAUDES Y FUNERARIAS'),
            array('id' => 15, 'class' => 'E', 'desc' => 'AZUCAR ENCOSTALADA'),
            array('id' => 16, 'class' => 'E', 'desc' => 'BALATAS Y EMBRAGUES'),
            array('id' => 17, 'class' => 'E', 'desc' => 'BARRO Y ARTICULOS ALFARERIA'),
            array('id' => 18, 'class' => 'E', 'desc' => 'BATERIAS ELECTRICAS AUTOMOTRICES'),
            array('id' => 19, 'class' => 'A', 'desc' => 'BAULES, PETECAS, METALES O PORTAFOLIOS'),
            array('id' => 20, 'class' => 'E', 'desc' => 'BICICLETAS'),
            array('id' => 21, 'class' => 'A', 'desc' => 'BONETERIA, MERCERIA O CEDERIA'),
            array('id' => 22, 'class' => 'A', 'desc' => 'BROCHAS O PINCELES'),
            array('id' => 23, 'class' => 'E', 'desc' => 'CACAO'),
            array('id' => 24, 'class' => 'E', 'desc' => 'CAFÉ EN GRANO Y CUALQUIER PRESENTACION'),
            array('id' => 25, 'class' => 'A', 'desc' => 'CAL, YESO Y CEMENTO'),
            array('id' => 26, 'class' => 'E', 'desc' => 'CALZADO O ZAPATOS'),
            array('id' => 27, 'class' => 'E', 'desc' => 'CAMARAS Y ARTICULOS FOTOGRAFICOS (EXC. PELICULAS Y VIDEOS)'),
            array('id' => 28, 'class' => 'D', 'desc' => 'CAMARON SECO, CHARALES SECOS'),
            array('id' => 29, 'class' => 'A', 'desc' => 'CANTERA'),
            array('id' => 30, 'class' => 'C', 'desc' => 'CARNE, EMBUTIDOS, SALCHICHONERIA Y DERIVADOS'),
            array('id' => 31, 'class' => 'A', 'desc' => 'CARTON O PAPEL (CAJAS DESARMADAS EN BOBINAS)'),
            array('id' => 32, 'class' => 'E', 'desc' => 'CASIMIRES'),
            array('id' => 33, 'class' => 'E', 'desc' => 'CERAMICA, LOZA O PORCELANA'),
            array('id' => 34, 'class' => 'A', 'desc' => 'CERAS, PARAFINAS O PASTAS PARA LIMPIAR'),
            array('id' => 35, 'class' => 'D', 'desc' => 'CEREALES, GRANOS Y SEMILLAS'),
            array('id' => 36, 'class' => 'E', 'desc' => 'CERILLO (CON PERMISO DE LAS AUTORIDADES)'),
            // array('id' => 37, 'class' => 'E', 'desc' => 'CERVEZA EMBOTELLADA, ENLATADA O DE BARRIL'),
            array('id' => 38, 'class' => 'E', 'desc' => 'DULCES, CHICLES, CHOCOLATES, COCOA'),
            array('id' => 39, 'class' => 'D', 'desc' => 'CHILES O FRUTAS SECAS, SECOS DE YUTE O POLIETILENO, HORTALIZAS SECAS Y/O DESHIDRATADAS Y/O NUECES, ALMENDRAS, AVENA'),
            // array('id' => 40, 'class' => 'E', 'desc' => 'CIGARROS, PUROS'),
            array('id' => 41, 'class' => 'A', 'desc' => 'COLCHONES O COLCHONETAS O ALMOHADAS'),
            array('id' => 42, 'class' => 'C', 'desc' => 'COLORES, PINTURAS O TINTAS'),
            array('id' => 43, 'class' => 'A', 'desc' => 'COLORANTES, JARABES O CONCENTRADOS NATURALES'),
            array('id' => 44, 'class' => 'A', 'desc' => 'CONSERVAS ALIMENTICIAS '),
            array('id' => 45, 'class' => 'E', 'desc' => 'MATERIALES DE CONSTRUCCION'),
            array('id' => 46, 'class' => 'E', 'desc' => 'COSMETICOS, PERFUMES Y ARTICULOS DE TOCADOR'),
            array('id' => 47, 'class' => 'A', 'desc' => 'COSTALES Y ARPILLAS, MATERIAL DE EMPAQUE VACIOS'),
            array('id' => 48, 'class' => 'B', 'desc' => 'ARTICULOS DE CRISTAL Y VIDRIO '),
            array('id' => 49, 'class' => 'E', 'desc' => 'ARTICULOS DEPORTIVOS (SE EXCLUYE ARMERIA)'),
            array('id' => 50, 'class' => 'C', 'desc' => 'DERIVADOS FRESCOS DE LA LECHE (MANTEQUILLAS, QUESOS YOGURT, ETC.)'),
            array('id' => 51, 'class' => 'A', 'desc' => 'DETERGENTES Y JABONES'),
            array('id' => 52, 'class' => 'E', 'desc' => 'DISCOS, CASSETTES O CD. MUSICALES'),
            array('id' => 53, 'class' => 'A', 'desc' => 'ESTAMBRES'),
            array('id' => 54, 'class' => 'E', 'desc' => 'FARMACEUTICOS Y ART. DE BELLEZA (CREMAS, LOCIONES, DESODORANTES O SIMILARES)'),
            array('id' => 55, 'class' => 'E', 'desc' => 'FERRETERIA'),
            array('id' => 56, 'class' => 'B', 'desc' => 'FLORES Y PLANTAS NATURALESFLORES Y PLANTAS NATURALES'),
            array('id' => 57, 'class' => 'B', 'desc' => 'FRUTAS, LEGUMBRES O VERDURAS FRESCAS, HORTALIZAS'),
            array('id' => 58, 'class' => 'A', 'desc' => 'GALLETAS, PASTAS ALIMENTICIAS (CAJAS DE CARTON)'),
            array('id' => 59, 'class' => 'A', 'desc' => 'HARINAS DE TRIGO Y MAIZ, AZUCAR GLASS'),
            array('id' => 60, 'class' => 'C', 'desc' => 'HELADOS, PALETAS Y SIMILARES'),
            array('id' => 61, 'class' => 'C', 'desc' => 'HERRAMIENTAS LIGERAS DE MANO'),
            array('id' => 62, 'class' => 'A', 'desc' => 'HERRERIA (SIN EMPAQUE)'),
            array('id' => 63, 'class' => 'E', 'desc' => 'HILOS, HILAZAS Y FIBRAS PARA HILADOS'),
            array('id' => 64, 'class' => 'A', 'desc' => 'HOJAS DE LAMINA ACANALADA,CORRUGADA Y ESTRUCTURA'),
            array('id' => 65, 'class' => 'C', 'desc' => 'HUEVO'),
            array('id' => 66, 'class' => 'A', 'desc' => 'HULE Y/O LATEX NATURAL'),
            array('id' => 67, 'class' => 'A', 'desc' => 'INSECTICIDAS O PLAGUICIDAS '),
            array('id' => 68, 'class' => 'A', 'desc' => 'JARCIERIA (FLEJADOS)'),
            array('id' => 69, 'class' => 'E', 'desc' => 'JOYERIA DE FANTASIA'),
            array('id' => 70, 'class' => 'E', 'desc' => 'JUGUETES'),
            array('id' => 71, 'class' => 'C', 'desc' => 'LECHE FRESCAS EN BOTES METALIDOS, PIPAS O ENVASES'),
            array('id' => 72, 'class' => 'A', 'desc' => 'CARTON, PLASTICO O VIDRIO'),
            array('id' => 73, 'class' => 'A', 'desc' => 'LECHE PROCESADA Y SUS DERIVADOS, EN CARTON, LATAS, ETC., (EN POLVO O ULTRAPATEUR'),
            array('id' => 74, 'class' => 'A', 'desc' => 'LIBROS, REVISTAS, PERIODICOS'),
            array('id' => 75, 'class' => 'A', 'desc' => 'LINOLEUMS, TELAS AHULADAS Y ESTERAS (ROLLOS FLEJADOS)'),
            array('id' => 76, 'class' => 'E', 'desc' => 'LLANTAS'),
            array('id' => 77, 'class' => 'A', 'desc' => 'MADERAS'),
            array('id' => 78, 'class' => 'A', 'desc' => 'MAQUINARIA, EQUIOPO Y REFACCIONES'),
            array('id' => 79, 'class' => 'B', 'desc' => 'MAQUINAS DE OFICINA'),
            array('id' => 80, 'class' => 'A', 'desc' => 'MARCOS, MOLDURAS O CUADROS SIN PINTAR'),
            array('id' => 81, 'class' => 'E', 'desc' => 'MARMOLES Y PIZARRAS (SIN EMPAQUE)'),
            array('id' => 82, 'class' => 'E', 'desc' => 'MATERIAL ELECTRICO EN GENERAL (EXC. ELECTRONICOS)'),
            array('id' => 83, 'class' => 'A', 'desc' => 'MENAJE DE CASA, CON RELACION Y VALORES UNITARIOS '),
            array('id' => 84, 'class' => 'E', 'desc' => 'MERCANCIAS EN GENERAL, NO CONTEMPLADAS EN LOS GRUPOS A, B, C, D Y E, EXCEPTO LOS'),
            array('id' => 85, 'class' => 'E', 'desc' => 'MERCANCIAS MIXTAS (MEZCLA DE GRUPOS A, B, C, D Y E,) EXCEPTUANDO BIENES EXCLUIDO'),
            array('id' => 86, 'class' => 'E', 'desc' => 'PRODUCTOS DE MATALES EN GENERAL'),
            array('id' => 87, 'class' => 'C', 'desc' => 'MIEL DE ABEJA'),
            array('id' => 88, 'class' => 'E', 'desc' => 'MOTOCICLETAS PROTEJIDAS CON CARTON Y FLEJADAS'),
            array('id' => 89, 'class' => 'E', 'desc' => 'MOTORES, BOMBAS, BOBINAS O TRANSFORMADORES ELECTRICOS'),
            array('id' => 90, 'class' => 'A', 'desc' => 'MUEBLES DE MADERA'),
            array('id' => 91, 'class' => 'A', 'desc' => 'MUEBLES DE METAL'),
            array('id' => 92, 'class' => 'A', 'desc' => 'MUEBLES SANITARIOS'),
            array('id' => 93, 'class' => 'A', 'desc' => 'PABILO, CERAS Y VELADORAS'),
            array('id' => 94, 'class' => 'E', 'desc' => 'PAPELERIA Y/O ARTICULOS DE ESCRITORIO'),
            array('id' => 95, 'class' => 'E', 'desc' => 'PESCADOS Y MARISCOS'),
            array('id' => 96, 'class' => 'A', 'desc' => 'PIELES CURTIDAS Y SIN CURTIR, PELETERIA EN GENERAL'),
            array('id' => 97, 'class' => 'C', 'desc' => 'PINTURAS'),
            array('id' => 98, 'class' => 'C', 'desc' => 'PLASTICOS, ARTICULOS DE PLASTICO, EMBASES, BOLSAS, ROLLOS'),
            array('id' => 99, 'class' => 'A', 'desc' => 'SUMPLEMENTO ALIMENTICIOS'),
            array('id' => 100, 'class' => 'E', 'desc' => 'PORCELANA'),
            array('id' => 101, 'class' => 'A', 'desc' => 'PRODUCTOS QUIMICOS EN GENERAL EXCEPTO LOS EXPLICITAMENTE EXCLUIDOS'),
            array('id' => 102, 'class' => 'E', 'desc' => 'REFACCIONES PARA AUTOS Y MOTOCICLETAS '),
            array('id' => 103, 'class' => 'E', 'desc' => 'REFRESCOS'),
            array('id' => 104, 'class' => 'A', 'desc' => 'ARTICULOS RELIGIOSOS'),
            array('id' => 105, 'class' => 'E', 'desc' => 'ROPA Y LENCERIA'),
            array('id' => 106, 'class' => 'A', 'desc' => 'SAL'),
            array('id' => 107, 'class' => 'E', 'desc' => 'SOMBREROS'),
            array('id' => 108, 'class' => 'E', 'desc' => 'TABACO EN RAMA O PACAS'),
            array('id' => 109, 'class' => 'E', 'desc' => 'TEJIDOS EN PUNTO'),
            array('id' => 110, 'class' => 'E', 'desc' => 'TELAS'),
            // array('id' => 111, 'class' => 'E', 'desc' => 'TEQUILA A GRANEL'),
            array('id' => 112, 'class' => 'A', 'desc' => 'TORTILLAS'),
            array('id' => 113, 'class' => 'E', 'desc' => 'ANIMALES EN PIE (CUBRIENDO UNICAMENTE ROT Y ROBO TOTAL)')
        );

        return $arrayMerchandise;
        /*foreach ($array as $key => $value) {
            print_r($key);
            print_r($value['tipo']);
        }*/
    }

    /**
     * Method will be used to call the main
     * method to reload the informacion is
     * set up for cleaning all the fields
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function redirect() {
        $this->__redirect();
    }

    /**
     * Method will be used to get the continent selected
     * by the user and then display all the countries
     * depeding the continent selected
     *
     * @method post
     * @param string $continente
     * @return array $data
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function countryByContinent() {
        $continent = json_decode(file_get_contents('php://input'), true);

        $data = $this->Pais_model->getCountryByContinent($continent['continente'], 0);

        echo json_encode(array('flag'       => 1,
                               'continente' => $continent,
                               'paises'     => $data->result_array()));
    }

    /**
     * Method will be used to get all the information
     * related to the states regarding to the country
     * has been selected by the user
     *
     * @method post
     * @param string $country
     * @return array $data
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function stateByCountry() {
        $country = json_decode(file_get_contents('php://input'), true);

        $data = $this->Ciudad_model->getStateByCountry($country['code']);

        echo json_encode(array('flag'   => 1,
                               'pais'   => $country,
                               'states' => $data->result_array()));
    }

    /**
     * Method will use once the user select the state and just
     * is going to be returned by the cities, all this information
     * should be displayed in the dropdown via ajax
     *
     * @method post
     * @param string $state
     * @return array $data
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function citiesByState() {
        $estado = json_decode(file_get_contents('php://input'), true);

        $data = $this->Ciudad_model->getCitiesByState($estado['state']);

        echo json_encode(array('flag'   => 1,
                               'state'  => $estado,
                               'cities' => $data->result_array()));
    }

    /**
     * Method will be used to load all the fields
     * of the dropdowns with the continent, city
     * and all the states of Mexico
     *
     * @return json $array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getNationalData() {
        $data  = $this->Ciudad_model->getStateByCountry('MEX');
        $data2 = $this->Pais_model->getCountryByContinent('North America', 1);
        $array_response = array('pais'   => $data2->result_array(),
                                'states' => $data->result_array(),
                                'flag'   => 1);

        echo json_encode($array_response);
    }

    /**
     * Method will be used for create the session variable
     * that is going to be shown once the user click to the
     * second section for create the number of order
     *
     * @param null
     * @return array $array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function setSessionAndOrder() {
        // Regenerate the session id
        session_regenerate_id();

        if ($this->session->userdata('__ci_last_regenerate') != '' || !empty($this->session->userdata('__ci_last_regenerate'))) {
            $session_id = $this->session->userdata('__ci_last_regenerate');
        } else {
    		session_regenerate_id();
    		$session_id = session_id();
    	}

        $array_data = array('session'        => $session_id,
                            'status'         => 'New',
                            'fecha_creacion' => date('d-m-Y H:i:s'));

        $id = $this->Cotizaciones_model->saveBaseData($array_data);

        // set session variable
        $this->session->set_userdata(array('session_id' => $session_id,
                                       'id'         => $id));

        echo json_encode(array('flag'     => 1,
                               'session'  => $session_id,
                               'order_id' => $id));
    }

    /**
     * Method will be used to create the PDF file
     * is going to be displayed in the browser
     * and giving the posibility to the user
     * to print the file
     *
     * @method GET
     * @param int $id
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function PDFPrint($id) {
        $data = $this->Cotizaciones_model->getDataSpecificRecord($id);

        $createArray = $this->createArrayPDFData($data->row_array());

        $temp = $this->template->merchandiseTemplate($createArray);
        $p = $this->pdf->__initPDF($temp, "Cotiza Carga", "Cotiza Carga");
        $p->Output("demo.pdf", 'I');
    }

    /**
     * Method will be used to create the
     * array is going to be returned by this method
     * and then will be sent to the template for
     * displaying the data
     *
     * @param array $data
     * @return array $array
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function createArrayPDFData($data) {
        $current_date = explode(" ", $data['fecha_creacion']);
        $seguridad    = explode(".", $data['seguridad']);

        $array = array(
            'orden_id'           => $data['id'],
            'current_date'       => $current_date[0],
            'nombres'            => $data['nombre_user'],
            'tipo_moneda'        => $data['tipo_moneda'],
            'nombre_giro'        => $data['tipo_giro'],
            'validAt'            => $data['valida_hasta'],
            'valor_merc'         => $data['valor_merc'],
            'tipo'               => $data['tipo_carga'],
            'nombre_merc'        => $data['nombre_merc'],
            'descripcion_merc'   => $data['desc_merc'],
            'empaque'            => $data['empaque'],
            'nombre_medio_trans' => $data['medio_trans'],
            'cobertura'          => $data['cobertura'],
            'prima_neta'         => $data['prima_neta'],
            'continente_origen'  => $data['continente_origen'],
            'pais_origen'        => $data['pais_origen'],
            'estado_origen'      => $data['estado_origen'],
            'ciudad_origen'      => $data['ciudad_origen'],
            'continente_destino' => $data['continente_destino'],
            'pais_destino'       => $data['pais_destino'],
            'estado_destino'     => $data['estado_destino'],
            'ciudad_destino'     => $data['ciudad_destino'],
            'security'           => $seguridad
        );

        return $array;
    }

    public function demo($id) {
        //echo "holas: " . $id;
        //$data = $this->Cotizaciones_model->getDataSpecificRecord($id);
        //$demo = $data->row_array();
        //var_dump($data->result_array());
        //$datas = array(
        //    'id' => $demo['id']
        //);
        //var_dump($datas);
        //die();
        //$data = $this->Pais_model->get_all_continents();
        //var_dump($data->result_array());
        //$dir __DIR__ . '../public';
        //$dir = dirname(dirname(__FILE__));
        //echo FCPATH . '\n';
        //echo $dir;
        //echo $dir . "\n";
        //$this->load->library('email');
        //$json_array = json_decode(file_get_contents('php://input'), true);
        $temp = $this->template->merchandiseTemplate(); //$data->result_array());
        //var_dump($temp);
        //ob_clean();
        $p = $this->pdf->__initPDF($temp, "Cotiza Carga", "Cotiza Carga"); //'<p>hola</p>');
        //var_dump($p);
        $p->Output("demo.pdf", 'I');
        //var_dump($p);
        //ob_clean();

        //$pdf->Output(FCPATH . 'public/files/example_006.pdf', 'F');

        //var_dump($pdfF);
        //$this->emailSend->__initEmail(array(), $pdf);
        //$this->email->demos(array(), $pdf);
        //$this->email->demos($pdf);
        //$this->email->from('r@ruvicdev.com', 'nombre');
        //$this->email->to('ruben.alonso21@gmail.com');
        //$this->email->bcc('email');

        //$this->email->subject('subject demo');
        //$this->email->message('template');

        //$data    = chunk_split($pdfFile);
        //$this->email->attach(FCPATH . 'public/files/example_006.pdf');

        //$this->email->send();
    }
}
