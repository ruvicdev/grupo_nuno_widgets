<!-- TEMPLATES **START** -->
<script type="text/x-handlebars-template" id="paises_origen_continente">
    {{#each data}}
        <div class="item" data-value="{{Paisnombre}}" data-code="{{PaisCodigo}}" data-area="{{PaisArea}}">
            {{PaisNombre}}
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="paises_destino_continente">
    {{#each data}}
        <div class="item" data-value="{{Paisnombre}}" data-code="{{PaisCodigo}}" data-area="{{PaisArea}}">
            {{PaisNombre}}
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="estado_origen_pais">
    {{#each data}}
        <div class="item" data-value="{{CiudadDistrito}}">
            {{CiudadDistrito}}
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="estado_destino_pais">
    {{#each data}}
        <div class="item" data-value="{{CiudadDistrito}}">
            {{CiudadDistrito}}
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="ciudad_origen_estado">
    {{#each data}}
        <div class="item">
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="ciudad_destino_estado">
    {{#each data}}
        <div class="item">
        </div>
    {{/each}}
</script>

<script type="text/x-handlebars-template" id="errors_messages">
    <div class="row">
        <div class="ui negative hidden message">
            <i class="close icon"></i>
            <div class="header">
                Error
            </div>
            <p>
                Por favor, llene los campos requeridos
            </p>
            <p id="messageErrorMoney">
                {{data}}
            </p>
        </div>
    </div>
</script>
<!-- TEMPLATES **END** -->

<div class="default-margin-top ui grid centered removePadding">
    <!-- SECTION FOR STEPS -->
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos Personales</div>
                <div class="description">Informacion Personal</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="truck icon"></i>
            <div class="content">
                <div class="title">Embarque</div>
                <div class="description">Datos del Embarque</div>
            </div>
        </div>
        <div class="disabled step" id="step-3">
            <i class="dollar icon"></i>
            <div class="content">
                <div class="title">Cotizacion</div>
                <div class="description">Cotizacion Final</div>
            </div>
        </div>
        <div class="disabled step" id="step-4">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Envio</div>
                <div class="description">Envio Cotizacion</div>
            </div>
        </div>
    </div>
</div>

<!-- will be added the modal view once the user clicks the button -->

<div class="ui grid centered">
    <!-- SECTION FOR FORMS -->
    <div class="row">
        <h1 class="ui header">
            Cotiza tu Carga
        </h1>
    </div>
    <div>&nbsp;</div>
    <form class="ui form">
        <div class="ui styled accordion">
            <!-- FIRST SECTION -->
            <div id="accordion-1" class="disabled-content1"><!-- disabled or enabled accordion -->
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos Personales
                </div>
                <div class="content active" id="content-1">
                    <div class="row" id="error_first_section"></div>
                    <div>&nbsp;</div>
                    <div class="two fields"><!-- header with data will be displayed -->
                        <div class="inline field">
                            <label class="left_align bold_text_title">
                                # Cotizacion
                            </label>
                            <span id="no_order">TBD</span>
                        </div>
                        <div class="inline field">
                            <label class="left_align bold_text_title">
                                Fecha de Cotizacion
                            </label>
                            <span>
                                <?php echo $dateFormat; ?>
                                <input type="hidden" id="currentDate"
                                       value="<?php echo $dateFormat; ?>">
                            </span>
                        </div>
                    </div><!-- header will be displayed -->
                    <div class="fields">
                        <div class="seven wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" id="vendedor"
                                       class="form-data-selected obj-send">
                                <label class="bold_text_title">
                                    ¿Eres vendedor de Grupo Nu&ntilde;o?
                                </label>
                            </div>
                        </div>
                        <div class="nine wide field">
                            <input type="text" name="nombre_vendedor" id="name_seller" 
                                   class="form-data-selected ignore" placeholder="Nombre del Vendedor">
                        </div>
                    </div>
    				<div class="fields"><!-- div checkbox -->
    					<div class="three wide field">
    						<label for="tipo_persona" class="left_align bold_text_title">
    							Tipo de Persona
    						</label>
    					</div>
    					<div class="five wide field required sec-1">
    						<div class="ui checkbox">
                                <input type="checkbox" data-value="fisica"
                                       data-class="hidden-segment-value"
                                       data-index="1"
                                       class="checkdata form-data-selected obj-send tperson">
    							<label>
    								Persona Fisica
    							</label>
    						</div>
    					</div>
    					<div class="five wide field required sec-1">
    						<div class="ui checkbox">
                                <input type="checkbox" data-value="moral"
                                       data-class="hidden-segment-value"
                                       data-index="2"
                                       class="checkdata form-data-selected obj-send tperson">
    							<label>
    								Persona Moral
    							</label>
    						</div>
    					</div>
    				</div><!-- div checkbox -->
                    <div class="ui segment hidden-segment-value first-step" data-value="moral"
                         id="moral-field"><!-- in case the radio button selected is moral -->
                        <div class="required field">
                            <label class="left_align bold_text_title">
                                Persona Moral
                            </label>
                            <input type="text" name="persona_moral" placeholder="Razon Social"
                                   class="form-data-selected ignore obj-send" data-secondary="moral">
                        </div>
                    </div><!-- in case the radio button selected is moral -->
                    <div class="ui segment hidden-segment-value first-step" data-value="fisica"
                         id="fisica-field"><!-- in case the radio is fisica -->
                        <!-- div class="required field">
                            <label class="left_align bold_text_title">
                                Persona Fisica
                            </label>
                            <input type="text" name="persona_fisica" placeholder="Persona Fisica"
                                   class="form-data-selected ignore obj-send" data-secondary="fisica">
                        </div -->
                        <div class="three fields">
                            <div class="required field">
                                <label class="left_align bold_text_title">
                                    Nombre
                                </label>
                                <input type="text" name="nombre" placeholder="Nombre"
                                       class="form-data-selected ignore obj-send" data-secondary="fisica">
                            </div>
                            <div class="required field">
                                <label class="left_align bold_text_title">
                                    Apellido Paterno
                                </label>
                                <input type="text" name="apellido_paterno" placeholder="Apellido Paterno"
                                       class="form-data-selected ignore obj-send" data-secondary="fisica">
                            </div>
                            <div class="required field">
                                <label class="left_align bold_text_title">
                                    Apellido Materno
                                </label>
                                <input type="text" name="apellido_materno" placeholder="Apellido Materno"
                                       class="form-data-selected ignore obj-send" data-secondary="fisica">
                            </div>
                        </div>
                    </div><!-- in case the radio is fisica -->
                    <div class="field">
                        <label class="left_align bold_text_title">
                            R.F.C.
                        </label>
                        <input type="text" name="rfc" placeholder="RFC"
                               class="form-data-selected ignore obj-send">
                    </div>
                    <div class="required field">
                        <label class="left_align bold_text_title">
                            Giro
                        </label>
                        <div class="ui selection dropdown">
                            <input name="giro" type="hidden" class="form-data-selected obj-send" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text giroText">Giro</div>
                            <div class="menu">
                                <?php foreach ($giro as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label class="left_align bold_text_title">
                            Volumen de Viajes
                        </label>
                        <div class="ui selection dropdown">
                            <input type="hidden" name="vol_viajes" class="form-data-selected obj-send" value="0">
                            <i class="dropdown icon"></i>
                            <div class="default text">Volumen de Viajes</div>
                            <div class="menu">
                                <div class="item" data-value="1_a_10">
                                    1 a 10
                                </div>
                                <div class="item" data-value="11_a_15">
                                    11 a 15
                                </div>
                                <div class="item" data-value="16_o_mas">
                                    16 o mas
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide required field">
                            <label class="left_align bold_text_title">
                                Correo Electronico
                            </label>
                            <input type="email" name="email" placeholder="Correo Electronico"
                                   class="form-data-selected">
                        </div>
                        <div class="eight wide required field">
                            <label>
                                Telefono
                            </label>
                            <input type="text" name="telefono" placeholder="Telefono"
                                   class="form-data-selected tel obj-send">
                        </div>
                    </div>
                    <div class="row">
                        <button class="ui right floated positive button submit-button">
                            Siguiente
                        </button>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- FINAL FIRST SECTION -->
            <!-- SECOND SECTION -->
            <div id="accordion-2" class="disabled-content"><!-- disabled or enabled accordion -->
                <div class="title" id="title-2">
                    <i class="dropdown icon"></i>
                    Datos del Embarque
                </div>
                <div class="content" id="content-2">
                    <div class="row" id="error_second_section"></div>
                    <div>&nbsp;</div>
    				<div class="two fields required">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Moneda
    						</label>
    					</div>
    					<div class="thirteen wide field">
    						<div class="ui selection dropdown moneda-main">
    							<input name="moneda" type="hidden" class="requiredClass"
                                       value="0">
    							<i class="dropdown icon"></i>
    							<div class="default text currencyText">Moneda</div>
    							<div class="menu">
    								<div class="item" data-value="mxn">
    									Pesos
    								</div>
    								<div class="item" data-value="usd">
    									Dolares
    								</div>
    							</div>
    						</div>
    					</div>
    				</div>
                    <div class="two fields required">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Valor de la Mercancia
    						</label>
    					</div>
    					<div class="thirteen wide field">
    						<div class="ui labeled input">
    							<div class="ui label">$</div>
    							<input type="text" name="valor_merc" placeholder="Valor de la Mercancia"
                                       class="requiredClass valMer">
    						</div>
    					</div>
                    </div>
                    <div class="two fields required">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Tipo de Mercancia
    						</label>
    					</div>
    					<div class="thirteen wide field">
    						<div class="ui search selection dropdown tipo-merc-main">
    							<input name="tipo_merc" type="hidden" class="requiredClass"
                                       value="0">
    							<i class="dropdown icon"></i>
    							<div class="default text tipoMercancia">Tipo de Mercancia</div>
    							<div class="menu">
    								<?php foreach ($merch as $key => $value): ?>
    									<div class="item" data-value="<?php echo $value['id']; ?>"
    													  data-desc="<?php echo $value['desc']; ?>">
    										<?php echo $value['desc']; ?>
                                            <input type="hidden"
                                                   data-name="<?php echo $value['desc']; ?>"
                                                   id="<?php echo $value['id']; ?>"
                                                   value="<?php echo $value['class']; ?>">
    									</div>
    								<?php endforeach; ?>
    							</div>
    						</div>
    					</div>
                    </div>
                    <div class="sixteen wide field required">
                        <label class="bold_text_title left_align">
                            Descripcion de la Mercancia
                        </label>
                        <textarea rows="5" class="requiredClass textareaData" id="descArea"></textarea>
                    </div>
                    <div class="two fields required">
                        <div class="three wide field">
                            <label class="left_align bold_text_title">
                                Empaque
                            </label>
                        </div>
                        <div class="thirteen wide field">
                            <input type="text" class="requiredClass" name="empaque"
                                   placeholder="Empaque" id="empaque">
                        </div>
                    </div>
                    <div class="fields">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Tipo
    						</label>
    					</div>
                        <div class="five wide field required">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="nueva" data-text="tipo"
                                       class="checkdata2 requiredClass tipo tipo-21" name="tipo"
                                       data-class="tipo-21">
                                <label>
                                    Mercancia Nueva
                                </label>
                            </div>
                        </div>
                        <div class="five wide field required">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="usada" data-text="tipo"
                                       class="checkdata2 requiredClass tipo tipo-21" name="tipo"
                                       data-class="tipo-21">
                                <label>
                                    Mercancia Usada
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="fields">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Cobertura
    						</label>
    					</div>
                        <div class="five wide field required">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="nacional" data-text="cobertura"
                                       class="requiredClass cob-22 checkdata2" name="cobertura"
                                       data-class="cob-22">
                                <label>
                                    Cobertura Nacional
                                </label>
                            </div>
                        </div>
                        <div class="six wide field required">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="internacional" data-text="cobertura"
                                       class="requiredClass cob-22 checkdata2" name="cobertura"
                                       data-class="cob-22">
                                <label>
                                    Cobertura Internacional
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label class="left_align bold_text_title">
                            Origen
                        </label>
                    </div>
                    <div class="four fields">
                        <div class="three wide field">
                            <label class="left_align bold_text_title">
                                Continente
                            </label>
                        </div>
                        <div class="five wide field">
                            <div class="ui selection dropdown continente-origen">
                                <input type="hidden" name="continente_origen" class="requiredClass"
                                       value="0">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Continente</div>
                                <div class="menu">
                                    <?php foreach($cont->result_array() as $continente): ?>
                                        <div class="item" data-value="<?php echo $continente['PaisContinente']; ?>">
                                            <?php echo $continente['PaisContinente']; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="two wide field" style="margin-left: 10px">
                            Pais
                        </div>
                        <div class="four wide field">
                            <div class="ui selection dropdown pais-origen">
                                <input type="hidden" name="pais_origen" class="requiredClass">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Pais</div>
                                <div class="menu" id="pais_origen_resultados">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="four fields">
                        <div class="three wide field">
                            <label class="left_align bold_text_title">
                                Estado
                            </label>
                        </div>
                        <div class="five wide field">
                            <div class="ui selection dropdown estado-origen">
                                <input type="hidden" name="estado_origen" class="requiredClass">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Estado</div>
                                <div class="menu" id="estado_origen_resultados">
                                </div>
                            </div>
                        </div>
                        <div class="one wide field" style="margin-left: 20px">
                            <label class="left_align bold_text_title">
                                Ciudad
                            </label>
                        </div>
                        <div class="six wide field" style="margin-left: 25px">
                            <input type="text" name="ciudad_origen" class="requiredClass"
                                   id="ciudad-elegida_origen" placeholder="Ciudad">
                            <!--div class="ui selection dropdown ciudad-origen">
                                <input type="hidden" name="ciudad_origen" class="requiredClass"
                                       value="0">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Ciudad</div>
                                <div class="menu" id="ciudad_origen_resultados">
                                </div>
                            </div-->
                        </div>
                    </div>
                    <div class="required field">
                        <label class="bold_text_title left_align">
                            Destino
                        </label>
                    </div>
                    <div class="four fields">
                        <div class="three wide field">
                            <label class="left_align bold_text_title">
                                Continente
                            </label>
                        </div>
                        <div class="five wide field">
                            <div class="ui selection dropdown continente-destino">
                                <input type="hidden" name="continente_destino" class="requiredClass"
                                       value="0">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Continente</div>
                                <div class="menu">
                                    <?php foreach($cont->result_array() as $continente): ?>
                                        <div class="item" data-value="<?php echo $continente['PaisContinente']; ?>">
                                            <?php echo $continente['PaisContinente']; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="two wide field" style="margin-left: 10px">
                            Pais
                        </div>
                        <div class="four wide field">
                            <div class="ui selection dropdown pais-destino">
                                <input type="hidden" class="requiredClass" name="pais_destino">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Pais</div>
                                <div class="menu" id="pais_destino_resultados">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="four fields">
                        <div class="three wide field">
                            <label class="left_align bold_text_title">
                                Estado
                            </label>
                        </div>
                        <div class="five wide field">
                            <div class="ui selection dropdown estado-destino">
                                <input type="hidden" name="estado_destino" class="requiredClass">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Estado</div>
                                <div class="menu" id="estado_destino_resultados">
                                </div>
                            </div>
                        </div>
                        <div class="one wide field" style="margin-left: 20px">
                            <label class="left_align bold_text_title">
                                Ciudad
                            </label>
                        </div>
                        <div class="six wide field" style="margin-left: 25px">
                            <input type="text" name="ciudad_destino" class="requiredClass"
                                   id="ciudad-elegida-origen" placeholder="Ciudad">
                            <!--div class="ui selection dropdown ciudad-origen">
                                <input type="hidden" name="ciudad_origen" class="requiredClass"
                                       value="0">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Ciudad</div>
                                <div class="menu" id="ciudad_origen_resultados">
                                </div>
                            </div-->
                        </div>
                    </div>
                    <div class="two fields required">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Medio de Transporte
    						</label>
    					</div>
    					<div class="thirteen wide field">
    						<div class="ui selection dropdown medio-transp-main">
    							<input name="medio_transp" type="hidden" class="requiredClass"
                                       value="0">
    							<i class="dropdown icon"></i>
    							<div class="default text conveyance">Medio de Transporte</div>
    							<div class="menu">
    								<?php foreach($trans as $key => $value): ?>
    									<div class="item" data-value="<?php echo $key; ?>" data-text="<?php echo $value; ?>">
    										<?php echo $value; ?>
    									</div>
    								<?php endforeach; ?>
    							</div>
    						</div>
    					</div>
                    </div>
                    <div class="row">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-2">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-2">
                                Cotizar
                            </button>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- FINAL SECOND SECTION -->
            <!-- THIRD SECTION -->
            <div id="accordion-3" class="disabled-content"><!-- disabled or enabled accordion -->
                <div class="title" id="title-3">
                    <i class="dropdown icon"></i>
                    Cotizacion Generada
                </div>
                <div class="content" id="content-3">
                    <div class="row" id="error_third_section"></div>
                    <div class="two fields">
                        <div class="inline field">
                            <label class="left_align bold_text_title">
                                # Cotizacion
                            </label>
                            <span id="order_id_3"><span>
                        </div>
                        <div class="inline field">
                            <label class="left_align bold_text_title">
                                Fecha de Cotizacion
                            </label>
                            <span><?php echo $dateFormat; ?></span>
                        </div>
                    </div>
                    <div class="disabled fields">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Prima Neta
    						</label>
    					</div>
    					<div class="thirteen wide field">
    						<div class="ui labeled input">
    							<div class="ui label">$</div>
    							<input type="text" name="prima_neta" placeholder="Prima Neta"
                                       class="prima-neta third-section" value="0" id="prima_neta">
    						</div>
    					</div>
                    </div>
                    <div class="disabled fields">
    					<div class="three wide field">
    						<label class="left_align bold_text_title">
    							Total a Pagar
    						</label>
    					</div>
    					<div class="thirteen wide field">
    						<div class="ui labeled input">
    							<div class="ui label">$</div>
    							<input type="text" name="total_pagar" placeholder="Total a Pagar"
                                       class="total-pagar third-section" value="0" id="total_pagar">
    						</div>
    					</div>
                    </div>
                    <div class="row">
                        <label class="left_align mini_messages_stars bold_text_title">
                            * No incluye IVA ni Derecho de Poliza.
                        </label>
                        <br />
                        <label class="left_align mini_messages_stars bold_text_title">
                            * Este cost&oacute; no aplica en gastos de expedici&oacute;n.
                        </label>
                    </div>
                    <div class="row">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-3">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-3">
                                Cotizar
                            </button>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- FINAL THIRD SECTION -->
            <!-- FOUR SECTION -->
            <div id="accordion-4" class="disabled-content">
                <div class="title" id="title-4">
                    <i class="dropdown icon"></i>
                    Preview / Envio
                </div>
                <div class="content" id="content-4">
                    <div class="row margin-top-buttons">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-4">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-4">
                                Confirmar y Enviar
                            </button>
                        </div>
                    </div>
                    <div class="clear-floats">&nbsp;</div>
                    <!-- HEADER OF THE PREVIEW -->
                    <div class="column">
                        <div class="ui segment">
                            <div class="field bold_text_title" style="float: left">
                                DATOS GENERALES
                            </div>
                            <div style="float: right">
                                <label class="bold_text_title">Numero Cotizacion: </label>
                                <label id="no_cotizacion" class="light_text_desc"></label>
                            </div>
                            <div style="clear: both"></div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Eres Vendedor?</label>
                                    <label id="vendedor_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Nombre del Vendedor:</label>
                                    <label id="nombre_vendedor_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Nombre</label>
                                    <label id="nombre" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Moneda</label>
                                    <label id="currency" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Email</label>
                                    <label id="email_txt" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field">
                                    <label class="bold_text_title">Telefono</label>
                                    <label id="tel_txt" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="two fields left_align inline">
                                <div class="eight wide field">
                                    <label class="bold_text_title">Giro</label>
                                    <label id="type" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Vigencia al</label>
                                    <label id="validAt" class="light_text_desc"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END HEADER OF THE PREVIEW -->
                    <!-- SECOND SECTION OF DATA -->
                    <div class="column margin-top-segment">
                        <div class="ui segment">
                            <div class="field left_align inline">
                                <label class="bold_text_title">Valor de la Mercancia</label>
                                <label id="merchandise_value" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Mercancia</label>
                                <label id="merchandise" class="light_text_desc"></label>
                            </div>
                            <div class="two field">
                                <div class="field left_align">
                                    <label class="bold_text_title">Descripcion de la  Mercancia</label>
                                </div>
                                <div class="field left_align">
                                    <label id="desc" class="light_text_desc"></label>
                                </div>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Empaque</label>
                                <label id="empaque_data" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Tipo</label>
                                <label id="kind" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Cobertura</label>
                                <label id="cover" class="light_text_desc"></label>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Origen</label>
                                <!--label id="origen" class="light_text_desc"></label-->
                                <div class="two fields left_align inline">
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Continente</label>
                                        <label id="continente_origen_label" class="light_text_desc"></label>
                                    </div>
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Pais</label>
                                        <label class="light_text_desc" id="pais_origen_label"></label>
                                    </div>
                                </div>
                                <div class="two fields left_align inline">
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Estado</label>
                                        <label class="light_text_desc" id="estado_origen_label"></label>
                                    </div>
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Ciudad</label>
                                        <label class="light_text_desc" id="ciudad_origen_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Destino</label>
                                <!-- label id="destination" class="light_text_desc"></label -->
                                <div class="two fields left_align inline">
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Continente</label>
                                        <label class="light_text_desc" id="continente_destino_label"></label>
                                    </div>
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Pais</label>
                                        <label class="light_text_desc" id="pais_destino_label"></label>
                                    </div>
                                </div>
                                <div class="two fields left_align inline">
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Estado</label>
                                        <label class="light_text_desc" id="estado_destino_label"></label>
                                    </div>
                                    <div class="eight wide field">
                                        <label class="bold_text_title">Ciudad</label>
                                        <label class="light_text_desc" id="ciudad_destino_label"></label>
                                    </div>
                                </div>
                            </div>
                            <div class="field left_align inline">
                                <label class="bold_text_title">Medio de transporte</label>
                                <label id="transport" class="light_text_desc"></label>
                            </div>
                            <div class="two fields left_align">
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Tarifa</label>
                                    <label id="tarifa" class="light_text_desc"></label>
                                </div>
                                <div class="eight wide field inline">
                                    <label class="bold_text_title">Prima Neta</label>
                                    <label id="prima_neta_text" class="light_text_desc"></label>
                                </div>
                            </div>
                            <!-- INNER SEGMENT START -->
                            <div class="ui segment">
                                <div class="two field">
                                    <div class="field left_align">
                                        <label class="bold_text_title">
                                            COBERTURAS
                                        </label>
                                    </div>
                                    <div class="hidden-segment-value" id="cob_nacionales">
                                        <div class="field left_align margin-left-blocks">
                                            <div class="field left_align">
                                                <label class="bold_text_title">
                                                    Cobertura Nacional
                                                </label>
                                            </div>
                                            <ul class="list-defined">
                                                <li>Riesgos ordinarios de transito</li>
                                                <li>Robo total c/ violencia</li>
                                                <li>Robo de bulto por entero</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="hidden-segment-value" id="cob_internacionales">
                                        <div class="field left_align margin-left-blocks">
                                            <div class="field left_align">
                                                <label class="bold_text_title">
                                                    Cobertura Internacional
                                                </label>
                                            </div>
                                            <ul class="list-defined">
                                                <li>Riesgos ordinarios de transito</li>
                                                <li>Robo total c/ violencia</li>
                                                <li>Contribuci&oacute;n a la aver&iacute;a gruesa</li>
                                                <li>Robo de bulto por entero</li>
                                                <li>Echaz&oacute;n y barredura</li>
                                                <li>Barater&iacute;a del capital</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="two field">
                                    <div class="field left_align">
                                        <label class="bold_text_title">
                                            DEDUCIBLES
                                        </label>
                                    </div>
                                    <div class="field left_align margin-left-blocks">
                                        <div class="field left_align">
                                            <label class="bold_text_title">
                                                Deducible 10%
                                            </label>
                                        </div>
                                        <ul class="list-defined">
                                            <li class="both">
                                                Riesgos Ordinarios de Tr&aacute;nsito
                                            </li>
                                            <li class="int">
                                                Contribuci&oacute;n a la aver&iacute;a gruesa
                                            </li>
                                            <li class="int">
                                                Echaz&oacute;n y barredura
                                            </li>
                                            <li class="int">
                                                Barater&iacute;a del capital
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="field left_align margin-left-blocks">
                                        <div class="field left_align">
                                            <label class="bold_text_title">
                                                Deducible 25%
                                            </label>
                                        </div>
                                        <ul class="list-defined">
                                            <li class="both">
                                                Robo total de la carga
                                            </li>
                                            <li class="both">
                                                Robo de bulto por entero
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="two field">
                                    <div class="field left_align">
                                        <label class="bold_text_title">
                                            MEDIDAS DE SEGURIDAD
                                        </label>
                                    </div>
                                    <div class="field left_align margin-left-blocks">
                                        <div class="field left_align">
                                            <label class="bold_text_title">
                                                Aplican las siguientes medidas de seguridad
                                            </label>
                                        </div>
                                        <ul class="list-defined">
                                            <li id="security_data"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- INNER SEGMENT END -->
                        </div>
                    </div>
                    <!-- END SECOND SECTION OF DATA -->
                    <!-- THIRD SECTION OF DATA -->
                    <div class="column margin-top-segment">
                        <div class="ui segment">
                            <div class="field">
                                CONDICIONES
                            </div>
                            <div class="two field">
                                <div class="field left_align">
                                    <label class="bold_text_title">
                                        TERMINOS Y CONDICIONES
                                    </label>
                                </div>
                                <div class="field left_align">
                                    <ol>
                                        <li>
                                            El dispositivo de GPS deber&aacute; estar activo y funcionando
                                            desde el origen del viaje y hasta su destino. No se
                                            tomar&aacute; como cumplida esta medida en caso de que el
                                            dispositivo colocado en el tracto o la caja no se
                                            encuentra activo, se encuentre apagado, exista un mal
                                            funcionamiento y/o las cuotas a las que est&aacute; obligado el
                                            contratante no hayan sido cubiertas y se haya suspendido
                                            el servicio por falta de pago.
                                        </li>
                                        <li>
                                            Para poder tomar como cumplida la medida del rastreo
                                            satelital se requiere de una empresa legalmente constituida
                                            que brinde el servicio. No se aceptar&aacute;n GPS de
                                            celulares, computadoras, tablets y/o cualquier
                                            instrumento electr&oacute;nico.
                                        </li>
                                        <li>
                                            En ning&uacute;n momento deber&aacute; la mercanc&iacute;a quedar sin
                                            vigilancia (chofer como m&iacute;nimo) y/o seguridad durante
                                            los per&iacute;odos de espera y descarga. En caso que el embarque
                                            lleve custodia, esta última deber&aacute; vigilar el embarque
                                            hasta que la mercanc&iacute;a sea totalmente recibida por la
                                            persona responsable.
                                        </li>
                                        <li>
                                            Hacer los viajes de tal manera que al llegar al punto de
                                            destino no se tenga que esperar fuera de estas instalaciones
                                            para ser recibido, sin vigilancia (chofer como m&iacute;nimo)
                                            y/o seguridad durante los periodos de espera o descarga.
                                        </li>
                                        <li>
                                            Uso de caja met&aacute;lica cerrada o en su defecto, si
                                            la naturaleza de la mercancía lo permite, contar con
                                            lonas en excelente estado (es decir no est&eacute;n rotas,
                                            rasgadas o perforadas) protegiendo la mercanc&iacute;a que en
                                            su totalidad.
                                        </li>
                                        <li>Rutas establecidas con control de tiempos.</li>
                                        <li>
                                            Dicha ruta deber&aacute; considerar hora de salida y hora de
                                            llegada al destino.
                                        </li>
                                        <li>
                                            Para el transporte terrestre as&iacute; como para las
                                            conexiones mar&iacute;timas el asegurado garantiza el
                                            uso de caminos de cuota cuando est&eacute;n disponibles,
                                            no sobrepasar el l&iacute;mite de capacidad de carga
                                            especificado para el cami&oacute;n y el contenedor.
                                        </li>
                                        <li>
                                            Las escalas para recarga de combustible, descanso, etc.,
                                            deber&aacute;n estar programadas, autorizadas y supervisadas.
                                        </li>
                                        <li>No se permiten desviaciones para fines personales.</li>
                                        <li>
                                            Uso de sellos o candados de acero o naval locks para
                                            contenedores y trailers.
                                        </li>
                                        <li>Uso de bit&aacute;cora de inspecci&oacute;n de las unidades por viaje.</li>
                                        <li>Utilizar carreteras de cuota siempre que existan.</li>
                                        <li>
                                            Deber&aacute;n indicarse rutas de traslado, cualquier
                                            variaci&oacute;n deber&aacute; revisarse y aprobarse con
                                            anterioridad a la salida de la plataforma con el
                                            veh&iacute;culo.
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="two field">
                                <div class="field left_align">
                                    <label class="bold_text_title">
                                        PAISES EXCLUIDOS
                                    </label>
                                </div>
                                <div class="field left_align">
                                    <p> Los embarques se excluyen en los siguientes paises: </p>
                                    <p>
                                        <ol>
                                            <li>Afganist&aacute;n</li>
                                            <li>Algeria</li>
                                            <li>Angola</li>
                                            <li>Arabia Saudita</li>
                                            <li>Armenia</li>
                                            <li>Azerbaijan</li>
                                            <li>Bangladesh</li>
                                            <li>Burkina Faso</li>
                                            <li>Burundi</li>
                                            <li>Camer&uacute;n (Marítimo: Bakassi Peninsula; Terrestre: Todo)</li>
                                            <li>Chad</li>
                                            <li>Corea del Norte</li>
                                            <li>Cote d'Ivoire (Costa de Marfil)</li>
                                            <li>Cuba</li>
                                            <li>Egipto</li>
                                            <li>Eritrea</li>
                                            <li>Etiop&iacute;a</li>
                                            <li>Filipinas</li>
                                            <li>Georgia</li>
                                            <li>Guinea</li>
                                            <li>Guinea Ecuatorial</li>
                                            <li>Golfo de Aden zona mar&iacute;tima y 70° E frente a
                                                las costas de Somalia, limitando al norte entre
                                                latitud 20° N, y al sur 12° S</li>
                                            <li>Golfo de Guinea, en la zona mar&iacute;tima entre
                                                3°6'13.966"W 5°8'44.274"N (Cote d’Ivoire – l&iacute;mite de Ghana)
                                                y 3°6'13.966"W 2°20'12.8"N al oeste, y
                                                9°46'18.636"E 2°20'12.8"N (l&iacute;mite Camer&uacute;n - Guinea Ecuatorial)
                                                al este; India (Marítimo: Gujarat; Terrestre: Kashmir,
                                                Assam, Nagaland, Bihar, Jharkand, Chhattisgarh)</li>
                                            <li>Indonesia</li>
                                            <li>Ir&aacute;n</li>
                                            <li>Irak</li>
                                            <li>Jord&aacute;n</li>
                                            <li>Kazakhstan</li>
                                            <li>Kenia</li>
                                            <li>Kyrgyzstan</li>
                                            <li>L&iacute;bano</li>
                                            <li>Liberia</li>
                                            <li>Libia (Mar&iacute;timo: las aguas territoriales a
                                                12 nm de Libia Terrestre: Todo)</li>
                                            <li>Madagascar</li>
                                            <li>Mali</li>
                                            <li>Mauritania</li>
                                            <li>Moldova (Transnistria)</li>
                                            <li>Myanmar (Burma)</li>
                                            <li>Nagorno-Karabakh</li>
                                            <li>Nepal</li>
                                            <li>Niger</li>
                                            <li>Nigeria</li>
                                            <li>Palestina (Cisjordania y Gaza)</li>
                                            <li>Israel dentro de los 45km de los l&iacute;mites
                                                Gaza-Israel, incluye Beersheba y Ashdod</li>
                                            <li>Pakist&aacute;n</li>
                                            <li>Rep&uacute;blica de &Aacute;frica Central</li>
                                            <li>Rep&uacute;blica Democr&aacute;tica del Congo</li>
                                            <li>Rep&uacute;blica Federal de Yugoslavia y Serbia</li>
                                            <li>Ruanda</li>
                                            <li>Rusia (Norte del C&aacute;ucaso)</li>
                                            <li>Sierra Leona</li>
                                            <li>Siria</li>
                                            <li>Somalia</li>
                                            <li>Sud&aacute;n del Norte</li>
                                            <li>Sud&aacute;n del Sur</li>
                                            <li>Sud&aacute;n</li>
                                            <li>Tajikistan</li>
                                            <li>Timor Oriental</li>
                                            <li>Turkmenistan</li>
                                            <li>Ucrania</li>
                                            <li>Uganda (Mar&iacute;timo: Lago Albert; Terrestre:
                                                Uganda del Norte)</li>
                                            <li>Uzbekistan</li>
                                            <li>Yemen</li>
                                            <li>Zimbabwe</li>
                                        </ol>
                                    </p>
                                </div>
                            </div>
                            <div class="two field">
                                <div class="field left_align">
                                    <label class="bold_text_title">
                                        MERCANCIAS EXCLUIDAS
                                    </label>
                                </div>
                                <div class="field left_align">
                                    <p>Las Mercancias Excluidas son:</p>
                                    <p>
                                        <ol>
                                            <li>
                                                Productos denominados como valores: Documentos o
                                                registros de negocios, dinero en efectivo, t&iacute;tulos
                                                valores, Instrumentos negociables, Tarjetas Cr&eacute;dito
                                                o d&eacute;bito, Estampillas, Cheques o tarjetas de consumo
                                                o similares, Billetes de loter&iacute;a o similares, Oro,
                                                metales preciosos y/o artículos hechos de o que
                                                contengan metales preciosos y/o piedras, pinturas,
                                                estatuas, obras de arte o antigüedades y los bienes
                                                de naturaleza similar.
                                            </li>
                                            <li>
                                                Pseudoefedrina, efedrina
                                            </li>
                                            <li>
                                                Bebidas Alcoh&oacute;licas, Licores
                                            </li>
                                            <li>
                                                Relojes y / o art&iacute;culos de joyer&iacute;a que
                                                no sean de fantas&iacute;a
                                            </li>
                                            <li>
                                                Equipo electr&oacute;nico port&aacute;til, laptops,
                                                celulares, PDA’s, Ipod, Ipad, videojuegos y sus accesorios
                                            </li>
                                            <li>
                                                Armas y / o municiones, fuegos artificiales, explosivos
                                            </li>
                                            <li>
                                                Yates, lanchas y / o embarcaciones de vela por sus
                                                propios medios
                                            </li>
                                            <li>
                                                Autom&oacute;viles y Motocicletas
                                            </li>
                                            <li>
                                                Mercanc&iacute;as a granel (excepto semillas y cereales)
                                            </li>
                                            <li>
                                                Cartas geogr&aacute;ficas, mapas o planos
                                            </li>
                                            <li>
                                                Cobre
                                            </li>
                                            <li>
                                                Polipropileno
                                            </li>
                                            <li>
                                                Contenedores
                                            </li>
                                            <li>
                                                Materiales de desecho
                                            </li>
                                            <li>
                                                Derivados del petr&oacute;leo y
                                                qu&iacute;micos inflamables
                                            </li>
                                        </ol>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END THIRD SECTION OF DATA -->
                    <div class="row margin-top-buttons">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-4">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton-4">
                                Confirmar y Enviar
                            </button>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/custom/merchandise.js'; ?>"></script>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/mathElements.js'; ?>"></script>