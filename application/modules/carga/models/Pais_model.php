<?php

/**
 * Class that contains all the function
 * for the table related to PAIS. Those
 * functions will be used to get data will be
 * displayed in the form
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
Class Pais_model extends CI_Model {

    /**
     * Constructor.......
     */
    public function __construct() {
        parent::__construct();

        // name of the table
        $this->table = "Pais";
    }

    /**
     * Method used for get all the continents will be
     * displayed in the form of the merchandise
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getAllContinents() {
        $this->db->distinct();
        $this->db->select('PaisContinente');
        $this->db->order_by('PaisContinente', 'ASC');
        $query = $this->db->get($this->table);

        return $query;
    }

    /**
     * Method used for getting all the countries
     * related to the continent selected by the
     * user once selected the dropdown should populate
     * with the results returned by this method
     *
     * @param string $continent
     * @param int $flag
     * @return array $countries
     *
     * @author Ruben Alonso Cortes Mendoza
     * @version 1.0
     * @company ruvicdev
     */
    public function getCountryByContinent($continent, $flag) {
        $this->db->select('PaisCodigo, PaisNombre, PaisArea')
                 ->where('PaisContinente', $continent);

        // Check if flag is active to just looking for in Mexico states
        if ($flag == 1 || $flag == '1') {
            $this->db->where('PaisCodigo', 'MEX');
        }

        $this->db->order_by('PaisNombre', 'ASC');
        $query = $this->db->get($this->table);

        return $query;
    }
}
