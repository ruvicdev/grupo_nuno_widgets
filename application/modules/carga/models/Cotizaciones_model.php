<?php

/**
 * Class that contains all the methods will be used
 * for saving information for the user in the database
 * and will contains a historical of the users cotizations
 * done in this widget
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class Cotizaciones_model extends CI_Model {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // name of the table
        $this->table = "cotizaciones";
    }

    /**
     * Method will be used for save the first time the data
     * and linked with the number of order regarding the
     * session_id created to save the data as history
     *
     * @param array $array
     * @return int id
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveBaseData($array) {
        $this->db->insert($this->table, $array);
        return $this->db->insert_id();
    }

    /**
     * Method will be used to get all the information of the user
     * in order to get all the data will be displayed in the
     * PDF
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getDataSpecificRecord($id) {
        $this->db->where('id', $id);
        $data = $this->db->get($this->table);

        return $data;
    }

    /**
     * Method is going to save specific information
     * about the the cotizations done by the users
     * once finish all the steps before to get the PDF file
     *
     * @param array $dataArray
     * @param integer $id
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function updateBaseData($dataArray, $id) {
        $data = $this->__getArrayData($dataArray);

        $this->db->where('id', $id);
        $this->db->update($this->table, $data);
    }

    /**
     * Method to create the array will be
     * used to update the information on
     * this table and then used to create
     * reports
     *
     * @param array $dataArray
     * @return array $returnArray
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    private function __getArrayData($dataArray) {
        $tipo_persona   = "";
        $nombre_persona = "";
        $seguridades    = "";

        // if to fill in user's data like name and kind of person
        if ($dataArray['tipo_persona'] == '1' || $dataArray['tipo_persona'] == 1) {
            $tipo_persona   = "Persona Fisica";
            $nombre_persona = $dataArray['nombre'] . " " . $dataArray['apellido_paterno'] . " " .
                              $dataArray['apellido_materno'];
        } else {
            $tipo_persona   = "Persona Moral";
            $nombre_persona = $dataArray['persona_moral'];
        }

        // security data
        foreach ($dataArray['security'] as $seguridad) {
            $seguridades .= $seguridad . " ";
        }

        $returnArray = array(
            'nombre_user'         => $nombre_persona,
            'user_email'          => $dataArray['email'],
            'user_telefono'       => $dataArray['telefono'],
            'user_rfc'            => strtoupper($dataArray['rfc']),
            'user_tipo'           => $tipo_persona,
            'tipo_giro'           => $dataArray['nombre_giro'],
            'status'              => 'Finished',
            'fecha_actualizacion' => date('d-m-Y H:i:s'),
            'tipo_giro_id'        => $dataArray['giro'],
            'vol_viajes'          => str_replace("_", " ", $dataArray['vol_viajes']),
            'tipo_persona'        => $dataArray['tipo_persona'],
            'tipo_moneda'         => $dataArray['tipo_moneda'],
            'moneda'              => strtoupper($dataArray['moneda']),
            'valor_merc'          => $dataArray['valor_merc'],
            'tipo_merc'           => $dataArray['tipo_merc'],
            'desc_merc'           => $dataArray['descripcion_merc'],
            'empaque'             => $dataArray['empaque'],
            'tipo_carga'          => $dataArray['tipo'],
            'cobertura'           => $dataArray['cobertura'],
            'tipo_trans_id'       => $dataArray['medio_transp'],
            'nombre_merc'         => $dataArray['nombre_merc'],
            'medio_trans'         => $dataArray['nombre_medio_trans'],
            'prima_neta'          => $dataArray['prima_neta'],
            'seguridad'           => trim($seguridades),
            'total_pagar'         => $dataArray['total_pagar'],
            'continente_origen'   => $dataArray['continente_origen'],
            'pais_origen'         => $dataArray['pais_origen'],
            'estado_origen'       => $dataArray['estado_origen'],
            'ciudad_origen'       => $dataArray['ciudad_origen'],
            'continente_destino'  => $dataArray['continente_destino'],
            'pais_destino'        => $dataArray['pais_destino'],
            'estado_destino'      => $dataArray['estado_destino'],
            'ciudad_destino'      => $dataArray['ciudad_destino'],
            'clase'               => $dataArray['class'],
            'valida_hasta'        => $dataArray['validAt'],
            'vendedor'            => $dataArray['vendedor'],
            'nombre_vendedor'     => $dataArray['nombre_vendedor']
        );

        return $returnArray;
    }
}
