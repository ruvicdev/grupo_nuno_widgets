<?php

/**
 * Class will contains all the information
 * related to the countries and cities
 * should display in the different fields
 * enabled in the forms
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class Ciudad_model extends CI_Model {

    /**
     * Constructor ......
     */
    public function __construct() {
        parent::__construct();

        // name of the table
        $this->table = "Ciudad";
    }

    /**
     * Method used for getting all the States
     * related to the country selected by the
     * user
     *
     * @param string $countryCode
     * @return array $cities
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getStateByCountry($countryCode) {
        $this->db->distinct()
                 ->select('CiudadDistrito')
                 ->where('PaisCodigo', $countryCode)
                 ->where_not_in('CiudadDistrito', 'Otra')
                 ->order_by('CiudadDistrito', 'ASC');
        $query = $this->db->get($this->table);

        return $query;
    }

    /**
     * Method used for getting all the cities related to
     * the state selected by the user once has been
     * selected the continent, country and states
     * before in the form
     *
     * @method post
     * @param string $state
     * @return array $data
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getCitiesByState($state) {
        $this->db->select('CiudadId, CiudadNombre')
                 ->where('CiudadDistrito', $state)
                 ->order_by('CiudadNombre', 'ASC');
        $query = $this->db->get($this->table);

        return $query;
    }
}
