<?php

/**
 * Class that contains all the methods will be used
 * for saving information for the user in the database
 * and will contains a historical of the users cotizations
 * done in this Club Salud widget
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class Club_Salud_Historial_Integrantes_Model extends CI_Model {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // name of the table
        $this->table = "Club_Salud_Historial_Integrantes";
    }

    /**
     * Save history data
     *
     * @param array $array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function insertData($array) {
        $this->db->insert_batch($this->table, $array);
    }

    /**
     * Get MAX id_integrantes from table
     *
     * @param array $array
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function getMaxIntegrantesId() {
        return $this->db->query("SELECT COALESCE(MAX(id_integrantes),0)+1 AS max_id_integrantes
                                 FROM `Club_Salud_Historial_Integrantes`
                                 WHERE 1=1")->row()->max_id_integrantes;
    }

}
