<?php (defined('BASEPATH')) OR exit('No direct script access allowed.');

/**
 * INSURANCE OF MAYOR MEDICAL EXPENSES
 *
 * Class that contains all the functionality for
 * the Insurance of Major & Minor Medical Expenses Quota widget
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
Class Salud extends MY_Controller {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        // load libraries
        $this->load->library('session');

        // load the models
        $this->load->model( array('Club_Salud_Historial_Model', 'Club_Salud_Historial_Integrantes_Model') );

        // create the object to export the PDF
        $this->pdf = new ExportFiles();

        // creates the email and send the data
        $this->emailSend = new EmailSend();

        // creates the template object
        $this->template = new Templates();

        // Load utils
        $this->load->library('utils/NumberToLetterConverter');
    }

    /**
     * Method will load all the information for the insurance quote
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function index() {
        $dataView = $this->__generateDropdowns();

        $content = $this->loadSingleView('index', $dataView, TRUE);
        $data    = $this->__setArray('content', $content);
        $data    = $this->__setArray('title', 'Cotiza tu Seguro GMM & Menores');

        $this->loadTemplate($data);
    }

    /**
     * Method will calculate the quotation with user data sent
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function previewData() {
        $data = json_decode(file_get_contents('php://input'), true);

        $email                  = $data["email"];
        $telefono               = $data["telefono"];
        $estado                 = $data["estado"];
        $ciudad                 = $data["ciudad"];
        //$cobertura            = $data["cobertura"];
        //$emergenciaExtranjero = $data["emergenciaExtranjero"];
        $integrantesArray       = $data["integrantes"]; //Array of integrantes

        //Verifica si el archivo del PDF de la Red Medica existe
        $pdfRedMedicaExist      = true;
        $estadoSeleccionado     = str_replace(" ","_",$estado);
        $urlPdfRedMedicaXEstado = "./public/files/RedMedicaClubSalud/RedMedica_".$estadoSeleccionado.".pdf";

        if (!file_exists($urlPdfRedMedicaXEstado)) {
            $pdfRedMedicaExist = false;
        }

        //MENSUAL (Calculo del seguro)
        $primerReciboTitularMes          = 1254.08; //1090; //Pago del primer recibo del titular
        $subsecuentesReciboTitularMes    = 906.08; //742;  //Pago del segundo recibo en adelante del titular
        $primerReciboConyugeMes          = 952.08; //858;  //Pago del primer recibo del conyuge
        $subsecuentesReciboConyugeMes    = 836.08; //742;  //Pago del segundo recibo en adelante del conyuge
        $primerReciboPrimerHijoMes       = 952.08; //858;    //Pago del primer recibo del primer hijo
        $subsecuentesReciboPrimerHijoMes = 836.08; //742;    //Pago del segundo recibo en adelante del primer hijo
        $segundoHijoAdelanteMes          = 300;    //150;  //Pago del segundo hijo, solo se cobra al inicio de la vigencia
        $pagoTotalInicialMes             = 0;      //Pago total Inicial MES
        $pagoTotalSubsecuenteMes         = 0;      //Pago total Subsecuente MES

        //*******NO SE USA ACTUALMENTE EN EL COTIZADOR WIDGET******
        //TRIMESTRAL (Calculo del seguro)
        $primerReciboTitularTri          = 2534; //Pago del primer recibo del titular
        $subsecuentesReciboTitularTri    = 2186; //Pago del segundo recibo en adelante del titular
        $primerReciboConyugeTri          = 2302; //Pago del primer recibo del conyuge
        $subsecuentesReciboConyugeTri    = 2186; //Pago del segundo recibo en adelante del conyuge
        $primerReciboPrimerHijoTri       = 2302; //Pago del primer recibo del primer hijo
        $subsecuentesReciboPrimerHijoTri = 2186; //Pago del segundo recibo en adelante del primer hijo
        $segundoHijoAdelanteTri          = 150;  //Pago del segundo hijo, solo se cobra al inicio de la vigencia
        $pagoTotalInicialTri             = 0;    //Pago total Inicial MES
        $pagoTotalSubsecuenteTri         = 0;    //Pago total Subsecuente MES

        $hijos = 0;
        if(is_array($integrantesArray)){
        foreach ($integrantesArray as $value) {
            switch ($value["integrante"]) {
                case 'TITULAR':
                    //Mesual
                    $pagoTotalInicialMes     += $primerReciboTitularMes;
                    $pagoTotalSubsecuenteMes += $subsecuentesReciboTitularMes;

                    //Trimestral
                    $pagoTotalInicialTri     += $primerReciboTitularTri;
                    $pagoTotalSubsecuenteTri += $subsecuentesReciboTitularTri;

                    break;
                case 'CONYUGE':
                    //Mesual
                    $pagoTotalInicialMes     += $primerReciboConyugeMes;
                    $pagoTotalSubsecuenteMes += $subsecuentesReciboConyugeMes;

                    //Trimestral
                    $pagoTotalInicialTri     += $primerReciboConyugeTri;
                    $pagoTotalSubsecuenteTri += $subsecuentesReciboConyugeTri;

                    break;
                case 'HIJO(A)':
                    $hijos++;

                    if($hijos == 1) {
                        //Mesual
                        $pagoTotalInicialMes     += $primerReciboPrimerHijoMes;
                        $pagoTotalSubsecuenteMes += $subsecuentesReciboPrimerHijoMes;

                        //Trimestral
                        $pagoTotalInicialTri     += $primerReciboPrimerHijoTri;
                        $pagoTotalSubsecuenteTri += $subsecuentesReciboPrimerHijoTri;

                        $hijos++;
                    }else {
                        //Mesual
                        $pagoTotalInicialMes += $segundoHijoAdelanteMes;

                        //Trimestral
                        $pagoTotalInicialTri += $segundoHijoAdelanteTri;
                    }

                    break;
            }
        }
        }

        //Calculo de la inversion diaria
        $numberToLetterConverter  = new NumberToLetterConverter();

        $inversionDiaria          = 0;
        $inversionDiaria          = ( $pagoTotalInicialMes + ($pagoTotalSubsecuenteMes * 11) ) / 365;
        $inversionDiaria          = round( $inversionDiaria, 2 );
        $inversionDiariaLetras    = strtolower( $numberToLetterConverter->to_word($inversionDiaria, 'MXN') );
        $inversionDiariaMasLetras = $inversionDiaria . ' M.N. ' . '(' . $inversionDiariaLetras . ')';

        // create json response
        $response = array('status'                          => 'success',
                          'pdfRedMedicaExist'               => $pdfRedMedicaExist,
                          'integrantesArray'                => $integrantesArray,
                          'pagoTotalInicialMes'             => $pagoTotalInicialMes,
                          'pagoTotalSubsecuenteMes'         => $pagoTotalSubsecuenteMes,
                          'inversionDiariaMasLetras'        => $inversionDiariaMasLetras,
                          'inversionDiaria'                 => $inversionDiaria,
                          'primerReciboTitularMes'          => $primerReciboTitularMes,
                          'subsecuentesReciboTitularMes'    => $subsecuentesReciboTitularMes,
                          'primerReciboConyugeMes'          => $primerReciboConyugeMes,
                          'subsecuentesReciboConyugeMes'    => $subsecuentesReciboConyugeMes,
                          'primerReciboPrimerHijoMes'       => $primerReciboPrimerHijoMes,
                          'subsecuentesReciboPrimerHijoMes' => $subsecuentesReciboPrimerHijoMes,
                          'segundoHijoAdelanteMes'          => $segundoHijoAdelanteMes);

        echo json_encode($response);
    }

    /**
     * Method will calculate the quotation with user data sent
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData() {
        $json_array = json_decode(file_get_contents('php://input'), true);

        //Save data into history tables------------------------------------------------------------------------------
        $arrayToInsertHistorial = array(
            'session'                    => session_id(),
            'vendedor'                   => $json_array['asesor'],
            'fullname'                   => $json_array['fullname'],
            'email'                      => $json_array['email'],
            'telefono'                   => $json_array['telefono'],
            'estado'                     => $json_array['estado'],
            'ciudad'                     => $json_array['ciudad'],
            //'cobertura'                  => $json_array['cobertura'],
            //'emergencia_extranjero'      => $json_array['emergenciaExtranjero'],
            'inversion_diaria'           => $json_array['inversionDiaria'],
            'pago_total_inicial_mes'     => $json_array['pagoTotalInicialMes'],
            'pago_total_subsecuente_mes' => $json_array['pagoTotalSubsecuenteMes'],
            'date_created'               => date("Y-m-d H:i:s") );

        //Insert data into Club_Salud_Historial table ------------------------------------------------------------
        $clubSaludHistorial_inserted_id =
            $this->Club_Salud_Historial_Model->insertData($arrayToInsertHistorial);

        //Insert data into Club_Salud_Historial_Integrantes table
        $max_id_integrantes = $this->Club_Salud_Historial_Integrantes_Model->getMaxIntegrantesId();

        $arrayToInsertIntegrantes = array();
        for ($i=0; $i < count( $json_array['integrantes'] ); $i++) {
            $arrayTemp = array(
                'id_integrantes' => $max_id_integrantes,
                'integrante'         => $json_array['integrantes'] [$i] ['integrante'],
                'nombre'             => $json_array['integrantes'] [$i] ['nombre'],
                'sexo'               => $json_array['integrantes'] [$i] ['sexo'],
                'edad'               => $json_array['integrantes'] [$i] ['edad']
            );

            array_push($arrayToInsertIntegrantes, $arrayTemp);
        }

        $clubSaludHistorialIntegrantes_inserted_id =
            $this->Club_Salud_Historial_Integrantes_Model->insertData($arrayToInsertIntegrantes);

        //Create relation between historial table and integrantes table
        $arrayToUpdateHistorial = array(
            'id_integrantes' => $max_id_integrantes);

        $this->Club_Salud_Historial_Model->updateData($arrayToUpdateHistorial, $clubSaludHistorial_inserted_id);
        //End data insert-------------------------------------------------------------------------------------------

        // Generate the Template and init the PDF file creation-----------------------------------------------------
        //add id of historial into json_array in order to show it in PDF
        $json_array["clubSaludHistorial_inserted_id"] = $clubSaludHistorial_inserted_id;

        $template = $this->template->insuranceMinorMETemplate($json_array);
        $PDF      = $this->pdf->__initPDFMinor($template, "Cotiza Club Salud", "Cotiza Club Salud");

        // Set PDF name and create PDF file
        $fileName = "cotizaClubSalud_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

        // Create the file
        //ob_clean();
        $PDF->Output(FCPATH . 'public/files/' . $fileName, 'F');
        $completePath = FCPATH . 'public/files/' . $fileName;
        $returnedPath = base_url() . 'public/files/' . $fileName;
        //End of PDF creater----------------------------------------------------------------------------------------

        // Send the information via e-mail
        $this->emailSend->__initEmailMinor($json_array, $PDF, $completePath, "Cotiza Club Salud");

        // create json response
        $response = array('status'   => 'success',
                          'pathFile' => $returnedPath);

        echo json_encode($response);
    }

    /**
     * Method will generate all dropdown information
     *
     * @return void
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __generateDropdowns() {
        $array_states      = array( 'Aguascalientes'                  => 'Aguascalientes',
                                    'Baja California'	              => 'Baja California',
                                    'Baja California Sur'             => 'Baja California Sur',
                                    'Chiapas'                         => 'Chiapas',
                                    'Chihuahua'	                      => 'Chihuahua',
                                    'Ciudad de Mexico'	              => 'Ciudad de Mexico',
                                    'Coahuila'	                      => 'Coahuila',
                                    'Colima'	                      => 'Colima',
                                    'Durango'                         => 'Durango',
                                    'Estado de Mexico'	              => 'Estado de Mexico',
                                    'Guanajuato'                      => 'Guanajuato',
                                    'Guerrero'                        => 'Guerrero',
                                    'Hidalgo'                         => 'Hidalgo',
                                    'Jalisco'                         => 'Jalisco',
                                    'Michoacan'                       => 'Michoacan',
                                    'Morelos'                         => 'Morelos',
                                    'Nayarit'                         => 'Nayarit',
                                    'Nuevo Leon'                      => 'Nuevo Leon',
                                    'Oaxaca'                          => 'Oaxaca',
                                    'Puebla'                          => 'Puebla',
                                    'Queretaro'                       => 'Queretaro',
                                    'Quintana Roo'                    => 'Quintana Roo',
                                    'San Luis Potosi'                 => 'San Luis Potosi',
                                    'Sinaloa'                         => 'Sinaloa',
                                    'Sonora'                          => 'Sonora',
                                    'Tabasco'                         => 'Tabasco',
                                    'Tamaulipas'                      => 'Tamaulipas',
                                    'Tlaxcala'                        => 'Tlaxcala',
                                    'Veracruz'                        => 'Veracruz',
                                    'Yucatan'                         => 'Yucatan',
                                    'Zacatecas'                       => 'Zacatecas' );

        $array_si_no       = array('SI' => 'Si',
                                   'NO' => 'No');

        $array_integrantes = array('0' => 'Titular',
                                   '1' => 'Conyuge',
                                   '2' => 'Hijo(a)');

        $array_sexo        = array('1' => 'Hombre',
                                   '0' => 'Mujer');

        $dataView          = array('array_states'      => $array_states,
                                   'array_si_no'       => $array_si_no,
                                   'array_integrantes' => $array_integrantes,
                                   'array_sexo'        => $array_sexo);

        return $dataView;
    }
}
