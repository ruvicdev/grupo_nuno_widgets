<script type="text/javascript"
      src="<?php echo base_url() . 'public/js/custom/salud.js'; ?>"></script>
<script type="text/javascript"
      src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>

<div class="default-margin-top ui grid centered removePadding">
    <!-- SECTION FOR STEPS -->
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos De Cotización</div>
                <div class="description">Ingresa los Datos de la Cotización</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Envío</div>
                <div class="description">Envío Cotización</div>
            </div>
        </div>
    </div>
</div>

<div class="ui grid centered">
    <form class="ui form">
        <div class="row">&nbsp;</div>
        <div>
          <h1 class="ui header">Multicotizador Gastos Médicos Mayores y Menores</h1>
        </div>
        <div>
            <h4 class="ui header">Llene todos los campos para ver su cotización</h3>
        </div>
        <div class="row">&nbsp;</div>
        <div class="ui styled accordion">
            <!--Errors for form page-->
            <div class="ui error message"></div>
            <!-- FIRST SECTION -->
            <div id="accordion-1"><!-- disabled or enabled accordion -->
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos De Cotización
                </div>
                <div class="content active" id="content-1">
                    <div class="two fields">
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="asesorchk" id="asesorcheckbox" tabindex="0" data-value="asesorchk" class="checkdata">
                                <label>
                                    ¿Eres Vendedor de Grupo Nuño?
                                </label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui segment hidden-segment-value" id="asesor-div">
                                <input type="text" name="asesor" id="asesor" placeholder="Nombre del Vendedor">
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>
                            Nombre Completo
                        </label>
                        <input type="text" name="fullname" id="fullname" placeholder="Nombre Completo">
                    </div>
                  <div class="two fields">
                      <div class="required field">
                          <label>
                              Correo Electrónico
                          </label>
                          <input type="text" name="email" id="email" placeholder="Corre Electrónico">
                      </div>
                      <div class="required field">
                          <label>
                              Teléfono
                          </label>
                          <input type="text" name="telefono" id="telefono" placeholder="Teléfono" class="form-data-selected tel">
                      </div>
                    </div>
                    <div class="two fields">
                        <div class="required field">
                            <label>
                                Estado
                            </label>
                            <div id ="estadoDropdown" class="ui selection dropdown">
                                <input type="hidden" name="estado" id="estado">
                                <i class="dropdown icon"></i>
                                <div class="default text">Seleccione</div>
                                <div class="menu">
                                    <?php foreach ($array_states as $key => $value): ?>
                                        <div class="item" data-value="<?php echo $key; ?>">
                                            <?php echo $value; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="required field">
                            <label>
                                Ciudad
                            </label>
                            <input type="text" name="ciudad" id="ciudad" placeholder="Ciudad">
                        </div>
                    </div>
                    <div class="inline fields">
                        <div class="field">
                            <label>
                                Cobertura Nacional
                            </label>
                        </div>
                    </div>
                    <!--div class="row">&nbsp;</div>
                    <div class="inline fields">
                        <div class="required field">
                            <label>
                                Cobertura
                            </label>
                        </div>
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="cobertura" id="nacionalcheckbox" tabindex="0" data-value="nacional" class="checkdata">
                                <label>
                                    Nacional
                                </label>
                            </div>
                        </div>&nbsp;&nbsp;&nbsp;
                        <div class="field">
                            <div class="ui checkbox">
                                <input type="checkbox" name="cobertura" id="nacional_internacionalcheckbox" tabindex="0" data-value="nacional_internacional" class="checkdata">
                                <label>
                                    Nacional e Internacional
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="ui segment hidden-segment-value" id="nacional-div">
                        <div class="required field">
                            <label>
                                Emergencia en el Extranjero
                            </label>
                            <div class="ui selection dropdown">
                                <input name="emergencias_extranjero" id="emergencias_extranjero" type="hidden">
                                <i class="dropdown icon"></i>
                                <div class="default text">Seleccione</div>
                                <div class="menu">
                                    <?php foreach ($array_si_no as $key => $value): ?>
                                        <div class="item" data-value="<?php echo $key; ?>">
                                            <?php echo $value; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">&nbsp;</div-->
                    <!--Errors for integrantes table-->
                    <div class="hidden-segment-value" id="errorsIntegrantesDiv"></div>
                    <div class="row">&nbsp;</div>
                    <div class="fields">
                        <table class="ui striped celled table segment" id="integrantes_table">
                            <thead>
                                <tr>
                                    <th class="center aligned">Integrante</th>
                                    <th class="center aligned">Nombre</th>
                                    <th class="center aligned">Sexo</th>
                                    <th class="center aligned">Edad</th>
                                    <th class="center aligned"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div id="newIntegranteDropdown" class="ui fluid selection dropdown">
                                            <input name="titulo_integrante" id="titulo_integrante" type="hidden">
                                            <i class="dropdown icon"></i>
                                            <div class="default text">Seleccione</div>
                                            <div class="menu">
                                                <?php foreach ($array_integrantes as $key => $value): ?>
                                                    <div class="item" data-value="<?php echo $key; ?>">
                                                        <?php echo $value; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="newNombre" placeholder="Nombre">
                                    </td>
                                    <td>
                                        <div id="newSexoDropdown" class="ui fluid selection dropdown">
                                            <input name="sexo_integrante" id="sexo_integrante" type="hidden">
                                            <i class="dropdown icon"></i>
                                            <div class="default text">Sexo&nbsp;&nbsp;&nbsp;</div>
                                            <div class="menu">
                                                <?php foreach ($array_sexo as $key => $value): ?>
                                                    <div class="item" data-value="<?php echo $key; ?>">
                                                        <?php echo $value; ?>
                                                    </div>
                                                <?php endforeach; ?>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" id="newEdad" placeholder="Edad" maxlength="2" class="form-data-selected edad">
                                    </td>
                                    <td>
                                        <font class="ui fluid primary button" id="addRowIntegrantesTable">
                                            +
                                        </font>
                                        <!--Save the total of rows addedd of integrantes-->
                                        <input id="integrantesRowsHidden" type="hidden">
                                        <input id="existeTitularHidden" type="hidden">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--Errors for form page-->
                    <div class="ui error message"></div>
                    <div class="row">
                        <button class="ui right floated positive button submit-button">
                            Cotizar
                        </button>
                    </div>
                    <div class="row">&nbsp;</div>
                    <div class="row">&nbsp;</div>
                </div>
            </div>
            <!-- FINAL FIRST SECTION -->
            <!-- SECOND SECTION -->
            <div id="accordion-2"  class="disabled-content"><!-- disabled or enabled accordion -->
                <div class="title" id="title-2">
                    <i class="dropdown icon"></i>
                    Preview / Envío
                </div>
                <div class="content" id="content-2">
                    <div class="row margin-top-buttons">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton-2">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button submit-button-2">
                                Confirmar y Enviar
                            </button>
                        </div>
                        <div class="clear-floats">&nbsp;</div>
                        <!-- HEADER OF THE PREVIEW -->
                        <div class="column">
                            <div class="ui segment">
                                <div class="field bold_text_title">
                                    DATOS GENERALES
                                </div>
                                <div class="field inline">
                                    <label class="bold_text_title">Nombre Completo:</label>
                                    <label id="fullname_preview" class="light_text_desc"></label>
                                </div>
                                <div class="two fields left_align">
                                    <div class="field inline">
                                        <label class="bold_text_title">Correo Electrónico:</label>
                                        <label id="email_preview" class="light_text_desc"></label>
                                    </div>
                                    <div class="field inline">
                                        <label class="bold_text_title">Teléfono:</label>
                                        <label id="telefono_preview" class="light_text_desc"></label>
                                    </div>
                                </div>
                                <div class="two fields left_align">
                                    <div class="field inline">
                                        <label class="bold_text_title">Estado:</label>
                                        <label id="estado_preview" class="light_text_desc"></label>
                                    </div>
                                    <div class="field inline">
                                        <label class="bold_text_title">Cuidad:</label>
                                        <label id="ciudad_preview" class="light_text_desc"></label>
                                    </div>
                                </div>
                                <div class="two fields left_align">
                                    <div class="field inline">
                                        <label class="bold_text_title">Cobertura:</label>
                                        <label id="cobertura_preview" class="light_text_desc"></label>
                                    </div>
                                    <div class="field inline">
                                        <label class="bold_text_title">Emergencias Extranjero:</label>
                                        <label id="emergencias_extranjero_preview" class="light_text_desc"></label>
                                    </div>
                                </div>
                                <div class="clear-floats">&nbsp;</div>
                                <div class="field bold_text_title">
                                    INTEGRANTES CAPTURADOS
                                </div>
                                <div class="fields">
                                    <table class="ui striped celled table segment" id="integrantes_table_preview">
                                        <thead>
                                            <tr>
                                                <th class="center aligned">Integrante</th>
                                                <th class="center aligned">Nombre</th>
                                                <th class="center aligned">Sexo</th>
                                                <th class="center aligned">Edad</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!--This seccion is loaded automatically from js function fillIntegrantesTablePreview()-->
                                        </tbody>
                                    </table>
                                </div>
                                <div class="ui buttons right floated">
                                    <button class="ui right floated negative button backButton-2">
                                        Regresar
                                    </button>
                                    <div class="or" data-text="o"></div>
                                    <button class="ui right floated positive button submit-button-2">
                                        Confirmar y Enviar
                                    </button>
                                </div>
                                <div class="row">
                                    <button id="redMedicaButton" class="ui blue button">
                                        Ver Red Médica
                                    </button>
                                </div>
                                <br/>
                                <div class="redMedicaDivX" id="portapdf" style="display:none">
                                    <a id="pdfRedMedicaDownload" href="" download>
                                        Descargar PDF Red Médica
                                    </a>
                                    <object type="text/html"
                                            id="pdfRedMedica"
                                            style="border: none;"
                                            standby="loading"
                                            width="100%"
                                            height="100%">
                                    </object>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- FINAL SECOND SECTION -->
        </div>
    </form>
</div>
