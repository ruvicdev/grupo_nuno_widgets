<?php

/**
 * Class contains all the method used to
 * check the information of the user if
 * he wants a insurance against the data
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */

class ProteccionDatos extends MY_Controller {

	/**
	 * Constructor ....
	 */
	public function __construct() {
		parent::__construct();

		// Load the model
		$this->load->model(array('ProteccionDatos_model', 'carga/Ciudad_model'));

		$this->pdf   	 = new ExportFiles();
        $this->emailSend = new EmailSend();
        $this->template  = new Templates();
	}

	/**
	 * Method will contain all the information
	 * that the user is going to type to know
	 * what is the price for the data protection
	 * insurance
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function index() {
		$limit   		  = $this->__getLimitResponsability();
		$limit['estados'] = $this->__getStates();

		$content = $this->loadSingleView('index', $limit, TRUE);
		$data	 = $this->__setArray('content', $content);
		$data	 = $this->__setArray('title', 'Seguro de Proteccion de Datos');

		$this->loadTemplate($data);
	}

	/**
	 * Method will be used to save the information
	 * is going to typed by the user once finished the
	 * to fill in the form
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function save() {
		$json_array = json_decode(file_get_contents('php://input'), TRUE);
		$data 		= $this->__createArray($json_array);
		$id			= $this->ProteccionDatos_model->saveData($data);

		$template 	= $this->template->proteccionDatosTemplate($data, $id);
		$PDF		= $this->pdf->__initPDF($template, 'Seguro Proteccion de Datos', 'Seguro Proteccion de Datos');

		$name		= "seguroProteccionDatos_" . strtotime(date('d-m-Y H:i:s')) . ".pdf";

		// Create a File
		$PDF->Output(FCPATH . 'public/files/' . $name, 'F');
		$completePath = FCPATH . 'public/files/' . $name;
		$returnedPath = base_url() . 'public/files/' . $name;

		$this->emailSend->__initEmailDataProtection($json_array, $PDF, $completePath, "Seguro Proteccion de Datos");

		echo json_encode(
			array(
				'flag' => 1,
				'pathFile' => $returnedPath
			)
		);
	}

	/**
	 * Method will be used to get the parameters that
	 * is going to be used for fill another options
	 * regarding to the option selected by the user
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __getLimitResponsability() {
		$array = array(
			'1' => '$ 130, 000',
			'2' => '$ 260, 000',
			'3' => '$ 390, 000',
			'4' => '$ 520, 000'
		);

		$array2 = array(
			'1' => 'Entre $ 0 MXN a $ 1,300,000 MXN',
			'2' => 'Entre $ 1,300,001 MXN a $ 2,600,000 MXN',
			'3' => 'Entre $ 2,600,001 MXN a $ 3,900,000 MXN',
			'4' => 'Entre $ 3,900,001 MXN a $ 5,200,000 MXN'
		);

		$arrayFinal = array(
			'limit' => $array,
			'gain'  => $array2
		);

		return $arrayFinal;
	}

	/**
	 * Method get the data will say to the user
	 * how much should pay if purchases the insurance
	 * to protect the data
	 *
	 * @param int $id
	 * @param int $id2
	 * @return array $json
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function getFinalResults($id, $id2) {
		$response = array();

		if ($id == '1') {
			if ($id2 == '1') {
				$response['factu'] = '$ 1,950 MXN';
			} else if ($id2 == '2') {
				$response['factu'] = '$ 2,600 MXN';
			} else if ($id2 == '3') {
				$response['factu'] = '$ 3,140 MXN';
			} else {
				$response['factu'] = '$ 3,617 MXN';
			}

			$response['deducible'] = '$ 6,500 MXN';
		} else if ($id == '2') {
			if ($id2 == '1') {
				$response['factu'] = '$ 3,120 MXN';
			} else if ($id2 == '2') {
				$response['factu'] = '$ 4,144 MXN';
			} else if ($id2 == '3') {
				$response['factu'] = '$ 4,648 MXN';
			} else {
				$response['factu'] = '$ 5,392 MXN';
			}

			$response['deducible'] = '$ 13,000 MXN';
		} else if($id == '3') {
			if ($id2 == '1') {
				$response['factu'] = '$ 4,095 MXN';
			} else if ($id2 == '2') {
				$response['factu'] = '$ 5,363 MXN';
			} else if ($id2 == '3') {
				$response['factu'] = '$ 6,006 MXN';
			} else {
				$response['factu'] = '$ 6,689 MXN';
			}

			$response['deducible'] = '$ 19,500 MXN';
		} else {
			if ($id2 == '1') {
				$response['factu'] = '$ 4,875 MXN';
			} else if ($id2 == '2') {
				$response['factu'] = '$ 6,419 MXN';
			} else if ($id2 == '3') {
				$response['factu'] = '$ 7,150 MXN';
			} else {
				$response['factu'] = '$ 7,508 MXN';
			}

			$response['deducible'] = '$ 26,000 MXN';
		}

		echo json_encode(
			array(
				'flag' => 1,
				'data' => $response
			)
		);
	}

	/**
	 * Method will be used to create an array will be
	 * used to pass as parameter and will contain the
	 * data typed by the user
	 *
	 * @param array $data
	 * @return array $finalData
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	private function __createArray($data) {
		$finalData = array();
		$nombreVendedor = "";
		$nombre  		= "";
		$paterno 		= "";
		$materno		= "";
		$empresa		= "";
		$contacto		= "";

		if ($data['es_vendedor'] == 'Si') {
			$nombreVendedor = $data['name_seller'];
		}

		if ($data['tipo_persona'] == 0 || $data['tipo_persona'] == '0') {
			$nombre  = $data['nombres'];
			$paterno = $data['a_paterno'];
			$materno = $data['a_materno'];
		} else {
			$empresa  = $data['empresa'];
			$contacto = $data['nombre_contacto'];
		}

		$cut1 = explode('_', $data['limite']);
		$cut2 = explode('_', $data['facturacion']);

		$finalData = array(
			'nombre' 	  	  => ($nombre != "") ? $nombre : "",
			'paterno' 	  	  => ($paterno != "") ? $paterno : "",
			'materno' 	  	  => ($materno != "") ? $materno : "",
			'nombre_empresa'  => ($empresa != "") ? $empresa : "",
			'nombre_contacto' => ($contacto != "") ? $contacto : "",
			'telefono'	  	  => $data['telefono'],
			'email' 	  	  => $data['email'],
			'limite_resp' 	  => $cut1[0],
			'limite_id'		  => $cut1[1],
			'factu_total' 	  => $cut2[0],
			'factu_id'		  => $cut2[1],
			'deducible'	  	  => $data['deducible'],
			'tbd' 		 	  => $data['tbd'],
			'es_vendedor'	  => $data['es_vendedor'],
			'nombre_vendedor' => $nombreVendedor,
			'creacion'    	  => date('d-m-Y H:i:s'),
			'actualizacion'   => date('d-m-Y H:i:s'),
			'tipo_persona'	  => $data['tipo_persona'],
			'rfc'			  => $data['rfc'],
			'domicilio'		  => $data['domicilio'],
			'cp'			  => $data['cp'],
			'colonia'		  => $data['colonia'],
			'estado'		  => $data['estado']
		);

		return $finalData;
	}

	/**
	 * Method will be used to get all the
	 * mexican states to show them to the
	 * user is filling the form in to get
	 * a price for the insurance
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	public function __getStates() {
		$data = $this->Ciudad_model->getStateByCountry('MEX');
		return $data->result_array();
	}

	public function demo() {
		//echo "holas: " . $id;
        //$data = $this->Cotizaciones_model->getDataSpecificRecord($id);
        //$demo = $data->row_array();
        //var_dump($data->result_array());
        //$datas = array(
        //    'id' => $demo['id']
        //);
        //var_dump($datas);
        //die();
        //$data = $this->Pais_model->get_all_continents();
        //var_dump($data->result_array());
        //$dir __DIR__ . '../public';
        //$dir = dirname(dirname(__FILE__));
        //echo FCPATH . '\n';
        //echo $dir;
        //echo $dir . "\n";
        //$this->load->library('email');
        //$json_array = json_decode(file_get_contents('php://input'), true);
        $temp = $this->template->proteccionDatosTemplate(); //$data->result_array());
        //var_dump($temp);
        //ob_clean();
        $p = $this->pdf->__initPDF($temp, "Cotiza Carga", "Cotiza Carga"); //'<p>hola</p>');
        //var_dump($p);
        $p->Output("demo.pdf", 'I');
        //var_dump($p);
        //ob_clean();

        //$pdf->Output(FCPATH . 'public/files/example_006.pdf', 'F');

        //var_dump($pdfF);
        //$this->emailSend->__initEmail(array(), $pdf);
        //$this->email->demos(array(), $pdf);
        //$this->email->demos($pdf);
        //$this->email->from('r@ruvicdev.com', 'nombre');
        //$this->email->to('ruben.alonso21@gmail.com');
        //$this->email->bcc('email');

        //$this->email->subject('subject demo');
        //$this->email->message('template');

        //$data    = chunk_split($pdfFile);
        //$this->email->attach(FCPATH . 'public/files/example_006.pdf');

        //$this->email->send();
	}
}
