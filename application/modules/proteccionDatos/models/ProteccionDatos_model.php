<?php
/**
 * Model will be used to get all the information
 * is going to be registered all the records generated
 * by the user wants to know any price
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company
 */
class ProteccionDatos_model extends CI_Model {

    /**
     * Constructor .....
     */
    public function __construct() {
        parent::__construct();

        $this->table = "proteccion_datos";
    }

    /**
     * Method will be used to save the information
     * is going to be used then at the moment to
     * generate the reports is going to be sent
     * to user
     *
     * @param array $data
     * @return int id
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function saveData($data) {
        $this->db->insert($this->table, $data);

        return $this->db->insert_id();
    }
}
