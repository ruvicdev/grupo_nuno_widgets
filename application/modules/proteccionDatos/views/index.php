<script type="text/x-handlebars-template" id="errors_messages">
    <div class="row">
        <div class="ui negative hidden message">
            <i class="close icon"></i>
            <div class="header">
                Error
            </div>
            <p>
                Por favor, llene los campos requeridos
            </p>
            <p id="messageErrorMoney">
                {{data}}
            </p>
        </div>
    </div>
</script>

<div class="default-margin-top ui grid centered removePadding">
    <!-- SECTION FOR STEPS -->
    <div class="ui mini steps center">
        <div class="active step" id="step-1">
            <i class="info circle icon"></i>
            <div class="content">
                <div class="title">Datos Personales</div>
                <div class="description">Informacion Personal</div>
            </div>
        </div>
        <div class="disabled step" id="step-2">
            <i class="file text outline icon"></i>
            <div class="content">
                <div class="title">Preview / Cotizacion</div>
                <div class="description">Cotizacion - Protecci&oacute;n de Datos</div>
            </div>
        </div>
    </div>
</div>

<div class="ui grid centered">
    <!-- SECTION FOR FORMS -->
    <div class="row">
        <h1 class="ui header">
            Seguro de Proteccion de Datos
        </h1>
    </div>
    <div>&nbsp;</div>
    <form class="ui form">
        <div class="ui styled accordion">
            <!-- FIRST SECTION -->
            <div id="accordion-1" class="disabled-content1">
                <div class="title active" id="title-1">
                    <i class="dropdown icon"></i>
                    Datos Asegurado
                </div>
                <div class="content active" id="content-1">
                    <div class="row" id="error_first_section"></div>
                    <div>&nbsp;</div>
                    <div class="fields">
                        <div class="seven wide field">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="Si" id="vendedor"
                                       class="form-data-selected obj-send">
                                <label class="bold_text_title">
                                    ¿Eres vendedor de Grupo Nu&ntilde;o?
                                </label>
                            </div>
                        </div>
                        <div class="nine wide field">
                            <input type="text" name="nombre_vendedor" id="name_seller"
                                   class="" placeholder="Nombre del Vendedor">
                        </div>
                    </div>

                    <div class="fields">
                        <input type="hidden" id="tipo_persona">
                        <div class="five wide field">
                            <label class="left_align bold_text_title" for="tipo_persona">
                                Tipo de Persona:
                            </label>
                        </div>
                        <div class="five wide field required sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="fisica"
                                       data-class="hidden-segment-value"
                                       data-index="1"
                                       class="checkdata required tperson">
    							<label>
    								Persona Fisica
    							</label>
    						</div>
                        </div>
                        <div class="five wide field required sec-1">
                            <div class="ui checkbox">
                                <input type="checkbox" data-value="moral"
                                       data-class="hidden-segment-value"
                                       data-index="2"
                                       class="checkdata required tperson">
    							<label>
    								Persona Moral
    							</label>
    						</div>
                        </div>
                    </div>

                    <div class="ui segment hidden-segment-value" data-value="moral" id="moral">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Empresa:
                            </label>
                            <input type="text" name="empresa" id="empresa" class="required" placeholder="Empresa">
                        </div>
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre del Contacto:
                            </label>
                            <input type="text" name="nombre_contacto" id="nombre_contacto" class="" placeholder="Nombre del Contacto">
                        </div>
                    </div>
                    <div class="ui segment hidden-segment-value" data-value="fisica" id="fisica">
                        <div class="field">
                            <label class="left_align bold_text_title">
                                Nombre(s):
                            </label>
                            <input type="text" name="nombres" id="nombres" class="required" placeholder="Nombre(s)">
                        </div>
                        <div class="fields">
                            <div class="eight wide field">
                                <label class="left_align bold_text_title">
                                    Apellido Paterno:
                                </label>
                                <input type="text" name="a_paterno" id="a_paterno" class="required" placeholder="Apellido Paterno">
                            </div>
                            <div class="eight wide field">
                                <label class="left_align bold_text_title">
                                    Apellido Materno:
                                </label>
                                <input type="text" name="a_materno" id="a_materno" class="required" placeholder="Apellido Materno">
                            </div>
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            R.F.C.
                        </label>
                        <input type="text" name="rfc" id="rfc"
                               placeholder="RFC" class="required">
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Domicilio:
                            </label>
                            <input type="text" class="required" name="domicilio"
                                   placeholder="Domicilio" id="domicilio">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Codigo Postal:
                            </label>
                            <input type="text" class="required" name="cp"
                                   id="cp" placeholder="Codigo Postal">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Estado:
                            </label>
                            <div class="ui selection dropdown estado">
                                <input type="hidden" name="estado" id="estado" class="required">
                                <i class="dropdown icon"></i>
                                <div class="default text conveyance">Estado</div>
                                <div class="menu" id="estado_list">
                                    <?php $i = 0; ?>
                                    <?php foreach($estados as $estado): ?>
                                        <div class="item">
                                            <?php echo $estados[$i]['CiudadDistrito']; ?>
                                            <?php $i++; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Colonia:
                            </label>
                            <input type="text" class="required" name="colonia"
                                   placeholder="Colonia" id="colonia">
                        </div>
                    </div>
                    <div class="fields">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Telefono:
                            </label>
                            <input type="text" name="telefono" id="telefono" class="required" placeholder="Telefono">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Email:
                            </label>
                            <input type="email" name="email" id="email" class="required" placeholder="Email">
                        </div>
                    </div>
                    <div class="field">
                        <label class="left_align bold_text_title">
                            L&iacute;mite de Responsabilidad (MXN):
                        </label>
                        <div class="ui selection dropdown" data-url="" id="first_data_dropdown">
                            <input type="hidden" name="limite" class="required" value="0" id="limite">
                            <i class="dropdown icon"></i>
                            <div class="default text">L&iacute;mite de Responsabilidad</div>
                            <div class="menu">
                               <?php foreach($limit as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $value . '_' . $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                               <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="field disabled">
                        <label class="left_align bold_text_title">
                            Facturacion total / Ingresos brutos totales anuales:
                        </label>
                        <div class="ui selection dropdown" data-url="getFinalResults" id="second_data_dropdown">
                            <input type="hidden" name="facturacion" class="required" value="0" id="facturacion">
                            <i class="dropdown icon"></i>
                            <div class="default text factuTotal_text">Facturacion total</div>
                            <div class="menu">
                               <?php foreach($gain as $key => $value): ?>
                                    <div class="item" data-value="<?php echo $value . '_' . $key; ?>">
                                        <?php echo $value; ?>
                                    </div>
                               <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                    <div class="fields deshabilitar_campo" id="campos_dinamicos">
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                Deducible MXN:
                            </label>
                            <label class="light_text_desc left_align" id="deducible_label"></label>
                            <input type="hidden" name="deducible" id="deducible">
                        </div>
                        <div class="eight wide field">
                            <label class="left_align bold_text_title">
                                TBD:
                            </label>
                            <label class="light_text_desc left_align" id="tbd_label"></label>
                            <input type="hidden" name="tbd" id="tbd">
                        </div>
                    </div>
                    <div class="row">
                        <button class="ui right floated positive button submit-button">
                            Siguiente
                        </button>
                    </div>
                    <div>&nbsp;</div>
                </div>
            </div>
            <div id="accordion-2" class="disabled-content">
                <div class="title" id="title-2">
                    <i class="dropdown icon"></i>
                    Preview / Cotizaci&oacute;n
                </div>
                <div class="content" id="content-2">
                    <div class="row margin-top-buttons">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton">
                                Confirmar y Enviar
                            </button>
                        </div>
                        <div class="clear-floats">&nbsp;</div>
                        <!-- HEADER OF THE PREVIEW -->
                        <div class="column"><!-- SECTION GENERAL INFORMATION -->
                            <div class="ui segment">
                                <div class="field bold_text_title" style="float: left;">
                                    Vendedor Grupo Nu&ntilde;o
                                </div>
                                <div style="clear: both;"></div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Eres Vendedor?</label>
                                    <label class="light_text_desc" id="eres_vendedor"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Nombre del Vendedor:</label>
                                    <label class="light_text_desc" id="nombre_vendedor"></label>
                                </div>
                            </div>
                        </div>
                        <div class="column margin-top-segment">
                            <div class="ui segment">
                                <div class="field bold_text_title" style="float: left;">
                                    DATOS GENERALES
                                </div>
                                <div style="clear: both"></div>
                                <div class="field left_align inline fisica_temp">
                                    <label class="bold_text_title">Nombre(s):</label>
                                    <label class="light_text_desc" id="nombre_txt"></label>
                                </div>
                                <div class="field left_align inline fisica_temp">
                                    <label class="bold_text_title">Apellido Paterno:</label>
                                    <label class="light_text_desc" id="ap_txt"></label>
                                </div>
                                <div class="field left_align inline fisica_temp">
                                    <label class="bold_text_title">Apellido Materno:</label>
                                    <label class="light_text_desc" id="am_txt"></label>
                                </div>
                                <div class="field left_align inline moral_temp">
                                    <label class="bold_text_title">Empresa:</label>
                                    <label class="light_text_desc" id="empresa_txt"></label>
                                </div>
                                <div class="field left_align inline moral_temp">
                                    <label class="bold_text_title">Nombre del Contacto:</label>
                                    <label class="light_text_desc" id="nombre_contacto_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">RFC:</label>
                                    <label class="light_text_desc" id="rfc_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Domicilio:</label>
                                    <label class="light_text_desc" id="domicilio_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Codigo Postal:</label>
                                    <label class="light_text_desc" id="cp_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Estado:</label>
                                    <label class="light_text_desc" id="estado_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Colonia:</label>
                                    <label class="light_text_desc" id="colonia_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Telefono:</label>
                                    <label class="light_text_desc" id="tel_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Email:</label>
                                    <label class="light_text_desc" id="email_txt"></label>
                                </div>
                            </div>
                        </div>
                        <div class="column margin-top-segment"><!-- SECTION PAYMENTS INFORMATION -->
                            <div class="ui segment">
                                <div class="field bold_text_title" style="float: left;">
                                    DATOS DEL SEGURO
                                </div>
                                <div style="clear: both;"></div>
                                <div class="left_align field inline">
                                    <label class="bold_text_title">Limite de Responsabilidad:</label>
                                    <label class="light_text_desc" id="limite_resp_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Facturacion total / Ingresos brutos totales anuales:</label>
                                    <label class="light_text_desc" id="facturacion_total_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">Deducible MXN:</label>
                                    <label class="light_text_desc" id="deducible_txt"></label>
                                </div>
                                <div class="field left_align inline">
                                    <label class="bold_text_title">TBD:</label>
                                    <label class="light_text_desc" id="tbd_txt"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row margin-top-buttons">
                        <div class="ui buttons right floated">
                            <button class="ui right floated negative button backButton">
                                Regresar
                            </button>
                            <div class="or" data-text="o"></div>
                            <button class="ui right floated positive button nextButton">
                                Confirmar y Enviar
                            </button>
                        </div>
                        <div class="clear-floats">&nbsp;</div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/utilElements.js'; ?>"></script>
<script type="text/javascript"
    src="<?php echo base_url() . 'public/js/classes/mathElements.js'; ?>"></script>
<script type="text/javascript"
        src="<?php echo base_url() . 'public/js/custom/proteccionDatos.js'; ?>"></script>
