<?php
/**
 * Clase that retrive data from excel
 * @author ruvicdev.com
 */

 require_once(FCPATH.'application/libraries/excelPHP/PHPExcel/IOFactory.php');

 class GetDataFromExcelFile{
     protected  $excelFileName;

     public function __construct($excelFileName = '') {
         $this->excelFileName = $excelFileName;
     }

     /**
     * Function that return timestamp date modified of the excel file
     * @return timestamp
     *
     */
     public function getFileModified() {
         $inputFileName     = FCPATH . 'public/files/excels/'.$this->excelFileName;
         $objPHPExcel       = PHPExcel_IOFactory::load($inputFileName);
         $modifiedDatestamp = $objPHPExcel->getProperties()->getModified();

         return $modifiedDatestamp;
     }

    /**
    * Function that return a value of each cell
    * @param $array_coordinates array that contain the coordinates of the excel files required
    *
    * pattern:
    * array('sheet_name' => 'variable_name:coordinate, variable_name:coordinate');
    *
    * e.g.
    * Get from cotizacones sheet the value of B-15 and save it in factor_edad variable
    * Get from cotizacones sheet the value of H-58 and save it in factor_tiempo variable
    * For coordinates: A = 0, B = 1, C = 3, D = 4 and go on...
    * i.e.
    * array('hoja_1' => 'nombre_variable_1:1-15,nombre_variable_2:7-58',
    *       'hoja_2' => 'nombre_variable_3:0-1,nombre_variable_4:0-2');
    */
    public function get_value_worksheet($array_coordinates) {
        $inputFileName = FCPATH . 'public/files/excels/'.$this->excelFileName;
        $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
        //Array that contain the variables name and its value, as a associative array
        $responseArray = array();

        foreach ($array_coordinates as $key => $value) {
            foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
                //Variable $key is the name of the sheet in the excel file
                if($worksheet->getTitle() == $key) {
                    //Variable $value is the coordinates that we need the value, they are separeted by ','
                    $valuePerRegister = explode(",", $value);
                    //Iterate by $valuePerRegister in order to get the variable name and coordinates
                    for ($i=0; $i < sizeOf($valuePerRegister); $i++) {
                        $variable_name = explode(":", $valuePerRegister[$i])[0];
                        $coordinates   = explode(":", $valuePerRegister[$i])[1];
                        $col           = explode("-", $coordinates)[0];
                        $row           = explode("-", $coordinates)[1];
                        $cell          = $worksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();//getFormattedValue();

                        //Save variables name and its value into a responseArray
                        $responseArray[trim($variable_name)] = trim($cell);
                    }
                }
            }
        }

        return $responseArray;
    }

    /**
    * Function that return an array of a range givenl
    * @param $rangeArrayString string that contain the range of the values that you want retrieve form excel file
    * @param $sheetName string that contain the name of the sheet of the excel file
    *
    * pattern:
    * 'F3:H103', 'NAME_SHEET'
    *
    */
    public function get_range_worksheet($rangeArray, $sheetName) {
       $inputFileName = FCPATH . 'public/files/excels/'.$this->excelFileName;
       $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);

       foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
          // echo 'WorkSheet' . $CurrentWorkSheetIndex++ . "\n";
          if($worksheet->getTitle() == $sheetName) {
            // Ref: http://hitautodestruct.github.io/PHPExcelAPIDocs/classes/PHPExcel_Worksheet.html#method_rangeToArray
              $rowData = $worksheet->rangeToArray($rangeArray, '', TRUE, TRUE, FALSE);
              return $rowData;
          }
      }
   }
 }
