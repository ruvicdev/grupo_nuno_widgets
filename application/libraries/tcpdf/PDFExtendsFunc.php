<?php

/**
 * Class that contains all the information to
 * set
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class PDFExtendsFunc extends TCPDF {

    // Generating variablesto check the images will be set in the header and footer
    public $headerImg = "";
    public $footerImg = "";

    /**
     * Method used to set the values
     * of the images for the header and
     * the footer
     *
     * @param string $header
     * @param string $footer
     * @return void
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function __set($header, $footer) {
        $this->headerImg = $header;
        $this->footerImg = $footer;
    }

    /**
     * Adding this method to set the information of the
     * header will be added to the PDF file will be
     * generated once the user finished the process
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function Header() {
        $html = '<img src="public/img/' . $this->headerImg . '"/>'; //logoGrupoNuno.png

        $this->writeHTML($html);
    }

    /**
     * Adding this method to set the information of the footer
     * will be added to the PDF file and is going to generate
     * once the user finished the process
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    public function Footer() {
        $image_file = FCPATH . 'public/img/' . $this->footerImg; //FooterClubSaludPdf.png
        $this->Image($image_file, 11, 249, 189, '', 'PNG', '', 'T', false, 300, '', false, false, false, false, false, false);
    }
}
