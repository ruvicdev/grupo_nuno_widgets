<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
        <meta name="viewport" content="width=device-width, init-scale=1.0, maximum-scale=1.0" />
        <title><?php echo $title; ?></title>
        <?php echo link_tag('public/img/favicon.ico', 'shortcut icon', 'image/ico'); ?>
        <?php echo link_tag('public/css/libs/semantic.min.css'); ?>
        <?php echo link_tag('public/css/custom/custom.css'); ?>
        <?php echo link_tag('public/css/custom/custom-pm.css'); ?>
        <?php echo link_tag('public/css/libs/jquery.datepick.css'); ?>

        <script type="text/javascript"
              src="<?php echo base_url(). 'public/js/libs/jquery-3.2.0.min.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/semantic.min.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/handlebars-v4.0.5.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/base.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/request.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/jquery.numeric.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/accounting.min.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/moment.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/jquery.plugin.js'; ?>"></script>
        <script type="text/javascript"
              src="<?php echo base_url() . 'public/js/libs/jquery.datepick.js'; ?>"></script>
        <script>window.$PATH = "<?php echo base_url(); ?>";</script>
    </head>
    <body>
        <!-- SECTION OVERLAY ** START ** -->
        <div class="ui page dimmer">
            <i class="spinner loading icon"></i>
            <div class="ui text large loader">
                Cargando...... Por favor espere
            </div>
        </div>
        <!-- SECTION OVERLAY ** END ** -->

        <!-- MODAL OPTION PRINT FILE **START** -->
        <div id="demos" class="ui basic modal">
            <div class="ui icon header">
                <i class="file pdf outline icon"></i>
            </div>
            <div class="content">
                <p>
                    Deseas imprimir la cotización que se realizó?
                </p>
            </div>
            <div class="actions">
                <div class="ui red basic cancel inverted button cancelPrint">
                    <i class="remove icon"></i>
                    Cancelar
                </div>
                <div class="ui green ok inverted button print">
                    <i class="checkmark icon"></i>
                    Imprimir
                </div>
            </div>
        </div>

        <!-- MODAL OPTION PRINT FILE **END** -->
        <!-- SECTION DATA -->
        <div id="contenido">
        <section id="header">
            <div class="ui grid centered column">
                <?php echo $header; ?>
            </div>
        </section>
        <!-- SECTION CONTENT -->
        <section id="content">
            <div class="ui container">
                <div class="ui column">
                    <div>
                        <?php echo $content; ?>
                    </div>
                </div>
            </div>
        </section>
        <!-- SECTION FOOTER -->
        <section id="footer" class="footer">
            <div class="ui grid">
                <div class="ui column">
                    <?php echo $footer; ?>
                </div>
            </div>
        </section>
    </div>
    </body>
</html>
