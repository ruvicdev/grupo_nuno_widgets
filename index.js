/**
 * Functions and methods will be used to
 * create the functionality of the widget
 * is going to be added to any page
 *
 * @version 1.0
 * @author ruvicdev <Ruben Alonso Cortes Mendoza & Victor Manuel Garza Chequer>
 * @company ruvicdev
 */
$(document).ready(function () {
    // FIND THE POSITION AND GET THE NEXT POSITION FOR THE IMAGE
    var pathToImgs   = window.location.origin + "/public/img/";
    var $imgPosition = 0;
    var last_id      = "";

    $('div.widget_ruvicdev > img').each(function (i, v) {
    	if (i == 0) {
    		$imgPosition = $(this).outerHeight(true);
    	} else {
    		$imgPosition += 10;

    		$(this).parent().parent().css("cssText", "margin-top: " + $imgPosition);
    	}
    });

    // Change the IMG to black instead of gray color
    $(document).on({
        mouseenter: function () {
            var imgName      = $(this).data('black');
            var completeName = pathToImgs + imgName;

            $(this).attr('src', completeName)
        },
        mouseleave: function () {
            var imgName      = $(this).data('gray');
            var completeName = pathToImgs + imgName;

            $(this).attr('src', completeName);
        }
    }, ".img:not(.no-hover)");

    $(".widget_ruvicdev").click(function (evt) {
        var id   = $(evt.currentTarget).attr('id');
        last_id  = id;
        var flag = $("#" + id + "widget").is(":visible");

        // Hide section deselected
        $(".widget_ruvicdev").each(function (i, v) {
            $("#template" + (i+1) + "widget").hide();

            // Validation to remove the blue color from the image and change to gray color image
            if ($("#template" + (i+1) + " > img").hasClass('no-hover')) {
                var nameImg = $("#template" + (i+1) + " > img").removeClass('no-hover')
                                                               .data('gray');
                $("#template" + (i+1) + " > img").attr('src', pathToImgs + nameImg);
            }
        });

        $("#" + id + " > img").addClass('no-hover');

        // Show the widget selected
        $("#" + id + "widget").show();

        // Hide all the sections and close the active widget
        if (last_id == id && flag == true) {
            var dataName = $("#" + id + " > img").data('gray');

            $("#" + id + "widget").hide();
            $("#" + id + " > img").attr('src', pathToImgs + dataName).removeClass('no-hover');
        } else {
            var dataName = $("#" + id + " > img").data('blue');

            $("#" + id + " > img").attr('src', pathToImgs + dataName);
        }
    });
});


window.onload = function () {
    var pathToImgs   = window.location.origin + "/public/img/";

	for (var i = 1; i <= $(".widget_ruvicdev").length; i++) {
		var id  = "#template" + i;
		var url = window.location.origin + $(id).data('url');

        //Add source to iframe template
		$("#template" + i + "widget").attr('src', url);

        //Add source to div image
        var defaultImgSrc = pathToImgs + $("#template" + i + " img").attr('data-gray');
        $("#template" + i + " img").attr('src', defaultImgSrc);
	}
}
