/**
 * Functions and methods will be used to
 * create the functionality of the widget
 * is going to be added to any page
 *
 * @version 1.0
 * @author ruvicdev <Ruben Alonso Cortes Mendoza & Victor Manuel Garza Chequer>
 * @company ruvicdev
 */
var j = jQuery.noConflict();

j(document).ready(function () {
    // FIND THE POSITION AND GET THE NEXT POSITION FOR THE IMAGE
    var pathToImgs   = window.location.origin + "/widgets/public/img/";
    var $imgPosition = 0;
    var last_id      = "";

    j('div.widget_ruvicdev > img').each(function (i, v) {
    	if (i == 0) {
    		$imgPosition = j(this).outerHeight(true);
    	} else {
    		$imgPosition += 10;

    		j(this).parent().parent().css("cssText", "margin-top: " + $imgPosition);
    	}
    });

    // Change the IMG to black instead of gray color
    j(document).on({
        mouseenter: function () {
            var imgName      = j(this).data('black');
            var completeName = pathToImgs + imgName;

            j(this).attr('src', completeName)
        },
        mouseleave: function () {
            var imgName      = j(this).data('gray');
            var completeName = pathToImgs + imgName;

            j(this).attr('src', completeName);
        }
    }, ".img:not(.no-hover)");

    j(".widget_ruvicdev").click(function (evt) {
        var id   = j(evt.currentTarget).attr('id');
        last_id  = id;
        var flag = j("#" + id + "widget").is(":visible");

        // Hide section deselected
        j(".widget_ruvicdev").each(function (i, v) {
            j("#template" + (i+1) + "widget").hide();

            // Validation to remove the blue color from the image and change to gray color image
            if (j("#template" + (i+1) + " > img").hasClass('no-hover')) {
                var nameImg = j("#template" + (i+1) + " > img").removeClass('no-hover')
                                                               .data('gray');
                j("#template" + (i+1) + " > img").attr('src', pathToImgs + nameImg);
            }
        });

        j("#" + id + " > img").addClass('no-hover');

        // Show the widget selected
        j("#" + id + "widget").show();

        // Hide all the sections and close the active widget
        if (last_id == id && flag == true) {
            var dataName = j("#" + id + " > img").data('gray');

            j("#" + id + "widget").hide();
            j("#" + id + " > img").attr('src', pathToImgs + dataName).removeClass('no-hover');
        } else {
            var dataName = j("#" + id + " > img").data('blue');

            j("#" + id + " > img").attr('src', pathToImgs + dataName);
        }
    });
});

window.onload = function () {
    var pathToImgs   = window.location.origin + "/widgets/public/img/";

	for (var i = 1; i <= j(".widget_ruvicdev").length; i++) {
		var id  = "#template" + i;
		var url = window.location.origin + j(id).data('url');

        //Add source to iframe template
		j("#template" + i + "widget").attr('src', url);

        //Add source to div image
        var defaultImgSrc = pathToImgs + j("#template" + i + " img").attr('data-gray');
        j("#template" + i + " img").attr('src', defaultImgSrc);
	}
}
