/**
 * Class will be used as main for load all the
 * action of the methods will be used in all
 * the systems
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class BaseClass {

    // Construtor
    constructor() {
    }

    // Init Dropdown
    initDropdown() {
        $('.ui.dropdown').dropdown({allowReselection: true});
    }

    // Init Accordion
    // flag to know if we need to deactivate the click event in the accordion
    initAccordion($flag = false) {
        $('.ui.accordion').accordion();

        if ($flag === true) {
            $(".title").css("cursor", "default");
            $(".title").click(function () {
                return false;
            });
        }
    }

    // Init Radio Checkbox
    initRadioCheckbox() {
        $('.ui.radio.checkbox').checkbox();
    }

    // Validate Display Information
    showInformationData (dataSelected, classSelected) {
        var data = "";

        $(classSelected).each(function (key, value) {
            var dataValue = $(this).data('value');

            if (dataValue == dataSelected) {
                data = dataValue;
            }
        });

        return data;
    }

    // create an object
    createObject() {
        var obj = {};

        return obj;
    }

    // create the template
    setTemplate($element, $id, $data) {
        var source = Handlebars.compile($element);
        $id.html(source($data));
    }
}
