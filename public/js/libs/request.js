/**
 * Class will be used to create all the
 * request enabled in REST API. With this
 * we can communicate with the server to
 * send data
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class Request {

    constructor() {
        var prefix = "index.php";

        if ("string" === typeof window.$PATH) {
            this.stringUrl = window.$PATH + prefix;
        }
    }

    // generate URL
    generateUrl($url) {
        if ("string" !== typeof $url) {
            return NaN;
        }

        if (/^\//g.test($url)) {
            return this.stringUrl + $url;
        } else {
            return this.stringUrl + "/" + $url;
        }
    }

    // get api calls
    getApi($url) {
        if ("string" !== typeof $url) {
            return ;
        }

        this.showDimmer();

        return $.ajax({
            type     : "GET",
            url      : this.generateUrl($url),
            dataType : "json",
            header   : {
                "Accept"       : "application/json",
                "Content-Type" : "application/json"
            }
        }).always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass('active') }, 2500);
        });
    }

    // post api calls
    postApi($url, $obj) {
        /*if (("string" !== typeof $url) || ("object" !== typeof $obj)) {
            console.log("entro");
            return ;
        }*/
        this.showDimmer();

        return $.ajax({
            type     : "POST",
            url      : this.generateUrl($url),
            data     : JSON.stringify($obj),
            dataType : "json",
            headers  : {
                "Accept"       : "application/json",
                "Content-Type" : "application/json"
            }
        }).always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass("active")}, 2500);
        });
    }

    // put api calls
    putApi($url, $obj) {
        if (("string" !== typeof $url) || ("object" !== typeof $obj)) {
            return ;
        }

        this.showDimmer();

        return $.ajax({
            type     : "POST",
            url      : this.generateUrl($url),
            data     : JSON.stringify($obj),
            dataType : "json",
            headers  : {
                "Accept"       : "application/json",
                "Content-Type" : "application/json"
            }
        }).always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass('active') }, 2500);
        });
    }

    // delete api calls
    deleteApi($url) {
        if ("string" !== typeof $url) {
            return ;
        }

        this.showDimmer();

        return $.ajax({
            type     : "POST",
            url      : this.generateUrl($url),
            dataType : "json",
            headers  : {
                "Accept"       : "application/json",
                "Content-Type" : "application/json"
            }
        }).always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass('active') }, 2500);
        });
    }

    // show dimmer
    showDimmer() {
        // Adding these validation in case the systems wants to
        // be loaded inside div instead of iframe element

        //if ($("#widget").length) {
        //    $(".ui.page.dimmer").css("cssText", "position: sticky !important;").addClass("active");
        //} else {
        $(".ui.page.dimmer").addClass("active");
        //}
    }
}
