/**
 * Functions and methods will be used to
 * create the functionality of the modal
  *
 * @version 1.0
 * @author ruvicdev <Ruben Alonso Cortes Mendoza & Victor Manuel Garza Chequer>
 * @company ruvicdev
 */
var j = jQuery.noConflict();

j(document).ready(function () {
    var pathToImgs   = window.location.origin + "/widgets/public/img/";

    j(function() {
        j( "#divmodal" ).dialog({
            autoOpen: false,
            width: 830,
            height: 700,
            resizable: false,
            draggable: false,
            dialogClass: "dialog_ruvicdev",
    	    closeOnEscape: true,
            modal: true,
            show: {
                effect: "fade",
                duration: 1000
            },
            hide: {
                effect: "fade",
                duration: 1000
            },
            close: function () {
                j("#iframeid").attr('src', "about:blank");
            }
        });

    	j( ".widget_ruvicdev" ).click(function() {
            j("#iframeid").attr('src', j(this).data('url'));
            j( "#divmodal" ).focus();
            j( "#divmodal" ).dialog( "open" );
        });

        j('body').on('click', '.ui-widget-overlay', function(e){
            j('#divmodal').dialog('close');
        });
    });


    // j(".widget_ruvicdev").click(function () {
    //     j("#iframeid").attr('src', j(this).data('url'));
    //     j( "#divmodal" ).focus();
    //
    //     j("#divmodal").dialog({
    //         width: 830,
    //         height: 700,
    //         modal: true,
	// 		resizable: false,
    //     	draggable: false,
    //         dialogClass: "dialog_ruvicdev",
    //         closeOnEscape: true,
    //         show: "fade",
    //         hide: "fade",
    //         close: function () {
    //             //j("#iframeid").attr('src', "about:blank");
    //         }
    //     });
    //
    //     return false;
    // });

    j('body').on('click', '.ui-widget-overlay', function(e){
      j('#divmodal').dialog('close');
    });

    j(document).on({
        mouseenter: function () {
            var imgName      = j(this).data('black');
            var completeName = pathToImgs + imgName;

            j(this).attr('src', completeName)
        },
        mouseleave: function () {
            var imgName      = j(this).data('blue');
            var completeName = pathToImgs + imgName;

            j(this).attr('src', completeName);
        }
    }, ".img:not(.no-hover)");
});

window.onload = function () {
    var pathToImgs   = window.location.origin + "/widgets/public/img/";

    var defaultImgSrcCarga = pathToImgs + j("#divcarga img").attr('data-blue');
    j("#divcarga img").attr('src', defaultImgSrcCarga);

    var defaultImgSrcSalud = pathToImgs + j("#divsalud img").attr('data-blue');
    j("#divsalud img").attr('src', defaultImgSrcSalud);
}
