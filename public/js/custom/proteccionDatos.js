/**
 * Class will contain all the information about
 * all the events and actions the user can do
 * with the functionality especified here in this
 * class
 *
 * @author Ruben Alonso Cortes Mendoza
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
 	// Create an object
	var utils = new UtilElements();
	var req	  = new Request();
	var obj   = "";

	// Hide employee name's field and unchecking checkbox
    $("#vendedor").prop('checked', false);
    $("#name_seller").hide();
    $(".print").addClass("seguro-responsabilidad-pdf");
    $("#telefono").numeric();
	$("#cp").numeric();
	$("#rfc").css('text-transform', 'uppercase');

	/**
	 * Event will be used to activate the
	 * functionality for the checkbox is going to
	 * active the hide or show the section
	 * of the company or normal user
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	$('input:checkbox.tperson').click(function() {
		utils.checkboxAsRadioButton(this, ".tperson");
	});

	/**
     * Check if the field used for type the
     * seller's name is enabled or disabled,
     * in case the value is disabled, should
     * add the class to validate if the value is empty
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#vendedor").change(function () {
    	if ($("#" + this.id).prop('checked')) {
    		$("#name_seller").addClass('required').show();
    	} else {
    		$("#name_seller").removeClass('required').val('').hide();
    	}
    });

	/**
	 * Event will fire the inputs depending the selection done
	 * by the user in the checkbox, the user is able to select
	 * if is a company that is requesting the insurance or is
	 * a person
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
    $(".checkdata").change(function () {
    	var value = $(this).data('value');

    	if (value == 'fisica') {
    		 $("#fisica").removeClass('hidden-segment-value');
    		 $("#moral").addClass('hidden-segment-value');
    		 $("#tipo_persona").val(value);

    		 // clean fields
    		 $("#empresa").val('').addClass('ignore');
             $("#nombre_contacto").val('').addClass('ignore');
    		 $("#nombres").removeClass('ignore');
    		 $("#a_paterno").removeClass('ignore');
    		 $("#a_materno").removeClass('ignore');
    	 } else {
    		 $("#moral").removeClass('hidden-segment-value');
    		 $("#fisica").addClass('hidden-segment-value');
    		 $("#tipo_persona").val(value);

    		 // clean the fields
    		 $("#nombres").val('').addClass('ignore');
    		 $("#a_paterno").val('').addClass('ignore');
    		 $("#a_materno").val('').addClass('ignore');
    		 $("#empresa").removeClass('ignore');
             $("#nombre_contacto").removeClass('ignore');
    	 }
     });

    /**
     * Event used to know if disabled the dropdown or call
     * made a request to get the another data will be displayed
     * to the user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.selection.dropdown").change(function () {
    	var $this = $(this);
    	var url   = $this.data('url');

    	if (url == 'getFinalResults') {
    		var $id  = $("#limite").val().split('_');
    		var $id2 = $("#facturacion").val().split('_');

    		req.getApi('/proteccionDatos/' + url + '/' + $id[1] + '/' + $id2[1]).done(function (response) {
    			if (response.flag == 1 || response.flag == '1') {
    				$("#deducible_label").text(response.data.deducible);
    				$("#deducible").val(response.data.deducible);

    				$("#tbd_label").text(response.data.factu);
    				$("#tbd").val(response.data.factu);

    				$("#campos_dinamicos").removeClass("deshabilitar_campo");
    			}
    		});
    	} else {
    		$("#second_data_dropdown").parent().removeClass('disabled');
			$(".ui.dropdown.limite").dropdown('set selected', 'Facturacion total');
			$(".factuTotal_text").text('Facturacion total');
			$("#campos_dinamicos").addClass('deshabilitar_campo');
    	}
    });

    /**
     * Event will be used to pass to preview section
     * where the user could see all the information
     * is going to be displayed once he filled in the
     * data required to create a cotization
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.button.submit-button").click(function (evt) {
    	evt.preventDefault();

    	var $this  	 	 = $(this);
    	var $count 		 = 0;
    	var $source 	 = $("#errors_messages").html();
    	var $destination = $("#error_first_section");
    	var $data  		 = "";

    	$(".required").each(function (i, value) {
    		if (value.tagName == 'INPUT') {

    			if (value.type == 'text') {
					var inputValue = $(this).hasClass('ignore');

					if (inputValue == false) {
	    				if (value.value == '') {
	    					utils.addRemoveClass($(this).parent(), 'add');
	    					$count++;
	    				} else {
	    					utils.addRemoveClass($(this).parent(), 'remove');
	    					obj = utils.createObjectForm(value.id, value.value);
	    				}
					}
    			}

    			if (value.type == 'hidden') {
    				if (value.value == 0 || value.value == '0') {
    					utils.addRemoveClass($(this).parent().parent(), 'add');
    					$count++;
    				} else {
    					utils.addRemoveClass($(this).parent().parent(), 'remove');
    					obj = utils.createObjectForm(value.id, utils.ucwords(value.value));
    				}
    			}

    			if (value.type == 'email') {
    				var emailField = utils.validateEmailData(value.value);

    				if (value.value == '') {
    					utils.addRemoveClass($(this).parent(), 'add');
    					$count++;
    				} else if (emailField == false) {
    					utils.addRemoveClass($(this).parent(), 'add');
    					$count++;
    				} else {
    					utils.addRemoveClass($(this).parent(), 'remove');
    					obj = utils.createObjectForm(value.id, value.value);
    				}
    			}
    		}

    		obj = utils.createObjectForm('deducible', $("#deducible").val());
    		obj = utils.createObjectForm('tbd', $("#tbd").val());
    	});

    	if ($count != 0) {
    		utils.setTemplate($source, $destination, { data: $data });
    		utils.removeClassHiddenMessage();
    	} else {
    		utils.addClassHiddenMessage();

			// Check if the user is seller or not
			if ($("#vendedor").prop('checked')) {
				$("#eres_vendedor").text('Si');
				$("#nombre_vendedor").text(obj.name_seller);

				obj = utils.createObjectForm('es_vendedor', 'Si');
			} else {
				$("#eres_vendedor").text('No');
				$("#nombre_vendedor").text('');

				obj = utils.createObjectForm('es_vendedor', 'No');
			}

			// check if the user is company or is person
			if ($("#tipo_persona").val() == 'fisica') {
				obj = utils.createObjectForm("tipo_persona", 0);

				// add the data to the preview
				$("#nombre_txt").text(obj.nombres);
	    		$("#ap_txt").text(obj.a_paterno);
	    		$("#am_txt").text(obj.a_materno);

				// add/remove hidden class
				$("#nombre_txt").parent().removeClass('fisica_temp');
				$("#ap_txt").parent().removeClass('fisica_temp');
				$("#am_txt").parent().removeClass('fisica_temp');
				$("#empresa_txt").parent().addClass('moral_temp');
                $("#nombre_contacto_txt").parent().addClass('moral_temp');
			} else {
				obj = utils.createObjectForm('tipo_persona', 1);
                obj = utils.createObjectForm('nombre_contacto', $("#nombre_contacto").val());

				// add the data to the preview
				$("#empresa_txt").text(obj.empresa);
                $("#nombre_contacto_txt").text(obj.nombre_contacto);

				// add/remove hidden class
				$("#nombre_txt").parent().addClass('fisica_temp');
				$("#ap_txt").parent().addClass('fisica_temp');
				$("#am_txt").parent().addClass('fisica_temp');
				$("#empresa_txt").parent().removeClass('moral_temp');
                $("#nombre_contacto_txt").parent().removeClass('moral_temp');
			}

			$("#rfc_txt").text(obj.rfc);
			$("#domicilio_txt").text(obj.domicilio);
			$("#cp_txt").text(obj.cp);
			$("#estado_txt").text(obj.estado);
			$("#colonia_txt").text(obj.colonia);
    		$("#tel_txt").text(obj.telefono);
    		$("#email_txt").text(obj.email);

    		// split values
    		var limitResponsability = obj.limite.split('_');
    		var totalEarnings		= obj.facturacion.split('_');

    		$("#limite_resp_txt").text(limitResponsability[0] + ' MXN');
    		$("#facturacion_total_txt").text(totalEarnings[0]);
    		$("#deducible_txt").text(obj.deducible);
    		$("#tbd_txt").text(obj.tbd);

    		// Hide this section and add the resume section
            $("#step-1").removeClass("active").addClass("completed");
            $("#accordion-1").addClass("disabled-content");
            $("#title-1").removeClass("active");
            $("#content-1").removeClass("active");

            $("#step-2").removeClass("disabled").addClass("active");
            $("#accordion-2").removeClass("disabled-content");
            $("#title-2").addClass("active");
            $("#content-2").addClass("active");
    	}

    });

    /**
     * Event will be used to return to the before section
     * where the user defines all the personal information
     * to check what is the plan wants to purchase
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.button.backButton").click(function (evt) {
    	evt.preventDefault();

    	// Hide this section and add the resume section
    	$("#step-1").removeClass("disabled").addClass("active");
        $("#accordion-1").removeClass("disabled-content");
        $("#title-1").addClass("active");
        $("#content-1").addClass("active");

        $("#step-2").removeClass("active").addClass("completed");
        $("#accordion-2").addClass("disabled-content");
        $("#title-2").removeClass("active");
        $("#content-2").removeClass("active");
    });

	/**
	 * Event will fire once the user click
	 * submit button to send data to backend
	 *
	 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
	 * @version 1.0
	 * @company ruvicdev
	 */
	$(".ui.button.nextButton").click(function (evt) {
		evt.preventDefault();

		var url = "/proteccionDatos/save";
		req.postApi(url, obj).done(function (response) {
			if (response.flag == 1 || response.flag == '1') {
				$(".print.seguro-responsabilidad-pdf").attr('id', response.pathFile);
				setTimeout(function () {
					utils.displayModal();
				}), 2500;
			}
		});
	});

	/**
     * Event will be used to print the information
     * in the PDF file in the browser although the
     * the file was sent via Email
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".print.seguro-responsabilidad-pdf").click(function (evt) {
        // Sending the request to the url to displaying the PDF file in the browser
        var url = $(this).attr('id');

        // Send PDF to print
        window.open(url, "_blank").focus();

        // Reload the page
        document.location.reload(true);
    });

	/**
     * Event will reload all the information once the
     * user has clicked on the cancel button
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".cancel.cancelPrint").click(function (evt) {
        document.location.reload(true);
    });
});
