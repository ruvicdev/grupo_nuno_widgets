/**
 * Class used for manage all the information
 * and actions will be enabled in this module.
 * The differents events will be attached to
 * the elements
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
    // Create an object
    var utils         = new UtilElements();
    var req           = new Request();
    var quotationData = new Object();
    var dataResponse  = new Object();

    //Check just one checkbox per group
    // $('input:checkbox').click(function() {
    //     $('input:checkbox').not(this).prop('checked', false);
    // });

    /**
     * Validate the option will be displayed depending on the
     * value selected by the user in the form
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $('.checkdata').change(function () {
        var result = $(this).data('value');

        if(result=="asesorchk") { //Checkbox vendedor
            if ($("#asesorcheckbox").is(":checked")) {
                $( "#asesor-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "field" );
            }else{
                $( "#asesor-div" ).removeClass( "field" ).addClass( "ui segment hidden-segment-value" );
            }
        }

        if(result=="incrementoPartoCesareaChk") { //Checkbox cobertura nacional
            if ($("#incrementoPartoCesarea").is(":checked")) {
                $( "#cesareaMonto-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "fields" );
            }else{
                $( "#cesareaMonto").val("");
                $( "#cesareaMonto-div" ).removeClass( "fields" ).addClass( "ui segment hidden-segment-value" );
            }
        }

        if(result=="enfermedadGraveChk") { //Checkbox cobertura nacional
            if ($("#coberturaIndemnizacionEnfermedadGrave").is(":checked")) {
                $( "#graveMonto-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "fields" );
            }else{
              $( "#graveMonto").val("");
                $( "#graveMonto-div" ).removeClass( "fields" ).addClass( "ui segment hidden-segment-value" );
            }
        }

    });

    //Validate only numbers in some fields with property class="form-data-selected <fieldName>"
    $(".edad").numeric();
    $(".tel").numeric();
    $(".sumaAsegurada").numeric();
    $(".coaseguro").numeric();
    $(".deducible").numeric();
    $(".cesareaMonto").numeric();
    $(".graveMonto").numeric();

    //Print pdf once ready
    $(".print").addClass("cotiza-minor-pdf");

    /**
     * Add row in integrantes table
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#addRowIntegrantesTable").click(function(){
        addRowIntegrantesTable();
    });

    /**
     * Delete row in integrantes table
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(document).on('click', '#deleteRow', function () {
        //Save the total of integrantes added
        $('#integrantesRowsHidden').val($('#integrantesRowsHidden').val()-1);

        var whichtr = $(this).closest('tr').transition('fade');
        whichtr.remove();

        var integrantesPorEnviarArray = getIntegrantes();
        var existeTitularEnArray = false;

        for (var i = 0; i < integrantesPorEnviarArray.length; i++) {
            if (integrantesPorEnviarArray[i]['integrante'] == 'TITULAR') {
                existeTitularEnArray = true;
                break;
            }
        }

        //Verifica si existe un titular en el arreglo de integrantes, si no envia un error al validar la forma
        existeTitularEnArray?$('#existeTitularHidden').val(1):$('#existeTitularHidden').val(0);

        return false;
    });

/**
 * function that add row in integrantes table
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
function addRowIntegrantesTable() {
    var newIntegrante = $('#newIntegranteDropdown').dropdown('get text').toUpperCase();
    var newNombre     = $('#newNombre').val().toUpperCase();
    var newSexo       = $('#newSexoDropdown').dropdown('get text').toUpperCase();
    var newEdad       = $('#newEdad').val().toUpperCase();
    var table_length  = ($("#integrantes_table > tbody > tr").length) - 1;

    if (integrantesFormValidate(newIntegrante, newNombre, newSexo, newEdad, table_length)) {
        var markup =
                    "<tr id='rowIntegrantes"+table_length+"'>"+
                        "<td class='center aligned' id='newIntegrante"+table_length+"'>"+newIntegrante+"</td>"+
                        "<td class='center aligned' id='newNombre"+table_length+"'>"+newNombre+"</td>"+
                        "<td class='center aligned' id='newSexo"+table_length+"'>"+newSexo+"</td>"+
                        "<td class='center aligned' id='newEdad"+table_length+"'>"+newEdad+"</td>"+
                        "<td class='center aligned'>"+
                            "<font class='ui fluid red button' id='deleteRow'>"+
                                "-"+
                            "</font>"
                        "</td>"+
                    "</tr>";

        $("#integrantes_table tbody").append(markup);

        //Clear values
        $('#newIntegranteDropdown').dropdown('restore defaults');
        $('#newIntegrante').val("");
        $('#newNombre').val("");
        $('#newSexoDropdown').dropdown('restore defaults');
        $('#newEdad').val("");

        //Save the total of rows added
        $('#integrantesRowsHidden').val(table_length+1);
        //Save integrante titular
        if (newIntegrante == 'TITULAR') {
            $('#existeTitularHidden').val(1);
        }
    }
}

/**
   * Event used for return to the past section if the user
   * click the return button to modify some information
   * added to the data filled in beforewi
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com> based on Ruben Alonso Cortes Mendoza method
   * @version 1.0
   * @company ruvicdev
   */
$(".ui.negative.button.backButton-2").click(function (evt) {
      evt.preventDefault();

      //return back the accordion
      $("#step-2").removeClass("active").addClass("disabled");
      $("#accordion-2").addClass("disabled-content");
      $("#content-2").removeClass("active");
      $("#title-2").removeClass("active");

      $("#step-1").removeClass("completed").addClass("active");
      $("#accordion-1").removeClass("disabled-content");
      $("#content-1").addClass('active');
      $("#title-1").addClass('active');
  });

  /**
   * Event used to create a request once the user clicks the button
   * to send the button and should appear the window to display the
   * option if the user wants to print the file
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".ui.positive.button.submit-button-2").click(function (evt) {
      evt.preventDefault();

      //Save necessary data into quotationData in order to show it in PDF file
      //Save prima bruta anual per member in the quotationData array
      quotationData.integrantes = dataResponse.integrantesArray;

      //sending the information to the ajax request to send email with PDF file
      $(".ui.page.dimmer").addClass("active");

      var request =
        $.ajax({ type     : 'POST',
                 url      : './multicotizadorGMM/saveData',
                 dataType : "json",
                 data     : JSON.stringify(quotationData),
                 headers  : {
                     "Accept"       : "application/json",
                     "Content-Type" : "application/json"
                 }
               }
              );

        request.done(function (response, textStatus, jqXHR){
            if (response.status == "success") {
                // set the complete path file in button
                $(".print.cotiza-minor-pdf").attr('id', response.pathFile);

                setTimeout(function () {
                    utils.displayModal();
                }, 2500);
            } else {
                // error message
            }
        });

        request.always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass("active")}, 2500);
        });
  });

  /**
   * Event used for giving the option to the user
   * to download and print the file with all the
   * information set during the process
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".print.cotiza-minor-pdf").click(function (evt) {
     // sending the request to the URL for displaying the
     url = $(this).attr('id');

     // send PDF file to print
     window.open(url, "_blank").focus();

     // create the request for reloading the page
     //var reloadUrl = "/merchandise/index";
     //req.getApi(reloadUrl);
     document.location.reload(true);
  });

  /**
   * Function that send data to php and calculate quotes
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com> based on Ruben Alonso Cortes Mendoza method
   * @version 1.0
   * @company ruvicdev
   */
  function previewData() {
      //Get form data Object-----------------------------------------------
      quotationData.asesor                                = $("#asesor").val().toUpperCase();
      quotationData.fullname                              = $("#fullname").val().toUpperCase();
      quotationData.email                                 = $("#email").val();
      quotationData.telefono                              = $("#telefono").val();
      quotationData.estado                                = $("#estado").val();
      quotationData.ciudad                                = $("#ciudad").val();
      quotationData.nivelHospitalario                     = $("#nivelHospitalario").val();
      quotationData.sumaAsegurada                         = $("#sumaAsegurada").val();
      quotationData.coaseguro                             = $("#coaseguro").val();
      quotationData.deducible                             = $("#deducible").val();
      quotationData.zona                                  = $("#zona").val();
      quotationData.tipoCotizacion                        = $("#tipoCotizacion").val();
      quotationData.tabuladorHonorariosMedicos            = $("#tabuladorHonorariosMedicos").val();
      quotationData.reduccionDeducibleAccidente           = $("#reduccionDeducibleAccidente").is(":checked")?"SI":"NO";
      quotationData.incrementoPartoCesarea                = $("#incrementoPartoCesarea").is(":checked")?"SI":"NO";
      quotationData.cesareaMonto                          = $("#cesareaMonto").val();
      quotationData.coberturaVisionIncremental            = $("#coberturaVisionIncremental").is(":checked")?"SI":"NO";
      quotationData.coberturaIndemnizacionEnfermedadGrave = $("#coberturaIndemnizacionEnfermedadGrave").is(":checked")?"SI":"NO";
      quotationData.graveMonto                            = $("#graveMonto").val();
      quotationData.emergenciaExtranjero                  = $("#emergenciaExtranjero").is(":checked")?"SI":"NO";
      quotationData.emergenciaTotalExtranjero             = $("#emergenciaTotalExtranjero").is(":checked")?"SI":"NO";

      //Add integrantes array to quotationData Object
      quotationData.integrantes = getIntegrantes();

      //Send data via POST
      $(".ui.page.dimmer").addClass("active");

      var request =
        $.ajax({ type     : 'POST',
                 url      : './MulticotizadorGMM/previewData',
                 dataType : "json",
                 data     : JSON.stringify(quotationData),
                 headers  : {
                     "Accept"       : "application/json",
                     "Content-Type" : "application/json"
                 }
               }
              );

        request.done(function (response, textStatus, jqXHR){
            if (response.status == "success") {
                dataResponse = response;

                fillIntegrantesTablePreview(response.integrantesArray);
            }else {
              //   Send Error
            }
        });

        request.always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass("active")}, 2500);
        });
  }

  function fillIntegrantesTablePreview(integrantesArray) {
      var integrantesTableTbody = "";
      $("#integrantes_table_preview tbody tr").remove();

      for (var i in integrantesArray) {
          integrantesTableTbody = integrantesTableTbody +
                                  "<tr id='rowIntegrantesPreview'>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].integrante+"</td>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].nombre+"</td>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].sexo+"</td>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].edad+"</td>"+
                                  "</tr>";
      }

      $("#integrantes_table_preview tbody").append(integrantesTableTbody);
  }

  /**
   * function that retrieve integrantes data
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  function getIntegrantes() {
      var integrantesArray = new Array();

      $("#integrantes_table tbody tr").each(function() {
          //Get only tr's with values, that user added
          if (this.id != "") {
              var integranteObj = new Object();

              integranteObj.integrante = $(this).find('td').eq(0).text();
              integranteObj.nombre     = $(this).find('td').eq(1).text();
              integranteObj.sexo       = $(this).find('td').eq(2).text();
              integranteObj.edad       = $(this).find('td').eq(3).text();

              integrantesArray.push(integranteObj);
          }
      });

      return integrantesArray;
  }

  /**
   * function that format a number
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  function formatNumber(n, currency) {
    return currency + n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
  }

/**
 * function that send data capured via POST
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
function goToPreview() {
    //Complete step-1
    $("#step-1").removeClass("active").addClass("completed");
    $("#accordion-1").addClass("disabled-content");
    $("#content-1").removeClass("active");
    $("#title-1").removeClass("active");

    //Active step-2
    $("#step-2").removeClass("disabled").addClass("active");
    $("#accordion-2").removeClass("disabled-content");
    $("#content-2").addClass("active");
    $("#title-2").addClass("active");

    //Fill fields filled to preview page
    $("#fullname_preview").text($("#fullname").val().toUpperCase());
    $("#email_preview").text($("#email").val().toLowerCase());
    $("#telefono_preview").text($("#telefono").val().toLowerCase());
    $("#estado_preview").text($("#estado").val());
    $("#ciudad_preview").text($("#ciudad").val().toLowerCase());
    $("#nivelHospitalarioDropdown_preview").text($("#nivelHospitalario").val());
    $("#sumaAsegurada_preview").text(formatNumber(($("#sumaAsegurada").val())*1,"$"));
    $("#coaseguro_preview").text($("#coaseguro").val().length <=4 ? ((($("#coaseguro").val())*100)+"%") : $("#coaseguro").val());
    $("#deducible_preview").text(formatNumber(($("#deducible").val())*1,"$"));
    $("#zona_preview").text($("#zona").val());
    $("#tipoCotizacionDropdown_preview").text($("#tipoCotizacion").val());
    $("#tabuladorHonorariosMedicosDropdown_preview").text($("#tabuladorHonorariosMedicos").val());
    $("#reduccionDeducibleAccidente").is(":checked")?$('#reduccionDeducibleAccidente_preview').prop('checked', true):$('#reduccionDeducibleAccidente_preview').prop('checked', false);
    $("#incrementoPartoCesarea").is(":checked")?$('#incrementoPartoCesarea_preview').prop('checked', true):$('#incrementoPartoCesarea_preview').prop('checked', false);
    $("#cesareaMonto_preview").text($("#cesareaMonto").val()==""?"No Aplica":formatNumber(($("#cesareaMonto").val())*1,"$"));
    $("#coberturaVisionIncremental").is(":checked")?$('#coberturaVisionIncremental_preview').prop('checked', true):$('#coberturaVisionIncremental_preview').prop('checked', false);
    $("#coberturaIndemnizacionEnfermedadGrave").is(":checked")?$('#coberturaIndemnizacionEnfermedadGrave_preview').prop('checked', true):$('#coberturaIndemnizacionEnfermedadGrave_preview').prop('checked', false);
    $("#graveMonto_preview").text($("#graveMonto").val()==""?"No Aplica":formatNumber(($("#graveMonto").val())*1,"$"));
    $("#emergenciaExtranjero").is(":checked")?$('#emergenciaExtranjero_preview').prop('checked', true):$('#emergenciaExtranjero_preview').prop('checked', false);
    $("#emergenciaTotalExtranjero").is(":checked")?$('#emergenciaTotalExtranjero_preview').prop('checked', true):$('#emergenciaTotalExtranjero_preview').prop('checked', false);

    //Send data (POST) and calculate quotes
    previewData();
}

/**
 * Validate integrantes form
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
function integrantesFormValidate(newIntegrante, newNombre, newSexo, newEdad, table_length) {
    //Validate fields entered
    var errorFlag = false;
    var htmlError = "<ul>";

    //Validate that can only add one titular and one conyuge
    for (i = 0; i < $('#integrantesRowsHidden').val(); i++) {
        if( $("#newIntegrante"+i).text() == "TITULAR" &&  newIntegrante == "TITULAR") {
            htmlError += "<li>Agregue solo un Titular</li>";
            errorFlag = true;
        }

        if( $("#newIntegrante"+i).text() == "CONYUGE" &&  newIntegrante == "CONYUGE") {
            htmlError += "<li>Agregue solo un Conyuge</li>";
            errorFlag = true;
        }
    }

    //Validate titular must be the first integrante.
    if(table_length == 0 && newIntegrante != "TITULAR" && newIntegrante != "SELECCIONE") {
        htmlError += "<li>Agregue primero un Titular</li>";
        errorFlag = true;
    }

    //Validate title of integrante
    if(newIntegrante.trim() == "SELECCIONE") {
        htmlError += "<li>Introdúzca Integrante</li>";
        errorFlag = true;
    }

    //Validate name of integrante
    if(newNombre.trim() == "") {
        htmlError += "<li>Introdúzca Nombre del Asegurado</li>";
        errorFlag = true;
    }

    //Validate sex of integrante
    if(newSexo.trim() == "SEXO") {
        htmlError += "<li>Introdúzca Sexo del Asegurado</li>";
        errorFlag = true;
    }

    //Validate age of integrante
    if(newEdad.trim() == "" || isNaN(newEdad.trim())) {
        htmlError += "<li>Introdúzca Edad del Asegurado válida</li>";
        errorFlag = true;
    }else {
        if(newIntegrante.trim() == "TITULAR" || newIntegrante.trim() == "CONYUGE") {
            if (newEdad.trim() > 60 || newEdad.trim() < 18) {
                htmlError += "<li>Edad máxima del asegurado titular o conyuge es de 60 años y no menor de 18 años</li>";
                errorFlag = true;
            }
        }

        if(newIntegrante.trim() == "HIJO(A)") {
            if (newEdad.trim() > 24) {
                htmlError += "<li>Edad máxima del asegurado Hijo(a) es de 24 años</li>";
                errorFlag = true;
            }
        }
    }

    if (errorFlag) { //Show errors
        htmlError += "</ul>";
        $('#errorsIntegrantesDiv').removeClass("hidden-segment-value").addClass("show-errors");
        $('#errorsIntegrantesDiv').html(htmlError);
        return false;
    }else { //Hide errors
        $('#errorsIntegrantesDiv').removeClass("show-errors").addClass("hidden-segment-value");
        $('#errorsIntegrantesDiv').html("");
        return true;
    }
}

/**
 * Validate email
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
 $.fn.form.settings.rules.emailValidation = function(value) {
     return utils.validateEmailData(value);
 }

 /**
  * Validate integrantes
  *
  * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
  * @version 1.0
  * @company ruvicdev
  */
  $.fn.form.settings.rules.integrantesValidation = function(value) {
      var integrantesRowsHidden =  $('#integrantesRowsHidden').val();
      var existeTitularHidden =  $('#existeTitularHidden').val();

      var returnValid = true;

      if (integrantesRowsHidden <= 0 || existeTitularHidden <= 0) {
          returnValid = false;
      }

      return returnValid;
  }

  /**
   * Validate only titular when tipo cotizacion = INDIVIDUAL
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
   $.fn.form.settings.rules.onlyTitularCotizacionIndividual = function(value) {
       var integrantesRowsHidden =  $('#integrantesRowsHidden').val();
       var tipoCotizacion        =  $('#tipoCotizacion').val();

       var returnValid = true;

       if (integrantesRowsHidden > 1 && tipoCotizacion == 'INDIVIDUAL') {
           returnValid = false;
       }

       return returnValid;
   }

 $('.ui.form')
 .form({
    fields: {
        fullname: {
            identifier: 'fullname',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        email: {
            identifier: 'email',
            rules: [
                {
                  type   : 'emailValidation[value]',
                  prompt : 'Capture {name} válido'
                }
            ]
        },
        telefono: {
            identifier: 'telefono',
            rules: [
                {
                  type   : 'regExp',
                  value  : /^[0-9]+$/,
                  prompt : 'Capture {name} válido'
                }
            ]
        },
        estadoDropdown: {
            identifier: 'estado',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },
        ciudad: {
            identifier: 'ciudad',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        nivelHospitalarioDropdown: {
            identifier: 'nivelHospitalario',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },
        sumaAsegurada: {
            identifier: 'sumaAsegurada',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        coaseguro: {
            identifier: 'coaseguro',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        deducible: {
            identifier: 'deducible',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        zona: {
            identifier: 'zona',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        tipoCotizacionDropdown: {
            identifier: 'tipoCotizacion',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },
        tabuladorHonorariosMedicosDropdown: {
            identifier: 'tabuladorHonorariosMedicos',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },
        cesareaMonto: {
            identifier: 'cesareaMonto',
            depends: 'incrementoPartoCesarea',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture Monto Por Parto o Cesárea'
                }
            ]
        },
        graveMonto: {
            identifier: 'graveMonto',
            depends: 'coberturaIndemnizacionEnfermedadGrave',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture Monto Por Enfermedad Grave'
                }
            ]
        },
        existeTitular: {
            identifier: 'existeTitularHidden',
            rules: [
                {
                  type   : 'integrantesValidation[value]',
                  prompt : 'Agregue al menos un integrante (Titular) a la cotización'
                }
            ]
        },
        existeTitularCotizacionINDIVIDUAL: {
            identifier: 'integrantesRowsHidden',
            rules: [
                {
                  type   : 'onlyTitularCotizacionIndividual[value]',
                  prompt : 'Agregue sólo el titular cuando el típo de cotización es INDIVIDUAL'
                }
            ]
        }
    },
    onSuccess: function(event, ) {
        event.preventDefault();
        goToPreview();
    }
  });
});
