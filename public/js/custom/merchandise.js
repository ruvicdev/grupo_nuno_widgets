/**
 * Class used for manage all the information
 * and actions will be enabled in this module.
 * The differents events will be attached to
 * the elements
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
  // Create an object
  var utils = new UtilElements();
  var math  = new MathElements();
  var req   = new Request();
  var obj   = "";

  // validate felds numeric
  $(".tel").numeric();
  $(".print").addClass("cotiza-carga-pdf");
  $("input[name='rfc']").css('text-transform', 'uppercase');

  // Call Methods directly from the class UtilElements
  utils.closeMessages(".close");

  // Hide employee name's field and unchecking checkbox
  $("#vendedor").prop('checked', false);
  $("#name_seller").hide();

  // Disabled the dropdowns second section (Origin, Destination)
  $(".ui.dropdown.continente-origen").addClass('disabled');
  $(".ui.dropdown.pais-origen").addClass('disabled');
  $(".ui.dropdown.estado-origen").addClass('disabled');
  $(".ui.dropdown.continente-destino").addClass('disabled');
  $(".ui.dropdown.pais-destino").addClass('disabled');
  $(".ui.dropdown.estado-destino").addClass('disabled');

  // event to put working checkbox as radio button
  $('input:checkbox.tipo-21').click(function() {
      utils.checkboxAsRadioButton(this, ".tipo-21");
  });
  $('input:checkbox.cob-22').click(function() {
        // validate the coverage to know if it's national and must just display Mexico and cities
        // of that country
        var $tipoCob = $(this).data('value');
        if ($tipoCob == "nacional") {
            // set the data value to defined as national
            $(".ui.dropdown.continente-origen").addClass('disabled').dropdown('set selected', 'North America');
            $(".ui.dropdown.continente-destino").addClass('disabled').dropdown('set selected', 'North America');

            // set the values regarding the request
            var url = "/carga/getNationalData";
            req.getApi(url).done(function (response) {
                if (response.flag == 1 || response.flag == '1') {
                    // Set values in countries dropdowns
                    var $source       = $("#paises_origen_continente").html();
                    var $destination  = $("#pais_origen_resultados");
                    var $source2      = $("#paises_destino_continente").html();
                    var $destination2 = $("#pais_destino_resultados");

                    utils.setTemplate($source, $destination, {data: response.pais});
                    utils.setTemplate($source2, $destination2, {data: response.pais});

                    // Set values in states dropdowns
                    var $sourceE       = $("#estado_origen_pais").html();
                    var $sourceE1      = $("#estado_destino_pais").html();
                    var $destinationE  = $("#estado_origen_resultados");
                    var $destinationE1 = $("#estado_destino_resultados");

                    utils.setTemplate($sourceE, $destinationE, {data: response.states});
                    utils.setTemplate($sourceE1, $destinationE1, {data: response.states});
                }
            });

            // Used for filled the data in the dropdowns
            setTimeout(function () {
                // set the country to defined as national
                $(".pais-origen .default.text.conveyance").text('Mexico');
                $(".pais-destino .default.text.conveyance").text('Mexico');
                $(".ui.dropdown.pais-origen").addClass('disabled').dropdown('set selected', 'Mexico');
                $(".ui.dropdown.pais-destino").addClass('disabled').dropdown('set selected', 'Mexico');
                $(".ui.dropdown.estado-origen").removeClass('disabled');
                $(".ui.dropdown.estado-destino").removeClass('disabled');
            }, 300);
        } else {
            // remove disabled class and restore default values
            $(".ui.dropdown.continente-origen").removeClass('disabled').dropdown('restore defaults');
            $(".ui.dropdown.continente-destino").removeClass('disabled').dropdown('restore defaults');
            $(".ui.dropdown.pais-origen").removeClass('disabled').dropdown('restore defaults');
            $(".ui.dropdown.pais-destino").removeClass('disabled').dropdown('restore defaults');
            $(".ui.dropdown.estado-origen").removeClass('disabled').dropdown('restore defaults');
            $(".ui.dropdown.estado-destino").removeClass('disabled').dropdown('restore defaults');

            // delete all the dynamic elements
            $("#pais_origen_resultados > div").remove();
            $("#pais_destino_resultados > div").remove();
            $("#estado_origen_resultados > div").remove();
            $("#estado_destino_resultados > div").remove();
        }

        utils.checkboxAsRadioButton(this, ".cob-22");
  });
  $('input:checkbox.tperson').click(function() {
        utils.checkboxAsRadioButton(this, ".tperson");
    });

  /**
     * Event will be used to put currency format
     * once the user begins to type the amount
     * he wants to set in the input
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
  $('input.valMer').keyup(function(event) {
        // skip for arrow keys
        if(event.which >= 37 && event.which <= 40){
            event.preventDefault();
        }

        $(this).val(function(index, value) {
            return value.replace(/\D/g, "")
                        .replace(/([0-9])([0-9]{2})$/, '$1.$2')
                        .replace(/\B(?=(\d{3})+(?!\d)\.?)/g, ",");
        });
    });

  $('.ui.search.selection').dropdown('setting', 'onChange', function(value, text, $choice){
        // clean the value and then set it
        obj = utils.createObjectForm("class", "");
        obj = utils.createObjectForm("class", $("#" + value).val());
    });

  /**
   * Method used to validate if the user is doing the
   * cotization is Grupo Nuno's employee or not. In case
   * the user is employee, should fill the name in of the 
   * field
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $("#vendedor").change(function () {
    if ($("#" + this.id).prop('checked')) {
      $("#name_seller").removeClass('ignore').show();
    } else {
      $("#name_seller").addClass('ignore').val('').hide();
    }
  });

  /**
   * Validate the option will be displayed depending on the
   * value selected by the user in the form
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $('.checkdata').change(function () {
    var result 	   = $(this).data('value');
		var $dataClass = $(this).data('class');

		var strClass = "." + $dataClass;
		var strID  	 = "#" + result + "-field";

    var $data  = utils.displayElements(result, strClass, '.first-step');

    $(".checkdata").removeClass('enabledCheckbox');
    $(".checkdata[data-value='" + result + "']").addClass('enabledCheckbox');
		$(strID + "[data-value='" + $data + "']").removeClass($dataClass);

    if (result == 'fisica') {
        $("input[data-secondary='fisica']").removeClass('ignore');

        // clear field persona moral
        $("input[data-secondary='moral']").val("");

        var secondary = $("input[data-secondary='moral']").hasClass('ignore');
        if (secondary == false) {
            $("input[data-secondary='moral']").addClass('ignore');
        }
    } else {
        $("input[data-secondary='moral']").removeClass('ignore');

        // clear field persona fisica
        $("input[data-secondary='fisica']").val("");

        var secondary = $("input[data-secondary='fisica']").hasClass('ignore');
        if (secondary == false) {
            $("input[data-secondary='fisica']").addClass('ignore');
        }
    }
  });

  /**
	 * Validate that all the fields has been filled for
	 * activate the button 'Next'. In case the required fields has not been
	 * selected, then the button will not be activated
	 *
	 * @author Ruben Alonso Cortes Mendoza
	 * @version 1.0
	 * @company ruvicdev
	 */
  $(".ui.form .submit-button").click(function (evt) {
        evt.preventDefault();

        var generalCounter  = 0;
        var checkboxCounter = 0;

        $(".form-data-selected").each(function (index, value) {
            var typeElement = utils.typeOfElement(value);

            // validate dropdowns
            if (typeElement == "hidden") {
                if (value.value == 0) {
                    // add class error
                    utils.addRemoveClass($(this).parent().parent(), "add");
                    generalCounter++;
                } else {
                    // remove class error
                    utils.addRemoveClass($(this).parent().parent(), "remove");
                    // add value to the object
                    obj = utils.createObjectForm(value.name, value.value);
                }
            }

            // validate radio buttons
            if (typeElement == "checkbox") {
                var radioFlag = $(".checkdata").hasClass('enabledCheckbox');

                if (radioFlag != true) {
                    checkboxCounter++;
                } else {
                    // remove class error
                    utils.addRemoveClass($(".sec-1"), "remove");
                    checkboxCounter = 0;

                    // add value to the object
                    obj = utils.createObjectForm(value.name, value.value);
                }

                if (checkboxCounter > 1) {
                    // add class error
                    utils.addRemoveClass($(".sec-1"), "add");
                    generalCounter++;
                }
            }

            // validate input text
            if (typeElement == 'text') {
                var input = $(this).hasClass('ignore');

                if (input == false) {
                    if ($(this).val() == '') {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        generalCounter++;
                    } else {
                        // remove class error
                        utils.addRemoveClass($(this).parent(), "remove");
                        // add value to the object
                        obj = utils.createObjectForm(value.name, value.value);
                    }
                }
            }

            // validate input email
            if (typeElement == 'email') {
                var emailValue = utils.validateEmailData(value.value);

                if (value.value == '') {
                    // add class error
                    utils.addRemoveClass($(this).parent(), "add");
                    generalCounter++;
                } else if (emailValue == false) {
                    // add class error
                    utils.addRemoveClass($(this).parent(), "add");
                    generalCounter++;
                } else {
                    // remove class error
                    utils.addRemoveClass($(this).parent(), "remove");
                    // add value to the object
                    obj = utils.createObjectForm(value.name, value.value);
                }
            }
        });

        // creating variables to get the values from the templates and destination of the message
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_first_section");
        var $data        = "";

        // TODO: Message error & mark fields required empty
        if (generalCounter != 0) {
            // set template for errors
            utils.setTemplate($source, $destination, {data: $data});
            //remove hidden class
            utils.removeClassHiddenMessage();
        } else {

            // check if vendedor is checked to added to the obj object
            if ($("#vendedor").prop('checked')) {
              obj = utils.createObjectForm('vendedor', 'Si');
            } else {
              obj = utils.createObjectForm('vendedor', '');
              obj = utils.createObjectForm('nombre_vendedor', '');
            }

            // add value to the object
            obj = utils.createObjectForm("rfc", $("input[name='rfc']").val());
            obj = utils.createObjectForm("nombre_giro", $(".giroText").text());
            obj = utils.createObjectForm("tipo_persona", $(".enabledCheckbox").data('index'));

            // Set the session in object and request in case obj session exists
            if (!obj.orden_id) {
                var $url = "/carga/setSessionAndOrder";
                req.getApi($url).done(function (response) {
                    if (response.flag == 1 || response.flag == '1') {
                        obj = utils.createObjectForm('orden_id', response.order_id);
                        obj = utils.createObjectForm('session', response.session);
                        $("#no_order").text(response.order_id);
                        $("#order_id_3").text(response.order_id);
                    }
                });
            }
            console.log(obj);
            utils.addClassHiddenMessage();

            $("#step-1").removeClass("active").addClass("completed");
            $("#accordion-1").addClass("disabled-content");
            $("#content-1").removeClass("active");
            $("#title-1").removeClass("active");

            $("#step-2").removeClass("disabled").addClass("active");
            $("#accordion-2").removeClass("disabled-content");
            $("#content-2").addClass("active");
            $("#title-2").addClass("active");
        }
    });

  /**
     * Event used for return to the past section if the user
     * click the return button to modify some information
     * added to the data filled in beforewi
     *
     * @author Ruben Alonso Cortes Mendoza
     * @version 1.0
     * @company ruvicdev
     */
  $(".ui.negative.button.backButton-2").click(function (evt) {
        evt.preventDefault();

        //return back the accordion
        $("#step-2").removeClass("active").addClass("disabled");
        $("#accordion-2").addClass("disabled-content");
        $("#content-2").removeClass("active");
        $("#title-2").removeClass("active");

        $("#step-1").removeClass("completed").addClass("active");
        $("#accordion-1").removeClass("disabled-content");
        $("#content-1").addClass('active');
        $("#title-1").addClass('active');
    });

  /**
     * Event will be used to add the class
     * will be used in the validation once
     * the user clicks the button to move the
     * next section
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
  $(".checkdata2").change(function () {
    var result 	   = $(this).data('text');
		var $dataClass = $(this).data('class');

    if (result == "cobertura") {
        if ($(this).is(":checked")) {
            $("input:checkbox.cob-22").removeClass("enabledCob");
            $(this).addClass("enabledCob");
        } else {
            $(this).removeClass("enabledCob");
        }
    } else if (result == "tipo") {
        if ($(this).is(":checked")) {
            $("input:checkbox.tipo-21").removeClass("enabledTip");
            $(this).addClass("enabledTip");
        } else {
            $(this).removeClass("enabledTip");
        }
    }
  });

  /**
     * Event will be used to pass all the information to the
     * final section for display final price for cecking what
     * is going to be the price for the merchandise will be moved
     * from one side to another side
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
  $(".ui.positive.button.nextButton-2").click(function (evt) {
        evt.preventDefault();

        // validate fields in second section
        var counter2         = 0;
        var counterCheckbox1 = 0;
        var counterCheckbox2 = 0;
        var claseTipoMerc    = "";

        $(".requiredClass").each(function (index, value) {
            var typeElement = utils.typeOfElement(value);

            if (typeElement == "hidden") {
                var name         = $(this).attr('name');
                var attrDropdown = $("input[name='" + name + "']").parent().attr('class');
                var finalClass   = attrDropdown.split(" ").join(".");
                var textDropdown = $("." + finalClass + " .text.conveyance").text();

                if ((value.value == 0 && name == 'moneda') ||
                    (value.value == 0 && name == 'medio_transp') ||
                    (value.value == 0 && name == 'tipo_merc') ||
                    (value.value == 0 && name == 'continente_origen') ||
                    (value.value == 0 && name == 'continente_destino')) {

                    // add class error
                    utils.addRemoveClass($(this).parent().parent(), "add");
                    counter2++;
                } else if ((textDropdown == 'Pais' && name == 'pais_origen') ||
                           (textDropdown == 'Pais' && name == 'pais_destino') ||
                           (textDropdown == 'Estado' && name == 'estado_origen') ||
                           (textDropdown == 'Estado' && name == 'estado_destino')) {

                    // add class error
                    utils.addRemoveClass($(this).parent().parent(), "add");
                    counter2++;
                } else {
                    // remove class error
                    utils.addRemoveClass($(this).parent().parent(), "remove");

                    // get the tipo merc for then get the class of the merc*/
                    if (name == 'tipo_merc' && value.name == 'tipo_merc') {
                        claseTipoMerc = value.value;
                    }
                    // add value to the object
                    obj = utils.createObjectForm(value.name, value.value);

                    // adding the origin country and destiny country
                    if ((name == 'pais_origen' && textDropdown != 'Pais') ||
                        (name == 'pais_destino' && textDropdown != 'Pais')) {
                        obj = utils.createObjectForm(name, textDropdown);
                    }
                }
            }

            if (typeElement == "checkbox") {
                var classAssigned = $(this).data('text');
                if (classAssigned == "tipo") {
                    if ($("input:checkbox[name='" + classAssigned + "']").is(":checked")) {
                        // remove class error
                        utils.addRemoveClass($(".tipo-21").parent().parent(), "remove");

                        counterCheckbox1 = 0;

                        // add value to the object
                        obj = utils.createObjectForm(value.name,
                                utils.upperCaseFirstLetter($(".enabledTip").data('value')));
                    } else {
                        // add class error
                        utils.addRemoveClass($(".tipo-21").parent().parent(), "add");

                        counterCheckbox1++;
                        counter2++;
                    }
                }

                if (classAssigned == "cobertura") {
                    if ($("input:checkbox[name='" + classAssigned + "']").is(":checked")) {
                        // remove class error
                        utils.addRemoveClass($(".cob-22").parent().parent(), "remove");

                        counterCheckbox2 = 0;
                        // add value to the object
                        obj = utils.createObjectForm(value.name,
                                utils.upperCaseFirstLetter($(".enabledCob").data('value')));
                    } else {
                        // add class error
                        utils.addRemoveClass($(".cob-22").parent().parent(), "add");

                        counterCheckbox2++;
                        counter2++;
                    }
                }
            }

            if ($.trim($("#descArea").val()).length != 0) {
                // remove class error
                utils.addRemoveClass($("#descArea").parent(), "remove");

                // add value to the object
                obj = utils.createObjectForm("descripcion_merc", $(".textareaData").val());
            } else {
                // add class error
                utils.addRemoveClass($("#descArea").parent(), "add");
                counter2++;
            }

            if (typeElement == "text") {
                if (value.value == "") {
                    // add class error
                    utils.addRemoveClass($(this).parent(), "add");
                    counter2++;
                } else {
                    utils.addRemoveClass($(this).parent(), "remove");

                    if (claseTipoMerc != "") {
                        var finalValue = $("input#" + claseTipoMerc).val();
                        obj = utils.createObjectForm("class", finalValue);
                        claseTipoMerc = "";
                    } else {
                        // add value to the object
                        obj = utils.createObjectForm(value.name, value.value);
                    }
                }
            }
        });

        // Defining the variables to displaye errors message
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_second_section");
        var $data        = "";

        // Validating the max amount allowed
        var totalMoney = $(".valMer").val();
        totalMoney = totalMoney.split(',').join('');
        if (totalMoney != '') {
            if (obj.moneda == 'mxn' || obj.moneda == 'MXN') {
                if (totalMoney > 8000000.00) {
                    $data = "El valor de la mercancia debe ser menor o igual a 8,000,000.00 pesos.";
                    counter2++;
                }
                else if (totalMoney <= 0.00) {
                    $data = "El valor de la mercancia debe ser mayor a $0.00 pesos.";
                    counter2++;
                } else {
                    //remove error message for the amount allowed for the merchandise
                    $("#messageErrorMoney").text("");
                }
            } else {
                if (totalMoney > 550000.00) {
                    $data = "El valor de la mercancia debe ser menor o igual a $550,000.00 dolares.";
                    counter2++;
                }
                else if (totalMoney <= 0.00) {
                    $data = "El valor de la mercancia debe ser mayor a $0.00 dolares.";
                    counter2++;
                }
                else {
                    //remove error message for the amount allowed for the merchandise
                    $("#messageErrorMoney").text("");
                }
            }
        }

        // TODO: poner todos los mensajes de error
        if (counter2 != 0) {
            // set template for errors
            utils.setTemplate($source, $destination, {data: $data});
            //remove hidden class
            utils.removeClassHiddenMessage();
        } else {
            // add hidden class
            utils.addClassHiddenMessage();

            //remove error message for the amount allowed for the merchandise
            $("#messageErrorMoney").text("");

            // add value to the object
            obj = utils.createObjectForm("tipo_moneda", $(".currencyText").text());
            obj = utils.createObjectForm("nombre_merc", $(".tipoMercancia").text());
            obj = utils.createObjectForm("nombre_medio_trans", $(".medio-transp-main .text.conveyance").text());
            obj = utils.createObjectForm("current_date", $("#currentDate").val());
            obj = utils.createObjectForm("empaque", $("#empaque").val());

            // adding valid at date
            var $date      = moment($("#currentDate").val(), "DD-MM-YYYY").add(5, 'days').format();
            var $valid_at  = $date.substring(0, 10);
            var $splitDate = $valid_at.split('-');
            var $validAt   = $splitDate[2] + "/" + $splitDate[1] + "/" + $splitDate[0];

            obj = utils.createObjectForm("validAt", $validAt);

            // validate the percentage
            var percentage = utils.getPercentage(obj.giro, obj.vol_viajes);

            // validating all the information to create the operations
            // will be displayed as total with and with no tax
            var $primaNeta = math.totalWithoutTax(obj.valor_merc, percentage);
            var $tarifa    = math.totalWithTax($primaNeta);

            // check the kind of security assigned
            if (obj.moneda == 'mxn' || obj.moneda == 'MXN') {
                obj = utils.createObjectForm("security",
                                             utils.getSecurityPesos(obj.class, obj.valor_merc));
            } else {
                obj = utils.createObjectForm("security",
                                             utils.getSecurityDollars(obj.class, obj.valor_merc));
            }

            var $primaNetaFormat = accounting.formatNumber($primaNeta, 2, ",", ".");
            var $tarifaFormat    = accounting.formatNumber($tarifa, 2, ",", ".");

            $("#prima_neta").val($primaNetaFormat);
            $("#total_pagar").val($tarifaFormat);

            $("#step-2").removeClass("active").addClass("completed");
            $("#accordion-2").addClass("disabled-content");
            $("#content-2").removeClass("active");
            $("#title-2").removeClass("active");

            $("#step-3").removeClass("disabled").addClass("active");
            $("#accordion-3").removeClass("disabled-content");
            $("#content-3").addClass("active");
            $("#title-3").addClass("active");
        }
    });

/**
 * Event used for return back to the before
 * section where the user could modify the information
 * has provided to create the possible price
 * should be paid
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonos21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
  $(".ui.negative.button.backButton-3").click(function (evt) {
        evt.preventDefault();

        //return back the accordion
        $("#step-3").removeClass("active").addClass("disabled");
        $("#accordion-3").addClass("disabled-content");
        $("#content-3").removeClass("active");
        $("#title-3").removeClass("active");

        $("#step-2").removeClass("completed").addClass("active");
        $("#accordion-2").removeClass("disabled-content");
        $("#content-2").addClass("active");
        $("#title-2").addClass("active");
     });

  /**
   * Event will be used for generate the next
   * section where the user could see all the
   * resume or preview of all the data has
   * selected during all the process
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".ui.positive.button.nextButton-3").click(function (evt) {
      evt.preventDefault();

      // validate the fields
      var counter = 0;

      $(".third-section").each(function (index, value) {
          if (value.value == '') {
              // add class error
              utils.addRemoveClass($(this).parent(), "add");
              counter++;
          } else {
              // remove class error
              utils.addRemoveClass($(this).parent(), "remove");

              // adding values to obj
              obj = utils.createObjectForm(value.name, accounting.formatNumber(value.value, 2, ",", "."));
          }
      });

      // Defining the variables to displaye errors message
      var $source      = $("#errors_messages").html();
      var $destination = $("#error_third_section");
      var $data        = "";

      if (counter != 0) {
          // set template for errors
          utils.setTemplate($source, $destination, {data: $data});
          //remove hidden class
          utils.removeClassHiddenMessage();
      } else {
          utils.addClassHiddenMessage();

          // adding the data to the preview
          if (obj.tipo_persona == 1) {
              var stringVariable = obj.nombre + " " + obj.apellido_paterno + " " + obj.apellido_materno;
              $("#nombre").text(stringVariable);
          } else {
              $("#nombre").text(obj.persona_moral);
          }

          if (obj.cobertura == 'nacional' || obj.cobertura == 'Nacional') {
              $("#cob_nacionales").removeClass("hidden-segment-value");
              $("#cob_internacionales").addClass("hidden-segment-value");
          } else {
              $("#cob_internacionales").removeClass("hidden-segment-value");
              $("#cob_nacionales").addClass("hidden-segment-value");
          }

          // New sections addedd
          $("#vendedor_txt").text(obj.vendedor);
          $("#nombre_vendedor_txt").text(obj.nombre_vendedor);

          $("#no_cotizacion").text("# " + obj.orden_id);
          $("#currency").text(obj.tipo_moneda);
          $("#type").text(obj.nombre_giro);
          $("#validAt").text(obj.validAt);
          $("#email_txt").text(obj.email);
          $("#tel_txt").text(obj.telefono);
          $("#merchandise_value").text(obj.valor_merc);
          $("#merchandise").text(obj.nombre_merc);
          $("#desc").text(obj.descripcion_merc);
          $("#empaque_data").text(obj.empaque);
          $("#kind").text(obj.tipo);
          $("#cover").text(obj.cobertura);
          $("#transport").text(obj.nombre_medio_trans);
          $("#tarifa").text(obj.total_pagar);
          $("#prima_neta_text").text(obj.prima_neta);
          var $html  = "";
          $.each(obj.security, function (key) {
              $html += "<li>" + obj.security[key] + "</li>";
          });
          $("#security_data").html($html);

          $("#continente_origen_label").text(obj.continente_origen);
          $("#pais_origen_label").text(obj.pais_origen);
          $("#estado_origen_label").text(obj.estado_origen);
          $("#ciudad_origen_label").text(obj.ciudad_origen);
          $("#continente_destino_label").text(obj.continente_destino);
          $("#pais_destino_label").text(obj.pais_destino);
          $("#estado_destino_label").text(obj.estado_destino);
          $("#ciudad_destino_label").text(obj.ciudad_destino);

          if (obj.cobertura == 'nacional' || obj.cobertura == 'Nacional') {
              $(".int").hide();
          } else {
              $(".int").show();
          }

          // move to the next section
          $("#step-3").removeClass("active").addClass("completed");
          $("#accordion-3").addClass("disabled-content");
          $("#content-3").removeClass("active");
          $("#title-3").removeClass("active");

          $("#step-4").removeClass("disabled").addClass("active");
          $("#accordion-4").removeClass("disabled-content");
          $("#content-4").addClass("active");
          $("#title-4").addClass("active");
      }
  });

  /**
   * Event used for return to the second
   * section and the user could change information
   * regarding to the data provided to create the
   * final total
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".backButton-4").click(function (evt) {
       evt.preventDefault();

       // return back to the accordion 2
       $("#step-4").removeClass("active").addClass("disabled");
       $("#accordion-4").addClass("disabled-content");
       $("#content-4").removeClass("active");
       $("#title-4").removeClass("active");

       $("#step-2").removeClass("completed").addClass("active");
       $("#accordion-2").removeClass("disabled-content");
       $("#content-2").addClass("active");
       $("#title-2").addClass("active");
  });

   /**
    * Event used to create a request once the user clicks the button
    * to send the button and should appear the window to display the
    * option if the user wants to print the file
    *
    * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
    * @version 1.0
    * @company ruvicdev
    */
   $(".ui.positive.button.nextButton-4").click(function (evt) {
       evt.preventDefault();

       //sending the information to the ajax request to send email with PDF file
       var url = "/carga/saveData";
       req.postApi(url, obj).done(function (response) {
           if (response.flag == 1 || response.flag == "1") {
               // set the complete path file in button
               $(".print.cotiza-carga-pdf").attr('id', response.pathFile);

               setTimeout(function () {
                   utils.displayModal();
               }, 2500);
           } else {
               // error message
           }
       });
   });

   /**
    * Event used for giving the option to the user
    * to download and print the file with all the
    * information set during the process
    *
    * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
    * @version 1.0
    * @company ruvicdev
    */
   $(".print.cotiza-carga-pdf").click(function (evt) {
      // sending the request to the URL for displaying the
      url = $(this).attr('id');

      // send PDF file to print
      window.open(url, "_blank").focus();
      //window.open("http://www.ruvicdev.com/widgets/index.php/merchandise/PDFPrint/" + obj.orden_id, "_blank").focus();

      // Create the request for reloading the page
      document.location.reload(true);
   });

   /**
    * Event will reload the page when the user
    * click cancel button. This action will be added to
    * manage the new request of the merchandise cotization
    * widget where the user start up process
    *
    * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
    * @version 1.0
    * @company ruvicdev
    */
   $(".cancelPrint").click(function (evt) {
   		// create the request for reloading the page
      	//var reloadUrl = "/merchandise/index";
      	//req.getApi(reloadUrl);
      	document.location.reload(true);
   });

   //////////
   // EVENTOS DE ORIGEN
   /////////

   /**
    * Event will be fired once the user selects the
    * continent in the origen and it will return
    * al the countries related to the option selected
    *
    * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
    * @version 1.0
    * @company ruvicdev
    */
   $('.ui.selection.continente-origen').dropdown('setting', 'onChange', function(value, text, $choice){
        // check the coberture selected
        var $cobertura = $('.cob-22.checkdata2:checked').data('value');

        if ($cobertura == 'internacional') {
            // create objet will be send by the postApi method
            var objPost = {'continente':value};
            // create the url and then request
            var url = "/carga/countryByContinent";
            req.postApi(url, objPost).done(function (response) {
                if (response.flag == 1 || response.flag == "1") {
                    var $source      = $("#paises_origen_continente").html();
                    var $destination = $("#pais_origen_resultados");

                    utils.setTemplate($source, $destination, {data: response.paises});
                } else {
                    // error message
                }
            });
        }
    });

   /**
    * Event will be fired once the user selects the
    * country in the origen and it will return
    * al the states related to the option selected
    *
    * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
    * @version 1.0
    * @company ruvicdev
    */
   $(".ui.selection.pais-origen").dropdown('setting', 'onChange', function (value, text, $choice) {
       // check the coberture selected
       var $cobertura = $('.cob-22.checkdata2:checked').data('value');

       if ($cobertura == 'internacional') {
           // create object will be sent by the postApi method
           var codeCountry = $(".item:contains('" + text + "')").data('code');
           var objPost = {'country' : text,
                          'code'    : codeCountry};
           // create the url and then request
           var url = "/carga/stateByCountry";
           req.postApi(url, objPost).done(function (response) {
               if (response.flag == 1 || response.flag == "1") {
                   var $source      = $("#estado_origen_pais").html();
                   var $destination = $("#estado_origen_resultados");

                   utils.setTemplate($source, $destination, {data: response.states});
               } else {
                   // error message
               }
           });
       }
   });

  /**
   * Event will be fired once the user selects the
   * country in the origen and it will return
   * al the states related to the option selected
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  /*$(".ui.selection.dropdown.ciudad-origen1").dropdown('setting', 'onChange', function (value, text, $choice) {
      // create object will send by the postApi method
      var objPost = {'state':value};
      // create the url and then request
      var url = "/merchandise/citiesByState";
      req.postApi(url, objPost).done(function (response) {
          if (response.flag == 1 || response.flag == "1") {
              var $source = $("#ciudad_origen_estado").html();
              var $destination = $("#ciudad_origen_resultados");

              utils.setTemplate($source, $destination, {data: response.cities});
          } else {
              // error message
          }
      });
  });*/

  //////
  // EVENTOS DE DESTINO
  //////

  /**
   * Event will be fired once the user selects the
   * continent in the destination and it will return
   * al the countries related to the option selected
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".ui.selection.dropdown.continente-destino").dropdown('setting', 'onChange', function (value, text, $choice) {
      // check the coberture selected
      var $cobertura = $('.cob-22.checkdata2:checked').data('value');

      if ($cobertura == 'internacional') {
          // create objet will be send by the postApi method
          var objPost = {'continente':value};
          // create the url and then request
          var url = "/carga/countryByContinent";
          req.postApi(url, objPost).done(function (response) {
              if (response.flag == 1 || response.flag == "1") {
                  var $source      = $("#paises_destino_continente").html();
                  var $destination = $("#pais_destino_resultados");

                  utils.setTemplate($source, $destination, {data: response.paises});
              } else {
                  // error message
              }
          });
      }
  });

  /**
   * Event will be fired once the user selects the
   * country in the destination and it will return
   * al the states related to the option selected
   *
   * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".ui.selection.dropdown.pais-destino").dropdown('setting', 'onChange', function (value, text, $choice) {
       // check the coberture selected
       var $cobertura = $('.cob-22.checkdata2:checked').data('value');

       if ($cobertura == 'internacional')  {
           // create object will be sent by the postApi method
           var codeCountry = $(".item:contains('" + text + "')").data('code');
           var objPost = {'country' : text,
                          'code'    : codeCountry};
           // create the url and then request
           var url = "/carga/stateByCountry";
           req.postApi(url, objPost).done(function (response) {
               if (response.flag == 1 || response.flag == "1") {
                   var $source      = $("#estado_destino_pais").html();
                   var $destination = $("#estado_destino_resultados");

                   utils.setTemplate($source, $destination, {data: response.states});
               } else {
                   // error message
               }
           });
       }
  });
});
