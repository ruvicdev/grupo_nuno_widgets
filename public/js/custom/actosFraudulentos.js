/**
 * Class will be used to mantain
 * all the events here. The events
 * will fire up when the user clicks
 * any button or dropdown
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
    // Create an object
    var utils = new UtilElements();
    var req   = new Request();
    var obj   = "";
    var total = 0;

    // Call Methods directly from the class UtilElements
    utils.closeMessages('.close');
    $(".print").addClass('actos_fraudulentos_pdf');
    $("#telefono").numeric();
    $("#cp").numeric();
    $("#no_empleados").numeric();
    $("#no_sucursales").numeric();

    /**
     * Event will be added to displaye the datepicker
     * that is going to be used to set the date in
     * some fields
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#vigencia_desde").datepick({
        dateFormat:'dd/mm/yyyy',
        onSelect: function (dates) {
            var from  = new Date(dates);
            var day   = from.getDate();
            var month = from.getMonth();
            var year  = from.getFullYear();

            var to    = new Date(year + 1, month, day);
            $("#vigencia_hasta").val(to.getDate()+"/"+utils.getMonthToDisplay(to.getMonth())+"/"+to.getFullYear());
        }
    });

    /**
     * Event will fire once the user clicks the
     * checkbox that are simulating the radio
     * button in a elegant way
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $('input:checkbox.tperson').click(function() {
        utils.checkboxAsRadioButton(this, ".tperson");
    });

    /**
     * Event will fire to display the message in
     * case the value is positive at the moment to
     * select 'Yes'
     *
     * @author Ruben alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".optionsAdd").change(function () {
        var $count = 0;

        /*$($(this)).each(function (i, value) {
            if (value.value == 'Si') {
                $("#label_historial").addClass('hidden-segment-value');
            } else {
                $("#label_historial").removeClass('hidden-segment-value');
            }
        });*/

        $(".optionsAdd").each(function (i, val) {
            if (val.value == 'No' || val.value == 'no') {
                $count++;
            }
        });

        if ($count > 0) {
            $("#label_historial").removeClass('hidden-segment-value');
            $(".ui.positive.button.section-4-button").attr('disabled', 'disabled');
        } else {
            $("#label_historial").addClass('hidden-segment-value');
            $(".ui.positive.section-4-button").removeAttr('disabled');
        }
    });

    /**
     * Event will be used to show or display the input to
     * type the information of the company or the name
     * of the user will hire the insurance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".checkdata").change(function () {
        var value = $(this).data('value');
        $(".checkdata").removeClass('enabledCheckbox');

        if (value == 'fisica') {
            $(".checkdata[data-value='" + value +  "']").addClass('enabledCheckbox');
            $("#fisica").removeClass('hidden-segment-value');
            $("#moral").addClass('hidden-segment-value');
            $("#tipo_persona").val(value);

            // clean fields
            $("#empresa").val('').removeClass('required first_section');
            $("#nombre_contacto").val('');
            $("#nombre_asegurado").addClass('required first_section');
        } else {
            $(".checkdata[data-value='" + value +  "']").addClass('enabledCheckbox');
            $("#moral").removeClass('hidden-segment-value');
            $("#fisica").addClass('hidden-segment-value');
            $("#tipo_persona").val(value);

            // clean the fields
            $("#nombre_asegurado").val('').removeClass('required first_section');
            $("#empresa").addClass('required first_section');
        }
    });

    // Hide employee name's field and unchecking checkbox
    $("#vendedor").prop('checked', false);
    $("#nombre_vendedor").hide();

    /**
     * Event will be fired once the user
     * has selected an option from the dropdowns
     * of the second section
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".optionsAdd").change(function () {

        $($(this)).each(function (i, val) {
            if (val.value == 'Si') {
                total++;
            } else {
                total--;
            }
        });

        if (total == 8) {
            $("#controles").val(0);
            $("#apreciacion_riesgo").val(1);
        } else {
            $("#controles").val(1);

            if (total == 7) {
                $("#apreciacion_riesgo").val(2);
            }

            if (total == 6) {
                $("#apreciacion_riesgo").val(3);
            }

            if (total == 5) {
                $("#apreciacion_riesgo").val(4);
            }

            if (total <= 4) {
                $("#apreciacion_riesgo").val(5);
            }
        }
    });

    /**
     * Event will fire once the user type the number
     * of employess greater than 300. If is that, the user
     * will have type a number less or equal than 300 to pass
     * the next section
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#no_empleados").keyup(function (evt) {
        evt.preventDefault();

        if ($("#no_empleados").val() > 300) {
            $("#error_count_employees").removeClass('hidden-segment-value');
            $(".ui.positive.button.section-3-button").attr('disabled', 'disabled');
        } else {
            $("#error_count_employees").addClass('hidden-segment-value');
            $(".ui.positive.button.section-3-button").removeAttr('disabled');
        }
    });

    /**
     * Event will fire once the user type the total
     * amount of profit by the company or user. In case
     * the amount is greater than the value defined, the
     * system will show error
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#ingresos_anuales").keyup(function (evt) {
        evt.preventDefault();

        if ($("#ingresos_anuales").val() > 2500000) {
            $("#error_ingreso_maximo").removeClass('hidden-segment-value');
            $(".ui.positive.button.section-3-button").attr('disabled', 'disabled');
        } else {
            $("#error_ingreso_maximo").addClass('hidden-segment-value');
            $(".ui.positive.button.section-3-button").removeAttr('disabled');
        }
    });

    /**
     * Event will be fired once the user set a number
     * of offices has the company. If the number typed
     * by the user is greater than 20, then should be
     * displayed an error
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
     $("#no_sucursales").keyup(function (evt) {
        evt.preventDefault();

        if ($("#no_sucursales").val() > 20) {
            $("#error_no_sucursales").removeClass('hidden-segment-value');
            $(".ui.positive.button.section-3-button").attr('disabled', 'disabled');
        } else {
            $("#error_no_sucursales").addClass('hidden-segment-value');
            $(".ui.positive.button.section-3-button").removeAttr('disabled');
        }
     });

    /**
     * Event will be used to check if the field of
     * seller's name is displayed or not depending on
     * if the user is making the cotization is Grupo
     * Nuno's sellers
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#vendedor").change(function () {
        if ($("#" + this.id).prop('checked')) {
            $("#nombre_vendedor").addClass('required first_section').val('').show();
        } else {
            $("#nombre_vendedor").removeClass('required first_section').val('').hide();
        }
    });

    /**
     * Method will be used to validate the first
     * part that contains all the information of
     * the data of person will be filled the form
     * to get a cotization of the insurance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.positive.button.section-2-button").click(function (evt) {
        evt.preventDefault();

        var total           = 0;
        var checkboxCounter = 0;
        var $this           = $(this);
        var $source         = $("#errors_messages").html();
        var $destination    = $("#error_first_section");
        var $data           = "";

        $(".required.first_section").each(function (i, value) {
            var typeElement = utils.typeOfElement(value);

            if (value.tagName == "INPUT") {
                if (typeElement == 'text') {
                    if (value.value == '') {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        total++;
                    } else {
                        utils.addRemoveClass($(this).parent(), "remove");

                        // add values
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }

                if (typeElement == 'checkbox') {
                    var radioFlag = $(".checkdata").hasClass('enabledCheckbox');

                    if (radioFlag != true) {
                        checkboxCounter++;
                    } else {
                        // remove class error
                        utils.addRemoveClass($(".sec-1"), "remove");
                        checkboxCounter = 0;

                        // add value to the object
                        obj = utils.createObjectForm(value.name, value.value);
                    }

                    if (checkboxCounter > 1) {
                        // add class error
                        utils.addRemoveClass($(".sec-1"), "add");
                        total++;
                    }
                }

                if (typeElement == 'email') {
                    var emailValue = utils.validateEmailData(value.value);

                    if (value.value == '') {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        total++;
                    } else if (emailValue == false) {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        total++;
                    } else {
                        // remove class error
                        utils.addRemoveClass($(this).parent(), "remove");
                        // add value to the object
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }
            }
        });

        if (total != 0) {
            // Set template with errors
            utils.setTemplate($source, $destination, { data: $data});
            utils.removeClassHiddenMessage();
        } else {
            utils.addClassHiddenMessage();

            // check if vendedor is checked to added to the obj object
            obj = utils.createObjectForm('tipo_persona', $("#tipo_persona").val());
            if ($("#vendedor").prop('checked')) {
              obj = utils.createObjectForm('vendedor', 'Si');
            } else {
              obj = utils.createObjectForm('vendedor', 'No');
            }

            if (obj.tipo_persona == 'moral') {
                obj = utils.createObjectForm('nombre_contacto', $("#nombre_contacto").val());
            } else {
                obj = utils.createObjectForm('nombre_contacto', '');
            }

            // Update the steps
            $("#step-1").removeClass('active').addClass('completed');
            $("#accordion-1").removeClass('active').addClass('completed');
            $("#content-1").removeClass('active').addClass('completed');
            $("#title-1").removeClass('active').addClass('completed');

            $("#step-2").removeClass('disabled').addClass('active');
            $("#accordion-2").removeClass('disabled-content');
            $("#content-2").addClass('active');
            $("#title-2").addClass('active');

            $('html, body').animate({
                scrollTop: ($('#test_section_two').offset().top)
            },500);
        }
    });

    /**
     * Event will be used to validate the second
     * part of the form where the user will be selected more
     * values and once the user finished to fill in
     * the form validate all the section and move the next
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.positive.button.section-3-button").click(function (evt) {
        evt.preventDefault();

        var total        = 0;
        var $this        = $(this);
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_second_section");
        var $data        = "";

        $(".required.second_section").each(function (i, value) {
            var typeElement = utils.typeOfElement(value);

            if (value.tagName == "INPUT") {
                if (typeElement == 'text') {
                    if (value.value == '') {
                        utils.addRemoveClass($(this).parent(), 'add');
                        total++;
                    } else {
                        utils.addRemoveClass($(this).parent(), 'remove');

                        if (value.id == 'no_empleados') {
                            if (parseInt(value.value) > 300) {
                                utils.addRemoveClass($(this).parent(), 'add');
                                $("#error_count_employees").removeClass('hidden-segment-value');
                                total++;
                            } else {
                                utils.addRemoveClass($(this).parent(), 'remove');
                                $("#error_count_employees").addClass('hidden-segment-value');
                            }
                        }

                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }

                if (typeElement == 'hidden') {
                    if (value.value == 0 || value.value == '0') {
                        utils.addRemoveClass($(this).parent().parent(), 'add');
                        total++;
                    } else {
                        utils.addRemoveClass($(this).parent().parent(), "remove");
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }
            }
        });

        if (total != 0) {
            // Set template with errors
            utils.setTemplate($source, $destination, { data: $data});
            utils.removeClassHiddenMessage();
        } else {
            utils.addClassHiddenMessage();

            // Update the steps
            $("#step-2").removeClass('active').addClass('completed');
            $("#accordion-2").removeClass('active').addClass('completed');
            $("#content-2").removeClass('active').addClass('completed');
            $("#title-2").removeClass('active').addClass('completed');

            $("#step-3").removeClass('disabled').addClass('active');
            $("#accordion-3").removeClass('disabled-content');
            $("#content-3").addClass('active');
            $("#title-3").addClass('active');

            $('html, body').animate({
                scrollTop: ($('#test_section_three').offset().top)
            },500);
        }
    });

    /**
     * Event will be added to check the third section
     * that the user will be added and is going to check
     * and select different options will be used
     * to validate the final price of the insurance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.positive.button.section-4-button").click(function (evt) {
        evt.preventDefault();

        var total        = 0;
        var $this        = $(this);
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_third_section");
        var $data        = "";

        $(".required.third_section").each(function (i, value) {
            var typeElement = utils.typeOfElement(value);

            if (value.tagName == 'INPUT') {
                if (typeElement == 'text') {
                    if (value.value == '') {
                        utils.addRemoveClass($(this).parent(), 'add');
                        total++;
                    } else {
                        utils.addRemoveClass($(this).parent(), 'remove');
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }

                if (typeElement == 'hidden') {
                    if (value.value == 0 || value.value == '0') {
                        utils.addRemoveClass($(this).parent().parent(), 'add');
                        total++;
                    } else {
                        utils.addRemoveClass($(this).parent().parent(), 'remove');
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }
            }
        });

        if (total != 0) {
            // Set template with errors
            utils.setTemplate($source, $destination, { data: $data});
            utils.removeClassHiddenMessage();
        } else {
            // Add information about hidden values not required
            obj = utils.createObjectForm("controles", $("#controles").val());
            obj = utils.createObjectForm("porcentaje_comision", $("#porcentaje_comision").val());
            obj = utils.createObjectForm("apreciacion_riesgo", $("#apreciacion_riesgo").val());
            obj = utils.createObjectForm("tasa_adicional", $("#tasa_adicional").val());
            obj = utils.createObjectForm("tienda_abarrotes", $("#tienda_abarrotes").val());
            obj = utils.createObjectForm("transportadores", $("#transportadores").val());
            obj = utils.createObjectForm("restaurantes", $("#restaurantes").val());
            obj = utils.createObjectForm("gasolineras", $("#gasolineras").val());

            var accountingPrimaNeta  = 0;
            var accountingDeducibles = 0;
            var accountingTotal      = 0;

            // We need to create a request to get the final data
            // URL
            var url = '/actosFraudulentos/makesCalculationData';
            req.postApi(url, obj).done(function (response) {
                if (response.mensaje1 == '' && response.mensaje2 == '' && response.mensaje3 == '') {
                    // Do all the process of the display the message error or the correct data in the prices
                    accountingPrimaNeta  = accounting.formatNumber(response.prima_neta, 2, ",", ".");
                    accountingDeducibles = accounting.formatNumber(response.deducibles, 2, ",", ".");
                    accountingTotal      = accounting.formatNumber(response.total, 2, ",", ".");

                    $("#prima_neta").val(accountingPrimaNeta);
                    $("#deducible").val(accountingDeducibles);
                    $("#total").val(accountingTotal);

                    utils.addClassHiddenMessage();
                    // Update the steps
                    $("#step-3").removeClass('active').addClass('completed');
                    $("#accordion-3").removeClass('active').addClass('completed');
                    $("#content-3").removeClass('active').addClass('completed');
                    $("#title-3").removeClass('active').addClass('completed');

                    $("#step-4").removeClass('disabled').addClass('active');
                    $("#accordion-4").removeClass('disabled-content');
                    $("#content-4").addClass('active');
                    $("#title-4").addClass('active');

                    $(".ui.positive.button.section-5-button").css('display', '');

                    if (response.mensaje4 != "") {
                        $("#detalle_siniestro").text(response.mensaje4).addClass('required').parent().removeClass('hidden-segment-value');
                    }

                    $('html, body').animate({
                        scrollTop: ($('#test_section_four').offset().top)
                    },500);
                } else {
                    // error messaje
                    var $source1      = $("#errors_messages").html();
                    var $destination1 = $("#error_four_section");
                    var $data1        = "";
                    var mensajes      = "";

                    accountingPrimaNeta  = accounting.formatNumber(response.prima_neta, 2, ",", ".");
                    accountingDeducibles = accounting.formatNumber(response.deducibles, 2, ",", ".");

                    $("#prima_neta").val(accountingPrimaNeta);
                    $("#deducible").val(accountingDeducibles);
                    $("#total").val("-");

                    if (response.mensaje1 != "") {
                        mensajes = response.mensaje1 + "\n";
                    }
                    if (response.mensaje2 != "") {
                        mensajes = response.mensaje2 + "\n";
                    }
                    if (response.mensaje3 != "") {
                        mensajes = response.mensaje3 + "\n";
                    }

                    if (response.mensaje4 != "") {
                        $("#detalle_siniestro").text(response.mensaje4).addClass('required').parent().removeClass('hidden-segment-value');
                    }

                    utils.setTemplate($source1, $destination1, { data: mensajes});
                    utils.removeClassHiddenMessage();
                    // Update the steps
                    $("#step-3").removeClass('active').addClass('completed');
                    $("#accordion-3").removeClass('active').addClass('completed');
                    $("#content-3").removeClass('active').addClass('completed');
                    $("#title-3").removeClass('active').addClass('completed');

                    $("#step-4").removeClass('disabled').addClass('active');
                    $("#accordion-4").removeClass('disabled-content');
                    $("#content-4").addClass('active');
                    $("#title-4").addClass('active');

                    $(".ui.positive.button.section-5-button").css('display', 'none');

                    $('html, body').animate({
                        scrollTop: ($('#test_section_four').offset().top)
                    },500);
                }
            });
        }
    });

    /**
     * Event will fire once the user clicks the button
     * to see the final price for this insurance. The
     * final value will be calculated from the last options
     * selected by the user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.positive.button.section-5-button").click(function (evt) {
        evt.preventDefault();

        var $total       = 0;
        var $this        = $(this);
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_four_section");
        var $data        = "";

        $(".required.four_section").each(function (i, value) {
            var typeElement = utils.typeOfElement(value);

            if (typeElement == 'textarea') {
                if (value.value == '') {
                    utils.addRemoveClass($(this).parent(), 'add');
                    total++;
                } else {
                    utils.addRemoveClass($(this).parent(), "remove");
                    obj = utils.createObjectForm(value.id, value.value);
                }
            }

            if (value.tagName == 'INPUT') {
                if (typeElement == 'text') {
                    if (value.value == '') {
                        utils.addRemoveClass($(this).parent(), 'add');
                        total++;
                    } else {
                        utils.addRemoveClass($(this).parent(), 'remove');
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }
            }
        });

        if ($total != 0) {
            alert("holas");
            // Set template with errors
            utils.setTemplate($source, $destination, { data: $data});
            utils.removeClassHiddenMessage();
        } else {
            // We need to create a request to get the final data
            // First section of the preview
            if (obj.tipo_persona == 'moral') {
                $("#tipo_asegurado").text("Nombre de la Empresa:");
                $("#nombre_asegurado_txt").text(obj.empresa);
            } else {
                $("#tipo_asegurado").text("Nombre del Asegurado:");
                $("#nombre_asegurado_txt").text(obj.nombre_asegurado);
            }
            
            if(obj.nombre_contacto != '') {
                $("#contacto_name").removeClass('hidden-segment-value');
                $("#nombre_contacto_txt").text(obj.nombre_contacto);
            } else {
                $("#contacto_name").addClass('hidden-segment-value');
            }
            $("#calle_numero_txt").text(obj.calle_numero);
            $("#rfc_txt").text(obj.rfc);
            $("#colonia_txt").text(obj.colonia);
            $("#del_mun_txt").text(obj.del_mun);
            $("#cp_txt").text(obj.cp);

            // Second section of the preview
            $("#vigencia_desde_txt").text(obj.vigencia_desde);
            $("#vigencia_hasta_txt").text(obj.vigencia_hasta);
            $("#renovacion_txt").text(obj.renovacion);
            $("#forma_pago_txt").text(utils.ucwords(obj.forma_pago));
            $("#ingresos_anuales_txt").text("$ " + accounting.formatNumber(obj.ingresos_anuales, 2, ",", "."));
            $("#maximo_txt").text(obj.maximo_dinero);
            $("#no_empleados_txt").text(obj.no_empleados);
            $("#maximo_emp_txt").text(obj.maximo_empleados);
            $("#limite_req_txt").text(obj.limite_requerido);
            $("#desde_txt").text("$ " + accounting.formatNumber(obj.desde, 2, ",", "."));
            $("#hasta_txt").text("$ " + accounting.formatNumber(obj.hasta, 2, ",", "."));
            $("#giro_empresa_txt").text(obj.giro_empresa);
            $("#no_sucursales_txt").text(obj.no_sucursales);

            // Third section of the preview
            //var restaurant = (obj.restaurantes == "1-0") ? "N/A" : "SI";
            //var tienda     = (obj.tienda_abarrotes == "1-0") ? "N/A" : "SI";
            //var transport  = (obj.transportadores == "1-0") ? "N/A" : "SI";
            //var gasolinera = (obj.gasolineras == "1-0") ? "N/A" : "SI";

            $("#auditoria_txt").text(obj.auditoria);
            $("#func_deberes_txt").text(obj.transaccion);
            $("#cuentas_clientes_txt").text(obj.pagos);
            $("#conteo_inventarios_txt").text(obj.conteo);
            $("#conciliaciones_txt").text(obj.conciliaciones);
            $("#segregadas_txt").text(obj.segregadas);
            $("#segregadas_ind_txt").text(obj.intervencion);
            $("#historial_sin_txt").text(obj.siniestro);
            $("#controles_txt").text(obj.controles);
            $("#ultimo_ano_txt").text("$ " + obj.ultimo_ano);
            $("#penultimo_ano_txt").text("$ " + obj.penultimo_ano);
            $("#antepenultimo_ano_txt").text("$ " + obj.antepenultimo_ano);
            $("#apreciacion_riesgo_txt").text(obj.apreciacion_riesgo);
            $("#porcentaje_comision_txt").text(obj.porcentaje_comision + "%");
            $("#tasa_adicional_txt").text(obj.tasa_adicional);
            $("#tienda_abarrote_txt").text(obj.tienda_abarrotes);
            $("#trans_bienes_txt").text(obj.transportadores);
            $("#restaurant_bar_txt").text(obj.restaurantes);
            $("#gasolineras_txt").text(obj.gasolineras);

            // Four section of the preview
            if (typeof obj.detalles_siniestros != undefined) {
                $("#detalles_siniestros_txt").parent().removeClass('hidden-segment-value');
                $("#detalles_siniestros_txt").text(obj.detalles_siniestros);
            } else {
                $("#detalles_siniestros_txt").parent().addClass('hidden-segment-value');
            }

            var primaNetaFormat  = accounting.formatNumber(obj.prima_neta, 2, ",", ".");
            var deduciblesFormat = accounting.formatNumber(obj.deducible, 2, ",", ".");
            var totalFormat      = accounting.formatNumber(obj.total, 2, ",", ".");

            $("#prima_neta_txt").text("$ " + primaNetaFormat);
            $("#deducible_10_txt").text("$ " + deduciblesFormat);
            $("#total_txt").text("$ " + totalFormat);


            utils.addClassHiddenMessage();
            // Update the steps
            $("#step-4").removeClass('active').addClass('completed');
            $("#accordion-4").removeClass('active').addClass('completed');
            $("#content-4").removeClass('active').addClass('completed');
            $("#title-4").removeClass('active').addClass('completed');

            $("#step-5").removeClass('disabled').addClass('active');
            $("#accordion-5").removeClass('disabled-content');
            $("#content-5").addClass('active');
            $("#title-5").addClass('active');

            $('html, body').animate({
                scrollTop: ($('#test_section_five').offset().top)
            },500);
        }
    });

    /**
     * Event will be fired once the user click
     * the button when he/she is confirming that
     * the data has been typed correctly
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".positive.button.nextButton-5").click(function (evt) {
        evt.preventDefault();

        // Send information to save in the server
        var url = "/actosFraudulentos/saveData";

        req.postApi(url, obj).done(function (response) {
            if (response.flag == 1 || response.flag == '1') {
                // Set the complete path for download PDF using the browser
                $(".print.actos_fraudulentos_pdf").attr('id', response.pathFile);

                setTimeout(function () {
                    utils.displayModal();
                }, 2500);
            } else {
                // error
            }
        });
    });

    /**
     * Event will be used to validate all the information
     * and it will be used to displaye the information
     * typed by the user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
     $(".print.actos_fraudulentos_pdf").click(function (evt) {
         // Sending the request to the url to displaying the PDF file in the browser
         var url = $(this).attr('id');

         // Send PDF to print
         window.open(url, "_blank").focus();

         // Reload the page
         document.location.reload(true);
     });

    /**
     * Events will be used to return the before section
     * the user has filled in with data required
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.negative.button.section-2-button-back").click(function (evt) {
        evt.preventDefault();

        //return back the accordion
        $("#step-2").removeClass("active").addClass("disabled");
        $("#accordion-2").addClass("disabled-content");
        $("#content-2").removeClass("active");
        $("#title-2").removeClass("active");

        $("#step-1").removeClass("completed").addClass("active");
        $("#accordion-1").removeClass("disabled-content");
        $("#content-1").addClass('active');
        $("#title-1").addClass('active');
    });

    $(".ui.negative.button.section-3-button-back").click(function (evt) {
        evt.preventDefault();

        //return back the accordion
        $("#step-3").removeClass("active").addClass("disabled");
        $("#accordion-3").addClass("disabled-content");
        $("#content-3").removeClass("active");
        $("#title-3").removeClass("active");

        $("#step-2").removeClass("completed").addClass("active");
        $("#accordion-2").removeClass("disabled-content");
        $("#content-2").addClass('active');
        $("#title-2").addClass('active');
    });

    $(".ui.negative.button.section-4-button-back").click(function (evt) {
        evt.preventDefault();

        //return back the accordion
        $("#step-4").removeClass("active").addClass("disabled");
        $("#accordion-4").addClass("disabled-content");
        $("#content-4").removeClass("active");
        $("#title-4").removeClass("active");

        $("#step-3").removeClass("completed").addClass("active");
        $("#accordion-3").removeClass("disabled-content");
        $("#content-3").addClass('active');
        $("#title-3").addClass('active');
    });

    $(".ui.negative.button.backButton-5").click(function (evt) {
        evt.preventDefault();

        //return back the accordion
        $("#step-5").removeClass("active").addClass("disabled");
        $("#accordion-5").addClass("disabled-content");
        $("#content-5").removeClass("active");
        $("#title-5").removeClass("active");

        $("#step-4").removeClass("completed").addClass("active");
        $("#accordion-4").removeClass("disabled-content");
        $("#content-4").addClass('active');
        $("#title-4").addClass('active');
    });
});
