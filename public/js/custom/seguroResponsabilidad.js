/**
 * Class will contain all the events enabled
 * to do different events. Those events will
 * give different functionality and the
 * page will works with these events that
 * will do different actions
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
	// Create an object
	var utils = new UtilElements();
	var req	  = new Request();
	var obj   = "";

	// Hide employee name's field and unchecking checkbox
    $("#vendedor").prop('checked', false);
    $("#name_seller").hide();
    $(".print").addClass("seguro-responsabilidad-pdf");
    $("#tel").numeric();
    $("#no_empleados").numeric();

    /**
     * Event will be used to activate the
     * functionality for the checkbox is going to
     * active the hide or show the section
     * of the company or normal user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $('input:checkbox.tperson').click(function() {
        utils.checkboxAsRadioButton(this, ".tperson");
    });

    /**
     * Check if the field used for type the
     * seller's name is enabled or disabled,
     * in case the value is disabled, should
     * add the class to validate if the value is empty
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#vendedor").change(function () {
    	if ($("#" + this.id).prop('checked')) {
    		$("#name_seller").addClass('required').show();
    	} else {
    		$("#name_seller").removeClass('required').val('').hide();
    	}
    });

    /**
     * Event will fire the inputs depending the selection done
     * by the user in the checkbox, the user is able to select
     * if is a company that is requesting the insurance or is
     * a person
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".checkdata").change(function () {
        var value = $(this).data('value');

        if (value == 'fisica') {
             $("#fisica").removeClass('hidden-segment-value');
             $("#moral").addClass('hidden-segment-value');
             $("#tipo_persona").val(value);

             // clean fields
             $("#empresa").val('').addClass('ignore');
             $("#nombre_contacto").val('').addClass('ignore');
             $("#nombres").removeClass('ignore');
             $("#a_paterno").removeClass('ignore');
             $("#a_materno").removeClass('ignore');
         } else {
             $("#moral").removeClass('hidden-segment-value');
             $("#fisica").addClass('hidden-segment-value');
             $("#tipo_persona").val(value);

             // clean the fields
             $("#nombres").val('').addClass('ignore');
             $("#a_paterno").val('').addClass('ignore');
             $("#a_materno").val('').addClass('ignore');
             $("#empresa").removeClass('ignore');
             $("#nombre_contacto").removeClass('ignore');
         }
     });

    /**
     * Validate the fields in the form
     * before to displaye the data selected by the user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".submit-button").click(function (event) {
    	event.preventDefault();

    	var $this  		 = $(this);
    	var count 		 = 0;
    	var $source 	 = $("#errors_messages").html();
    	var $destination = $("#error_first_section");
    	var $data		 = "";

    	$(".required").each(function (i, value) {
    		if (value.tagName == 'INPUT') {
    			if (value.type == 'hidden') {
    				if (value.value == 0 || value.value == '0') {
    					utils.addRemoveClass($(this).parent().parent(), "add");
    					count++;
    				} else {
    					utils.addRemoveClass($(this).parent().parent(), "remove");
    					obj = utils.createObjectForm(value.id, value.value);
    				}
    			}

    			if (value.type == 'text') {
                    var inputValue = $(this).hasClass('ignore');

                    if (inputValue == false) {
    	    			if (value.value == '') {
    	    				utils.addRemoveClass($(this).parent(), "add");
    	    				count++;
    	    			} else {
    	    				utils.addRemoveClass($(this).parent(), "remove");
    	    				obj = utils.createObjectForm(value.id, value.value);
    	    			}
                    }
	    		}

                if (value.type == 'email') {
                    var emailValue = utils.validateEmailData(value.value);

                    if (value.value == '') {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        count++;
                    } else if (emailValue == false) {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        count++;
                    } else {
                        // remove class error
                        utils.addRemoveClass($(this).parent(), "remove");
                        // add value to the object
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }
    		}
    	});

		if (count != 0) {
			utils.setTemplate($source, $destination, { data: $data });
			utils.removeClassHiddenMessage();
		} else {
			utils.addClassHiddenMessage();

			// check if vendedor is checked to added to the obj object
            if ($("#vendedor").prop('checked')) {
              obj = utils.createObjectForm('vendedor', 'Si');
            } else {
              obj = utils.createObjectForm('vendedor', '');
              obj = utils.createObjectForm('nombre_vendedor', '');
            }

            // check if the user is company or is person
            if ($("#tipo_persona").val() == 'fisica') {
                obj = utils.createObjectForm("tipo_persona", 0);
                obj = utils.createObjectForm("empresa", "");
                obj = utils.createObjectForm("nombre_contacto", "");

                // add the data to the preview
                $("#nombre_txt").text(obj.nombres);
                $("#ap_txt").text(obj.a_paterno);
                $("#am_txt").text(obj.a_materno);

                // add/remove hidden class
                $("#nombre_txt").parent().removeClass('fisica_temp');
                $("#ap_txt").parent().removeClass('fisica_temp');
                $("#am_txt").parent().removeClass('fisica_temp');
                $("#empresa_txt").parent().addClass('moral_temp');
                $("#nombre_contacto_txt").parent().addClass('moral_temp');
            } else {
                obj = utils.createObjectForm('tipo_persona', 1);
                obj = utils.createObjectForm("a_materno", "");
                obj = utils.createObjectForm("a_paterno", "");
                obj = utils.createObjectForm("nombres", "");
                obj = utils.createObjectForm("nombre_contacto", $("#nombre_contacto").val());

                // add the data to the preview
                $("#empresa_txt").text(obj.empresa);
                $("#nombre_contacto_txt").text(obj.nombre_contacto);

                // add/remove hidden class
                $("#nombre_txt").parent().addClass('fisica_temp');
                $("#ap_txt").parent().addClass('fisica_temp');
                $("#am_txt").parent().addClass('fisica_temp');
                $("#empresa_txt").parent().removeClass('moral_temp');
                $("#nombre_contacto_txt").parent().removeClass('moral_temp');
            }

            // Set all the values in the object
            obj = utils.createObjectForm('sublimite', $("#sublimite").val());
            obj = utils.createObjectForm('deducible', $("#deducible").val());
            obj = utils.createObjectForm('laboral', $("#laboral").val());
            obj = utils.createObjectForm('prima', $("#prima").val());

            var facturas 	  = obj.facturacion.split('-');
            var indemnizacion = obj.indemnizacion.split('-');

            // Set the information in the template dinamically
            $("#eres_vendedor").text(obj.vendedor);
            $("#nombre_vendedor").text(obj.name_seller);
            $("#nombre_empresa_txt").text(' '); //obj.nombre);
            $("#direccion_empresa_txt").text(obj.direccion);
            $("#email_txt").text(obj.email);
            $("#tel_txt").text(obj.tel);
            $("#fecha_txt").text(obj.fecha);
            $("#numero_empleados_txt").text(obj.no_empleados);
            $("#facturacion_txt").text(facturas[0]);
            $("#limite_txt").text(indemnizacion[0]);
            $("#sublimite_txt").text(obj.sublimite);
            $("#deducible_txt").text(obj.laboral);
            $("#dedicible_cantidad_txt").text(obj.deducible);
            $("#prima_neta_txt").text(obj.prima);

            // Hide this section and add the resume section
            $("#step-1").removeClass("active").addClass("completed");
            $("#accordion-1").addClass("disabled-content");
            $("#title-1").removeClass("active");
            $("#content-1").removeClass("active");


            $("#step-2").removeClass("disabled").addClass("active");
            $("#accordion-2").removeClass("disabled-content");
            $("#title-2").addClass("active");
            $("#content-2").addClass("active");
		}
    });

    /**
     * Method will be used to fill the information
     * will be enabled to the user. After this event
     * is called, will be done a ajax call to get the
     * correct informaton displayed
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.selection.dropdown").change(function () {
    	var $this 	= $(this);
    	var $url    = $this.data('url');
    	var $params = "";
    	var $data	= "";

    	if ($url == 'getLimitCompensation') {
    		var completeValue = $("#facturacion").val();
    		var id = completeValue.split("-");

    		var $source 	 = $("#second_dropdown").html();
    		var $destination = $(".menu.limite_menu");
    		$params 		 = "/" + parseInt(id[1]);
    	} else {
    		var completeValue = $("#indemnizacion").val();
    		var $id = completeValue.split("-");

  			$params  = "/" + parseInt($id[2]) + "/" + parseInt($id[1]);
    	}

    	req.getApi('/seguroResponsabilidad/' + $url + $params).done(function (response) {
    		if (response.flag == 1 || response.flag == '1') {
    			if ($url == 'getLimitCompensation') {
    				$("div").removeClass('deshabilitar_campo_main');
    				utils.setTemplate($source, $destination, { data: response.options});
    			} else {
    				$("div").removeClass('deshabilitar_campo');

    				$("#sublimite_label").text(response.info.sublimite + " USD");
    				$("#sublimite").val(response.info.sublimite + " USD");

    				$("#deducible_label").text(response.info.deducible + " USD");
    				$("#deducible").val(response.info.deducible + " USD");

    				$("#laboral_label").text(response.info.laboral + " USD");
    				$("#laboral").val(response.info.laboral + " USD");

    				$("#prima_label").text(response.info.prima + " USD");
    				$("#prima").val(response.info.prima + " USD");
    			}
    		}
    	});
    });

    /**
     * Event will be used to return again to the last
     * section and will the change to edit the information
     * selected by the user
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.button.backButton").click(function (e) {
    	e.preventDefault();

    	// Hide this section and add the resume section
    	$("#step-1").removeClass("disabled").addClass("active");
        $("#accordion-1").removeClass("disabled-content");
        $("#title-1").addClass("active");
        $("#content-1").addClass("active");

        $("#step-2").removeClass("active").addClass("completed");
        $("#accordion-2").addClass("disabled-content");
        $("#title-2").removeClass("active");
        $("#content-2").removeClass("active");
    });

    /**
     * Event will be used to send the information for
     * create the report file with all the data set
     * by the user, also will be used to send the note
     * with all the data of this insurance
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".ui.button.nextButton").click(function (evt) {
    	evt.preventDefault();

    	var url = '/seguroResponsabilidad/save';
    	req.postApi(url, obj).done(function (response) {
    		if (response.flag == 1 || response.flag == '1') {
    			$(".print.seguro-responsabilidad-pdf").attr('id', response.pathFile);
    			setTimeout(function () {
    				utils.displayModal();
    			}, 2500);
    		}
    	});
    });

	/**
     * Event will reload all the information once the
     * user has clicked on the cancel button
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".cancel.cancelPrint").click(function (evt) {
        document.location.reload(true);
    });

    /**
     * Event will be used to print the information
     * in the PDF file in the browser although the
     * the file was sent via Email
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".print.seguro-responsabilidad-pdf").click(function (evt) {
        // Sending the request to the url to displaying the PDF file in the browser
        var url = $(this).attr('id');

        // Send PDF to print
        window.open(url, "_blank").focus();

        // Reload the page
        document.location.reload(true);
    });
});
