/**
 * Class used for manage all the information
 * and actions will be enabled in this module.
 * The differents events will be attached to
 * the elements
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
    // Create an object
    var utils         = new UtilElements();
    var req           = new Request();
    var quotationData = new Object();
    var dataResponse  = new Object();

    //Check just one checkbox per group
    //Cobertura nacional INICIO
    // $('input:checkbox').click(function() {
    //     $('input:checkbox').not(this).prop('checked', false);
    // });
    //Cobertura nacional FIN

    //Calendar Vigencia
    $('#finVigencia').datepick({ dateFormat: 'dd-mm-yyyy' });

    //Filled Vigencia today + 1 year
    var fecha = new Date(),
        dia = fecha.getDate(),
        mes = fecha.getMonth() + 1,
        anio = fecha.getFullYear() + 1;

    $("#finVigencia").val(dia+"-"+mes+"-"+anio);

    //Firstable the fnVigencia must be disabled, until prorrateada checkbox is click it
    $( "#finVigencia" ).prop("disabled", true);

    /**
     * Validate the option will be displayed depending on the
     * value selected by the user in the form
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $('.checkdata').change(function () {
        var result = $(this).data('value');

        if(result=="asesorchk") { //Checkbox vendedor
            if ($("#asesorcheckbox").is(":checked")) {
                $( "#asesor-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "field" );
            }else{
                $( "#asesor-div" ).removeClass( "field" ).addClass( "ui segment hidden-segment-value" );
            }
        }

        if(result=="coberturaExtranjero") { //Checkbox Cobertura en el Extranjero
            if ($("#coberturaExtranjero").is(":checked")) {
                $( "#coberturaExtranjeroDeducible-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "fields" );
            }else {
                $( "#coberturaExtranjeroDeducible-div" ).removeClass( "fields" ).addClass( "ui segment hidden-segment-value" );
                $("#coberturaDeducible").val("");
            }
        }

        if (result=="emergenciaExtranjero"){ //Checkbox Emergencia en el Extranjero
            if ($("#emergenciaExtranjero").is(":checked")) {
                $( "#emergenciaExtranjeroDeducible-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "fields" );
            }else {
                $( "#emergenciaExtranjeroDeducible-div" ).removeClass( "fields" ).addClass( "ui segment hidden-segment-value" );
                $("#emergenciaDeducible").val("");
            }
        }

        if (result=="prorrateada"){ //Checkbox Prorrateada
            if ($("#prorrateada").is(":checked")) {
                $( "#finVigencia" ).prop("disabled", false);
            }else {
                $( "#finVigencia" ).prop("disabled", true);
            }
        }

        //Cobertura nacional INICIO
        // if(result=="nacional") { //Checkbox cobertura nacional
        //     if ($("#nacionalcheckbox").is(":checked")) {
        //         $( "#nacional-div" ).removeClass( "ui segment hidden-segment-value" ).addClass( "fields" );
        //     }else{
        //         $( "#nacional-div" ).removeClass( "fields" ).addClass( "ui segment hidden-segment-value" );
        //     }
        // }
        //
        // if (result=="nacional_internacional"){ //Checkbox cobertura nacional e internacional
        //     $( "#nacional-div" ).removeClass( "fields" ).addClass( "ui segment hidden-segment-value" );
        // }
        //Cobertura nacional FIN
    });

    //Validate only numbers in some fields
    $(".edad").numeric();
    $(".tel").numeric();
    $(".deducible").numeric();

    //Print pdf once ready
    $(".print").addClass("cotiza-minor-pdf");

    /**
     * Add row in integrantes table
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#addRowIntegrantesTable").click(function(){
        addRowIntegrantesTable();
    });

    /**
     * Delete row in integrantes table
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(document).on('click', '#deleteRow', function () {
        //Save the total of integrantes added
        $('#integrantesRowsHidden').val($('#integrantesRowsHidden').val()-1);

        var whichtr = $(this).closest('tr').transition('fade');
        whichtr.remove();

        var integrantesPorEnviarArray = getIntegrantes();
        var existeTitularEnArray = false;

        for (var i = 0; i < integrantesPorEnviarArray.length; i++) {
            if (integrantesPorEnviarArray[i]['integrante'] == 'TITULAR') {
                existeTitularEnArray = true;
                break;
            }
        }

        //Verifica si existe un titular en el arreglo de integrantes, si no envia un error al validar la forma
        existeTitularEnArray?$('#existeTitularHidden').val(1):$('#existeTitularHidden').val(0);

        return false;
    });

/**
 * function that add row in integrantes table
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
function addRowIntegrantesTable() {
    var newIntegrante = $('#newIntegranteDropdown').dropdown('get text').toUpperCase();
    var newNombre     = $('#newNombre').val().toUpperCase();
    var newSexo       = $('#newSexoDropdown').dropdown('get text').toUpperCase();
    var newEdad       = $('#newEdad').val().toUpperCase();
    var table_length  = ($("#integrantes_table > tbody > tr").length) - 1;

    if (integrantesFormValidate(newIntegrante, newNombre, newSexo, newEdad, table_length)) {
        var markup =
                    "<tr id='rowIntegrantes"+table_length+"'>"+
                        "<td class='center aligned' id='newIntegrante"+table_length+"'>"+newIntegrante+"</td>"+
                        "<td class='center aligned' id='newNombre"+table_length+"'>"+newNombre+"</td>"+
                        "<td class='center aligned' id='newSexo"+table_length+"'>"+newSexo+"</td>"+
                        "<td class='center aligned' id='newEdad"+table_length+"'>"+newEdad+"</td>"+
                        "<td class='center aligned'>"+
                            "<font class='ui fluid red button' id='deleteRow'>"+
                                "-"+
                            "</font>"
                        "</td>"+
                    "</tr>";

        $("#integrantes_table tbody").append(markup);

        //Clear values
        $('#newIntegranteDropdown').dropdown('restore defaults');
        $('#newIntegrante').val("");
        $('#newNombre').val("");
        $('#newSexoDropdown').dropdown('restore defaults');
        $('#newEdad').val("");

        //Save the total of rows added
        $('#integrantesRowsHidden').val(table_length+1);
        //Save integrante titular
        if (newIntegrante == 'TITULAR') {
            $('#existeTitularHidden').val(1);
        }
    }
}

/**
   * Event used for return to the past section if the user
   * click the return button to modify some information
   * added to the data filled in beforewi
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com> based on Ruben Alonso Cortes Mendoza method
   * @version 1.0
   * @company ruvicdev
   */
$(".ui.negative.button.backButton-2").click(function (evt) {
      evt.preventDefault();

      //return back the accordion
      $("#step-2").removeClass("active").addClass("disabled");
      $("#accordion-2").addClass("disabled-content");
      $("#content-2").removeClass("active");
      $("#title-2").removeClass("active");

      $("#step-1").removeClass("completed").addClass("active");
      $("#accordion-1").removeClass("disabled-content");
      $("#content-1").addClass('active');
      $("#title-1").addClass('active');
  });

  /**
     * Event used for visualize medical network
     *
     * @author Victor M. Garza Chequer <victor.chequer@gmail.com> based on Ruben Alonso Cortes Mendoza method
     * @version 1.0
     * @company ruvicdev
     */
     $("#redMedicaButton").click(function (evt) {
         evt.preventDefault();

         if( $("#portapdf").css("display") == "inline"  ){
             $("#portapdf").css("display", "none");
             $("#redMedicaButton").html("Ver Red Médica");
         }else {
             $("#portapdf").css("display", "inline");
             $("#redMedicaButton").html("Ocultar Red Médica");
         }

         //window.open('../public/files/RedMedicaClubSalud/RedMedica_Gdl_Mty_Df.pdf');
    });

  /**
   * Save data and get cotization do POST in "saveData"
   * function on CubreDeducibleGMM php file
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".ui.positive.button.submit-button-2").click(function (evt) {
      evt.preventDefault();

      //Save necessary data into quotationData in order to show it in PDF file
      //Save prima neta anual per member in the quotationData array
      quotationData.integrantes = dataResponse.integrantesArray;

      //sending the information via ajax request and send email with PDF file attached
      $(".ui.page.dimmer").addClass("active");

      var request =
        $.ajax({ type     : 'POST',
                 url      : './cubreDeducibleGMM/saveData',
                 dataType : "json",
                 data     : JSON.stringify(quotationData),
                 headers  : {
                     "Accept"       : "application/json",
                     "Content-Type" : "application/json"
                 }
               }
              );

        request.done(function (response, textStatus, jqXHR){
            if (response.status == "success") {
                // set the complete path file in button
                $(".print.cotiza-minor-pdf").attr('id', response.pathFile);

                setTimeout(function () {
                    utils.displayModal();
                }, 2500);
            } else {
                // error message
            }
        });

        request.always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass("active")}, 2500);
        });

    //   var url = "./insuranceMinorME/saveData";
    //   req.postApi(url, quotationData).done(function (response) {
    //       if (response.status == "success") {
    //           // set the complete path file in button
    //           $(".print.cotiza-minor-pdf").attr('id', response.pathFile);
    //
    //           setTimeout(function () {
    //               utils.displayModal();
    //           }, 2500);
    //       } else {
    //           // error message
    //       }
    //   });
  });

  /**
   * Event used for giving the option to the user
   * to download and print the file with all the
   * information set during the process
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  $(".print.cotiza-minor-pdf").click(function (evt) {
     // sending the request to the URL for displaying the
     url = $(this).attr('id');

     // send PDF file to print
     window.open(url, "_blank").focus();

     // create the request for reloading the page
     //var reloadUrl = "/merchandise/index";
     //req.getApi(reloadUrl);
     document.location.reload(true);
  });

  /**
   * Function that send data to php and calculate quotes
   * This funcion is called by onSuccess function of validate form
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com> based on Ruben Alonso Cortes Mendoza method
   * @version 1.0
   * @company ruvicdev
   */
  function previewData() {
      //Get form data Object-----------------------------------------------
      quotationData.asesor               = $("#asesor").val()==""?"Sin Asesor":$("#asesor").val().toUpperCase();
      quotationData.fullname             = $("#fullname").val().toUpperCase();
      quotationData.email                = $("#email").val();
      quotationData.telefono             = $("#telefono").val();
      quotationData.estado               = $("#estado").val();
      quotationData.ciudad               = $("#ciudad").val();
      quotationData.finVigencia          = $("#finVigencia").val();
      quotationData.deduciblePoliza      = $("#deduciblePoliza").val();
      quotationData.coberturaExtranjero  = $("#coberturaExtranjero").is(":checked")?"Amparada":"Excluida";
      quotationData.coberturaDeducible   = $("#coberturaDeducible").val();
      quotationData.emergenciaExtranjero = $("#emergenciaExtranjero").is(":checked")?"Amparada":"Excluida";
      quotationData.emergenciaDeducible  = $("#emergenciaDeducible").val();
      quotationData.prorrateada          = $("#prorrateada").is(":checked")?"Prorrateada":"No Prorrateada";
      //------------------------------------------------------------------

      //Add integrantes array to quotationData Object
      quotationData.integrantes = getIntegrantes();

      //Send data via POST
      $(".ui.page.dimmer").addClass("active");

      var request =
        $.ajax({ type     : 'POST',
                 url      : './cubreDeducibleGMM/previewData',
                 dataType : "json",
                 data     : JSON.stringify(quotationData),
                 headers  : {
                     "Accept"       : "application/json",
                     "Content-Type" : "application/json"
                 }
               }
              );

        request.done(function (response, textStatus, jqXHR){
            if (response.status == "success") {
                dataResponse = response;

                fillIntegrantesTablePreview(response.integrantesArray);
            }else {
              // Send Error
            }
        });

        request.always(function () {
            setTimeout(function () { $(".ui.page.dimmer").removeClass("active")}, 2500);
        });

    //   var url = "./insuranceMinorME/previewData";

    //   req.postApi(url, quotationData).done(function (response) {
    //       if (response.status == "success") {
    //           dataResponse = response;
    //
    //           fillIntegrantesTablePreview(response.integrantesArray);
    //       }else {
    //         //   Send Error
    //       }
    //   });
  }

  function fillIntegrantesTablePreview(integrantesArray) {
      var integrantesTableTbody = "";
      $("#integrantes_table_preview tbody tr").remove();

      for (var i in integrantesArray) {
          integrantesTableTbody = integrantesTableTbody +
                                  "<tr id='rowIntegrantesPreview'>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].integrante+"</td>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].nombre+"</td>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].sexo+"</td>"+
                                      "<td class='center aligned' '>"+integrantesArray[i].edad+"</td>"+
                                  "</tr>";
      }

      $("#integrantes_table_preview tbody").append(integrantesTableTbody);
  }

  /**
   * function that retrieve integrantes data
   *
   * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
   * @version 1.0
   * @company ruvicdev
   */
  function getIntegrantes() {
      var integrantesArray = new Array();

      $("#integrantes_table tbody tr").each(function() {
          //Get only tr's with values, that user added
          if (this.id != "") {
              var integranteObj = new Object();

              integranteObj.integrante = $(this).find('td').eq(0).text();
              integranteObj.nombre     = $(this).find('td').eq(1).text();
              integranteObj.sexo       = $(this).find('td').eq(2).text();
              integranteObj.edad       = $(this).find('td').eq(3).text();

              integrantesArray.push(integranteObj);
          }
      });

      return integrantesArray;
  }

/**
 * function that send data capured via POST
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
function goToPreview() {
    //Complete step-1
    $("#step-1").removeClass("active").addClass("completed");
    $("#accordion-1").addClass("disabled-content");
    $("#content-1").removeClass("active");
    $("#title-1").removeClass("active");

    //Active step-2
    $("#step-2").removeClass("disabled").addClass("active");
    $("#accordion-2").removeClass("disabled-content");
    $("#content-2").addClass("active");
    $("#title-2").addClass("active");

    //Filled fields to preview page
    $("#fullname_preview").text($("#fullname").val().toUpperCase());
    $("#email_preview").text($("#email").val().toLowerCase());
    $("#telefono_preview").text($("#telefono").val().toLowerCase());
    $("#estado_preview").text($("#estado").val());
    $("#ciudad_preview").text($("#ciudad").val().toLowerCase());
    $("#finVigencia_preview").text($("#finVigencia").val());
    $("#deduciblePoliza_preview").text($("#deduciblePoliza").val());
    //Checkbox coberturaExtranjero_preview
    $("#coberturaExtranjero").is(":checked")?$('#coberturaExtranjero_preview').prop('checked', true):$('#coberturaExtranjero_preview').prop('checked', false);
    $("#coberturaDeducible_preview").text($("#coberturaDeducible").val()==""?"No Aplica":$("#coberturaDeducible").val());
    //Checkbox emergenciaExtranjero_preview
    $("#emergenciaExtranjero").is(":checked")?$('#emergenciaExtranjero_preview').prop('checked', true):$('#emergenciaExtranjero_preview').prop('checked', false);
    $("#emergenciaDeducible_preview").text($("#emergenciaDeducible").val()==""?"No Aplica":$("#emergenciaDeducible").val());
    //Checkbox prorrateada
    $("#prorrateada").is(":checked")?$('#prorrateada_preview').prop('checked', true):$('#prorrateada_preview').prop('checked', false);

    //Send data (POST) and calculate quotes
    previewData();
}

/**
 * Validate integrantes form
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
function integrantesFormValidate(newIntegrante, newNombre, newSexo, newEdad, table_length) {
    //Validate fields entered
    var errorFlag = false;
    var htmlError = "<ul>";

    //Validate that can only add one titular and one conyuge
    for (i = 0; i < $('#integrantesRowsHidden').val(); i++) {
        if( $("#newIntegrante"+i).text() == "TITULAR" &&  newIntegrante == "TITULAR") {
            htmlError += "<li>Agregue solo un Titular</li>";
            errorFlag = true;
        }

        if( $("#newIntegrante"+i).text() == "CONYUGE" &&  newIntegrante == "CONYUGE") {
            htmlError += "<li>Agregue solo un Conyuge</li>";
            errorFlag = true;
        }
    }

    //Validate titular must be the first integrante.
    if(table_length == 0 && newIntegrante != "TITULAR" && newIntegrante != "SELECCIONE") {
        htmlError += "<li>Agregue primero un Titular</li>";
        errorFlag = true;
    }

    //Validate title of integrante
    if(newIntegrante.trim() == "SELECCIONE") {
        htmlError += "<li>Introdúzca Integrante</li>";
        errorFlag = true;
    }

    //Validate name of integrante
    if(newNombre.trim() == "") {
        htmlError += "<li>Introdúzca Nombre del Asegurado</li>";
        errorFlag = true;
    }

    //Validate sex of integrante
    if(newSexo.trim() == "SEXO") {
        htmlError += "<li>Introdúzca Sexo del Asegurado</li>";
        errorFlag = true;
    }

    //Validate age of integrante
    if(newEdad.trim() == "" || isNaN(newEdad.trim())) {
        htmlError += "<li>Introdúzca Edad del Asegurado válida</li>";
        errorFlag = true;
    }else {
        if(newIntegrante.trim() == "TITULAR" || newIntegrante.trim() == "CONYUGE") {
            if (newEdad.trim() > 60) {
                htmlError += "<li>Edad máxima del asegurado titular o conyuge es de 60 años</li>";
                errorFlag = true;
            }
        }

        if(newIntegrante.trim() == "HIJO(A)") {
            if (newEdad.trim() > 24) {
                htmlError += "<li>Edad máxima del asegurado Hijo(a) es de 24 años</li>";
                errorFlag = true;
            }
        }
    }

    if (errorFlag) { //Show errors
        htmlError += "</ul>";
        $('#errorsIntegrantesDiv').removeClass("hidden-segment-value").addClass("show-errors");
        $('#errorsIntegrantesDiv').html(htmlError);
        return false;
    }else { //Hide errors
        $('#errorsIntegrantesDiv').removeClass("show-errors").addClass("hidden-segment-value");
        $('#errorsIntegrantesDiv').html("");
        return true;
    }
}

/**
 * Validate email
 *
 * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
 $.fn.form.settings.rules.emailValidation = function(value) {
     return utils.validateEmailData(value);
 }

 /**
  * Validate seller name
  *
  * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
  * @version 1.0
  * @company ruvicdev
  */
  $.fn.form.settings.rules.sellerNameValidation = function() {
      var response = true;

      if ($("#asesorcheckbox").is(":checked") && $("#asesor").val() == '') {
          response = false;
      }

      return response;
  }

 /**
  * Validate integrantes
  *
  * @author Victor M. Garza Chequer <victor.chequer@gmail.com>
  * @version 1.0
  * @company ruvicdev
  */
  $.fn.form.settings.rules.integrantesValidation = function(value) {
      var integrantesRowsHidden =  $('#integrantesRowsHidden').val();
      var existeTitularHidden =  $('#existeTitularHidden').val();

      var returnValid = true;

      if (integrantesRowsHidden <= 0 || existeTitularHidden <= 0) {
          returnValid = false;
      }

      return returnValid;
  }

 $('.ui.form')
 .form({
    fields: {
        fullname: {
            identifier: 'fullname',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Capture {name}'
                }
            ]
        },
        email: {
            identifier: 'email',
            rules: [
                {
                  type   : 'emailValidation[value]',
                  prompt : 'Seleccione {name} válido'
                }
            ]
        },
        telefono: {
            identifier: 'telefono',
            rules: [
                {
                  type   : 'regExp',
                  value  : /^[0-9]+$/,
                  prompt : 'Seleccione {name} válido'
                }
            ]
        },
        estadoDropdown: {
            identifier: 'estado',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },
        ciudad: {
            identifier: 'ciudad',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },
        asesorchk: {
            identifier: 'asesor',
            rules: [
                {
                  type   : 'sellerNameValidation',
                  prompt : 'Seleccione Nombre del Vendedor'
                }
            ]
        },
        coberturaDeducible: {
            identifier: 'coberturaDeducible',
            depends: 'coberturaExtranjero',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name} en el Extranjero'
                }
            ]
        },
        emergenciaDeducible: {
            identifier: 'emergenciaDeducible',
            depends: 'emergenciaExtranjero',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name} en el Extranjero'
                }
            ]
        },
        deduciblePoliza: {
            identifier: 'deduciblePoliza',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name} Póliza'
                }
            ]
        },
        /*cobertura: {
            identifier: 'cobertura',
            rules: [
                {
                  type   : 'checked',
                  prompt : 'Seleccione Cobertura'
                }
            ]
        },
        emergencias_extranjero: {
            identifier: 'emergencias_extranjero',
            depends: 'nacionalcheckbox',
            rules: [
                {
                  type   : 'empty',
                  prompt : 'Seleccione {name}'
                }
            ]
        },*/
        /*integrantes: {
            identifier: 'integrantesRowsHidden',
            rules: [
                {
                  type   : 'integrantesValidation[value]',
                  //type   : 'integer[1..100]',
                  prompt : 'Agregue al menos un integrante (Titular) a la cotización'
                }
            ]
        },*/
        existeTitular: {
            identifier: 'existeTitularHidden',
            rules: [
                {
                  type   : 'integrantesValidation[value]',
                  //type   : 'integer[1..100]',
                  prompt : 'Agregue al menos un integrante (Titular) a la cotización'
                }
            ]
        }
    },
    onSuccess: function(event, ) {
        event.preventDefault();
        goToPreview();
    }
  });
});
