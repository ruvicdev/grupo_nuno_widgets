/**
 * Class will be used to get all the
 * functionality enabled in the insurance
 * pets widget. This file will contains the
 * different events attached to the widget
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
$(document).ready(function () {
    // Create an object
    var utils = new UtilElements();
    var req   = new Request();
    var obj   = "";

    // Call Methods directly from the class UtilElements
    utils.closeMessages('.close');
    $("#rfc").css('text-transform', 'uppercase');
    $("#cp").numeric();
    $("#tel").numeric();
    $(".print").addClass("seguro-mascota-pdf");

    // Hide employee name's field and unchecking checkbox
    $("#vendedor").prop('checked', false);
    $("#name_seller").hide();

    // event to put working checkbox as radio button
    $("input:checkbox.tipo-21").click(function () {
        utils.checkboxAsRadioButton(this, ".tipo-21");
    });
    $("input:checkbox.sexo-21").click(function () {
        utils.checkboxAsRadioButton(this, ".sexo-21");
    });
    $("input:checkbox.pedigree-21").click(function () {
        utils.checkboxAsRadioButton(this, ".pedigree-21");
    });
    $("input:checkbox.registro-21").click(function () {
        utils.checkboxAsRadioButton(this, ".registro-21");
    });
    $('input:checkbox.tperson').click(function() {
        utils.checkboxAsRadioButton(this, ".tperson");
    });

    /**
     * Event will fire the inputs depending the selection done
     * by the user in the checkbox, the user is able to select
     * if is a company that is requesting the insurance or is
     * a person
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".checkdata").change(function () {
        var value = $(this).data('value');

        if (value == 'fisica') {
             $("#fisica").removeClass('hidden-segment-value');
             $("#moral").addClass('hidden-segment-value');
             $("#tipo_persona").val(value);

             // clean fields
             $("#empresa").val('').addClass('ignore');
             $("#nombre_contacto").val('').addClass('ignore');
             $("#nombre").removeClass('ignore');
             $("#paterno").removeClass('ignore');
             $("#materno").removeClass('ignore');
         } else {
             $("#moral").removeClass('hidden-segment-value');
             $("#fisica").addClass('hidden-segment-value');
             $("#tipo_persona").val(value);

             // clean the fields
             $("#nombre").val('').addClass('ignore');
             $("#paterno").val('').addClass('ignore');
             $("#materno").val('').addClass('ignore');
             $("#empresa").removeClass('ignore');
             $("#nombre_contacto").removeClass('ignore');
         }
     });

    /**
     * Adding new parameters to set the
     * class will use to verify if the
     * checkbox is checked or not
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".checkdata2").change(function () {
        var $kindCheckbox = $(this).data('text');

        // Check the checkbox with class 'tipo'
        if ($kindCheckbox == 'tipo') {
            if ($(this).is(':checked')) {
                $("input:checkbox.tipo-21").removeClass('enabledTipo');
                $(this).addClass('enabledTipo');
            } else {
                $(this).removeClass('enabledTipo');
            }
        }

        // Check the checkbox with class 'sexo'
        if ($kindCheckbox == 'sexo') {
            if ($(this).is(':checked')) {
                $("input:checkbox.sexo-21").removeClass('enabledSexo');
                $(this).addClass('enabledSexo');
            } else {
                $(this).removeClass('enabledSexo');
            }
        }

        // Check the checkbox with class 'pedigree'
        if ($kindCheckbox == 'pedigree') {
            if ($(this).is(':checked')) {
                $("input:checkbox.pedigree-21").removeClass('enabledPedigree');
                $(this).addClass('enabledPedigree');
            } else {
                $(this).removeClass('enabledPedigree');
            }
        }

        // Check the checkbox with class 'registro'
        if ($kindCheckbox == 'registro_option') {
            if ($(this).is(':checked')) {
                $("input:checkbox.registro-21").removeClass('enabledRegistro_option');
                $(this).addClass('enabledRegistro_option');
            } else {
                $(this).removeClass('enabledRegistro_option');
            }
        }
    });

    /**
     * Event will be used to manage the options related
     * with the textarea used for type the description
     * of when and what was the accident or issue with
     * the pet
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".sufrido").change(function (evt) {
        var $totalOps = 0;

        $(".sufrido").each(function (i, v) {
            if (v.value == 'Si') {
                $totalOps++;
            }
        });

        if ($totalOps == 0) {
            $("#desc_ant").attr('disabled', true).val('N/A');
        } else {
            $("#desc_ant").removeAttr('disabled').val('');

            // Clear the object in this field because the user should have the new data that will be typed by the user
            obj = utils.createObjectForm('desc_ant', '');
        }
    });

    /**
     * Event will be used to check if the field of
     * seller's name is displayed or not depending on
     * if the user is making the cotization is Grupo
     * Nuno's sellers
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $("#vendedor").change(function () {
        if ($("#" + this.id).prop('checked')) {
          $("#name_seller").addClass('first-section').show();
        } else {
          $("#name_seller").removeClass('first-section').val('').hide();
        }
    });

    /**
     * Event will be used to send the data to the
     * next section of the event and the users must
     * be used to fill in the data of the pet
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.pet-section").click(function (evt) {
        evt.preventDefault();

        var $this        = $(this);
        var total        = 0;
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_first_section");
        var $data        = "";

        $(".first-section").each(function (i, value) {
            var typeElement = utils.typeOfElement(value);

            if (value.tagName == "INPUT") {
                if (value.type == 'text') {
                    var inputValue = $(this).hasClass('ignore');

                    if (inputValue == false) {
                        if (value.value == '') {
                            utils.addRemoveClass($(this).parent(), "add");
                            count++;
                        } else {
                            utils.addRemoveClass($(this).parent(), "remove");
                            obj = utils.createObjectForm(value.id, value.value);
                        }
                    }
                }

                if (typeElement == 'email') {
                    var emailValue = utils.validateEmailData(value.value);

                    if (value.value == '') {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        total++;
                    } else if (emailValue == false) {
                        // add class error
                        utils.addRemoveClass($(this).parent(), "add");
                        total++;
                    } else {
                        // remove class error
                        utils.addRemoveClass($(this).parent(), "remove");
                        // add value to the object
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }

               /* if (typeElement != 'email') {
                    if ($("#" + value.id).val() == '') {
                        // It's going to display the error message and add the error class to the input
                        utils.addRemoveClass($("#" + value.id).parent(), "add");
                        total++;
                    } else {
                        // It's going to remove all the error messages and add to the obj the values
                        utils.addRemoveClass($("#" + value.id).parent(), "remove");
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }*/
            }
        });

        if (total != 0) {
            // Set template for errors & remove hidden class
            utils.setTemplate($source, $destination, {data: $data});
            utils.removeClassHiddenMessage();
        } else {
            // check if vendedor is checked to added to the obj object
            if ($("#vendedor").prop('checked')) {
              obj = utils.createObjectForm('vendedor', 'Si');
            } else {
              obj = utils.createObjectForm('vendedor', '');
              obj = utils.createObjectForm('nombre_vendedor', '');
            }

            // check if the user is company or is person
            if ($("#tipo_persona").val() == 'fisica') {
                obj = utils.createObjectForm("tipo_persona", 0);
                obj = utils.createObjectForm("empresa", "");
                obj = utils.createObjectForm("nombre_contacto", "");

                // add the data to the preview
                $("#nombre_txt").text(obj.nombre + " " + obj.paterno + " " + obj.materno);

                // add/remove hidden class
                $("#nombre_txt").parent().removeClass('fisica_temp');
                $("#empresa_txt").parent().addClass('moral_temp');
                $("#nombre_contacto_txt").parent().addClass('moral_temp');
            } else {
                obj = utils.createObjectForm('tipo_persona', 1);
                obj = utils.createObjectForm("a_materno", "");
                obj = utils.createObjectForm("a_paterno", "");
                obj = utils.createObjectForm("nombres", "");
                obj = utils.createObjectForm("nombre_contacto", $("#nombre_contacto").val());

                // add the data to the preview
                $("#empresa_txt").text(obj.empresa);
                $("#nombre_contacto_txt").text(obj.nombre_contacto);

                // add/remove hidden class
                $("#nombre_txt").parent().addClass('fisica_temp');
                $("#empresa_txt").parent().removeClass('moral_temp');
                $("#nombre_contacto_txt").parent().removeClass('moral_temp');
            }

            // Remove the error message and change the active step-by-step section
            utils.addClassHiddenMessage();

            $("#step-1").removeClass('active').addClass('completed');
            $("#accordion-1").addClass('disabled-content');
            $("#content-1").removeClass('active');
            $("#title-1").removeClass('active');

            $("#step-2").removeClass('disabled').addClass('active');
            $("#accordion-2").removeClass('disabled-content');
            $("#content-2").addClass('active');
            $("#title-2").addClass('active');
        }

    });

    /**
     * Method will be used to send the pet's data to the
     * next section will be used to display the information
     * typed by the owner of the pet.
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.report-section").click(function (evt) {
        evt.preventDefault();

        var $this        = $(this);
        var total        = 0;
        var $source      = $("#errors_messages").html();
        var $destination = $("#error_second_section");
        var $data        = "";

        $(".second-section").each(function (i, value) {
            if (value.tagName == 'INPUT') {
                if (value.type == 'text') {
                    if ($("#" + value.id).val() == '') {
                        // It's going to display the error message and add the error class to the input
                        utils.addRemoveClass($("#" + value.id).parent(), "add");
                        total++;
                    } else {
                        // It's going to remove all the error messages and add to the obj the values
                        utils.addRemoveClass($("#" + value.id).parent(), "remove");
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }

                if (value.type == 'hidden') {
                    if ($(this).val() == 0 || $(this).val() == '0') {
                        // It's going to display the error message and add the error class to the input
                        utils.addRemoveClass($(this).parent().parent(), "add");
                        total++;
                    } else {
                        // Validate if the pet has microchip or not
                        if (this.id == 'microchip' && $(this).val() == 'No') {
                            obj = utils.createObjectForm('no_microchip', '');
                        }

                        if (this.id == 'paquete') {
                            obj = utils.createObjectForm("precio", $("#" + value.value).val());
                            obj = utils.createObjectForm("paquete_desc", $("#" + value.value).data('name'));
                        }
                        // It's going to remove all the error messages and add to the obj the values
                        utils.addRemoveClass($(this).parent().parent(), "remove");
                        obj = utils.createObjectForm(value.id, value.value);
                    }
                }

                if (value.type == 'checkbox') {
                    var $classAssigned = $(this).data('text');

                    if ($("input:checkbox[name='" + $classAssigned + "']").is(':checked')) {
                        // It's going to remove all the error messages and add to the obj the values
                        utils.addRemoveClass($("div." + $classAssigned), "remove");
                        utils.addRemoveClass($("input:checkbox[name='" + $classAssigned + "']").parent().parent(),
                                             "remove");
                        var nameCheck = utils.upperCaseFirstLetter($classAssigned);

                        // Validate if needs to clean the object or not
                        if ($(".enabled" + nameCheck).data('value') == 'Si' && $(".enabled" + nameCheck).data('class') == 'registro-21') {
                            obj = utils.createObjectForm(value.name, $(".enabled" + nameCheck).data('value'));
                        } else if ($(".enabled" + nameCheck).data('value') == 'No' && $(".enabled" + nameCheck).data('class') == 'registro-21') {
                            obj = utils.createObjectForm(value.name, $(".enabled" + nameCheck).data('value'));
                            obj = utils.createObjectForm('registro', '');
                            obj = utils.createObjectForm('num_registro', '');
                        } else {
                            obj = utils.createObjectForm(value.name, $(".enabled" + nameCheck).data('value'));
                        }
                    } else {
                        // It's going to display the error message and add the error class to the input
                        utils.addRemoveClass($("div." + $classAssigned), "add");
                        utils.addRemoveClass($("input:checkbox[name='" + $classAssigned + "']").parent().parent(),
                                            "add");
                        total++;
                    }
                }
            }

            if (value.tagName == 'TEXTAREA') {
                if ($.trim($("#" + value.id).val()) == '') {
                    // It's going to display the error message and add the error class to the input
                    utils.addRemoveClass($("#" + value.id).parent(), "add");
                    total++;
                } else {
                    // It's going to remove all the error messages and add to the obj the values
                    utils.addRemoveClass($("#" + value.id).parent(), "remove");
                    obj = utils.createObjectForm(value.id, value.value);
                }
            }
        });

        if (total != 0) {
            // Set template for errors & remove hidden class
            utils.setTemplate($source, $destination, {data: $data});
            utils.removeClassHiddenMessage();
        } else {
            // Remove the error message and change the active step-by-step section
            utils.addClassHiddenMessage();
            obj = utils.createObjectForm("otra_raza", $("#otra_raza").val());
            // Split the value to display price and pet insurance package
            //var splitData = obj.paquete_desc.split("-");

            // check the values were disabled
            $("#cuenta_registro").text(obj.registro_option);
            if (obj.registro_option == 'No') {
                delete obj.registro;
                delete obj.num_registro;
            } else {
                $("#no_registro_txt").text(obj.num_registro);
                $("#registro_txt").text(obj.registro);
            }

            if (obj.microchip == 'No') {
                delete obj.no_microchip;
            } else {
                $("#no_micro_txt").text(obj.no_microchip);
            }

            // Adding Seller Section
            $("#vendedor_txt").text(obj.vendedor);
            $("#nombre_vendedor_txt").text(obj.name_seller);

            // Adding the values to the preview section
            $("#nombre_txt").text(obj.nombre + " " + obj.paterno + " " + obj.materno);
            $("#rfc_txt").text(obj.rfc.toUpperCase());
            $("#direccion_txt").text(obj.direccion);
            $("#codigo_postal_txt").text(obj.cp);
            $("#colonia_txt").text(obj.colonia);
            $("#municipio_txt").text(obj.municipio);
            $("#estado_txt").text(obj.estado);
            $("#email_txt").text(obj.email);
            $("#tel_txt").text(obj.tel);
            $("#nombre_masc_txt").text(obj.mascota_n);
            $("#edad_masc_txt").text(obj.edad);
            //$("#paquete_txt").text(splitData[0]);
            //$("#precio_txt").text(splitData[1]);
            $("#tipo_txt").text(obj.tipo);
            $("#sexo_txt").text(obj.sexo);
            $("#raza_txt").text(obj.raza);
            $("#otra_raza_txt").text(obj.otra_raza);
            $("#color_txt").text(obj.color);
            $("#esterilizado_txt").text(obj.esteril);
            $("#pedigree_txt").text(obj.pedigree);
            $("#sen_part_txt").text(obj.s_part);
            $("#micro_txt").text(obj.microchip);
            $("#accidente_txt").text(obj.accidente);
            $("#cript_txt").text(obj.criptorquidismo);
            $("#fractura_txt").text(obj.fractura);
            $("#hernias_txt").text(obj.hernias);
            $("#sida_f").text(obj.sida_felino);
            $("#luxacion_p").text(obj.luxacion);
            $("#cardio").text(obj.displacia);
            $("#ehrlichia_e").text(obj.cardiopatias);
            $("#displacia_c").text(obj.ehrlichia);
            $("#borrelia_e").text(obj.leucemia);
            $("#leucemia_vf").text(obj.borrelia);
            $("#enfermedad_o").text(obj.oculares);
            $("#gusano_de_c").text(obj.gusano_c);
            $("#enfermedad_d").text(obj.dermatologicas);
            $("#enfermedad_p").text(obj.parasitarias);
            $("#convulcion_desmayo").text(obj.convulsiones);
            $("#afirm_txt").text(obj.desc_ant);

            // Move the steps
            $("#step-2").removeClass('active').addClass('completed');
            $("#accordion-2").addClass('disabled-content');
            $("#content-2").removeClass('active');
            $("#title-2").removeClass('active');

            $("#step-3").removeClass('disabled').addClass('active');
            $("#accordion-3").removeClass('disabled-content');
            $("#content-3").addClass('active');
            $("#title-3").addClass('active');
        }
    });

    /**
     * Method will be used to clear all the fields depending on
     * the section opened at the moment the user click the
     * reset button. It will clear all the fields to retype
     * again the values
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.clear-fields").click(function (evt) {
        evt.preventDefault();

        // Get seccion clicked
        var section = $(this).data('type');

        if (section == "first") {
            $("." + section + "-section").each(function (i, v) {
                if (this.tagName == 'INPUT') {
                    $(this).val('');

                    utils.addRemoveClass($(this).parent(), "remove");
                }
            });
        } else {
            $("." + section + "-section").each(function (i, v) {
                if (this.tagName == 'INPUT') {
                    if (this.type == 'text') {
                        $(this).val('');

                        // Remove error class
                        utils.addRemoveClass($(this).parent(), "remove");
                    }

                    if (this.type == 'checkbox') {
                        $(this).prop('checked', false);

                        // remove class enabledXXXX
                        $(this).removeClass('enabledTipo');
                        $(this).removeClass('enabledSexo');
                        $(this).removeClass('enabledPedigree');
                        $(this).removeClass('enabledRegistro');

                        // Remove error class
                        utils.addRemoveClass($("." + $(this).data('text')), "remove");
                        utils.addRemoveClass($(this).parent().parent(), "remove");
                    }

                    if (this.type == 'hidden') {
                        $(".ui.dropdown").dropdown("restore defaults");

                        // Remove error class
                        utils.addRemoveClass($(this).parent().parent(), "remove");
                    }
                }

                if (this.tagName == 'TEXTAREA' && this.type == 'textarea') {
                    $(this).val('');

                    // Remove error class
                    utils.addRemoveClass($(this).parent(), "remove");
                }
            });
        }

        // Remove all error messages
        utils.addClassHiddenMessage();
    });

    /**
     * Event will be used to return back to the last one section
     * that was filled by the user out. This buttons will help to the
     * user to change the data typed in personal information
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.back-form").click(function (evt) {
        evt.preventDefault();

        // Adding the events used to go back the last one section
        $("#step-2").removeClass("active").addClass("disabled");
        $("#accordion-2").addClass("disabled-content");
        $("#content-2").removeClass("active");
        $("#title-2").removeClass("active");

        $("#step-1").removeClass("disabled completed").addClass("active");
        $("#accordion-1").removeClass("disabled-content");
        $("#content-1").addClass("active");
        $("#title-1").addClass("active");
    });

    /**
     * Event will be enabled to send data to
     * the server for execute all the process
     * enabled and save data.
     *
     * @author Ruben Alosno Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.nextButton-4").click(function (evt) {
        evt.preventDefault();

        // Send information to save in the server
        var url = "/mascotas/saveData";
        req.postApi(url, obj).done(function (response) {
            if (response.flag == 1 || response.flag == '1') {
                // Set the complete path for download PDF using the browser
                $(".print.seguro-mascota-pdf").attr('id', response.pathFile);

                setTimeout(function () {
                    utils.displayModal();
                }, 2500);
            } else {
                // error
            }
        });
    });

    /**
     * Event will be enabled to return the last
     * one section to enabled with the pet
     * information to the user will be able to
     * edit that information
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.backButton-4").click(function (evt) {
        evt.preventDefault();

        // Adding the events used to go back the last one section
        $("#step-3").removeClass("active").addClass("disabled");
        $("#accordion-3").addClass("disabled-content");
        $("#content-3").removeClass("active");
        $("#title-3").removeClass("active");

        $("#step-2").removeClass("disabled completed").addClass("active");
        $("#accordion-2").removeClass("disabled-content");
        $("#content-2").addClass("active");
        $("#title-2").addClass("active");
    });

    /**
     * Event will dispatch the action to
     * open a modal with all the information
     * related to the pet insurance packages
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".button.help-button").click(function (evt) {
        evt.preventDefault();

        $(".ui.modal.product-information").modal('show');
    });

    /**
     * Event will reload all the information once the
     * user has clicked on the cancel button
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".cancel.cancelPrint").click(function (evt) {
        document.location.reload(true);
    });

    /**
     * Event will be used to print the information
     * in the PDF file in the browser although the
     * the file was sent via Email
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".print.seguro-mascota-pdf").click(function (evt) {
        // Sending the request to the url to displaying the PDF file in the browser
        var url = $(this).attr('id');

        // Send PDF to print
        window.open(url, "_blank").focus();

        // Reload the page
        document.location.reload(true);
    });

    /**
     * Event will be enabled by the selection
     * done by the user. If the user selected 'Yes',
     * the input is going to be enabled otherwise
     * is disabled
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".microchip .menu .item").click(function (evt) {
        evt.preventDefault();

        var $value = $(evt.currentTarget).data('value');

        if ($value == "Si") {
            $("#no_microchip").removeClass('second-section-disabled').addClass('second-section').parent().removeClass('disabled');
        } else {
            $("#no_microchip").removeClass('second-section').addClass('second-section-disabled').val('')
                              .parent().addClass('disabled');
        }
    });

    /**
     * Event will fire once the user select if the
     * pet is registered national or international
     * otherwise the fields will be disabled and the
     * user could not select any value
     *
     * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
     * @version 1.0
     * @company ruvicdev
     */
    $(".registro_option").click(function (evt) {
        var $value = $(evt.currentTarget).data('value');

        if ($value == 'Si') {
            $("div.field.registro").removeClass('disabled');
            $("#registro").removeClass('second-section-disabled').addClass('second-section')
                          .parent().removeClass('disabled');
            $("#num_registro").removeClass('second-section-disabled').addClass('second-section').val('')
                              .parent().removeClass('disabled');
        } else {
            $("div.field.registro").addClass('disabled');
            $(".registro .text.registroTexto").text('Registro');
            $(".ui.dropdown.registro").addClass('disabled').dropdown('set selected', 'Registro');
            $("#registro").removeClass('second-section').addClass('second-section-disabled')
                          .parent().addClass('disabled');
            $("#num_registro").removeClass('second-section').addClass('second-section-disabled').val('')
                              .parent().addClass('disabled');
        }
    });
});
