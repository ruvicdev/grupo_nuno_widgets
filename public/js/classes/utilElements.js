/**
 * Class will be used to manage and handle all the
 * functionality used for the dropdown elements
 * that will be used in all the system
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class UtilElements extends BaseClass {

    constructor() {
        super();

        this.obj = this.createObject();

        this.initDropdown();
        this.initAccordion(true);
        this.initRadioCheckbox();
    }

    // display elements
    displayElements($data, $class, $wildcard) {
        var results = this.showInformationData($data, $class);
		$($wildcard).addClass('hidden-segment-value');

		return results;
    }

    // get type of element
    typeOfElement($value) {
        var typeValue = $value.type;

        return typeValue;
    }

    // close messages in the forms
    closeMessages($actions) {
        $(".message " + $actions).on('click', function() {
            $(this).closest('.message')
                   .transition('fade');
        });
    }

    // remove class hidden message
    removeClassHiddenMessage() {
        $(".ui.message").removeClass('hidden');
    }

    // add class hidden message
    addClassHiddenMessage() {
        $(".ui.message").addClass('hidden');
    }

    // add/remove class error in form
    addRemoveClass($root, $type) {
        if ($type == "add") {
            $root.addClass("error");
        } else {
            $root.removeClass("error");
        }
    }

    // validate email
    validateEmailData($value) {
         var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

         return re.test($value);
    }

    // checkbox working as radio button
    checkboxAsRadioButton($this, $clase) {
        return $("input:checkbox" + $clase).not($this).prop("checked", false);
    }

    // create object dinamically
    createObjectForm(key, value) {
        this.obj[key] = value;

        return this.obj;
    }

    // get percentage depending on the options
    getPercentage($giro, $vol_viajes) {
        var percentage = 0;

        if (parseInt($giro) == 1 || parseInt($giro) == 2 ||
            parseInt($giro) == 3 || parseInt($giro) == 4) {
            if ("1_a_10" == $vol_viajes) {
                percentage = 0.35;
            } else if("11_a_15" == $vol_viajes) {
                percentage = 0.32;
            } else {
                percentage = 0.30;
            }
        } else if (parseInt($giro) == 5 || parseInt($giro) == 6) {
            if ("1_a_10" == $vol_viajes) {
                percentage = 0.40;
            } else if("11_a_15" == $vol_viajes) {
                percentage = 0.38;
            } else {
                percentage = 0.35;
            }
        }

        return percentage;
    }

    // validate security in pesos
    getSecurityPesos($class, $amount) {
        var $amount   = $amount.split(',').join('');
        var $security = [];

        if (parseFloat($amount) >= 0.01 && parseFloat($amount) <= 8000000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "GPS en la unidad.";
        }
        if (parseFloat($amount) >= 100000.00 && parseFloat($amount) <= 8000000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "Carreteras de couta.";
        }
        if (parseFloat($amount) >= 500000.00 && parseFloat($amount) <= 8000000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "Rasteabilidad.";
        }
        if (parseFloat($amount) >= 1000000.00 && parseFloat($amount) <= 8000000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "Trazabilidad.";
        }
        if (parseFloat($amount) >= 1200000.00 && parseFloat($amount) <= 8000000.00
            && ($class == 'C' || $class == 'E')) {
            $security[$security.length] = "Custodia.";
        }
        if (parseFloat($amount) >= 3000000.00 && parseFloat($amount) <= 8000000.00
            && ($class == 'A' || $class == 'B'
            || $class == 'D' || $class == 'F')) {
            $security[$security.length] = "Custodia.";
        }

        return $security;
    }

    // validate security in dollars
    getSecurityDollars($class, $amount) {
        var $amount   = $amount.split(',').join('');
        var $security = [];

        if (parseFloat($amount) >= 0.01 && parseFloat($amount) <= 550000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "GPS en la unidad. \n";
        }
        if (parseFloat($amount) >= 6700.00 && parseFloat($amount) <= 550000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "Carreteras de couta. \n";
        }
        if (parseFloat($amount) >= 8000.00 && parseFloat($amount) <= 550000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "Rasteabilidad. \n";
        }
        if (parseFloat($amount) >= 10000.00 && parseFloat($amount) <= 550000.00
            && ($class == 'A' || $class == 'B' || $class == 'C'
            || $class == 'D' || $class == 'E' || $class == 'F')) {
            $security[$security.length] = "Trazabilidad. \n";
        }
        if (parseFloat($amount) >= 80000.00 && parseFloat($amount) <= 550000.00
            && ($class == 'C' || $class == 'E')) {
            $security[$security.length] = "Custodia. \n";
        }
        if (parseFloat($amount) >= 200000.00 && parseFloat($amount) <= 550000.00
            && ($class == 'A' || $class == 'B'
            || $class == 'D' || $class == 'F')) {
            $security[$security.length] = "Custodia. \n";
        }

        return $security;
    }

    // display modal
    displayModal() {
        // Adding these validation in case the systems wants to
        // be loaded inside div instead of iframe element

        //if ($("#widget").length) {
        /* $(".ui.basic.modal")
              .modal({context: 'div#contenido'})
              .modal('show');

        $("#demos").css("cssText", "position:relative !important; top: 25% !important");
        $("#demos .content").css("cssText", "text-align: center !important");
        $("#demos .actions").css("cssText", "text-align: center !important");

        $(".scrolling.dimmable > .dimmer").css("cssText", "position: absolute !important");
        $(".modals.dimmer .ui.scrolling.modal").css("cssText", "position: sticky !important;")
                                               .css("cssText", "left: 0px !important;")
                                               .css("cssText", "top: 0px !important;");    */
        //} else {
            $(".ui.basic.modal").modal('show');
        //}
    }

    // get month will be displayed
    getMonthToDisplay($month) {
        var months = new Array();
        months[0]  = '01';
        months[1]  = '02';
        months[2]  = '03';
        months[3]  = '04';
        months[4]  = '05';
        months[5]  = '06';
        months[6]  = '07';
        months[7]  = '08';
        months[8]  = '09';
        months[9]  = '10';
        months[10] = '11';
        months[11] = '12';

        return months[$month];
    }

    // upper case first letter
    upperCaseFirstLetter($word) {
        var $data = $word.charAt(0).toUpperCase() + $word.slice(1).toLowerCase();
        return $data;
    }

    // Upper case the first letter for each word of a whole phrase
    ucwords($word) {
        var str = $word.toLowerCase();

        return str.replace(/(^([a-zA-Z\p{M}]))|([ -][a-zA-Z\p{M}])/g,
                    function($1){
                        return $1.toUpperCase();
                    }
                  );
    }
}
