/**
 * This class is created for adding all the methods
 * related with the math functions or with numbers for
 * validate fields in the forms
 *
 * @author Ruben Alonso Cortes Mendoza <ruben.alonso21@gmail.com>
 * @version 1.0
 * @company ruvicdev
 */
class MathElements extends BaseClass {
	constructor () {
		super();
	}

	// doing the total without tax
	totalWithoutTax($amount, $percentage) {
		$amount = $amount.split(',').join('');

		return (parseFloat($amount) * parseFloat($percentage)) / 100;
	}

	// doing the total with tax
	totalWithTax($amount) {
		return parseFloat($amount) * 1.16;
	}
}
