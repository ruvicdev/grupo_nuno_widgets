<html>
    <style>
    .tooltip {
        position: relative;
        display: inline-block;
        border-bottom: 1px dotted black;
    }

    .tooltip .tooltiptext {
        top: 0px;
        left: -130;
        visibility: hidden;
        width: 120px;
        background-color: black;
        color: #fff;
        text-align: center;
        border-radius: 6px;
        padding: 5px 0;

        position: absolute;
        z-index: 1;
    }

    .tooltip:hover .tooltiptext {
        visibility: visible;
    }
    </style>
    <head>
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.0.min.js"></script>
        <script type="text/javascript" src="index.js"></script>
    </head>
    <body>
        <div style="float: right; margin-top: 100px; z-index: 100;">
            <!--template_1-->
            <div style="background-color: transparent">
                <div id="template1"
                     class="widget_ruvicdev tooltip"
                     data-url="/widgets/index.php/carga"
                     style="background-color: white; align: left; border: 0px; margin-right: 1px">
                    <img class="img"
                         data-blue="shipping_blue.png"
                         data-black="shipping_black.png"
                         data-gray="shipping_gray.png"
                         width="54px"
                         height="60px"
                         src="">
                </div>
                <iframe id="template1widget"
                        width="770px"
                        height="600px"
                        style="border: 0px solid #FFF;
                               display: none;
                               float: right;
                               border-radius: 5px 0 0 5px;
                               -moz-border-radius: 5px 0 0 5px;
                               -webkit-border-radius:5px 0 0 5px;
                               -webkit-box-shadow: 3px 3px 3px 3px #ccc;
                               -moz-box-shadow: 3px 3px 3px 3px #ccc;
                               box-shadow:3px 3px 3px 3px #ccc;" />
                </iframe>
                <iframe id="template2widget"
                        width="770px"
                        height="600px"
                        style="border: 0px solid #FFF;
                               display: none;
                               float: right;
                               border-radius: 5px 0 0 5px;
                               -moz-border-radius: 5px 0 0 5px;
                               -webkit-border-radius:5px 0 0 5px;
                               -webkit-box-shadow: 3px 3px 3px 3px #ccc;
                               -moz-box-shadow: 3px 3px 3px 3px #ccc;
                               box-shadow:3px 3px 3px 3px #ccc;" />
                </iframe>
            </div>
            <!--template_2-->
            <div style="background-color: transparent; margin-top: 50px">
                <div id="template2"
                     class="widget_ruvicdev tooltip"
                     data-url="/widgets/index.php/salud"
                     style="background-color: white; align: left; border: 0px; margin-right: 1px">
                    <img class="img"
                         data-blue="emergency_blue.png"
                         data-black="emergency_black.png"
                         data-gray="emergency_gray.png"
                         width="54px"
                         height="60px"
                         src="">
                </div>
            </div>
        </div>
    </body>
</html>
