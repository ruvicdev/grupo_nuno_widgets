-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:27:07
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apreciacion_actos_fraudulentos`
--

CREATE TABLE `apreciacion_actos_fraudulentos` (
  `id` int(11) NOT NULL,
  `percentage` varchar(50) NOT NULL COMMENT 'porcentage a usar en la operacion',
  `status` tinyint(1) NOT NULL COMMENT 'status del recors, 1-enabled, 0-disabled',
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `apreciacion_actos_fraudulentos`
--

INSERT INTO `apreciacion_actos_fraudulentos` (`id`, `percentage`, `status`, `date_created`) VALUES
(1, '-0.9', 1, '2018-01-23 00:00:00'),
(2, '-0.5', 1, '2018-01-23 00:00:00'),
(3, '1', 1, '2018-01-23 00:00:00'),
(4, '2', 1, '2018-01-23 00:00:00'),
(5, '3', 1, '2018-01-23 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `apreciacion_actos_fraudulentos`
--
ALTER TABLE `apreciacion_actos_fraudulentos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `apreciacion_actos_fraudulentos`
--
ALTER TABLE `apreciacion_actos_fraudulentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
