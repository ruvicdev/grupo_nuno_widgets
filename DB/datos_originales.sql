-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:27:20
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_originales`
--

CREATE TABLE `datos_originales` (
  `id` int(11) NOT NULL,
  `region` varchar(100) NOT NULL,
  `sum_incurred` varchar(400) NOT NULL,
  `sum_premium` varchar(400) NOT NULL,
  `count_policy` varchar(200) NOT NULL,
  `comisiones` varchar(100) NOT NULL,
  `gastos_generales` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `datos_originales`
--

INSERT INTO `datos_originales` (`id`, `region`, `sum_incurred`, `sum_premium`, `count_policy`, `comisiones`, `gastos_generales`, `status`, `creacion`) VALUES
(1, 'Chihuahua', '18240.77', '8379.72', '18.00', '15', '15', 1, '2018-01-26 00:00:00'),
(2, 'Ciudad Juarez', '-', '11675.41', '5.0', '15', '15', 1, '2018-01-26 00:00:00'),
(3, 'Guadalajara', '202455.78', '53821.17', '46.00', '15', '15', 1, '2018-01-27 00:00:00'),
(4, 'Hermosillo (10)', '-', '12436.05', '1.00', '15', '15', 1, '2018-01-27 00:00:00'),
(5, 'Hermosillo (Juarez)', '214989.83', '30511.56', '29.00', '15', '15', 1, '2018-01-27 00:00:00'),
(6, 'Leon', '-', '44042.52', '35.00', '15', '15', 1, '2018-01-27 00:00:00'),
(7, 'Mexico', '138469.70', '199421.34', '157.00', '15', '15', 1, '2018-01-27 00:00:00'),
(8, 'Monterrey', '43030.04', '75538.93', '59.00', '15', '15', 1, '2018-01-27 00:00:00'),
(9, 'Puebla', '283271.52', '212017.32', '165.00', '15', '15', 1, '2018-01-27 00:00:00'),
(10, 'Satelite', '19477.94', '4032.81', '3.00', '15', '15', 1, '2018-01-27 00:00:00'),
(11, 'Tijuana', '346.88', '15729.49', '35.00', '15', '15', 1, '2018-01-27 00:00:00'),
(12, 'Veracruz', '-', '7436.60', '6.00', '15', '15', 1, '2018-01-27 00:00:00'),
(13, 'Villahermosa', '3086.49', '6385.09', '5.00', '15', '15', 1, '2018-01-27 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `datos_originales`
--
ALTER TABLE `datos_originales`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `datos_originales`
--
ALTER TABLE `datos_originales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
