-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-07-2017 a las 21:43:17
-- Versión del servidor: 10.0.30-MariaDB
-- Versión de PHP: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Club_Salud_Historial`
--

CREATE TABLE IF NOT EXISTS `Club_Salud_Historial` (
  `id` bigint(20) unsigned NOT NULL,
  `session` varchar(100) COLLATE utf8_bin NOT NULL,
  `fullname` char(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `telefono` int(11) NOT NULL DEFAULT '0',
  `estado` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ciudad` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cobertura` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `emergencia_extranjero` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `id_integrantes` int(11) NOT NULL,
  `inversion_diaria` decimal(15,2) NOT NULL,
  `pago_total_inicial_mes` int(11) NOT NULL,
  `pago_total_subsecuente_mes` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Club_Salud_Historial`
--
ALTER TABLE `Club_Salud_Historial`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ID` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Club_Salud_Historial`
--
ALTER TABLE `Club_Salud_Historial`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
