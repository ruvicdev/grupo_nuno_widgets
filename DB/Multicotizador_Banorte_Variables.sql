-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 27-05-2018 a las 04:58:38
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multicotizador_banorte_variables`
--

DROP TABLE IF EXISTS `multicotizador_banorte_variables`;
CREATE TABLE IF NOT EXISTS `multicotizador_banorte_variables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `external_files_cotizaciones_id` bigint(20) NOT NULL,
  `nivelHospitalario_1` text COLLATE utf8_bin NOT NULL,
  `nivelHospitalario_2` text COLLATE utf8_bin NOT NULL,
  `catalogoHonorarios_1` text COLLATE utf8_bin NOT NULL,
  `catalogoHonorarios_2` text COLLATE utf8_bin NOT NULL,
  `maternidadMonto_1` text COLLATE utf8_bin NOT NULL,
  `maternidadMonto_2` text COLLATE utf8_bin NOT NULL,
  `maternidadMonto_3` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_1` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_2` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_3` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_4` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_5` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_6` text COLLATE utf8_bin NOT NULL,
  `enfermedadesGravesMonto_7` text COLLATE utf8_bin NOT NULL,
  `tipoCotizacion_1` text COLLATE utf8_bin NOT NULL,
  `tipoCotizacion_2` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_1` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_2` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_3` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_4` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_5` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_6` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_7` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_8` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_9` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_10` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_11` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_12` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_13` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_14` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_15` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_16` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_17` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_18` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_19` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_20` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_21` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_22` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_23` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_24` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_25` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_26` text COLLATE utf8_bin NOT NULL,
  `sumaAsegurada_27` text COLLATE utf8_bin NOT NULL,
  `deducible_1` text COLLATE utf8_bin NOT NULL,
  `deducible_2` text COLLATE utf8_bin NOT NULL,
  `deducible_3` text COLLATE utf8_bin NOT NULL,
  `deducible_4` text COLLATE utf8_bin NOT NULL,
  `deducible_5` text COLLATE utf8_bin NOT NULL,
  `deducible_6` text COLLATE utf8_bin NOT NULL,
  `deducible_7` text COLLATE utf8_bin NOT NULL,
  `deducible_8` text COLLATE utf8_bin NOT NULL,
  `deducible_9` text COLLATE utf8_bin NOT NULL,
  `deducible_10` text COLLATE utf8_bin NOT NULL,
  `deducible_11` text COLLATE utf8_bin NOT NULL,
  `deducible_12` text COLLATE utf8_bin NOT NULL,
  `deducible_13` text COLLATE utf8_bin NOT NULL,
  `deducible_14` text COLLATE utf8_bin NOT NULL,
  `deducible_15` text COLLATE utf8_bin NOT NULL,
  `deducible_16` text COLLATE utf8_bin NOT NULL,
  `deducible_17` text COLLATE utf8_bin NOT NULL,
  `deducible_18` text COLLATE utf8_bin NOT NULL,
  `deducible_19` text COLLATE utf8_bin NOT NULL,
  `deducible_20` text COLLATE utf8_bin NOT NULL,
  `coaseguro_1` text COLLATE utf8_bin NOT NULL,
  `coaseguro_2` text COLLATE utf8_bin NOT NULL,
  `coaseguro_3` text COLLATE utf8_bin NOT NULL,
  `coaseguro_4` text COLLATE utf8_bin NOT NULL,
  `coaseguro_5` text COLLATE utf8_bin NOT NULL,
  `coaseguro_6` text COLLATE utf8_bin NOT NULL,
  `coaseguro_7` text COLLATE utf8_bin NOT NULL,
  `coaseguro_8` text COLLATE utf8_bin NOT NULL,
  `zona_1` text COLLATE utf8_bin NOT NULL,
  `zona_2` text COLLATE utf8_bin NOT NULL,
  `zona_3` text COLLATE utf8_bin NOT NULL,
  `zona_4` text COLLATE utf8_bin NOT NULL,
  `zona_5` text COLLATE utf8_bin NOT NULL,
  `zona_6` text COLLATE utf8_bin NOT NULL,
  `tarifa_basica_h_m_id` int(11) NOT NULL,
  `factor_suma_asegurada_id` int(11) NOT NULL,
  `factor_nivel_hospitalario_id` int(11) NOT NULL,
  `factor_deducible_id` int(11) NOT NULL,
  `factor_coaseguro_id` int(11) NOT NULL,
  `factor_honorario_id` int(11) NOT NULL,
  `factor_zona_id` int(11) NOT NULL,
  `factor_maternidad_id` int(11) NOT NULL,
  `estados_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
