-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:27:31
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducible_ingresos`
--

CREATE TABLE `deducible_ingresos` (
  `id` int(11) NOT NULL,
  `rango_uno` varchar(200) NOT NULL,
  `rango_dos` varchar(200) NOT NULL,
  `porcentaje` varchar(50) NOT NULL,
  `precio_final` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `deducible_ingresos`
--

INSERT INTO `deducible_ingresos` (`id`, `rango_uno`, `rango_dos`, `porcentaje`, `precio_final`, `status`, `creacion`) VALUES
(1, '1', '1000000', '2.500', '10000', 1, '2018-01-25 00:00:00'),
(2, '1000001', '15000000', '2.500', '20000', 1, '2018-01-25 00:00:00'),
(3, '15000001', '20000000', '2.50', '25000', 1, '2018-01-25 00:00:00'),
(4, '20000001', '30000000', '2.50', '30000', 1, '2018-01-25 00:00:00'),
(5, '30000001', '40000000', '2.50', '35000', 1, '2018-01-25 00:00:00'),
(6, '40000001', '60000000', '2.50', '40000', 1, '2018-01-25 00:00:00'),
(7, '60000001', '70000000', '2.50', '45000', 1, '2018-01-25 00:00:00'),
(8, '70000001', '80000000', '2.50', '50000', 1, '2018-01-25 00:00:00'),
(9, '80000001', '100000000', '2.50', '55000', 1, '2018-01-25 00:00:00'),
(10, '100000001', '120000000', '2.50', '60000', 1, '2018-01-25 00:00:00'),
(11, '120000001', '150000000', '2.50', '65000', 1, '2018-01-25 00:00:00'),
(12, '150000001', '200000000', '2.50', '70000', 1, '2018-01-25 00:00:00'),
(13, '200000001', '210000000', '2.50', '75000', 1, '2018-01-25 00:00:00'),
(14, '210000001', '220000000', '2.50', '85000', 1, '2018-01-25 00:00:00'),
(15, '220000001', '230000000', '2.50', '95000', 1, '2018-01-25 00:00:00'),
(16, '230000001', '240000000', '2.50', '105000', 1, '2018-01-25 00:00:00'),
(17, '240000001', '250000000', '2.50', '120000', 1, '2018-01-25 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `deducible_ingresos`
--
ALTER TABLE `deducible_ingresos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `deducible_ingresos`
--
ALTER TABLE `deducible_ingresos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
