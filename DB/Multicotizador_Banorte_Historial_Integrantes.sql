-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 27-05-2018 a las 04:58:27
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multicotizador_banorte_historial_integrantes`
--

DROP TABLE IF EXISTS `multicotizador_banorte_historial_integrantes`;
CREATE TABLE IF NOT EXISTS `multicotizador_banorte_historial_integrantes` (
  `id_integrantes` bigint(20) NOT NULL DEFAULT '0',
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `integrante` char(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nombre` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sexo` char(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `edad` int(2) NOT NULL DEFAULT '0',
  `prima_neta_anual` text COLLATE utf8_bin NOT NULL,
  `prima_neta_semestral` text COLLATE utf8_bin NOT NULL,
  `prima_neta_trimestral` int(11) NOT NULL,
  `prima_neta_mensual` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
