-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:28:29
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prima_minima`
--

CREATE TABLE `prima_minima` (
  `id` int(11) NOT NULL,
  `region` varchar(100) NOT NULL,
  `sum_incurred` varchar(300) NOT NULL,
  `sum_prima_minima` varchar(400) NOT NULL,
  `count_policy` varchar(100) NOT NULL,
  `comisiones` varchar(100) NOT NULL,
  `gastos_generales` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `prima_minima`
--

INSERT INTO `prima_minima` (`id`, `region`, `sum_incurred`, `sum_prima_minima`, `count_policy`, `comisiones`, `gastos_generales`, `status`, `creacion`) VALUES
(1, 'Chihuahua', '18240.77', '15546.59', '18.00', '15', 15, 1, '2018-01-27 00:00:00'),
(2, 'Ciudad Juarez', '-', '11758.75', '5.00', '15', 15, 1, '2018-01-27 00:00:00'),
(3, 'Guadalajara', '202455.78', '67944.86', '46.00', '15', 15, 1, '2018-01-27 00:00:00'),
(4, 'Hermosillo (10)', '-', '12436.05', '1.00', '15', 15, 1, '2018-01-27 00:00:00'),
(5, 'Hermosillo (Juarez)', '214989.83', '37678.15', '29.00', '15', 15, 1, '2018-01-27 00:00:00'),
(6, 'Leon', '-', '48958.63', '35.00', '15', 15, 1, '2018-01-27 00:00:00'),
(7, 'Mexico', '138469.70', '232497.07', '157.00', '15', 15, 1, '2018-01-27 00:00:00'),
(8, 'Monterrey', '43030.04', '86275.38', '59.00', '15', 15, 1, '2018-01-27 00:00:00'),
(9, 'Puebla', '283271.52', '246582.41', '165.00', '15', 15, 1, '2018-01-27 00:00:00'),
(10, 'Satelite', '19477.94', '4462.77', '3.00', '15', 15, 1, '2018-01-27 00:00:00'),
(11, 'Tijuana', '346.88', '29790.66', '35.00', '15', 15, 1, '2018-01-27 00:00:00'),
(12, 'Veracruz', '-', '8763.34', '6.0', '15', 15, 1, '2018-01-27 00:00:00'),
(13, 'Villahermosa', '3086.49', '6981.89', '5.00', '15', 15, 1, '2018-01-27 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `prima_minima`
--
ALTER TABLE `prima_minima`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `prima_minima`
--
ALTER TABLE `prima_minima`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
