-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 22, 2017 at 08:17 PM
-- Server version: 5.7.17
-- PHP Version: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `widgets`
--

-- --------------------------------------------------------

--
-- Table structure for table `mascotas_historial`
--

CREATE TABLE `mascotas_historial` (
  `id` int(11) NOT NULL,
  `nombre` varchar(400) COLLATE utf8_bin NOT NULL,
  `rfc` varchar(50) COLLATE utf8_bin NOT NULL,
  `direccion` varchar(500) COLLATE utf8_bin NOT NULL,
  `cp` varchar(10) COLLATE utf8_bin NOT NULL,
  `colonia` varchar(100) COLLATE utf8_bin NOT NULL,
  `municipio` varchar(100) COLLATE utf8_bin NOT NULL,
  `estado` varchar(100) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(100) COLLATE utf8_bin NOT NULL,
  `email` varchar(200) COLLATE utf8_bin NOT NULL,
  `nombre_mascota` varchar(200) COLLATE utf8_bin NOT NULL,
  `edad` varchar(50) COLLATE utf8_bin NOT NULL,
  `tipo` varchar(10) COLLATE utf8_bin NOT NULL,
  `sexo` varchar(10) COLLATE utf8_bin NOT NULL,
  `raza` varchar(150) COLLATE utf8_bin NOT NULL,
  `otra_raza` varchar(150) COLLATE utf8_bin DEFAULT NULL,
  `color` varchar(100) COLLATE utf8_bin NOT NULL,
  `esterilizado` varchar(10) COLLATE utf8_bin NOT NULL,
  `pedigree` varchar(10) COLLATE utf8_bin NOT NULL,
  `registro_option` varchar(10) COLLATE utf8_bin NOT NULL,
  `registro` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `no_registro` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `senas_particulares` text COLLATE utf8_bin NOT NULL,
  `microchip` varchar(100) COLLATE utf8_bin NOT NULL,
  `no_microchip` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `accidente` varchar(10) COLLATE utf8_bin NOT NULL,
  `criptorquidismo` varchar(10) COLLATE utf8_bin NOT NULL,
  `fractura` varchar(10) COLLATE utf8_bin NOT NULL,
  `hernias` varchar(10) COLLATE utf8_bin NOT NULL,
  `sida_felino` varchar(10) COLLATE utf8_bin NOT NULL,
  `luxacion` varchar(10) COLLATE utf8_bin NOT NULL,
  `displacia` varchar(10) COLLATE utf8_bin NOT NULL,
  `cardiopatias` varchar(10) COLLATE utf8_bin NOT NULL,
  `ehrlichia` varchar(10) COLLATE utf8_bin NOT NULL,
  `leucemia` varchar(10) COLLATE utf8_bin NOT NULL,
  `borrelia` varchar(10) COLLATE utf8_bin NOT NULL,
  `oculares` varchar(10) COLLATE utf8_bin NOT NULL,
  `gusano_c` varchar(10) COLLATE utf8_bin NOT NULL,
  `dermatologicas` varchar(10) COLLATE utf8_bin NOT NULL,
  `parasitarias` varchar(10) COLLATE utf8_bin NOT NULL,
  `convulsiones` varchar(10) COLLATE utf8_bin NOT NULL,
  `especificaciones` text COLLATE utf8_bin NOT NULL,
  `precio` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `paquete_id` int(11) DEFAULT NULL,
  `paquete_desc` varchar(350) COLLATE utf8_bin DEFAULT NULL,
  `fecha_creacion` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `mascotas_historial`
--

INSERT INTO `mascotas_historial` (`id`, `nombre`, `rfc`, `direccion`, `cp`, `colonia`, `municipio`, `estado`, `telefono`, `email`, `nombre_mascota`, `edad`, `tipo`, `sexo`, `raza`, `otra_raza`, `color`, `esterilizado`, `pedigree`, `registro_option`, `registro`, `no_registro`, `senas_particulares`, `microchip`, `no_microchip`, `accidente`, `criptorquidismo`, `fractura`, `hernias`, `sida_felino`, `luxacion`, `displacia`, `cardiopatias`, `ehrlichia`, `leucemia`, `borrelia`, `oculares`, `gusano_c`, `dermatologicas`, `parasitarias`, `convulsiones`, `especificaciones`, `precio`, `paquete_id`, `paquete_desc`, `fecha_creacion`) VALUES
(1, '123123123 123213 123123', '123123', '123123', '123123', '123123', '12312', '3123123', '123123', '123123@23123.cm', 'qweqwe', '12', 'Perro', 'Macho', '123123', '123132', '123123', 'No', 'No', 'No', '', '', '123123123', 'No', '', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', 'No', '12313213', NULL, NULL, NULL, '05-10-2017 22:00:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mascotas_historial`
--
ALTER TABLE `mascotas_historial`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mascotas_historial`
--
ALTER TABLE `mascotas_historial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
