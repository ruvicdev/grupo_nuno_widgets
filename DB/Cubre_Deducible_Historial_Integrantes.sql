-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 26-01-2018 a las 03:59:55
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cubre_deducible_historial_integrantes`
--

DROP TABLE IF EXISTS `cubre_deducible_historial_integrantes`;
CREATE TABLE IF NOT EXISTS `cubre_deducible_historial_integrantes` (
  `id_integrantes` bigint(20) NOT NULL DEFAULT '0',
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `integrante` char(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nombre` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sexo` char(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `edad` int(2) NOT NULL DEFAULT '0',
  `prima_neta_anual` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
