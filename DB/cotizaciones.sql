-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 23, 2017 at 07:33 PM
-- Server version: 5.7.17
-- PHP Version: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `widgets`
--

-- --------------------------------------------------------

--
-- Table structure for table `cotizaciones`
--

CREATE TABLE `cotizaciones` (
  `id` int(11) NOT NULL,
  `session` varchar(100) COLLATE utf8_bin NOT NULL,
  `user_email` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `user_telefono` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `user_rfc` varchar(70) COLLATE utf8_bin DEFAULT NULL,
  `user_tipo` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `tipo_giro` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_bin NOT NULL,
  `fecha_creacion` varchar(50) COLLATE utf8_bin NOT NULL,
  `fecha_actualizacion` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `tipo_giro_id` int(11) DEFAULT NULL,
  `vol_viajes` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `tipo_persona` int(11) DEFAULT NULL,
  `moneda` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `valor_merc` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `tipo_merc` int(11) DEFAULT NULL,
  `empaque` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `tipo_carga` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `cobertura` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `tipo_trans_id` int(11) DEFAULT NULL,
  `nombre_merc` varchar(500) COLLATE utf8_bin DEFAULT NULL,
  `medio_trans` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `prima_neta` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `total_pagar` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `continente_origen` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `pais_origen` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `estado_origen` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ciudad_origen` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `continente_destino` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `pais_destino` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `estado_destino` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `ciudad_destino` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `clase` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `valida_hasta` varchar(100) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cotizaciones`
--
ALTER TABLE `cotizaciones`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cotizaciones`
--
ALTER TABLE `cotizaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
