-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 27-05-2018 a las 04:58:13
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `multicotizador_banorte_historial`
--

DROP TABLE IF EXISTS `multicotizador_banorte_historial`;
CREATE TABLE IF NOT EXISTS `multicotizador_banorte_historial` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session` varchar(100) COLLATE utf8_bin NOT NULL,
  `vendedor` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `fullname` char(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `telefono` int(11) NOT NULL DEFAULT '0',
  `estado` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ciudad` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `id_integrantes` int(11) NOT NULL,
  `tipo_cotizacion` text COLLATE utf8_bin NOT NULL,
  `tabulador_honorarios_medicos` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `nivel_hospitalario` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `suma_asegurada` decimal(15,2) NOT NULL,
  `coaseguro` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `deducible` decimal(15,2) NOT NULL,
  `reduccion_deducible_accidente` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `incremento_parto_cesarea` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cesarea_monto` decimal(15,2) NOT NULL,
  `cobertura_vision_incremental` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cobertura_indemnizacion_enfermedad_grave` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `enfermedad_grave_monto` decimal(15,2) NOT NULL,
  `emergencia_extranjero` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `fecha_creacion` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
