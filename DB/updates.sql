ALTER TABLE `mascotas_historial` ADD `sida_felino` VARCHAR(10) NOT NULL AFTER `hernias`,
                                 ADD `luxacion` VARCHAR(10) NOT NULL AFTER `sida_felino`,
                                 ADD `displacia` VARCHAR(10) NOT NULL AFTER `luxacion`,
                                 ADD `cardiopatias` VARCHAR(10) NOT NULL AFTER `displacia`,
                                 ADD `ehrlichia` VARCHAR(10) NOT NULL AFTER `cardiopatias`,
                                 ADD `leucemia` VARCHAR(10) NOT NULL AFTER `ehrlichia`,
                                 ADD `borrelia` VARCHAR(10) NOT NULL AFTER `leucemia`,
                                 ADD `oculares` VARCHAR(10) NOT NULL AFTER `borrelia`,
                                 ADD `gusano_c` VARCHAR(10) NOT NULL AFTER `oculares`,
                                 ADD `dermatologicas` VARCHAR(10) NOT NULL AFTER `gusano_c`,
                                 ADD `parasitarias` VARCHAR(10) NOT NULL AFTER `dermatologicas`,
                                 ADD `convulsiones` VARCHAR(10) NOT NULL AFTER `parasitarias`;
ALTER TABLE `mascotas_historial` CHANGE `precio` `precio` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NULL,
                                 CHANGE `paquete_id` `paquete_id` INT(11) NULL,
                                 CHANGE `paquete_desc` `paquete_desc` VARCHAR(350) CHARACTER SET utf8 COLLATE utf8_bin NULL;

ALTER TABLE `mascotas_historial` CHANGE `registro` `registro` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_bin NULL,
                                 CHANGE `no_registro` `no_registro` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NULL,
                                 CHANGE `no_microchip` `no_microchip` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_bin NULL;
ALTER TABLE `mascotas_historial` ADD `registro_option` VARCHAR(10) NOT NULL AFTER `pedigree`;

/** COTIZACIONES UPDATES 23/10/2017 **/
ALTER TABLE `cotizaciones` ADD `vendedor` VARCHAR(10) NULL AFTER `valida_hasta`,
                           ADD `nombre_vendedor` VARCHAR(250) NULL AFTER `vendedor`;

/**Club_Salud_Historial 23/10/2017 **/
ALTER TABLE `Club_Salud_Historial` ADD `vendedor` VARCHAR(50);

/** MASCOTAS WIDGETS 24/10/2017 **/
ALTER TABLE `mascotas_historial` ADD `vendedor` VARCHAR(10) NULL AFTER `fecha_creacion`,
                                 ADD `nombre_vendedor` VARCHAR(250) NULL AFTER `vendedor`;

/** Proteccion_Datos UPDATES 27/12/2017 **/
ALTER TABLE `proteccion_datos` ADD `es_vendedor` INT NOT NULL AFTER `tbd`,
                               ADD `nombre_vendedor` VARCHAR(100) NOT NULL AFTER `es_vendedor`;

/** Proteccion_Datos UPDATES 03/01/2018 **/
ALTER TABLE `proteccion_datos` ADD `limite_id` INT NOT NULL AFTER `actualizacion`,
                               ADD `factu_id` INT NOT NULL AFTER `limite_id`;
ALTER TABLE `proteccion_datos` CHANGE `es_vendedor` `es_vendedor` VARCHAR(5) NOT NULL;

/** Proteccion_Datos UPDATES 06/01/2018 **/
ALTER TABLE `proteccion_datos` ADD `tipo_persona` INT NOT NULL COMMENT '0-Persona Fisica 1-Persona Moral' AFTER `factu_id`,
                               ADD `nombre_empresa` VARCHAR(250) NULL AFTER `tipo_persona`,
                               ADD `rfc` VARCHAR(50) NULL AFTER `nombre_empresa`,
                               ADD `domicilio` VARCHAR(200) NOT NULL AFTER `rfc`,
                               ADD `cp` VARCHAR(10) NOT NULL AFTER `domicilio`,
                               ADD `colonia` VARCHAR(300) NOT NULL AFTER `cp`,
                               ADD `estado` VARCHAR(100) NOT NULL AFTER `colonia`;
ALTER TABLE `proteccion_datos` CHANGE `nombre` `nombre` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
                               CHANGE `paterno` `paterno` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
                               CHANGE `materno` `materno` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

/** Actos_Fraudulentos 31/01/2018 **/
ALTER TABLE `actos_fraudulentos` ADD `tipo_persona` VARCHAR(20) NOT NULL AFTER `id`;
