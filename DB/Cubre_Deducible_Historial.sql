-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 04-02-2018 a las 05:34:04
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cubre_deducible_historial`
--

DROP TABLE IF EXISTS `cubre_deducible_historial`;
CREATE TABLE IF NOT EXISTS `cubre_deducible_historial` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session` varchar(100) COLLATE utf8_bin NOT NULL,
  `fullname` char(50) COLLATE utf8_bin NOT NULL DEFAULT '',
  `email` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `telefono` int(11) NOT NULL DEFAULT '0',
  `estado` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `ciudad` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `id_integrantes` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `vendedor` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `cobertura_extranjero` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `cobertura_deducible` decimal(15,2) NOT NULL,
  `emergencia_extranjero` char(2) COLLATE utf8_bin NOT NULL DEFAULT '',
  `emergencia_deducible` decimal(15,2) NOT NULL,
  `fin_vigencia` datetime NOT NULL,
  `deducible_poliza` decimal(15,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
