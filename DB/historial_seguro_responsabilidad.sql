-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 14, 2017 at 08:06 PM
-- Server version: 5.7.17
-- PHP Version: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `widgets`
--

-- --------------------------------------------------------

--
-- Table structure for table `historial_seguro_responsabilidad`
--

CREATE TABLE `historial_seguro_responsabilidad` (
  `id` int(11) NOT NULL,
  `empresa` varchar(400) COLLATE utf8_bin NOT NULL,
  `direccion` varchar(400) COLLATE utf8_bin NOT NULL,
  `email` varchar(200) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(30) COLLATE utf8_bin NOT NULL,
  `fecha_cotizacion` varchar(20) COLLATE utf8_bin NOT NULL,
  `numero_empleados` varchar(50) COLLATE utf8_bin NOT NULL,
  `facturacion` varchar(250) COLLATE utf8_bin NOT NULL,
  `indemnizacion` varchar(250) COLLATE utf8_bin NOT NULL,
  `sublimite_responsabilidad` varchar(250) COLLATE utf8_bin NOT NULL,
  `deducible` varchar(250) COLLATE utf8_bin NOT NULL,
  `deducible_responsabilidad` varchar(250) COLLATE utf8_bin NOT NULL,
  `prima_neta_anual` varchar(250) COLLATE utf8_bin NOT NULL,
  `es_vendedor` varchar(5) COLLATE utf8_bin NOT NULL,
  `nombre_vendedor` varchar(200) COLLATE utf8_bin NOT NULL,
  `id_facturacion` int(11) NOT NULL,
  `id_seccion_facturacion` int(11) NOT NULL,
  `fecha_creacion` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `historial_seguro_responsabilidad`
--

INSERT INTO `historial_seguro_responsabilidad` (`id`, `empresa`, `direccion`, `email`, `telefono`, `fecha_cotizacion`, `numero_empleados`, `facturacion`, `indemnizacion`, `sublimite_responsabilidad`, `deducible`, `deducible_responsabilidad`, `prima_neta_anual`, `es_vendedor`, `nombre_vendedor`, `id_facturacion`, `id_seccion_facturacion`, `fecha_creacion`) VALUES
(1, 'qweqwewqeqw qweqweqweqw qweqweqweqw', 'qweqweqweqw wqeqwewqewqe qeqweqeqewq qwwewqeqweqw', 'qwewqe@asdas.com', 'qweqqweqweqwe', '07-11-2017 20:23:57', '123', 'Desde $5M USD hasta $15M USD', '$ 1,000,000', '$ 500,000 USD', '$ 5,000 USD', '$ 10,000 USD', '$ 1,875 USD', 'Si', 'qwewqe', 2, 3, '07-11-2017 20:23:57'),
(2, 'werwer', 'werwerwer', 'qweqw@qwe.com', 'wwer123123', '07-11-2017 20:27:05', 'wer23', 'Desde $5M USD hasta $15M USD', '$ 500,000', '$ 500,000 USD', '$ 5,000 USD', '$ 10,000 USD', '$ 1,400 USD', 'Si', 'werwer', 2, 2, '07-11-2017 20:27:05'),
(3, 'asdasd', 'asdasd', '123@123.cpom', '123123', '09-11-2017 22:59:11', '123', 'Desde $15M USD hasta $25M USD', '$ 2,000,000', '$ 500,000 USD', '$ 10,000 USD', '$ 15,000 USD', '$ 2,925 USD', 'Si', 'asdsad', 3, 4, '09-11-2017 22:59:11'),
(4, 'wqeqwe', 'qweqwe', '123123@12323.com', '123213', '09-11-2017 23:09:35', '123123', 'Desde $15M USD hasta $25M USD', '$ 2,000,000', '$ 500,000 USD', '$ 10,000 USD', '$ 15,000 USD', '$ 2,925 USD', 'Si', 'qweqwe', 3, 4, '09-11-2017 23:09:35'),
(5, 'asdasd', 'asdasd', '12321@213213.com', '123123', '09-11-2017 23:16:17', '123123', 'Desde $15M USD hasta $25M USD', '$ 2,000,000', '$ 500,000 USD', '$ 10,000 USD', '$ 15,000 USD', '$ 2,925 USD', 'Si', 'asdasd', 3, 4, '09-11-2017 23:16:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `historial_seguro_responsabilidad`
--
ALTER TABLE `historial_seguro_responsabilidad`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `historial_seguro_responsabilidad`
--
ALTER TABLE `historial_seguro_responsabilidad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
