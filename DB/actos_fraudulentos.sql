-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:26:25
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actos_fraudulentos`
--

CREATE TABLE `actos_fraudulentos` (
  `id` int(11) NOT NULL,
  `nombre_asegurado` varchar(170) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(50) NOT NULL,
  `direccion` varchar(200) NOT NULL,
  `rfc` varchar(50) NOT NULL,
  `colonia` varchar(200) NOT NULL,
  `municipio` varchar(200) NOT NULL,
  `codigo_postal` varchar(20) NOT NULL,
  `vigencia_desde` varchar(100) NOT NULL,
  `vigencia_hasta` varchar(100) NOT NULL,
  `renovacion` varchar(10) NOT NULL,
  `forma_pago` varchar(50) NOT NULL,
  `ingresos_anuales` varchar(100) NOT NULL,
  `maximo_dinero` varchar(100) NOT NULL,
  `no_empleados` varchar(50) NOT NULL,
  `maximo_empleados` varchar(100) NOT NULL,
  `limite_requerido` varchar(200) NOT NULL,
  `giro_empresa` varchar(200) NOT NULL,
  `no_sucursales` varchar(100) NOT NULL,
  `auditoria` varchar(10) NOT NULL,
  `transaccion` varchar(10) NOT NULL,
  `pagos` varchar(10) NOT NULL,
  `conteo` varchar(10) NOT NULL,
  `conciliaciones` varchar(10) NOT NULL,
  `segregadas` varchar(10) NOT NULL,
  `intervencion` varchar(10) NOT NULL,
  `siniestro` varchar(10) NOT NULL,
  `controles` varchar(10) NOT NULL,
  `ultimo_ano` varchar(50) NOT NULL,
  `penultimo_ano` varchar(50) NOT NULL,
  `antepenultimo_ano` varchar(50) NOT NULL,
  `apreciacion_riesgo` varchar(100) NOT NULL,
  `porcentaje_comision` varchar(50) NOT NULL,
  `tasa_adicional` varchar(50) NOT NULL,
  `tienda_abarrotes` varchar(10) NOT NULL,
  `transportadores` varchar(10) NOT NULL,
  `restaurantes` varchar(10) NOT NULL,
  `gasolineras` varchar(10) NOT NULL,
  `detalle_siniestro` text DEFAULT NULL,
  `prima_neta` varchar(200) NOT NULL,
  `deducible` varchar(200) NOT NULL,
  `total` varchar(200) NOT NULL,
  `desde` varchar(200) NOT NULL,
  `hasta` varchar(200) NOT NULL,
  `vendedor` varchar(10) NOT NULL,
  `nombre_vendedor` varchar(200) NOT NULL,
  `fecha_creacion` varchar(100) NOT NULL,
  `fecha_actualizacion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `actos_fraudulentos`
--

INSERT INTO `actos_fraudulentos` (`id`, `nombre_asegurado`, `email`, `telefono`, `direccion`, `rfc`, `colonia`, `municipio`, `codigo_postal`, `vigencia_desde`, `vigencia_hasta`, `renovacion`, `forma_pago`, `ingresos_anuales`, `maximo_dinero`, `no_empleados`, `maximo_empleados`, `limite_requerido`, `giro_empresa`, `no_sucursales`, `auditoria`, `transaccion`, `pagos`, `conteo`, `conciliaciones`, `segregadas`, `intervencion`, `siniestro`, `controles`, `ultimo_ano`, `penultimo_ano`, `antepenultimo_ano`, `apreciacion_riesgo`, `porcentaje_comision`, `tasa_adicional`, `tienda_abarrotes`, `transportadores`, `restaurantes`, `gasolineras`, `detalle_siniestro`, `prima_neta`, `deducible`, `total`, `desde`, `hasta`, `vendedor`, `nombre_vendedor`, `fecha_creacion`, `fecha_actualizacion`) VALUES
(1, 'asdasd', 'asdasd@qwe.cn', '12313', '12313', '123123', '123123', '123213', '123123', '28/01/2018', '28/01/2019', 'Si', 'Contado', '250000000', '1', '76', '300', '$ 2,500,000.00', '2', '4', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', 'Si', '0', '0', '0', '0', '1', '15', '25%', 'N/A', 'N/A', 'N/A', 'N/A', '', '38,812.50', '120,000.00', '83,422.33', '250000.00', '5000000.00', 'No', '', '29-01-2018 00:15:10 AM', '29-01-2018 00:19:58');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actos_fraudulentos`
--
ALTER TABLE `actos_fraudulentos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actos_fraudulentos`
--
ALTER TABLE `actos_fraudulentos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
