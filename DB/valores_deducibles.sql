-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:28:39
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valores_deducibles`
--

CREATE TABLE `valores_deducibles` (
  `id` int(11) NOT NULL,
  `rango_uno` varchar(100) NOT NULL,
  `rango_dos` varchar(100) NOT NULL,
  `final_price` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `tipo_moneda` tinyint(1) NOT NULL COMMENT '1- Pesos, 0- Dolares',
  `creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `valores_deducibles`
--

INSERT INTO `valores_deducibles` (`id`, `rango_uno`, `rango_dos`, `final_price`, `status`, `tipo_moneda`, `creacion`) VALUES
(1, '0', '0', '0', 1, 1, '2018-01-24 00:00:00'),
(2, '1', '50000', '5000', 1, 1, '2018-01-24 00:00:00'),
(3, '50001', '100000', '10000', 1, 1, '2018-01-24 00:00:00'),
(4, '100001', '150000', '20000', 1, 1, '2018-01-24 00:00:00'),
(5, '150001', '200000', '30000', 1, 1, '2018-01-24 00:00:00'),
(6, '200001', '250000', '40000', 1, 1, '2018-01-24 00:00:00'),
(7, '0', '0', '0', 1, 0, '2018-01-25 00:00:00'),
(8, '1', '5000', '500', 1, 0, '2018-01-25 00:00:00'),
(9, '5001', '10000', '1000', 1, 0, '2018-01-25 00:00:00'),
(10, '10001', '15000', '2000', 1, 0, '2018-01-25 00:00:00'),
(11, '15001', '20000', '3000', 1, 0, '2018-01-25 00:00:00'),
(12, '20001', '25000', '4000', 1, 0, '2018-01-25 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `valores_deducibles`
--
ALTER TABLE `valores_deducibles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `valores_deducibles`
--
ALTER TABLE `valores_deducibles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
