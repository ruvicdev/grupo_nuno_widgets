-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-07-2017 a las 18:53:28
-- Versión del servidor: 10.0.30-MariaDB
-- Versión de PHP: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Club_Salud_Historial_Integrantes`
--

CREATE TABLE IF NOT EXISTS `Club_Salud_Historial_Integrantes` (
  `id_integrantes` bigint(20) NOT NULL DEFAULT '0',
  `id` bigint(20) unsigned NOT NULL,
  `integrante` char(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nombre` char(30) COLLATE utf8_bin NOT NULL DEFAULT '',
  `sexo` char(10) COLLATE utf8_bin NOT NULL DEFAULT '',
  `edad` int(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Club_Salud_Historial_Integrantes`
--
ALTER TABLE `Club_Salud_Historial_Integrantes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Club_Salud_Historial_Integrantes`
--
ALTER TABLE `Club_Salud_Historial_Integrantes`
  MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
