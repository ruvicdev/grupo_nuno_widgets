-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 05-01-2018 a las 03:46:06
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proteccion_datos`
--

CREATE TABLE `proteccion_datos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `paterno` varchar(200) NOT NULL,
  `materno` varchar(200) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `limite_resp` varchar(50) NOT NULL,
  `factu_total` varchar(100) NOT NULL,
  `deducible` varchar(30) NOT NULL,
  `tbd` varchar(30) NOT NULL,
  `es_vendedor` varchar(5) NOT NULL,
  `nombre_vendedor` varchar(100) NOT NULL,
  `creacion` varchar(30) NOT NULL,
  `actualizacion` varchar(30) NOT NULL,
  `limite_id` int(11) NOT NULL,
  `factu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `proteccion_datos`
--

INSERT INTO `proteccion_datos` (`id`, `nombre`, `paterno`, `materno`, `telefono`, `email`, `limite_resp`, `factu_total`, `deducible`, `tbd`, `es_vendedor`, `nombre_vendedor`, `creacion`, `actualizacion`, `limite_id`, `factu_id`) VALUES
(1, 'rrrr', 'wwww', 'cccc', '123123123', '123123@123123.com', '$ 260, 000', 'Entre $ 0 MXN a $ 1,300,000 MXN', '$ 13,000 MXN', '$ 3,120 MXN', 'No', '', '04-01-2018 04:18:26', '04-01-2018 04:18:26', 2, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `proteccion_datos`
--
ALTER TABLE `proteccion_datos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `proteccion_datos`
--
ALTER TABLE `proteccion_datos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
