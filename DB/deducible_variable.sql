-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:28:18
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducible_variable`
--

CREATE TABLE `deducible_variable` (
  `id` int(11) NOT NULL,
  `region` varchar(100) NOT NULL,
  `sum_incurred` varchar(300) NOT NULL,
  `sum_premium` varchar(400) NOT NULL,
  `comisiones` varchar(100) NOT NULL,
  `gastos_generales` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `deducible_variable`
--

INSERT INTO `deducible_variable` (`id`, `region`, `sum_incurred`, `sum_premium`, `comisiones`, `gastos_generales`, `status`, `creacion`) VALUES
(1, 'Chihuahua', '17115.10', '8379.72', '15', '15', 1, '2018-01-27 00:00:00'),
(2, 'Ciudad Juarez', '-', '11675.41', '15', '15', 1, '2018-01-27 00:00:00'),
(3, 'Guadalajara', '188414.52', '52821.17', '15', '15', 1, '2018-01-27 00:00:00'),
(4, 'Hermosillo (10)', '-', '12436.05', '15', '15', 1, '2018-01-27 00:00:00'),
(5, 'Hermosillo (Juarez)', '199543.27', '30511.56', '15', '15', 1, '2018-01-27 00:00:00'),
(6, 'Leon', '-', '44042.52', '15', '15', 1, '2018-01-27 00:00:00'),
(7, 'Mexico', '149848.13', '199421.34', '15', '15', 1, '2018-01-27 00:00:00'),
(8, 'Monterrey', '42785.59', '75538.93', '15', '15', 1, '2018-01-27 00:00:00'),
(9, 'Puebla', '279882.28', '212017.32', '15', '15', 1, '2018-01-27 00:00:00'),
(10, 'Satelite', '18234.84', '4032.81', '15', '15', 1, '2018-01-27 00:00:00'),
(11, 'Tijuana', '1010.60', '15729.49', '15', '15', 1, '2018-01-27 00:00:00'),
(12, 'Veracruz', '-', '7436.60', '15', '15', 1, '2018-01-27 00:00:00'),
(13, 'Villahermosa', '3476.25', '6385.09', '15', '15', 1, '2018-01-27 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `deducible_variable`
--
ALTER TABLE `deducible_variable`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `deducible_variable`
--
ALTER TABLE `deducible_variable`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
