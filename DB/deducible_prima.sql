-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 29-01-2018 a las 02:28:02
-- Versión del servidor: 10.2.11-MariaDB
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `deducible_prima`
--

CREATE TABLE `deducible_prima` (
  `id` int(11) NOT NULL,
  `region` varchar(100) NOT NULL,
  `comisiones` varchar(100) NOT NULL,
  `gastos_generales` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `creacion` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `deducible_prima`
--

INSERT INTO `deducible_prima` (`id`, `region`, `comisiones`, `gastos_generales`, `status`, `creacion`) VALUES
(1, 'Chihuahua', '15', '15', 1, '2018-01-27 00:00:00'),
(2, 'Ciudad Juarez', '15', '15', 1, '2018-01-27 00:00:00'),
(3, 'Guadalajara', '15', '15', 1, '2018-01-27 00:00:00'),
(4, 'Hermosillo', '15', '15', 1, '2018-01-27 00:00:00'),
(5, 'Leon', '15', '15', 1, '2018-01-27 00:00:00'),
(6, 'Mexico', '15', '15', 1, '2018-01-27 00:00:00'),
(7, 'Monterrey', '15', '15', 1, '2018-01-27 00:00:00'),
(8, 'Puebla', '15', '15', 1, '2018-01-27 00:00:00'),
(9, 'Satelite', '15', '15', 1, '2018-01-27 00:00:00'),
(10, 'Tijuana', '15', '15', 1, '2018-01-27 00:00:00'),
(11, 'Veracruz', '15', '15', 1, '2018-01-27 00:00:00'),
(12, 'Villahermosa', '15', '15', 1, '2018-01-27 00:00:00');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `deducible_prima`
--
ALTER TABLE `deducible_prima`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `deducible_prima`
--
ALTER TABLE `deducible_prima`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
