-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 04-02-2018 a las 05:38:46
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `widgets`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cubre_deducible_variables`
--

DROP TABLE IF EXISTS `cubre_deducible_variables`;
CREATE TABLE IF NOT EXISTS `cubre_deducible_variables` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `external_files_cotizaciones_id` bigint(20) NOT NULL,
  `cotizador_basica_no_reclamos_promedio_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_asegurados_promedio_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_ex_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_coaseguro_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_reclamos_promedio_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_asegurados_promedio_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_ex_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_coaseguro_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_reclamos_promedio_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_asegurados_promedio_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_ex_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_coaseguro_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_reclamos_promedio_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_no_asegurados_promedio_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_ex_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_coaseguro_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_x_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_x_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_x_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_x_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_basica_n` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_reclamos_promedio_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_asegurados_promedio_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_ex_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_coaseguro_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_reclamos_promedio_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_asegurados_promedio_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_ex_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_coaseguro_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_reclamos_promedio_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_asegurados_promedio_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_ex_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_coaseguro_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_reclamos_promedio_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_no_asegurados_promedio_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_ex_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_coaseguro_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_x_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_x_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_x_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_x_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_cobertura_ext_n` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_reclamos_promedio_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_asegurados_promedio_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_ex_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_coaseguro_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_reclamos_promedio_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_asegurados_promedio_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_ex_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_coaseguro_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_reclamos_promedio_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_asegurados_promedio_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_ex_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_coaseguro_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_reclamos_promedio_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_no_asegurados_promedio_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_ex_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_coaseguro_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_x_0_25` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_x_26_50` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_x_51_75` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_x_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizador_emergencia_ext_n` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_total_estadistica_promedio_grupo_0_25` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_unicamente_extranjero_promedio_grupo_0_25` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_total_estadistica_promedio_grupo_26_50` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_unicamente_extranjero_promedio_grupo_26_50` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_total_estadistica_promedio_grupo_51_75` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_unicamente_extranjero_promedio_grupo_51_75` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_total_estadistica_promedio_grupo_76_MAS` text COLLATE utf8_bin NOT NULL,
  `factor_extranjero_unicamente_extranjero_promedio_grupo_76_MAS` text COLLATE utf8_bin NOT NULL,
  `cotizacion_gasto_administracion` text COLLATE utf8_bin NOT NULL,
  `cotizacion_gasto_adquisicion` text COLLATE utf8_bin NOT NULL,
  `cotizacion_utilidad` text COLLATE utf8_bin NOT NULL,
  `date_created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
